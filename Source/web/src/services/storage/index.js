import axios from 'axios';

import config from '../../config';

const getFile = fileId => {
  return axios.get(`${config.base_api}/storage/file/${fileId}`);
}

const uploadFile = file => {
  return axios.post(`${config.base_api}/storage/upload`, file);
}

const removeFile = id => {
  return axios.delete(`${config.base_api}/storage/delete/${id}`);
}

export default {
  getFile,
  uploadFile,
  removeFile
}
