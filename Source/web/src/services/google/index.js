// import ApiCalendar from 'react-google-calendar-api';
import axios from 'axios';

import config from '../../config';

const connect = () => {
}

const uploadDrive = file => {
  return axios.post(`${config.base_api}/google/upload/drive`, file);
}

export default {
  connect,
  uploadDrive
}
