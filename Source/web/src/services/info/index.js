import axios from 'axios';

import config from '../../config';

const getProvince = () => {
  return axios.get(`${config.base_api}/info/provinces`);
}

const setDistrict = (province) => {
  return axios.post(`${config.base_api}/info/districts`, {province});
}

const setSchool = (district, province) => {
  return axios.post(`${config.base_api}/info/schools`, {district, province});
}

const addProfile = (profileData) => {
  const body = {
    data: profileData
  }
  return axios.post(`${config.base_api}/info/addProfile`, body);
}

const addEducation = (educationData) => {
  const body = {
    data: educationData
  }
  return axios.post(`${config.base_api}/info/addEducation`, body);
}

const addActivities = (activitiesData) => {
  const body = {
    data: activitiesData
  }
  return axios.post(`${config.base_api}/info/addActivities`, body);
}

const addCertificate = (certificateData) => {
  const body = {
    data: certificateData
  }
  return axios.post(`${config.base_api}/info/addCertificate`, body);
}

const getProfile = (userId) => {
  const configReq = {
    headers: { userid: userId }
  }
  return axios.get(`${config.base_api}/info/getProfile`, configReq);
}

const getEducation = (userId) => {
  return axios.get(`${config.base_api}/info/getEducation/${userId}`);
}

const getActivities = (userId) => {
  return axios.get(`${config.base_api}/info/getActivities/${userId}`);
}

const getCertificate = (userId) => {
  return axios.get(`${config.base_api}/info/getCertificate/${userId}`);
}

const getActivitiesById = (id) => {
  return axios.get(`${config.base_api}/info/getActivitiesById/${id}`);
}

const getCertificateById = (id) => {
  return axios.get(`${config.base_api}/info/getCertificateById/${id}`);
}

const updateProfie = (profileData) => {
  const body = {
    data: profileData
  }
  return axios.put(`${config.base_api}/info/updateProfile`, body);
}

const updateEducation = (educationData) => {
  const body = {
    data: educationData
  }
  return axios.put(`${config.base_api}/info/updateEducation`, body);
}

const updateActivities = (activitiesData) => {
  const body = {
    data: activitiesData
  }
  return axios.put(`${config.base_api}/info/updateActivities`, body);
}

const updateCertificate = (certificateData) => {
  const body = {
    data: certificateData
  }
  return axios.put(`${config.base_api}/info/updateCertificate`, body);
}

const changeStatusActivities = (id, status) => {
  const body = {
    status: status
  }
  return axios.put(`${config.base_api}/info/changeStatusActivities/${id}`, body);
}

const changeStatusCertificate = (id, status) => {
  const body = {
    status: status
  }
  return axios.put(`${config.base_api}/info/changeStatusCertificate/${id}`, body);
}

const removeEducation = (id) => {
  return axios.delete(`${config.base_api}/info/removeEducation/${id}`);
}

const removeActivities = (id) => {
  return axios.delete(`${config.base_api}/info/removeActivities/${id}`);
}

const removeCertificate = (id) => {
  return axios.delete(`${config.base_api}/info/removeCertificate/${id}`);
}

const autoCompleteSchool = (value) => {
  const body = {
    key: value
  }
  return axios.post(`${config.base_api}/info/autoCompleteSchool`, body)
}

export default {
  getProvince,
  setDistrict,
  setSchool,
  addProfile,
  addEducation,
  addActivities,
  addCertificate,
  getProfile,
  getEducation,
  getActivities,
  getCertificate,
  getActivitiesById,
  getCertificateById,
  updateProfie,
  updateEducation,
  updateActivities,
  updateCertificate,
  removeEducation,
  removeActivities,
  removeCertificate,
  autoCompleteSchool,
  changeStatusActivities,
  changeStatusCertificate
}
