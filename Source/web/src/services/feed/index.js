// import ApiCalendar from 'react-google-calendar-api';
import axios from 'axios';

import config from '../../config';

const getUniversity = () => {
  return axios.get(`${config.base_api}/feed/university`);
}

const getNotification = (id) => {
  return axios.get(`${config.base_api}/feed/noti/${id}`);
}

const getEnttrong = () => {
  return axios.get(`${config.base_api}/feed/enttrong`);
}

const getSangfans = () => {
  return axios.get(`${config.base_api}/feed/sangfans`);
}

const getFeedById = (id) => {
  const configReq = {
    headers: { id: id }
  }
  return axios.get(`${config.base_api}/feed/getId`, configReq);
}

const getSangfansById = (id) => {
  const configReq = {
    headers: { id: id }
  }
  return axios.get(`${config.base_api}/feed/sangfansGetId`, configReq);
}

const fetchUrl = (url) => {
  const configReq = {
    headers: { url: url }
  }
  return axios.get(`${config.base_api}/feed/fetch`, configReq );
}

const updateReadBy = (id, userId) => {
  const data = {
    id: id,
    userId: userId
  }
  return axios.put(`${config.base_api}/feed/readBy`, data );
}

const settingNotification = (id, data) => {
  return axios.put(`${config.base_api}/feed/setting/${id}`, data );
}

const getSetting = (id) => {
  return axios.get(`${config.base_api}/feed/setting/${id}`);
}

export default {
  getUniversity,
  getEnttrong,
  getSangfans,
  getNotification,
  getFeedById,
  getSangfansById,
  fetchUrl,
  updateReadBy,
  settingNotification,
  getSetting
}
