import axios from 'axios';

import config from '../../config'

const createPortfolio = data => {
  const body = {
    data: data
  }
  return axios.post(`${config.base_api}/port/createPort`, body);
}

const createTemplate = data => {
  const body = {
    data: data
  }
  return axios.post(`${config.base_api}/port/createTemplate`, body);
}

const updateTemplate = data => {
  const body = {
    data: data
  }
  return axios.put(`${config.base_api}/port/update`, body);
}

const updatePortfolio = data => {
  const body = {
    data: data
  }
  return axios.put(`${config.base_api}/port/updatePort`, body);
}

const findTemplateById = id => {
  return axios.get(`${config.base_api}/port/findOne/${id}`);
}

const deletePortfolio = id => {
  return axios.delete(`${config.base_api}/port/delete/${id}`);
}

const deleteTemplate = id => {
  return axios.delete(`${config.base_api}/port/deleteTemplate/${id}`);
}

const setNamePortfolio = (id, name) => {
  return axios.post(`${config.base_api}/port/setName/${id}`, {name: name});
}

const findPortfolioById = id => {
  return axios.get(`${config.base_api}/port/findOnePort/${id}`);
}

const findPortfolioByUserId = id => {
  return axios.get(`${config.base_api}/port/findPortByUserId/${id}`);
}

const findAllTemplates = () => {
  return axios.get(`${config.base_api}/port/findAll`);
}

const getAllTheme = () => {
  return axios.get(`${config.base_api}/port/theme`)
}

export default {
  createTemplate,
  createPortfolio,
  deletePortfolio,
  deleteTemplate,
  updateTemplate,
  updatePortfolio,
  findTemplateById,
  findPortfolioById,
  findPortfolioByUserId,
  findAllTemplates,
  setNamePortfolio,
  getAllTheme
}
