import axios from 'axios';

import config from '../../config'

const signUp = signUpData => {
  const body = {
    data: signUpData
  }
  return axios.post(`${config.base_api}/auth/signup`, body);
}

const signIn = signInData => {
  const body = {
    data: signInData
  }
  return axios.post(`${config.base_api}/auth/signin`, body);
}

const facebookAuth = token => {
  const body = {
    access_token: token
  }
  return axios.post(`${config.base_api}/auth/facebook/token`, body);
}

const googleAuth = token => {
  const body = {
    access_token: token
  }
  return axios.post(`${config.base_api}/auth/google/token`, body);
}

const googleDrive = token => {
  const body = {
    access_token: token
  }
  return axios.post(`${config.base_api}/auth/google/drive`, body);
}

const sendEmailResetPassword = email => {
  return axios.post(`${config.base_api}/auth/resetPassword`, {email})
} 

const getUserForResetPassword = token => {
  const params = {
    token: token
  }
  return axios.get(`${config.base_api}/auth/reset`, {params})
}

const updatePasswordForReset = (email, password) => {
  const data = {
    email: email,
    password: password
  }
  return axios.put(`${config.base_api}/auth/updatePasswordForReset`, {data})
} 

const updatePasswordForChange = (email, current_password ,password) => {
  const data = {
    email: email,
    current_password: current_password,
    password: password
  }
  return axios.put(`${config.base_api}/auth/updatePasswordForChange`, {data})
} 

const readNewFeed = (id) => {
  return axios.put(`${config.base_api}/auth/readNewFeed/${id}`)
} 

const getCurrentUser = token => {
  const configReq = {
    headers: { Authorization: `Bearer ${token}` }
  }
  return axios.get(`${config.base_api}/auth/currentUser`, configReq);
}

const userDriveGoogle = token => {
  const configReq = {
    headers: { Authorization: `Bearer ${token}` }
  }
  return axios.get(`${config.base_api}/auth/userDriveGoogle`, configReq);
}

const getChkTokenExpired = token => {
  const configReq = {
    headers: { Authorization: `Bearer ${token}` }
  }
  return axios.get(`${config.base_api}/auth/token`, configReq)
}

export default {
  signIn,
  signUp,
  facebookAuth,
  googleAuth,
  googleDrive,
  sendEmailResetPassword,
  getUserForResetPassword,
  updatePasswordForReset,
  updatePasswordForChange,
  readNewFeed,
  getCurrentUser,
  userDriveGoogle,
  getChkTokenExpired,
}
