import firebase, { Fprovider, Gprovider } from '../../firebase';
import { REGISTER_SUCCESS, REGISTER_ERROR, LOGIN_SUCCESS, LOGIN_ERROR, CURRENT_USER, LOGOUT_SUCCESS } from '../types/auth';
import { LOADING_START, LOADING_END } from '../types/all';
import i18next from 'i18next';
import { NotificationManager } from 'react-notifications';
import history from '../../history';
import authService from '../../services/auth';
import storageService from '../../services/storage';


export const signup = (signupData) => async dispatch => {
    dispatch({ type: LOADING_START, payload: i18next.t("lbl_registering") })
    try {
        const response = await authService.signUp(signupData)
        const token = response.data.token;
        localStorage.setItem('TOKEN', token);
        const userResponse = await authService.getCurrentUser(token);
        dispatch({ type: REGISTER_SUCCESS, payload: userResponse.data.user })
        window.OneSignal.sendTag('userId', userResponse.data.user._id)
        history.go(-(history.length));
        history.replace('/')
        NotificationManager.success(i18next.t('lbl_register_success'),null,3000);
    } catch(err){
        dispatch({ type: REGISTER_ERROR })
        NotificationManager.error(i18next.t(err.response.data.code), null ,3000);
    } finally {
        dispatch({ type: LOADING_END })
    }
}

export const signin = (signinData) => async dispatch => {
    dispatch({ type: LOADING_START, payload: i18next.t("lbl_logging_in") })
    try {
        const response = await authService.signIn(signinData)
        const token = response.data.token;
        localStorage.setItem('TOKEN', token);
        const userResponse = await authService.getCurrentUser(token);
        const user = userResponse.data.user;
        if (user.photoId) {
            const fileResponse = await storageService.getFile(user.photoId);
            user.photoUrl = `data:${fileResponse.data.file.contentType};base64,${fileResponse.data.file.url}`
        }
        dispatch({ type: LOGIN_SUCCESS, payload: userResponse.data.user })
        window.OneSignal.sendTag('userId', userResponse.data.user._id)
        history.go(-(history.length));
        history.replace('/')
        NotificationManager.success(i18next.t('lbl_login_success'),null,3000);
    } catch(err){
        dispatch({ type: LOGIN_ERROR })
        NotificationManager.error(i18next.t(err.response.data.code), null ,3000);
    } finally {
        dispatch({ type: LOADING_END })
    }
}

export const logOut = () => dispatch => {
    dispatch({ type: LOADING_START, payload: i18next.t("lbl_logging_out") })
    dispatch({type: LOGOUT_SUCCESS, payload: null})
    localStorage.removeItem('TOKEN');
    window.OneSignal.deleteTag('userId')
    history.go(-(history.length));
    history.replace('/signin')
    dispatch({ type: LOADING_END })
}

export const signinFacebook = (accessToken) => async dispatch => {
    dispatch({ type: LOADING_START, payload: i18next.t("lbl_logging_in") })
    try {
        const response = await authService.facebookAuth(accessToken);
        const token = response.data.token;
        localStorage.setItem('TOKEN', token);
        const userResponse = await authService.getCurrentUser(token);
        const user = userResponse.data.user;
        if (user.photoId) {
            const fileResponse = await storageService.getFile(user.photoId);
            user.photoUrl = `data:${fileResponse.data.file.contentType};base64,${fileResponse.data.file.url}`
        }
        dispatch({ type: LOGIN_SUCCESS, payload: userResponse.data.user })
        window.OneSignal.sendTag('userId', userResponse.data.user._id)
        history.go(-(history.length));
        history.replace('/')
        NotificationManager.success(i18next.t('lbl_login_success'),null,3000);
    } catch(err) {
        dispatch({ type: LOGIN_ERROR })
    } finally {
        dispatch({ type: LOADING_END })
    }
}

export const signinGoogle= (accessToken) => async dispatch => {
    dispatch({ type: LOADING_START, payload: i18next.t("lbl_logging_in") })
    try {
        const response = await authService.googleAuth(accessToken);
        const token = response.data.token;
        localStorage.setItem('TOKEN', token);
        const userResponse = await authService.getCurrentUser(token);
        const user = userResponse.data.user;
        if (user.photoId) {
            const fileResponse = await storageService.getFile(user.photoId);
            user.photoUrl = `data:${fileResponse.data.file.contentType};base64,${fileResponse.data.file.url}`
        }
        dispatch({ type: LOGIN_SUCCESS, payload: userResponse.data.user })
        window.OneSignal.sendTag('userId', userResponse.data.user._id)
        history.go(-(history.length));
        history.replace('/')
        NotificationManager.success(i18next.t('lbl_login_success'),null,3000);
    } catch(err) {
        dispatch({ type: LOGIN_ERROR })
    } finally {
        dispatch({ type: LOADING_END })
    }
}

export const forgotPassword = (email) => dispatch => {
    return authService.sendEmailResetPassword(email);
}

export const resetPassword = (email, password) => async dispatch => {
    dispatch({ type: LOADING_START, payload: null })
    try {
        const response = await authService.updatePasswordForReset(email, password);
        history.go(-(history.length));
        history.replace('/')
        NotificationManager.success(i18next.t('lbl_update_password_success'),null,3000);
    } catch(err){
        if(err.response.data.code)
            NotificationManager.error(i18next.t(err.response.data.code), null ,3000);
    } finally {
        dispatch({ type: LOADING_END })
    }
}

export const changePassword = (email, current_password, password) => async dispatch => {
    dispatch({ type: LOADING_START, payload: null })
    try {
        const response = await authService.updatePasswordForChange(email, current_password, password);
        history.push('/')
        NotificationManager.success(i18next.t('lbl_update_password_success'),null,3000);
    } catch(err){
        NotificationManager.error(i18next.t(err.response.data.code), null ,3000);
    } finally {
        dispatch({ type: LOADING_END })
    }
}

export const currentUser = (token) =>async dispatch => {
    const userResponse = await authService.getCurrentUser(token);
    const user = userResponse.data.user;
    if (user && user.photoId) {
        const fileResponse = await storageService.getFile(user.photoId);
        user.photoUrl = `data:${fileResponse.data.file.contentType};base64,${fileResponse.data.file.url}`
    }
    dispatch({type: CURRENT_USER, payload: userResponse.data.user})
    window.OneSignal.sendTag('userId', userResponse.data.user._id)
} 
