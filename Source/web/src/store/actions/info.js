import { CURRENT_PROFILE } from '../types/info';

export const setCurrentProfile = (index) => dispatch => {
  dispatch({ type: CURRENT_PROFILE, payload: index })
}