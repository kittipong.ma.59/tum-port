import { LOADING_START, LOADING_END, SET_CURRENT_PATH, SET_DARK_MODE, GET_NOTIFICATION } from '../types/all';
import feedServices from '../../services/feed';
import moment from 'moment';

export const setDarkMode = (isDarkMode) => dispatch => {
  localStorage.setItem('DARK_MODE', isDarkMode);
  dispatch({ type: SET_DARK_MODE, payload: isDarkMode });
}

export const loadingStart = (text) => dispatch => {
  dispatch({ type: LOADING_START, payload: text })
}
export const loadingEnd = () => dispatch => {
  dispatch({ type: LOADING_END })
}

export const setCurrentPath = (path) => dispatch => {
  dispatch({ type: SET_CURRENT_PATH, payload: path })
}

export const getNotifications = (userId) => async dispatch => {
    let count = 0;
    const response = await feedServices.getNotification(userId);
    for (let i = 0; i < response.data.length; i++) {
      response.data[i].readed = response.data[i].read_by.filter(k => k.userId == userId).length > 0 ? true : false;
      if(response.data[i].readed == false && moment(response.data[i].pubDate).isAfter(moment().add(-1, 'M'))) {
        count += 1;
      }
    }
    dispatch({ type: GET_NOTIFICATION, payload: { data: response.data, count: count } })
}