import { LOADING_START, LOADING_END, SET_CURRENT_PATH, SET_DARK_MODE, GET_NOTIFICATION } from "../types/all";

const initStateDarkMode = {
    dark: localStorage.getItem('DARK_MODE')
}

const initState = {
    loading: false,
    text: null
}

const initStateCurrentPath = {
    path: null
}

const initStateFeed = {
    data: [],
    count: 0
}

export const feedReducer = (state = initStateFeed, action) => {
    const type = action.type;
    const payload = action.payload;
    if(type === GET_NOTIFICATION ){
       return { ...state, data: payload.data, count: payload.count};
    }else{
        return state
    }
}

export const darkModeReducer = (state = initStateDarkMode, action) => {
    const type = action.type;
    const payload = action.payload;
    if(type === SET_DARK_MODE ){
       return { ...state, dark: payload};
    }else{
        return state
    }
}

export const loadingReducer = (state = initState, action) => {
    const type = action.type;
    const payload = action.payload;
    if(type === LOADING_START ){
       return { ...state, loading: true, text: payload };
    }else if(type === LOADING_END ){
        return { ...state, loading: false, text: null };
    }else{
        return state
    }
}

export const currentPathReducer = (state = initStateCurrentPath, action) => {
    const type = action.type;
    const payload = action.payload;
    if(type === SET_CURRENT_PATH){
        return { ...state, path: payload }
    }else{
        return state
    }
}