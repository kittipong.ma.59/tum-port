import { CURRENT_USER, LOGIN_SUCCESS, LOGIN_ERROR, LOGOUT_SUCCESS, REGISTER_SUCCESS, REGISTER_ERROR } from "../types/auth";

const initState = {
    user: null,
    error: null
}

export default (state = initState, action) => {
    const type = action.type;
    const payload = action.payload;
    switch(type){
        case LOGIN_SUCCESS:
            return { ...state, user: payload, error: null };
        case LOGIN_ERROR:
            return { ...state, user: null, error: payload };
        case REGISTER_SUCCESS:
            return { ...state, user: payload, error: null };
        case REGISTER_ERROR:
            return { ...state, user: null, error: payload };
        case LOGOUT_SUCCESS:
            return { ...state, user: payload, error: null };
        case CURRENT_USER:
            return { ...state, user: payload };
        default:
            return state;
    }
}