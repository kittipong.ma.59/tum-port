import { CURRENT_PROFILE } from "../types/info";

const initState = {
    key: "1"
}

export const currenProfileReducer = (state = initState, action) => {
    const type = action.type;
    const payload = action.payload;
    if(type === CURRENT_PROFILE ){
       return { ...state, key: payload };
    }else{
        return state
    }
}