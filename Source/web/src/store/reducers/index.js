import { combineReducers } from 'redux';
// import { firebaseReducer } from 'react-redux-firebase';
import authReducer from './auth';
import { loadingReducer, currentPathReducer, darkModeReducer, feedReducer } from './all';
import { currenProfileReducer } from './info';

export default combineReducers({
    // firebaseReducer,
    auth: authReducer,
    loading: loadingReducer,
    currentPath: currentPathReducer,
    currentProfile: currenProfileReducer,
    darkMode: darkModeReducer,
    feed: feedReducer
});