import firebase from 'firebase/app'
import 'firebase/auth'
import config from './config'

firebase.initializeApp(config);

export const Fprovider = new firebase.auth.FacebookAuthProvider()
Fprovider.addScope('public_profile')
Fprovider.setCustomParameters({
  'display': 'popup'
})

export const Gprovider = new firebase.auth.GoogleAuthProvider()
Gprovider.setCustomParameters({
  'display': 'popup'
})

export default firebase;