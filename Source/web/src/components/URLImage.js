import React, { Component } from 'react';
import { Image } from 'react-konva';

class URLImage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            image: null
        }
    }

    componentDidMount = () => {
      this.loadImage();
    };

    componentWillUnmount = () => {
      this.image.removeEventListener('load', this.handleLoad);
    };
    

    loadImage = () => {
        this.image = new window.Image();
        this.image.src = this.props.src;
        this.image.addEventListener('load', this.handleLoad);
    }

    handleLoad = () => {
        this.setState({
            image: this.image
        })
    }
    

    render() {
        return (
            <Image
                x={this.props.x}
                y={this.props.y}
                image={this.state.image}
                width={this.props.width}
                height={this.props.height}
                scaleX={this.props.scaleX}
                scaleY={this.props.scaleY}
                stroke={this.props.stroke}
                strokeWidth={this.props.strokeWidth}
                draggable={this.props.draggable}
                name={this.props.name}
                key={this.props.key}
                onContextMenu={this.props.onContextMenu}
                onClick={this.props.onClick}
                onDragEnd={this.props.onDragEnd}
                onTransformEnd={this.props.onTransformEnd}
            />
        );
    }
}
 
export default URLImage;