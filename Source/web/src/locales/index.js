import CommonEN from './common/en.json';
import DesignEN from './design/en.json';
import NavbarEN from './Navbar/en.json';
import LoginEN from './Login/en.json';
import RegisterEN from './Register/en.json';
import ValidationEN from './Validation/en.json';
import ToastEN from './toast/en.json';
import InformationEN from './information/en.json';
import PortfolioEN from './portfolio/en.json';

import CommonTH from './common/th.json';
import DesignTH from './design/th.json';
import NavbarTH from './Navbar/th.json';
import LoginTH from './Login/th.json';
import RegisterTH from './Register/th.json';
import ValidationTH from './Validation/th.json';
import ToastTH from './toast/th.json';
import InformationTH from './information/th.json';
import PortfolioTH from './portfolio/th.json';

export default {
  en: {
    translation: { 
      ...CommonEN,
      ...DesignEN,
      ...NavbarEN,
      ...LoginEN,
      ...RegisterEN,
      ...ValidationEN,
      ...ToastEN,
      ...InformationEN,
      ...PortfolioEN
    }
  },
  th: {
    translation: {
      ...CommonTH,
      ...DesignTH,
      ...NavbarTH,
      ...LoginTH,
      ...RegisterTH,
      ...ValidationTH,
      ...ToastTH,
      ...InformationTH,
      ...PortfolioTH
    }
  }
}