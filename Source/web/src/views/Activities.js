import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next'
import { Link } from 'react-router-dom';

// Import Scss
import '../scss/information.scss'

// Import Store
import { setCurrentPath, loadingStart, loadingEnd } from '../store/actions/all'

// Import Services
import infoService from '../services/info';
import storageService from '../services/storage';

import {
  Button,
  Divider,
  Icon,
  Card,
  Table,
  Typography,
  Modal,
  Upload,
  Form,
  Empty,
  List,
  Row,
  Col,
  Menu,
  Dropdown,
  Checkbox
} from 'antd';

import moment from 'moment';
import ApiCalendar from 'react-google-calendar-api/ApiCalendar';
import Avatar from 'react-avatar';
import { NotificationManager } from 'react-notifications';
import reactcss from 'reactcss';

export class Activities extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resizeWidth: window.innerWidth,
      sizeMobile: 576,
      dataSource: [],
      previewVisible: false,
      sign: ApiCalendar.sign,
      visible: false,
      user: null,
      calendarList: null
    }
    this.signUpdate = this.signUpdate.bind(this);
		ApiCalendar.onLoad(() => {
			this.signUpdate(ApiCalendar.sign);
			ApiCalendar.listenSign(this.signUpdate);
    });
  }

  signUpdate(sign) {
    this.setState({
      sign
    })
  }

  async handleItemClick(event, name) {
		if (name === 'sign-in') {
      await ApiCalendar.handleAuthClick()
		} else if (name === 'sign-out') {
      await ApiCalendar.handleSignoutClick();
		}
  }

  async changeStatus(id, status) {
    const { loadingStart, loadingEnd, t } = this.props;
    loadingStart(null)
    const { dataSource } = this.state;
    const index = dataSource.findIndex((obj => obj._id == id))
    if(dataSource[index].imageId) {
      dataSource[index].status = status;
      await infoService.changeStatusActivities(id, status)
      this.setState({ dataSource })
    } else {
      NotificationManager.error(t('image'), t('cannot-be-displayed') ,1500);
    }
    loadingEnd()
  } 

  setActivities = async () => {
    const {userInfo} = this.props;
    const response = await infoService.getActivities(userInfo._id);
    let dataSource = [];
    let data = response.data;
    if(data.length > 0) {
      for (let i=0; i<data.length;i++) {
        if (data[i].imageId) {
          const fileResponse = await storageService.getFile(data[i].imageId);
          data[i].imageUrl = `data:${fileResponse.data.file.contentType};base64,${fileResponse.data.file.url}`;
        }
        dataSource.push(data[i]);
      }
      this.setState({ dataSource });
    }
  }

  componentDidMount= async () => {
    window.addEventListener('resize', this.handleResize);
    const { onSetCurrentPath, location, userInfo, t } = this.props;
    onSetCurrentPath(location.pathname);
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
    if(userInfo) {
      this.setActivities();
    }
  }

  componentWillReceiveProps = (nextProps) => {
    const { location, t } = nextProps;
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
  }

  handleResize = (e) => {
    e.preventDefault();
    this.setState({
      resizeWidth: window.innerWidth
    })
  }

  showConfirmDelete = (id) => {
    const { t } = this.props;
    const { dataSource } = this.state;
    Modal.confirm({
      title: t('lbl_confirm_delete'),
      okText: t('lbl_ok'),
      cancelText: t('lbl_cancel'),
      onOk: async () => {
        const imageId = dataSource.find(item => item._id == id).imageId;
        if (imageId)
          await storageService.removeFile(imageId);
        await infoService.removeActivities(id);
        this.setState({
          dataSource: dataSource.filter(item => item._id != id)
        })
      },
      onCancel() {},
    });
  }

  openCalendar = async () => {
    const { loadingStart, loadingEnd, t } = this.props;
    const { dataSource } = this.state;
    loadingStart(null)
    let user = null;
    let calendarList = null;
    if(this.state.sign) {
      user = ApiCalendar.gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
      const {result} = await ApiCalendar.gapi.client.calendar.events.list({
        'calendarId': 'primary',
        'showDeleted': false,
        'singleEvents': true,
        'orderBy': 'startTime'
      });
      result.items.forEach(item => {
        if(item.creator.self && item.summary && item.description) {
          if(!calendarList) {
            calendarList = [];
          }
          item.selected = false;
          item.disabled = dataSource.some(key => key.calendarId == item.id);
          calendarList.push(item)
        }
      })
    }
    this.setState({
      visible: true,
      user,
      calendarList
    })
    loadingEnd()
  }

  addActivitiesByCalendar = async () => {
    const { calendarList } = this.state;
    const {loadingStart, loadingEnd, t, userInfo} = this.props;
    loadingStart(null);
    try {
      const dataFilter = calendarList.filter(item => item.selected == true);
      for (let i = 0; i < dataFilter.length; i++) {
        console.log(dataFilter[i])
        const data = {
          title: dataFilter[i].summary,
          description: dataFilter[i].description,
          userId: userInfo._id,
          calendarId: dataFilter[i].id,
          location: dataFilter[i].location ? dataFilter[i].location : '',
          date: dataFilter[i].start ? new moment(dataFilter[i].start.dateTime).valueOf() : '',
          status: false
        }
        await infoService.addActivities(data);
      }
      NotificationManager.success(t('lbl_successfully_saved'),null,3000);
      this.setState({visible: false})
      this.setActivities()
    }
    catch(err) {
      console.log(err)
    } finally {
      loadingEnd()
    }
  }

  render() {
    const { t, i18n } = this.props;
    const { dataSource, resizeWidth, sizeMobile, previewVisible, imagePreview, user, calendarList } = this.state;
    const { Title } = Typography;

    const locationSvg = () => (
      <svg t="1585217384681" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="1749" width="16px" height="16px"><path d="M512 0C313.856 0 153.6 160.256 153.6 358.4c0 268.8 358.4 665.6 358.4 665.6s358.4-396.8 358.4-665.6c0-198.144-160.256-358.4-358.4-358.4z m0 486.4a128 128 0 0 1 0-256 128 128 0 0 1 0 256z" fill="currentColor" p-id="1750"></path></svg>
    )

    const dateSvg = () => (
      <svg t="1585218112259" fill="currentColor" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2493" width="16px" height="16px"><path d="M513.82 960c-247 0-448-200.92-448-448s201-448 448-448a43.94 43.94 0 1 1 0 87.87c-198.53 0-360.14 161.58-360.14 360.12s161.55 360.06 360.14 360.06c114.36 0 219.36-52.45 288.17-144a43.91 43.91 0 0 1 70.26 52.69C786.74 894.72 656.11 960 513.82 960z m386.6-299.07A42.09 42.09 0 0 1 889 659.4a43.83 43.83 0 0 1-31-53.81A362.56 362.56 0 0 0 870.3 512c0-147.38-88.26-278.48-224.84-333.9a44 44 0 0 1 33.06-81.45c169.89 69 279.68 232 279.68 415.35a448 448 0 0 1-15.32 116.33 44 44 0 0 1-42.45 32.61z m-217.93-55.44H510.62a44 44 0 0 1-44-44V321.35a43.94 43.94 0 1 1 87.87 0v196.27h128a43.93 43.93 0 1 1 0 87.87z m0 0" p-id="2494"></path></svg>
    )

    const LocationIcon = props => <Icon component={locationSvg} {...props} />;
    const DateIcon = props => <Icon component={dateSvg} {...props} />;
    
    const columns = [
      {
        title: t('lbl_image'),
        dataIndex: 'imageUrl',
        key: 'image',
        width: resizeWidth < sizeMobile ? '25%' : '15%',
        align: 'center',
        render: imageUrl => (
          imageUrl ? <img onClick={() => this.setState({previewVisible: true, imagePreview: imageUrl})} src={imageUrl} className="w-100" style={{cursor: 'pointer'}}/>
          : <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} className="m-0" description={t('lbl_no_image')} />
        )
      },
      {
        title: t('lbl_title'),
        key: 'title',
        width: resizeWidth < sizeMobile ? '40%' : '40%',
        render: (item) => resizeWidth < sizeMobile ? item.title : [
          <strong>{item.title}</strong>,
          <p>{item.description}</p>
        ]
      },
      {
        title: t('lbl_location'),
        dataIndex: 'location',
        key: 'location',
        className: 'sm-d-none',
        width: '15%'
      },
      {
        title: t('lbl_date'),
        dataIndex: 'date',
        key: 'date',
        className: 'sm-d-none',
        width: '10%',
        render: (item) => (
          item && (moment(item).locale(i18n.language === 'en-US' ? 'en' : 'th').add(543, 'years').format('LL'))
        )
      },
      {
        title: t('lbl_status'),
        key: 'status',
        className: 'sm-d-none text-center',
        width: '10%',
        render: (item) => (
          item.status && item.status == true ? <button style={styles.badge} class="badge badge-success p-2 cursor-pointer" onClick={() => this.changeStatus(item._id, false)}>{t('lbl_show')}</button> : <button style={styles.badge} class="badge badge-danger p-2 cursor-pointer" onClick={() => this.changeStatus(item._id, true)}>{t('lbl_hide')}</button>
        )
      },
      {
        title: null,
        key: 'action',
        width: '10%',
        render: (item) => (
          <span>
            <Button onClick={() => this.props.history.push(`/activities/editActivities`, { id: item._id})} size={resizeWidth < sizeMobile ? "small" : 'default'} icon="form" className="d-inline-flex align-items-center m-1"><span className="text-action">{t('lbl_edit')}</span></Button>
            <Button onClick={() => this.showConfirmDelete(item._id)} size={resizeWidth < sizeMobile ? "small" : 'default'} type="danger" icon="delete" className="d-inline-flex align-items-center m-1"><span className="text-action">{t('lbl_delete')}</span></Button>
          </span>
        )
      },
    ]

    const menuCalendar = (
      <Menu>
        <Menu.Item key="1" onClick={e => this.handleItemClick(e, 'sign-in')}>
          {t('lbl_change_account')}
        </Menu.Item>
        <Menu.Item key="2" onClick={e => this.handleItemClick(e, 'sign-out')}>
          {t('lbl_sign_out')}
        </Menu.Item>
      </Menu>
    );

    const styles = reactcss({
      'default': {
        badge: {
          border: 'none',
          cursor: 'pointer',
          outline: 'inherit'
        }
      }
    })

    return (
      <div className="activities-page px-md-4" style={{padding: resizeWidth < sizeMobile ? '0 16px': ''}}>
        <Card title={null} bordered={false} className="mb-4 shadow-sm position-relative" bodyStyle={{padding: resizeWidth < sizeMobile ?'.75rem': '1.5rem'}}>
          {!this.state.visible ?
            <React.Fragment> 
              <Title level={4}>{t('lbl_activities')}</Title>
              <Divider className="my-0" style={{backgroundColor: '#1890ff', height: 2}}/>
              <div className="text-right my-2">
                <Link to="/activities/addActivities" className="mr-md-2"><Button type="primary"className="mb-2">{t('lbl_add_activities')}</Button></Link>
                {!this.state.sign ?
                  <Button className="mb-2" type="primary" ghost onClick={(e) => this.handleItemClick(e, 'sign-in') }><img className="position-relative mr-2" style={{height: 20, top: -2}} src={require('../assets/icons/Google_Calendar_icon.svg')}/>  {t('lbl_connect_google_calendar')}</Button>
                :
                <Dropdown.Button onClick={this.openCalendar} overlay={menuCalendar} className="dropdown-calendar">
                  <img className="position-relative mr-2" style={{height: 20, top: -2}} src={require('../assets/icons/Google_Calendar_icon.svg')}/>  {t('lbl_add_from_google_calendar')}
                </Dropdown.Button>
                }
              </div>
              <Table 
                columns={columns} 
                dataSource={dataSource} 
                pagination={{ pageSize: 10 }} 
                locale={{emptyText: (<Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={t('lbl_no_data')} />)}}
                expandedRowRender={resizeWidth < sizeMobile ? record => [
                  record.location && (
                    <p style={{ margin: 0 }}>
                      <LocationIcon className="position-relative" style={{top: -3}}/> &nbsp;
                      {record.location}
                    </p>
                  ),
                  record.date && (
                    <p style={{ margin: 0 }}>
                      <DateIcon className="position-relative" style={{top: -3}}/> &nbsp;
                      {moment(record.date).locale(i18n.language === 'en-US' ? 'en' : 'th').add(543, 'years').format('LL')}
                    </p>
                  ),
                  <p style={{ margin: 0 }}>{record.description}</p>,
                  <div className="mt-1">
                    {record.status && record.status == true ? <button style={styles.badge} class="badge badge-success p-2 cursor-pointer" onClick={() => this.changeStatus(record._id, false)}>{t('lbl_show')}</button> : <button style={styles.badge} class="badge badge-danger p-2 cursor-pointer" onClick={() => this.changeStatus(record._id, true)}>{t('lbl_hide')}</button>}
                  </div>
                ] : false}
              ></Table>
              <Modal visible={previewVisible} footer={null} onCancel={() => this.setState({previewVisible: false})}>
                <img  style={{ width: '100%' }} src={imagePreview} />
              </Modal>
            </React.Fragment>
          :
          this.state.sign && user &&
          <div style={{minHeight: '70vh'}}>
            <List>
              <List.Item key={'user_calendar'} className="py-0">
                <List.Item.Meta
                  avatar={
                    <Avatar src={user.getImageUrl()} name={user.getName()} round={true} size={50} />
                  }
                  title={user.getName()}
                  description={user.getEmail()}
                />
                <div>
                  {calendarList && <Button disabled={ calendarList.some(item => item.selected == true) ? false : true} className="m-1" size={ resizeWidth < sizeMobile ? "small": 'default'} type="primary" onClick={this.addActivitiesByCalendar}>{t('lbl_add')}</Button>}
                  <Button className="m-1" size={ resizeWidth < sizeMobile ? "small": 'default'} onClick={() => this.setState({visible: false})}>{t('lbl_cancel')}</Button>
                </div>
              </List.Item>
            </List>
            <Divider className="my-2" style={{backgroundColor: '#1890ff', height: 2}}/>
              {calendarList ?
                <Row>
                  <List
                    itemLayout="horizontal"
                    className="list-google-calendar"
                    dataSource={calendarList}
                    pagination={{
                      pageSize: 5
                    }}
                    size="small"
                    renderItem={item => (
                      <Card size="small" className="mb-3" style={{ width: '100%', borderRadius: 8, borderLeft: '5px solid #1890ff' }}>
                        <Card.Meta
                          avatar={<Checkbox disabled={item.disabled} value={item.selected} onChange={e => {item.selected = e.target.checked; this.setState({calendarList})}}/>}
                          title={<span className="text-body font-weight-bold">{item.summary}</span>}
                          description={[
                            <span className="text-dark">{item.description}</span>,
                            item.location && (
                            <p className="mb-0">
                              <LocationIcon className="position-relative" style={{top: -3}}/> &nbsp;
                              {item.location}
                            </p>
                            )
                          ]}
                        />
                        <Divider className="mb-2 mt-3"/>
                        <span className="text-black-50">{moment(item.start.dateTime).add(543, 'years').format('DD MMM YYYY | hh:mm')}</span>
                      </Card>
                    )}
                  />
                </Row>
              :
                <Empty description={t('lbl_no_data')} />
            }
          </div>
          }
        </Card>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userInfo: state.auth.user
})

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
    loadingStart: (text) => dispatch(loadingStart(text)),
    loadingEnd: () => dispatch(loadingEnd())
  }
}

export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(Activities))
