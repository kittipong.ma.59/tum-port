import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next'
import { Link } from 'react-router-dom';

// Import Store
import { setCurrentPath } from '../store/actions/all'

// Import Services
import infoService from '../services/info';

import {
  Button,
  Row,
  Col,
  Card,
  Divider,
  Icon,
  Typography,
  Tooltip,
  Timeline,
  Empty
} from 'antd';

const colorIcon = ['#eb2f96', '#722ed1', '#52c41a', '#f5222d', '#fadb14'];

export class Education extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sizeMobile: 576,
      resizeWidth: window.innerWidth,
      education: null,
    }
  }

  componentDidMount= async () => {
    window.addEventListener('resize', this.handleResize);
    const { onSetCurrentPath, location, userInfo, t } = this.props
    onSetCurrentPath(location.pathname)
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
    this.shuffle(colorIcon);
    if(userInfo) {
      const educationResponse = await infoService.getEducation(userInfo._id);
      if(educationResponse.data.length > 0) {
        this.setState({education: educationResponse.data})
      }
    }
  }

  componentWillReceiveProps = (nextProps) => {
    const { location, t } = nextProps;
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
  }

  handleResize = (e) => {
    e.preventDefault();
    this.setState({
      resizeWidth: window.innerWidth
    })
  }

  shuffle = (a) => {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  render() {
    const { resizeWidth, sizeMobile, education } = this.state;
    const { userInfo, t } = this.props;
    const { Title } = Typography;

    const SchoolSvg = () => (
      <svg width="20px" height="20px" fill="currentColor" t="1580630004243" class="icon" viewBox="0 0 1160 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="13996" >
        <path d="M1093.290667 923.306667h-37.751467V341.128533c0-32.768-27.716267-60.484267-63.010133-60.484266h-143.701334v-42.8032c0-7.5776 0-27.716267-110.865066-131.072C624.571733 0.955733 599.381333 0.955733 589.277867 0.955733c-10.103467 0-35.293867 0-153.736534 103.287467C319.624533 207.598933 319.624533 225.28 319.624533 235.3152v42.8032h-138.581333c-35.362133 0-63.0784 27.784533-63.0784 60.484267v584.704H62.532267a27.989333 27.989333 0 0 0-27.716267 27.716266 27.989333 27.989333 0 0 0 27.716267 27.784534l1033.284266-2.525867a27.989333 27.989333 0 0 0 27.716267-27.784533c0-15.086933-15.086933-25.1904-30.242133-25.1904z m-103.287467-587.1616c5.051733 0 7.509333 2.4576 7.509333 4.983466v582.178134h-151.210666V336.145067h143.701333z m-519.168-189.098667C533.845333 91.7504 571.665067 63.965867 586.752 58.9824c35.293867 15.086933 183.978667 151.210667 206.6432 186.504533v677.888h-47.854933v-194.013866c0-60.484267-50.449067-108.407467-113.390934-108.407467H538.897067c-63.010133 0-113.4592 47.9232-113.4592 108.407467v194.013866H370.005333v-677.888c10.103467-12.629333 47.9232-52.974933 100.829867-98.304z m219.272533 776.260267H478.4128v-196.608c0-30.173867 27.716267-52.8384 60.484267-52.8384H632.149333c32.768 0 60.484267 22.664533 60.484267 52.906666v196.608h-2.525867zM170.939733 341.128533c0-2.525867 2.525867-4.983467 7.509334-4.983466h136.123733v587.1616H168.413867V341.128533h2.525866z" p-id="13997"></path>
        <path d="M453.154133 441.9584h244.462934a27.989333 27.989333 0 0 0 27.716266-27.716267 27.989333 27.989333 0 0 0-27.716266-27.716266H453.290667a27.989333 27.989333 0 0 0-27.784534 27.716266c2.525867 15.1552 12.629333 27.716267 27.716267 27.716267z m0 103.355733h244.462934a27.989333 27.989333 0 0 0 27.716266-27.716266 27.989333 27.989333 0 0 0-27.716266-27.716267H453.290667a27.989333 27.989333 0 0 0-27.784534 27.716267 27.989333 27.989333 0 0 0 27.716267 27.716266z m0-209.169066h244.462934a27.989333 27.989333 0 0 0 27.716266-27.784534 27.989333 27.989333 0 0 0-27.716266-27.716266H453.290667a27.989333 27.989333 0 0 0-27.784534 27.716266 27.989333 27.989333 0 0 0 27.716267 27.784534z" p-id="13998"></path>
      </svg>
    );

    const SchoolIcon = props => <Icon component={SchoolSvg} {...props} />;

    const extraTimelineItem = education && education.map((item, index) => {
      return (
        <Timeline.Item dot={<div className="icon-timeline-badge" style={{background: colorIcon[index]}}><SchoolSvg /></div>}>
          <div className="position-relative timeline-section">
            <div className={index %2 == 0 ? 'timeline-time-left': 'timeline-time-right'} style={{fontSize: 16}}>
              {item.startYear ? item.startYear : ''} - {item.endYear ? item.endYear : ''}
            </div>
            <div className={index %2 == 0 ? 'timeline-panal-right': 'timeline-panal-left' }>
              <Title style={{fontSize: 16, color: colorIcon[index]}}>{item.school ? item.school : '-'}</Title>
              <p className="mb-0">{t('lbl_education_level')}: {item.educationLevel ? item.educationLevel : '-'}</p>
              <p className="mb-0">{t('lbl_curriculum')}: {item.curriculum ? item.curriculum : '-'}</p>
              <p className="mb-0">{t('lbl_gpax')}: {item.gpax ? item.gpax : '-'}</p>
            </div>
          </div>
        </Timeline.Item>
      );
    });

    const timelineItems = [].concat(extraTimelineItem && extraTimelineItem).concat(<Timeline.Item></Timeline.Item>);

    return (
      <Row style={{padding: resizeWidth < sizeMobile ? '0 12px' : '0 24px'}} className="py-3">
        <Row gutter={24}>
          <Col md={{span: 24}} lg={{span: 24}}>
            <Card title={null} bordered={false} className="mb-4 px-md-3 shadow-sm position-relative" >
              <Title level={4}>{t('lbl_education')}</Title>
              <Divider className="mt-0 mb-5" style={{backgroundColor: '#1890ff', height: 2}}/>
              {education ? [
              <Link to='/education/editEducation'><Button type="primary" ghost size="small" style={{right: 24, top: 16}} className="position-absolute">{t('lbl_edit')}</Button></Link>,
                <Timeline mode={resizeWidth < sizeMobile ? "left" : 'alternate'}  className="px-md-3 px-1 pt-3">
                  {timelineItems}
                </Timeline>
              ]: <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={t('lbl_no_data')}>
                  <Link to='/education/editEducation'><Button type="primary">{t('lbl_create_new.')}</Button></Link>
                </Empty>
            }
            </Card>
          </Col>
        </Row>
      </Row>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userInfo: state.auth.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
  }
}

export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(Education));
