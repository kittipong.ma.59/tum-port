import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

// Import Store
import { setCurrentPath } from '../store/actions/all';
import { setCurrentProfile } from '../store/actions/info';

// Import Services
import infoService from '../services/info';

import {
  Button,
  Row,
  Col,
  Card,
  Divider,
  Icon,
  Typography,
  Tooltip
} from 'antd';

import Avatar from 'react-avatar';
import moment from 'moment';

export class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sizeMobile: 576,
      resizeWidth: window.innerWidth,
      profile: null
    }
  }

  componentDidMount= async () => {
    window.addEventListener('resize', this.handleResize);
    const { onSetCurrentPath, location, userInfo, t } = this.props
    onSetCurrentPath(location.pathname)
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
    if(userInfo) {
      const profileResponse = await infoService.getProfile(userInfo._id);
      if(profileResponse.data) {
        this.setState({
          profile: profileResponse.data
        })
      }
    }
  }

  componentWillReceiveProps = (nextProps) => {
    const { location, t } = nextProps;
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
  }

  handleResize = (e) => {
    e.preventDefault();
    this.setState({
      resizeWidth: window.innerWidth
    })
  }

  render() {
    const { resizeWidth, sizeMobile, profile } = this.state;
    const { userInfo, t } = this.props;
    const { Title } = Typography;
    const CakeSvg = () => (
      <svg width="15px" height="15px" fill="currentColor" t="1580575291685"  viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3763" >
        <path d="M832 1016.746667h-640a128 128 0 0 1-128-128v-298.666667a15.786667 15.786667 0 0 1 0-4.693333A158.72 158.72 0 0 1 177.066667 315.733333H384V224.853333a85.333333 85.333333 0 0 1 85.333333-85.333333h85.333334a85.333333 85.333333 0 0 1 85.333333 85.333333V315.733333h209.066667a159.146667 159.146667 0 0 1 158.72 158.72 157.013333 157.013333 0 0 1-47.786667 113.066667v301.226667a128 128 0 0 1-128 128z m-682.666667-384v256a42.666667 42.666667 0 0 0 42.666667 42.666666h640a42.666667 42.666667 0 0 0 42.666667-42.666666v-256a162.133333 162.133333 0 0 1-137.386667-42.666667 158.293333 158.293333 0 0 1-224 0 158.293333 158.293333 0 0 1-224 0 162.56 162.56 0 0 1-139.946667 40.533333z m587.946667-170.666667a42.666667 42.666667 0 0 1 42.666667 31.573333A73.386667 73.386667 0 1 0 849.066667 401.066667H177.066667a73.386667 73.386667 0 1 0 70.826666 92.16 42.666667 42.666667 0 0 1 42.666667-31.573334 42.666667 42.666667 0 0 1 42.666667 31.573334 73.386667 73.386667 0 0 0 141.653333 0 42.666667 42.666667 0 0 1 42.666667-31.573334 42.666667 42.666667 0 0 1 42.666666 31.573334 73.386667 73.386667 0 0 0 141.653334 0 42.666667 42.666667 0 0 1 35.413333-31.573334zM469.333333 315.733333h85.333334V224.853333h-85.333334zM525.226667 128a42.666667 42.666667 0 0 1-39.68-27.733333l-12.373334-31.573334A42.666667 42.666667 0 0 1 554.666667 36.266667l12.373333 31.573333a42.666667 42.666667 0 0 1-24.746667 55.04 39.253333 39.253333 0 0 1-17.066666 5.12z" p-id="3764"></path>
      </svg>
    );

    const CurriciculumSvg = () => (
      <svg width="15px" height="15px" t="1580580772971" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="5371" >
        <path d="M544 1018H116c-27.6 0-50-21.9-50-48.8V80.8C66 53.9 88.4 32 116 32h688c27.6 0 50 21.9 50 48.8v574.1h-40V80.8c0-4.8-4.6-8.8-10-8.8H116c-5.4 0-10 4-10 8.8v888.4c0 4.8 4.6 8.8 10 8.8h428v40z" fill="currentColor" p-id="5372"></path>
        <path d="M753.4 1023h-29.2l-1.2-0.1c-7.5-0.9-14.5-3.8-20.4-8.5-5.9-4.7-10.3-11-12.8-18l-7-19.7-19 9c-6.8 3.2-14.3 4.5-21.7 3.6-7.5-0.8-14.5-3.7-20.4-8.3l-0.9-0.7-20.8-20.8-0.8-1c-4.6-5.9-7.4-13-8.2-20.4-0.8-7.4 0.5-14.8 3.6-21.6l9-19-19.8-7.1c-7-2.5-13.2-6.9-17.9-12.7l-0.1-0.1c-4.7-5.9-7.6-12.9-8.5-20.4l-0.1-1.2v-29.2l0.1-1.2c0.9-7.5 3.8-14.5 8.5-20.3 4.7-5.9 11-10.3 18-12.8l19.7-7-9-18.9c-3.2-6.8-4.5-14.3-3.6-21.7 0.8-7.5 3.7-14.5 8.3-20.4l0.7-0.9 20.8-20.8 1-0.8c6-4.6 13-7.5 20.4-8.2 7.4-0.8 14.8 0.5 21.5 3.6l19 9 7-19.8c2.5-7 6.9-13.2 12.7-17.9l0.1-0.1c5.9-4.7 12.9-7.6 20.4-8.5l1.2-0.1h29.2l1.2 0.1c7.5 0.9 14.5 3.8 20.3 8.5 5.9 4.7 10.3 11 12.8 18.1l7 19.7 19-9c6.8-3.2 14.3-4.5 21.7-3.6 7.5 0.8 14.5 3.7 20.4 8.3l0.9 0.7 20.8 20.8 0.8 1c4.6 6 7.5 13 8.2 20.4 0.8 7.4-0.5 14.8-3.6 21.5l-9 19 19.8 7c7 2.5 13.2 6.9 17.9 12.7 4.7 5.9 7.7 12.9 8.6 20.5l0.1 1.2v29.2l-0.1 1.2c-0.9 7.5-3.8 14.5-8.5 20.3-4.7 5.9-11 10.3-18.1 12.8l-19.7 7 9 19c3.2 6.8 4.5 14.3 3.6 21.7-0.8 7.5-3.7 14.5-8.3 20.4l-0.7 0.9-20.8 20.8-1 0.8c-5.9 4.6-13 7.5-20.4 8.2-7.4 0.8-14.8-0.5-21.5-3.6l-19-9-7 19.8c-2.5 7.1-6.9 13.2-12.7 17.9l-0.1 0.1c-5.9 4.7-12.9 7.6-20.4 8.5l-1 0.1z m-25.8-40H750v-0.1l14.2-40.1 9-3.2c3.6-1.3 7-2.7 10.7-4.4l8.6-4.1 38.4 18.2 15.9-15.9v-0.1l-18.3-38.5 4.2-8.6c1.5-3.2 3-6.7 4.4-10.6l3.2-8.9 40.1-14.3V830h-0.1l-40-14.2-3.2-8.9c-1.3-3.7-2.8-7.2-4.4-10.6l-4.1-8.6 18.3-38.5-15.8-15.8h-0.1l-38.5 18.3-8.6-4.2c-3.2-1.5-6.7-3-10.6-4.4l-8.9-3.2L750 700h-22.4v0.1l-14.2 40-9 3.2c-3.6 1.3-7 2.7-10.6 4.4l-8.6 4.1-38.4-18.2-15.8 15.8v0.1l18.3 38.5-4.2 8.6c-1.5 3.1-2.9 6.5-4.4 10.5l-3.2 8.9-40.1 14.3v22.4h0.1l40.1 14.3 3.2 8.9c1.3 3.5 2.7 7 4.4 10.7l4.1 8.6-18.2 38.4 15.9 15.9h0.1l38.5-18.3 8.6 4.2c3.2 1.5 6.6 3 10.6 4.4l8.9 3.2 13.9 40z" fill="currentColor" p-id="5373"></path>
        <path d="M738.8 907.1c-36.2 0-65.6-29.4-65.6-65.6s29.4-65.6 65.6-65.6c36.2 0 65.6 29.4 65.6 65.6s-29.4 65.6-65.6 65.6z m0-100.6c-19.3 0-35 15.7-35 35s15.7 35 35 35 35-15.7 35-35-15.7-35-35-35zM710.5 292h-501c-11 0-20-9-20-20s9-20 20-20h501c11 0 20 9 20 20s-9 20-20 20zM710.5 558.4h-501c-11 0-20-9-20-20s9-20 20-20h501c11 0 20 9 20 20s-9 20-20 20zM470 824.8H198.5c-11 0-20-9-20-20s9-20 20-20H470c11 0 20 9 20 20s-9 20-20 20z" fill="currentColor" p-id="5374"></path>
      </svg>
    );
    
    const SchoolSvg = () => (
      <svg width="15px" height="15px" fill="currentColor" t="1580581433919" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2122" >
        <path d="M63.613318 960.51613 63.613318 826.000484 960.386682 826.000484l0 134.515646L63.613318 960.51613zM915.546428 870.837668 108.451526 870.837668l0 44.839231 807.094902 0L915.546428 870.837668z" p-id="2123"></path>
        <path d="M63.613318 400.030859 960.386682 400.030859l0 44.838208L63.613318 444.869067 63.613318 400.030859z" p-id="2124"></path>
        <path d="M153.268244 826.000484 153.268244 534.547529l-44.816718 0 0-44.839231 134.51667 0 0 44.839231-44.839231 0 0 291.451932L153.268244 825.99946z" p-id="2125"></path>
        <path d="M377.483842 826.000484 377.483842 534.547529l-44.838208 0 0-44.839231 134.515646 0 0 44.839231L422.32205 534.547529l0 291.451932L377.483842 825.99946z" p-id="2126"></path>
        <path d="M601.67795 826.000484 601.67795 534.547529l-44.861744 0 0-44.839231 134.541229 0 0 44.839231-44.839231 0 0 291.451932L601.67795 825.99946z" p-id="2127"></path>
        <path d="M825.870012 826.000484 825.870012 534.547529 781.031804 534.547529l0-44.839231 134.514623 0 0 44.839231-44.837184 0 0 291.451932L825.870012 825.99946z" p-id="2128"></path>
        <path d="M512.000512 63.74072 63.613318 310.352397l0 89.677439 44.838208 0 0-67.257823L512.000512 108.578927l403.545916 224.194108 0 67.257823 44.839231 0 0-89.677439L512.000512 63.74072z" p-id="2129"></path>
        <path d="M512.000512 351.098403c-49.523927 0-89.678462-40.152489-89.678462-89.676415 0-49.523927 40.154535-89.677439 89.678462-89.677439 49.523927 0 89.677439 40.153512 89.677439 89.677439C601.67795 310.945915 561.523415 351.098403 512.000512 351.098403zM512.000512 216.582757c-24.761963 0-44.839231 20.054755-44.839231 44.839231 0 24.761963 20.077268 44.838208 44.839231 44.838208 24.783453 0 44.839231-20.076244 44.839231-44.838208C556.839743 236.636488 536.783964 216.582757 512.000512 216.582757z" p-id="2130"></path>
      </svg>
    );
    
    const PhoneSvg = () => (
      <svg width="30px" height="30px"  t="1580586321234"  viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3678">
        <path d="M352.349091 266.24l71.68 101.003636a32.116364 32.116364 0 0 1 0 34.443637l-26.530909 46.545454a32.116364 32.116364 0 0 0 0 33.512728 475.694545 475.694545 0 0 0 136.378182 135.912727 46.545455 46.545455 0 0 0 41.425454 2.327273l46.545455-21.876364a22.807273 22.807273 0 0 1 21.876363 0l107.054546 66.094545a26.065455 26.065455 0 0 1 10.705454 33.047273c-9.774545 24.669091-30.254545 61.905455-61.44 68.421818-46.545455 9.774545-154.530909-14.894545-287.650909-148.014545s-167.563636-240.174545-153.6-308.596364A128.930909 128.930909 0 0 1 325.818182 257.396364a22.807273 22.807273 0 0 1 26.996363 8.843636z" fill="currentColor" p-id="3679"></path>
        <path d="M512 1024a512 512 0 1 1 512-512 512 512 0 0 1-512 512z m0-977.454545a465.454545 465.454545 0 1 0 465.454545 465.454545A465.454545 465.454545 0 0 0 512 46.545455z" fill="currentColor"  p-id="3680"></path>
      </svg>
    );

    const LineSvg = () => (
      <svg width="30px" height="30px" t="1580586491186" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="1853" >
        <path d="M608.20001 408.4v142.2c0 3.6-2.8 6.4-6.4 6.4h-22.8c-2.2 0-4.2-1.2-5.2-2.6l-65.2-88v84.4c0 3.6-2.8 6.4-6.4 6.4h-22.8c-3.6 0-6.4-2.8-6.4-6.4v-142.2c0-3.6 2.8-6.4 6.4-6.4H502.00001c2 0 4.2 1 5.2 2.8l65.2 88v-84.4c0-3.6 2.8-6.4 6.4-6.4h22.8c3.6-0.2 6.6 2.8 6.6 6.2z m-164-6.4h-22.8c-3.6 0-6.4 2.8-6.4 6.4v142.2c0 3.6 2.8 6.4 6.4 6.4h22.8c3.6 0 6.4-2.8 6.4-6.4v-142.2c0-3.4-2.8-6.4-6.4-6.4z m-55 119.2h-62.2v-112.8c0-3.6-2.8-6.4-6.4-6.4h-22.8c-3.6 0-6.4 2.8-6.4 6.4v142.2c0 1.8 0.6 3.2 1.8 4.4 1.2 1 2.6 1.8 4.4 1.8h91.4c3.6 0 6.4-2.8 6.4-6.4v-22.8c0-3.4-2.8-6.4-6.2-6.4zM728.20001 402h-91.4c-3.4 0-6.4 2.8-6.4 6.4v142.2c0 3.4 2.8 6.4 6.4 6.4h91.4c3.6 0 6.4-2.8 6.4-6.4v-22.8c0-3.6-2.8-6.4-6.4-6.4H666.00001v-24h62.2c3.6 0 6.4-2.8 6.4-6.4V468c0-3.6-2.8-6.4-6.4-6.4H666.00001v-24h62.2c3.6 0 6.4-2.8 6.4-6.4v-22.8c-0.2-3.4-3-6.4-6.4-6.4zM960.00001 227.4V798c-0.2 89.6-73.6 162.2-163.4 162H226.00001c-89.6-0.2-162.2-73.8-162-163.4V226c0.2-89.6 73.8-162.2 163.4-162H798.00001c89.6 0.2 162.2 73.6 162 163.4z m-123.2 245.2c0-146-146.4-264.8-326.2-264.8-179.8 0-326.2 118.8-326.2 264.8 0 130.8 116 240.4 272.8 261.2 38.2 8.2 33.8 22.2 25.2 73.6-1.4 8.2-6.6 32.2 28.2 17.6 34.8-14.6 187.8-110.6 256.4-189.4 47.2-52 69.8-104.6 69.8-163z" fill="currentColor" p-id="1854"></path>
      </svg>
    );

    const CakeIcon = props => <Icon component={CakeSvg} {...props} />;
    const SchoolIcon = props => <Icon component={SchoolSvg} {...props} />;
    const CurriciculumIcon = props => <Icon component={CurriciculumSvg} {...props} />;
    const PhoneIcon = props => <Icon component={PhoneSvg} {...props} />;
    const LineIcon = props => <Icon component={LineSvg} {...props} />;

    return (
      <Row style={{padding: resizeWidth < sizeMobile ? '0 12px' : '0 24px'}} className="py-3">
        <Row gutter={24}>
          <Col md={{span: 24}} lg={{span: 8}}>
            <Card title={null} bordered={false} className="mb-4 shadow-sm position-relative" >
              <Link to="/profile/editProfile"><Button onClick={() => this.props.setCurrentProfile("1")} type="primary" ghost size="small" style={{right: 24, top: 16}} className="position-absolute">{t('lbl_edit')}</Button></Link>
              {userInfo && [
                <div className="text-center my-3">
                  <Avatar src={userInfo.photoUrl} name={userInfo.firstname+' '+userInfo.lastname} round={true} size={80} />
                </div>,
                <div className="text-center color-black-85" style={{fontWeight: 500, fontSize: 20}}>
                  {userInfo.firstname+' '+userInfo.lastname}
                </div>,
                <div className="text-center mb-3">
                  {userInfo.email}
                </div>
              ]}
              <p className="mb-2 pl-4 position-relative">
                <Tooltip placement="top" title={t('lbl_nickname')}>
                  <Icon type="user" className="position-absolute" style={{fontSize: 14, left: 0, top: 3}}/>
                </Tooltip>
                {profile && profile.nickname? profile.nickname : '-'}
              </p> 
              <p className="mb-2 pl-4 position-relative">
                <Tooltip placement="top" title={t('lbl_identification_no')}>
                  <Icon type="credit-card" className="position-absolute" style={{fontSize: 14, left: 0, top: 3}}/>
                </Tooltip>
                {profile && profile.identificationNo? profile.identificationNo : '-'}
              </p> 
              <p className="mb-2 pl-4 position-relative">
                <Tooltip placement="top" title={t('lbl_date_of_birth')}>
                  <CakeIcon className="position-absolute" style={{left: 0, top: 3}}/>
                </Tooltip>
                {profile && profile.dateOfBirth? moment(profile.dateOfBirth).add(543, 'years').format('LL') : '-'}
              </p>
              <p className="mb-2 pl-4 position-relative">
                <Tooltip placement="top" title={t('lbl_address')}>
                  <Icon type="environment" className="position-absolute" style={{fontSize: 14, left: 0, top: 3}}/>
                </Tooltip>
                {profile && profile.address ? profile.address +', ' : '' }
                {profile && profile.subdistrict ? profile.subdistrict+', ' : '' }
                {profile && profile.district ? profile.district + ', ' : '' }
                {profile && profile.province ? profile.province +', ' : '' }
                {profile && profile.zipcode ? profile.zipcode : ''}
              </p>
              <Divider className="my-2" dashed type="horizontal" />
              <Row gutter={24}>
                <Col span={12}>
                  <p className="mb-1 position-relative ">
                    <span className="mr-2" >{t('lbl_nationality')} : </span>
                    <span>{profile && profile.nationality ? profile.nationality : '-'}</span>
                  </p>
                </Col>
                <Col span={12}>
                  <p className="mb-1 position-relative ">
                    <span className="mr-2" >{t('lbl_race')} : </span>
                    <span>{profile && profile.race ? profile.race : '-'}</span>
                  </p>
                </Col>
                <Col span={12}>
                  <p className="mb-1 position-relative">
                    <span className="mr-2" >{t('lbl_religion')} : </span>
                    <span>{profile && profile.religion ? profile.religion : '-'}</span>
                  </p>
                </Col>
                <Col span={12}>
                  <p className="mb-1 position-relative">
                    <span className="mr-2" >{t('lbl_blood_type')} : </span>
                    <span>{profile && profile.bloodType ? profile.bloodType : '-'}</span>
                  </p>
                </Col>
                <Col span={12}>
                  <p className="mb-1 position-relative">
                    <span className="mr-2" >{t('lbl_weight_no')} : </span>
                    <span>{profile && profile.weight ? profile.weight : '-'}</span>
                  </p>
                </Col>
                <Col span={12}>
                  <p className="mb-0 position-relative">
                    <span className="mr-2" >{t('lbl_height_no')} : </span>
                    <span>{profile && profile.height ? profile.height : '-'}</span>
                  </p>
                </Col>
              </Row>
              <Divider className="my-2" dashed type="horizontal" />
              <p className="mb-2 pl-4 position-relative">
                <Tooltip placement="top" title={t('lbl_school')}>
                  <SchoolIcon className="position-absolute" style={{fontSize: 14, left: 0, top: 3}}/>
                </Tooltip>
                {profile && profile.school? profile.school : '-'}
              </p> 
              <p className="mb-2 pl-4 position-relative">
                <Tooltip placement="top" title={t('lbl_curriculum')}>
                  <CurriciculumIcon className="position-absolute" style={{fontSize: 14, left: 0, top: 3}}/>
                </Tooltip>
                {profile && profile.curriculum? profile.curriculum : '-'}
              </p> 
            </Card>
          </Col>
          <Col md={{span: 24}} lg={{span: 16}}>
            <Col span={24} className="px-0">
              <Card title={t('lbl_contact_info')} extra={<Link to="/profile/editProfile"><Button onClick={() => this.props.setCurrentProfile("2")} type="primary" ghost size="small">{t('lbl_edit')}</Button></Link>} bordered={false} className="mb-4 shadow-sm" headStyle={{fontSize: 18}} bodyStyle={{paddingTop: 16}}>
                <Row gutter={24} className="px-3">
                  <Col md={{span: 24}} lg={{span: 12}} className="mb-3">
                    <Row gutter={24} className="d-flex align-items-start">
                      <Col className="float-left">
                        <PhoneIcon style={{fontSize: 30}}/>
                      </Col>
                      <Col className="d-inline-block">
                        <p className="mb-1" style={{color: '#8c8c8c', fontSize: 12}}>{t('lbl_phone_no')}</p>
                        <p className="mb-1">{profile && profile.phoneNo ? profile.phoneNo : '-'}</p>
                      </Col>
                    </Row>
                  </Col>
                  <Col md={{span: 24}} lg={{span: 12}} className="mb-3">
                    <Row gutter={24} className="d-flex align-items-start">
                      <Col className="float-left">
                        <Icon type="facebook" style={{fontSize: 30}}/>
                      </Col>
                      <Col className="d-inline-block">
                        <p className="mb-1" style={{color: '#8c8c8c', fontSize: 12}}>{t('lbl_facebook')}</p>
                        <p className="mb-1">{profile && profile.facebook ? profile.facebook : '-'}</p>
                      </Col>
                    </Row>
                  </Col>
                  <Col md={{span: 24}} lg={{span: 12}} className="mb-3">
                    <Row gutter={24} className="d-flex align-items-start">
                      <Col className="float-left">
                        <LineIcon style={{fontSize: 30,}}/>
                      </Col>
                      <Col className="d-inline-block">
                        <p className="mb-1" style={{color: '#8c8c8c', fontSize: 12}}>{t('lbl_line')}</p>
                        <p className="mb-1">{profile && profile.line ? profile.line : '-'}</p>
                      </Col>
                    </Row>
                  </Col>
                  <Col md={{span: 24}} lg={{span: 12}} className="mb-3">
                    <Row gutter={24} className="d-flex align-items-start">
                      <Col className="float-left">
                        <Icon type="instagram" style={{fontSize: 30}}/>
                      </Col>
                      <Col className="d-inline-block">
                        <p className="mb-1" style={{color: '#8c8c8c', fontSize: 12}}>{t('lbl_instagram')}</p>
                        <p className="mb-1">{profile && profile.instagram ? profile.instagram : '-'}</p>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Card>
            </Col>
            <Col span={24} className="px-0">
              <Card title={t('lbl_family_info')} extra={<Link to="/profile/editProfile"><Button onClick={() => this.props.setCurrentProfile("3")} type="primary" ghost size="small">{t('lbl_edit')}</Button></Link>} bordered={false} className="mb-4 shadow-sm" headStyle={{fontSize: 18}} bodyStyle={{paddingTop: 16}}>
                <Row gutter={24}>
                  <Col sm={{span: 24}} md={{span: 12}}>
                    <div className="row">
                      <div className="col-3">
                        <Tooltip placement="top" title={t('lbl_father')}>
                          <img src="svgs/icon_head_father_two.svg" style={{width: 50, height: 50}}/>
                        </Tooltip>
                      </div>
                      <div className="col-9">
                        <p className="mb-2" style={{fontWeight: 600, fontSize: 16}}>{profile && profile.fatherName ? profile.fatherName : '-'}</p>
                        <p className="mb-1">{t('lbl_occupation')} : {profile && profile.fatherOccupation ? profile.fatherOccupation : '-'}</p>
                        <p className="mb-1">{t('lbl_tel')} : {profile && profile.fatherPhoneNo ? profile.fatherPhoneNo : '-'}</p>
                        <p className="mb-2">{t('lbl_age')} : {profile && profile.fatherAge ? profile.fatherAge : '-'}</p>
                      </div>
                    </div>
                  </Col>
                  <Col sm={{span: 24}} md={{span: 12}}>
                    <div className="row">
                      <div className="col-3">
                        <Tooltip placement="top" title={t('lbl_mother')}>
                          <img src="svgs/icon_head_mother_two.svg" style={{width: 50, height: 50}}/>
                        </Tooltip>
                      </div>
                      <div className="col-9">
                        <p className="mb-2" style={{fontWeight: 600, fontSize: 16}}>{profile && profile.motherName ? profile.motherName : '-'}</p>
                        <p className="mb-1">{t('lbl_occupation')} : {profile && profile.motherOccupation ? profile.motherOccupation : '-'}</p>
                        <p className="mb-1">{t('lbl_tel')} : {profile && profile.motherPhoneNo ? profile.motherPhoneNo : '-'}</p>
                        <p className="mb-2">{t('lbl_age')} : {profile && profile.motherAge ? profile.motherAge : '-'}</p>
                      </div>
                    </div>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Col>
        </Row>
      </Row>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userInfo: state.auth.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
    setCurrentProfile: (key) => dispatch(setCurrentProfile(key))
  }
}

export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(Profile));
