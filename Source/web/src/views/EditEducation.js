import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next'

// Import Store
import { setCurrentPath, loadingStart, loadingEnd } from '../store/actions/all'
import { currentUser } from '../store/actions/auth'

// Import Services
import infoService from '../services/info';
import authService from '../services/auth';
import storageService from '../services/storage';

// Import Scss
import '../scss/information.scss'

import {
  AutoComplete,
  Button,
  Card,
  Col,
  Row,
  Form,
  Icon,
  Input,
  Divider,
  Select,
  Typography,
} from 'antd'

import { NotificationManager } from 'react-notifications';

let educationId = 0;

export class EditEducation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resultSchool: [],
      resizeWidth: window.innerWidth,
      sizeMobile: 576,
      educationLevel: ['อนุบาลศึกษา', 'ประถมศึกษา', 'มัธยมศึกษาตอนต้น', 'มัธยมศึกษาตอนปลาย', 'อาชีวศึกษา'],
      initialSchool: [],
      initialEducationLevel: [],
      initialCurriculum: [],
      initialGpax: [],
      initialStartYear: [],
      initialEndYear: [],
      removeId: []
    }
  }

  componentDidMount= async () => {
    window.addEventListener('resize', this.handleResize);
    const { onSetCurrentPath, location, userInfo, t } = this.props
    onSetCurrentPath(location.pathname)
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
    educationId = 0
    this.setState({
      resultSchool: []
    })
    const { setFieldsValue } = this.props.form;
    const education = await infoService.getEducation(userInfo._id);
    if(education.data.length > 0) {
      let initialKey = [];
      let initialId = [];
      let initialSchool = [];
      let initialEducationLevel = [];
      let initialCurriculum = [];
      let initialGpax = [];
      let initialStartYear = [];
      let initialEndYear = [];
      education.data.map(async (item,index) => {
        initialKey.push(educationId++);
        initialId.push(item._id);
        initialSchool.push(item.school);
        initialEducationLevel.push(item.educationLevel);
        initialStartYear.push(item.startYear);
        initialEndYear.push(item.endYear);
        if(item.curriculum) initialCurriculum.push(item.curriculum);
        if(item.gpax) initialGpax.push(item.gpax);
      })
      setFieldsValue({
        educationKeys: initialKey,
        _id: initialId,
      });
      this.setState({initialSchool, initialEducationLevel, initialCurriculum, initialGpax, initialStartYear, initialEndYear })
    }
  }

  componentWillReceiveProps = (nextProps) => {
    const { location, t } = nextProps;
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
  }

  handleResize = (e) => {
    e.preventDefault();
    this.setState({
      resizeWidth: window.innerWidth
    })
  }

  handleSubmit = async e => {
    e.preventDefault();
    const { loadingStart, loadingEnd, userInfo, onPercentInfo, t } = this.props;
    const { removeId } = this.state;
    loadingStart();
    try {
      const { validateFields, getFieldValue } = this.props.form;
      const values = await validateFields();
      if(values.educationKeys.length > 0) {
        values.educationKeys.map(async (item, index) => {
          let schoolData = {
            _id: values._id[index],
            userId: userInfo._id,
            school: values.school[index],
            educationLevel: values.educationLevel[index],
            startYear: values.startYear[index],
            endYear: values.endYear[index],
            curriculum: values.curriculum[index],
            gpax: values.gpax[index]
          };
          if(schoolData._id) {
            await infoService.updateEducation(schoolData);
          } else {
            await infoService.addEducation(schoolData);
          }
        });
      }
      if (removeId.length > 0) {
        removeId.map(async item => {
          await infoService.removeEducation(item);
        });
      }
      this.props.history.go(-1);
      NotificationManager.success(t('lbl_successfully_saved'),null,3000);

    } catch (err) {
      console.log(err)
    } finally {
      loadingEnd();
    }
  }


  removeEducation = (k) => {
    const { form } = this.props;
    const keys = form.getFieldValue('educationKeys');
    const Ids = form.getFieldValue('_id');
    let removeId = this.state.removeId;
    let indexId = 0;
    keys.map((item, index) => {
      if(item == k) {
        indexId = index;
      }
    })
    if(Ids[indexId]) {
      removeId.push(Ids[indexId])
    }
    this.setState({ removeId });
    form.setFieldsValue({
      educationKeys: keys.filter(key => key !== k),
      _id: Ids.filter(key => key !== Ids[indexId]),
    });
  };

  addEducation = () => {
    const { form } = this.props;
    const keys = form.getFieldValue('educationKeys');
    const _ids = form.getFieldValue('_id');
    const nextKeys = keys.concat(educationId++);
    const nextIds = _ids.concat(null);

    let resultSchool = this.state.resultSchool;
    resultSchool.push([]);
    this.setState({ resultSchool });
    form.setFieldsValue({
      educationKeys: nextKeys,
      _id: nextIds,
    });
  };

  handleSearchSchool = async (value, key) => {
    let resultSchool = this.state.resultSchool;
    const response = await infoService.autoCompleteSchool(value);
    if(!value) {
      resultSchool[key] = [];
      this.setState({ resultSchool })
    } 
    else {
      if(response.data) {
        resultSchool[key] = response.data;
        this.setState({ resultSchool })
      }
    }
  }

  render() {
    const { resizeWidth, sizeMobile, resultSchool, initialSchool, initialEducationLevel, initialCurriculum, initialGpax, initialStartYear, initialEndYear } = this.state;
    const { t } = this.props;
    const { Title } = Typography;
    const { getFieldDecorator, getFieldValue } = this.props.form;

    getFieldDecorator('educationKeys', { initialValue: [] });
    getFieldDecorator('_id', { initialValue: [] });

    const educationKeys = getFieldValue('educationKeys');
 
    const formEducationItems = educationKeys && educationKeys.map((k, index) => ([
      <Row gutter={24}>
        <Col sm={{ span: 24 }} md={{ span: 12 }} >
          <Form.Item
            label={t('lbl_school')}
            key={`school${k}`}
          >
            {getFieldDecorator(`school[${k}]`, {
              validateTrigger: ['onBlur'],
              initialValue: initialSchool[k],
              rules: [
                {
                  required: true,
                  whitespace: true,
                  message: t("required", {field: t('lbl_school')}),
                },
              ]
            })(
              <AutoComplete className="w-100" optionLabelProp="text" onSearch={value => this.handleSearchSchool(value, k)} placeholder={t('lbl_school')}>
                {resultSchool[k] && resultSchool[k].map(item => 
                  <AutoComplete.Option 
                    text={item.SchoolName} 
                    key={item.SchoolName}
                  >
                    {`${item.SchoolName}, ${item.District}, ${item.Province}, ${item.PostCode}`}
                  </AutoComplete.Option>)
                }
              </AutoComplete>
            )}
          </Form.Item>
        </Col>
        <Col sm={{ span: 24 }} md={{ span: 12 }} >
          <Form.Item
            label={t('lbl_education_level')}
            key={`educationLevel${k}`}
          >
            {getFieldDecorator(`educationLevel[${k}]`, {
              validateTrigger: ['onBlur'],
              initialValue: initialEducationLevel[k],
              rules: [
                {
                  required: true,
                  whitespace: true,
                  message: t("required_select", {field: t('lbl_education_level')}),
                },
              ]
            })(
              <Select 
                placeholder={t('lbl_education_level')}
              >
                {
                  this.state.educationLevel.map(item => 
                  <Select.Option value={item}>{item}</Select.Option>
                  )
                }
              </Select>
            )}
          </Form.Item>
        </Col>
        <Col sm={{ span: 24 }} md={{ span: 12 }} >
          <Form.Item
            label={t('lbl_start_year')}
            required={false}
            key={`startYear${k}`}
          >
            {getFieldDecorator(`startYear[${k}]`, {
              validateTrigger: ['onBlur'],
              initialValue: initialStartYear[k],
              rules: [
                {
                  required: true,
                  whitespace: true,
                  message: t("required", {field: t('lbl_start_year')}),
                },
              ]
            })(
              <Input allowClear placeholder={t('lbl_start_year')} />
            )}
          </Form.Item>
        </Col>
        <Col sm={{ span: 24 }} md={{ span: 12 }} >
          <Form.Item
            label={t('lbl_end_year')}
            required={false}
            key={`endYear${k}`}
          >
            {getFieldDecorator(`endYear[${k}]`, {
              validateTrigger: ['onBlur'],
              initialValue: initialEndYear[k],
              rules: [
                {
                  required: true,
                  whitespace: true,
                  message: t("required", {field: t('lbl_end_year')}),
                },
              ]
            })(
              <Input allowClear placeholder={t('lbl_end_year')} />
            )}
          </Form.Item>
        </Col>
        <Col sm={{ span: 24 }} md={{ span: 12 }} >
          <Form.Item
            label={t('lbl_curriculum')}
            required={false}
            key={`curriculum${k}`}
          >
            {getFieldDecorator(`curriculum[${k}]`, {
              validateTrigger: ['onBlur'],
              initialValue: initialCurriculum[k],
            })(
              <Input allowClear placeholder={t('lbl_curriculum')} />
            )}
          </Form.Item>
        </Col>
        <Col sm={{ span: 24 }} md={{ span: 12 }} >
          <Form.Item
            label={t('lbl_gpax')}
            required={false}
            key={`gpax${k}`}
          >
            {getFieldDecorator(`gpax[${k}]`, {
              validateTrigger: ['onBlur'],
              initialValue: initialGpax[k],
            })(
              <Input allowClear placeholder={t('lbl_gpax')} />
            )}
          </Form.Item>
        </Col>
      </Row>,
      <div className="text-right">
        <Button type="danger" onClick={() => this.removeEducation(k)}>{t('lbl_delete')}</Button>
      </div>,
      <Divider type="horizontal" dashed className="my-3"/>
    ]));

    return (
      <div className="container px-3">
        <Row gutter={24}>
          <Col md={{span: 24}} lg={{span: 24}} className="px-3">
            <Card title={null} bordered={false} className="mb-4 px-3 shadow-sm position-relative" >
              <Title level={4}>{t('/education/editeducation')}</Title>
              <Form onSubmit={this.handleSubmit} className="my-3" hideRequiredMark={false} labelCol={{ span: 8 }} wrapperCol={{ span: 16 }}>
                {formEducationItems}
                { educationKeys.length < 4 &&
                <Row gutter={24}>
                  <Col sm={{ span: 24 }} md={{ span: 8 }} > 
                    <Form.Item>
                      <Button type="dashed" block onClick={this.addEducation} className="d-flex align-items-center justify-content-center">
                        <Icon type="plus" /> {t('lbl_add_education')}
                      </Button>
                    </Form.Item>
                  </Col>
                </Row>
                }
                <Row gutter={24} className="px-2">
                  <Col sm={{ span: 24 }} md={{ span: 24 }} > 
                    <Form.Item className="text-sm-center text-md-left">
                      <Button htmlType="submit" type="primary" className="mt-3 mr-2" >
                        {t('lbl_save')}
                      </Button>
                      <Button type="basic" className="mt-3" onClick={() => this.props.history.go(-1)}>
                        {t('lbl_cancel')}
                      </Button>
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Card>
          </Col>
        </Row>
      </div>    
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userInfo: state.auth.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
    loadingStart: (text) => dispatch(loadingStart(text)),
    loadingEnd: () => dispatch(loadingEnd()),
  }
}

export default Form.create({ name: 'education' })(withTranslation()(connect(mapStateToProps, mapDispatchToProps)(EditEducation)))
