import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next'

// Import Store
import { loadingStart, loadingEnd, setCurrentPath } from '../store/actions/all'
import { currentUser } from '../store/actions/auth'
import { setCurrentProfile } from '../store/actions/info'

// Import Services
import infoService from '../services/info';
import storageService from '../services/storage';

// Import Scss
import '../scss/information.scss'

import {
  AutoComplete,
  Button,
  Card,
  Col,
  DatePicker,
  Row,
  Form,
  Input,
  Icon,
  Divider,
  Select,
  Typography,
  Upload,
  Menu
} from 'antd';

import moment from 'moment';
import InputAddress from 'react-thailand-address-autocomplete';
import ImgCrop from 'antd-img-crop';
import { NotificationManager } from 'react-notifications';
import Avatar from 'react-avatar';

export class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sizeMobile: 576,
      resizeWidth: window.innerWidth,
      previewAvatarVisible: false,
      previewAvatarImage: '',
      avatarsFileList: [],
      avatarImage: null,
      resultSchool: [],
      profile: null,
    }
  }

  componentDidMount= async () => {
    const { onSetCurrentPath, location, t } = this.props
    onSetCurrentPath(location.pathname)
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
    window.addEventListener('resize', this.handleResize);
    const { userInfo } = this.props;

    if(userInfo) {
      const profile = await infoService.getProfile(userInfo._id);
      if(profile.data) {
        this.setState({
          profile: profile.data
        })
      }
    }
  }

  componentWillReceiveProps = (nextProps) => {
    const { location, t } = nextProps;
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
  }

  handleResize = (e) => {
    e.preventDefault();
    this.setState({
      resizeWidth: window.innerWidth
    })
  }

  changeAddress = (e) => {
    const { setFieldsValue } = this.props.form;
    setFieldsValue({
      [e.target.name]: e.target.value
    });
  }
  selectAddress = (fullAddress) => {
    const { subdistrict, district, province, zipcode } = fullAddress;
    const { setFieldsValue } = this.props.form;
    setFieldsValue({
      subdistrict,
      district,
      province,
      zipcode
    });
  }

  handleSubmit = async e => {
    e.preventDefault();
    const { loadingStart, loadingEnd, userInfo, onCurrentUser, t, currentProfile } = this.props;
    const { avatarsFileList } = this.state;
    loadingStart();
    try {
      const { validateFields } = this.props.form;
      const values = await validateFields();
      if(values){
        values.photoId = userInfo.photoId;
        if (avatarsFileList.length !== 0 && avatarsFileList[0].originFileObj && currentProfile === '1') {
          let formData = new FormData();
          formData.append("file", avatarsFileList[0].originFileObj);
          const response = await storageService.uploadFile(formData);
          values.photoId = response.data.file.id
        }
        if (userInfo.photoId && avatarsFileList.length !== 0 && avatarsFileList[0].originFileObj && currentProfile === '1') {
          await storageService.removeFile(userInfo.photoId);
        } else if (userInfo.photoId && avatarsFileList.length === 0 && currentProfile === '1') {
          await storageService.removeFile(userInfo.photoId);
          values.photoId = null;
        }
        values.userId = userInfo._id
        if(values.dateOfBirth) {
          values.dateOfBirth = values.dateOfBirth.valueOf();
        }
          await infoService.updateProfie(values);
        const token = localStorage.getItem("TOKEN");
        onCurrentUser(token);
        NotificationManager.success(t('lbl_successfully_saved'),null,3000);
        this.props.history.go(-1);
        window.scrollTo(0,0)
      }
    }
    catch(err) {
      console.log(err);
    }
    finally {
      loadingEnd();
    }
  }

  getBase64Avatar =async file => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () =>resolve(reader.result);
      reader.onerror = error => reject(error);
    })
  }

  searchSchool = async (value) => {
    let resultSchool = [];
    const response = await infoService.autoCompleteSchool(value);
    console.log(response.data)
    if(!value) {
      resultSchool = []
    } else {
      if(response.data) {
        (response.data).map(item => {
          resultSchool.push(item)
        })
      }
    }
    this.setState({resultSchool})
  }

  render() {
    const { sizeMobile, resizeWidth, profile, resultSchool, avatarsFileList, avatarImage, previewAvatarImage, previewAvatarVisible} = this.state;
    const { t, userInfo, currentProfile } = this.props;
    const { Title, Text } = Typography;
    const { TextArea } = Input;
    const { Option } = Select;
    const { getFieldDecorator } = this.props.form;

    const uploadButtonAvatar = (
      <div>
        <Icon type='plus' style={{fontSize: 20}}/>
        <div className="ant-upload-text">{t('lbl_upload')}</div>
      </div>
    );

    const childrenSchool = resultSchool ? resultSchool.map(item => <AutoComplete.Option text={item.SchoolName} key={item.SchoolName}>{`${item.SchoolName}, ${item.District}, ${item.Province}, ${item.PostCode}`}</AutoComplete.Option>) : null;
    
    const TelSvg = () => (
      <svg t="1580719062184" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4272" width="48" height="48">
        <path d="M512 1024C229.248 1024 0 794.752 0 512S229.248 0 512 0s512 229.248 512 512-229.248 512-512 512z" fill="#6477F6" p-id="4273"></path>
        <path d="M838.672 712.077c0 10.543-3.47 16.485-10.392 26.183-0.682 0.968-72.562 94.805-112.188 93.555-111.319-3.49-238.668-109.097-328.843-197.107-90.13-87.974-198.34-213.482-201.921-322.607v-0.883c0-38.225 94.886-107.847 95.833-108.509 25.775-17.595 54.163-11.204 65.674 4.86 7.04 9.738 69.93 116.79 78.145 129.893 3.56 5.705 5.312 12.639 5.312 20.242 0 9.759-4.32 23.863-10.008 34.403-5.554 10.306-62.73 64.772-71.649 79.994 9.658 13.443 43.844 58.283 94.76 107.954 50.52 49.365 95.418 82.375 109.257 91.844 15.583-8.669 75.989-66.017 86.447-71.395 19.804-10.204 39.552-11.352 52.889-3.087 12.585 7.767 121.143 69.5 131.457 76.797 9.776 6.897 15.13 19.217 15.13 32.598l0.097 5.265z" fill="#FFFFFF" p-id="4274"></path>
      </svg>
    );

    const FacebookSvg = () => (
      <svg t="1580720510096" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="5271" width="48" height="48">
        <path d="M512 512m-512 0a512 512 0 1 0 1024 0 512 512 0 1 0-1024 0Z" fill="#FFFFFF" p-id="5272"></path>
        <path d="M512 512m-448 0a448 448 0 1 0 896 0 448 448 0 1 0-896 0Z" fill="#4267B2" p-id="5273"></path>
        <path d="M441.6 409.6v-51.2c0-115.2 89.6-204.8 204.8-204.8 19.2 0 32 0 51.2 6.4v115.2c-64 6.4-121.6 57.6-121.6 128v6.4h121.6l-19.2 121.6H582.4V832c0 12.8 0 32 6.4 44.8H441.6V531.2H326.4V409.6h115.2z" fill="#FFFFFF" p-id="5274"></path>
      </svg>
    );

    const LineSvg = () => (
      <svg t="1580721149264" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="7961" width="48" height="48">
        <path d="M517.632 43.52C252.416 43.52 37.376 218.624 37.376 434.688c0 205.824 194.56 374.272 441.856 390.144 0 0 8.704 20.48 2.048 59.904-3.072 20.992-21.504 39.936-19.456 54.784 2.048 13.824 19.968 25.088 29.696 20.48 214.016-100.864 382.976-263.168 390.144-269.824C954.368 622.08 998.4 532.48 998.4 434.688c0-216.064-215.552-391.168-480.768-391.168z m-159.744 498.176c-3.584 3.072-9.216 5.12-16.384 5.12H236.544c-9.728 0-16.384-2.048-20.48-6.144-4.096-4.096-6.144-11.264-6.144-20.48V354.304c0-8.704 2.048-15.36 6.144-19.968 4.096-4.608 9.216-6.656 15.36-6.656 6.656 0 11.776 2.048 15.872 6.656 4.096 4.608 6.144 10.752 6.144 19.968v156.672h88.064c7.168 0 12.288 1.536 16.384 5.12 3.584 3.584 5.632 7.68 5.632 12.8 0 5.632-1.536 9.728-5.632 12.8z m62.464-17.92c0 8.704-2.048 15.36-6.144 19.968-4.096 4.608-9.216 6.656-15.872 6.656-6.144 0-11.264-2.048-15.36-6.656-4.096-4.608-6.144-11.264-6.144-19.968V354.304c0-8.704 2.048-15.36 6.144-19.968 4.096-4.608 9.216-6.656 15.36-6.656 6.656 0 11.776 2.048 15.872 6.656 4.096 4.608 6.144 10.752 6.144 19.968v169.472z m220.672-1.536c0 18.944-7.68 28.16-23.552 28.16-4.096 0-7.68-0.512-10.752-1.536-3.072-1.024-6.144-3.072-8.704-5.12-2.56-2.56-5.12-5.12-7.68-8.704s-4.608-6.656-7.168-10.24L502.272 399.36v126.464c0 8.192-2.048 14.336-5.632 18.432-3.584 4.096-8.704 6.144-14.848 6.144s-11.264-2.048-14.848-6.144c-3.584-4.096-5.632-10.24-5.632-18.432V359.424c0-7.168 1.024-12.8 2.56-16.384 2.048-4.608 5.12-8.192 9.216-10.752s8.704-4.096 13.824-4.096c4.096 0 7.168 0.512 10.24 2.048 2.56 1.024 5.12 3.072 7.168 5.12 2.048 2.048 4.096 5.12 6.656 8.192 2.048 3.584 4.608 7.168 6.656 10.752L601.6 481.28V353.28c0-8.192 1.536-14.336 5.12-18.944 3.584-4.096 8.192-6.144 14.336-6.144 6.144 0 11.264 2.048 14.848 6.144 3.584 4.096 5.632 10.24 5.632 18.944v168.96z m201.728 19.968c-3.584 3.072-8.704 4.608-15.36 4.608h-118.272c-9.728 0-16.384-2.048-20.48-6.144-4.096-4.096-6.144-11.264-6.144-20.48V357.888c0-6.144 1.024-11.264 2.56-15.36 2.048-4.096 4.608-6.656 8.704-8.704 4.096-2.048 9.216-2.56 15.36-2.56H824.32c7.168 0 12.288 1.536 15.36 4.608 3.584 3.072 5.12 7.168 5.12 12.288s-1.536 9.216-5.12 12.288c-3.584 3.072-8.704 4.608-15.36 4.608h-98.304v52.736h90.112c6.656 0 11.776 1.536 14.848 4.608 3.072 3.072 5.12 7.168 5.12 11.776 0 5.12-1.536 8.704-4.608 11.776s-8.192 4.608-14.848 4.608h-90.112v61.44h101.376c6.656 0 11.776 1.536 15.36 4.608s5.12 7.68 5.12 12.8c-0.512 5.12-2.048 9.216-5.632 12.8z" fill="#6FE344" p-id="7962"></path>
      </svg>
    );

    const InstagramSvg = () => (
      <svg t="1580721636316" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="12513" width="48" height="48">
        <path d="M229.518 1024h122.206L158.896 900.414 0.396 807.796C7.318 928.18 107.432 1024 229.518 1024z" fill="#FDE494" p-id="12514"></path>
        <path d="M670.896 953.38l-138.012-35.31H229.518c-68.144 0-123.586-55.442-123.586-123.586v-67.752l-52.966-56.84L0 679.256v115.226c0 4.472 0.144 8.908 0.396 13.314L351.724 1024h417.534l-98.362-70.62z" fill="#FEA150" p-id="12515"></path>
        <path d="M883.31 880.28c-22.488 23.274-53.986 37.788-88.828 37.788H532.884L769.258 1024h25.224c82.404 0 154.792-43.658 195.28-109.054l-54.044-32.188-52.408-2.478z" fill="#FF5D4A" p-id="12516"></path>
        <path d="M918.068 652.028v142.456c0 33.304-13.266 63.55-34.758 85.798l106.454 34.666c21.692-35.04 34.236-76.312 34.236-120.466v-125.61l-54.792-30.782-51.14 13.938z" fill="#E45261" p-id="12517"></path>
        <path d="M1024 298.38l-56.406-13.432-49.526 29.652v337.428L1024 668.872z" fill="#FF4D95" p-id="12518"></path>
        <path d="M1024 229.518c0-115.266-85.416-210.946-196.272-227.1L688.86 60.202l-70.366 45.73h175.988c68.144 0 123.586 55.442 123.586 123.586V314.6L1024 298.38v-68.862z" fill="#CB319C" p-id="12519"></path>
        <path d="M635.464 0L504.1 49.272l-103.61 56.66h218.004L827.728 2.416A230.44 230.44 0 0 0 794.482 0H635.464z" fill="#8A3293" p-id="12520"></path>
        <path d="M512 812.138c36.716 0 71.908-6.64 104.452-18.76l-172.04-78.094-182.356-37.314C315.892 758.78 407.826 812.138 512 812.138z" fill="#FF5D4A" p-id="12521"></path>
        <path d="M658.306 610.716C626.54 657.646 572.81 688.552 512 688.552c-80.546 0-148.616-54.236-169.734-128.092l-47.364-29.602-81.694 9.076a298.268 298.268 0 0 0 48.85 138.034l354.396 115.41c76.606-28.528 138.474-87.514 170.916-162.136l-63.506-32.17-65.558 11.644z" fill="#E45261" p-id="12522"></path>
        <path d="M335.448 512c0-43.756 16.036-83.814 42.494-114.692l-75.316-10.536-76.5 33.784A298.956 298.956 0 0 0 211.862 512c0 9.424 0.488 18.73 1.342 27.932l129.06 20.524A175.948 175.948 0 0 1 335.448 512z m476.69 0c0-64.276-20.318-123.892-54.856-172.78l-74.794-10.616-76.836 33.834c49.746 31.262 82.9 86.596 82.9 149.562 0 36.54-11.16 70.522-30.246 98.716l129.062 20.524A298.324 298.324 0 0 0 812.138 512z" fill="#FF4D95" p-id="12523"></path>
        <path d="M512 335.448c34.388 0 66.484 9.918 93.652 26.99l151.63-23.218C702.89 262.234 613.222 211.862 512 211.862c-133.62 0-247.108 87.778-285.874 208.69l151.816-23.248c32.404-37.82 80.464-61.856 134.058-61.856z m282.482-35.31c38.94 0 70.62-31.68 70.62-70.62s-31.68-70.62-70.62-70.62c-38.94 0-70.62 31.68-70.62 70.62s31.68 70.62 70.62 70.62z" fill="#CB319C" p-id="12524"></path>
        <path d="M105.932 627.128L54.94 601.694 0 592.632v86.626l105.932 47.474z" fill="#FF5D4A" p-id="12525"></path>
        <path d="M0 506.028v86.604l105.932 34.496V522.874l-55.04-19.788z" fill="#E45261" p-id="12526"></path>
        <path d="M105.932 438.958l-54.434-0.492L0 455.18v50.848l105.932 16.846z" fill="#FF4D95" p-id="12527"></path>
        <path d="M105.932 359.514l-56.11 5.692L0 411.924v43.256l105.932-16.222z" fill="#CB319C" p-id="12528"></path>
        <path d="M0 411.924l105.932-52.41v-120.79L43.8 244.666 0 286.482z" fill="#8A3293" p-id="12529"></path>
        <path d="M410.118 0L168.412 92.962 2.774 193.856A230.44 230.44 0 0 0 0 229.518v56.964l105.932-47.756v-9.21c0-68.144 55.442-123.586 123.586-123.586h170.974L635.464 0H410.118z" fill="#523494" p-id="12530"></path>
        <path d="M229.518 0C115.09 0 19.956 84.176 2.774 193.856L410.118 0h-180.6z" fill="#2D2D87" p-id="12531"></path>
      </svg>
    );

    const TelIcon = props => <Icon component={TelSvg} {...props} />;
    const FacebookIcon = props => <Icon component={FacebookSvg} {...props} />;
    const LineIcon = props => <Icon component={LineSvg} {...props} />;
    const InstagramIcon = props => <Icon component={InstagramSvg} {...props} />;

    return (
      <Row style={{padding: resizeWidth < sizeMobile ? '0 12px' : '0 24px'}} className="py-3">
        <Card title={null} bordered={false} className="mb-4 shadow-sm position-relative" >
          <Row gutter={24}>
            <Col sm={{span: 24}} md={{span: 6}} className="px-0">
              <Menu
                mode="inline"
                defaultSelectedKeys={[this.props.currentProfile]}
                onClick={e => this.props.setCurrentProfile(e.key)}
              >
                <Menu.Item key={1}>{t('lbl_general_info')}</Menu.Item>
                <Menu.Item key={2}>{t('lbl_contact_info')}</Menu.Item>
                <Menu.Item key={3}>{t('lbl_family_info')}</Menu.Item>
              </Menu>
            </Col>
            <Col sm={{span: 24}} md={{span: 18}}>
              <Form onSubmit={this.handleSubmit} className="my-3" hideRequiredMark={true} labelCol={{ span: 24 }} wrapperCol={{ span: 24 }}>
                { currentProfile === '1' && (
                <div>
                  <Title level={4} className="mb-4">{t('lbl_general_info')}</Title>
                  <Col md={{span: 24}} lg={{span: 12}} className="text-center" style={{padding: 20, float: resizeWidth < 768 ? 'none': 'right'}}>
                    <div className="mb-3">
                      <Avatar src={avatarImage ? avatarImage : userInfo.photoUrl} name={userInfo.firstname+' '+userInfo.lastname} round={true} size={144} />
                    </div>
                    <ImgCrop modalTitle={t('lbl_edit_image')} modalWidth={resizeWidth <= sizeMobile ? 300 : 520} width={375} height={375} beforeCrop={(file) => {
                      const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/jpg' || file.type === 'image/png';
                      if(!isJpgOrPng) {
                        NotificationManager.error(t("jpg/png-only"), null ,3000);
                      }
                      return isJpgOrPng
                    }}>
                      <Upload
                        name="avatars"
                        fileList={avatarsFileList}
                        showUploadList={{ showDownloadIcon: false, showPreviewIcon: false }}
                        action={() => {}}
                        onChange={async ({file}) => {
                          let fileList = [];
                          let image = null;
                          if (file.status !== 'removed') {
                            fileList = [{ ...file, status: 'done' }]
                            image = await this.getBase64Avatar(file.originFileObj);
                          }
                          this.setState({
                            avatarsFileList: fileList,
                            avatarImage: image
                          });
                        }}
                      >
                        <Button className="d-flex align-items-center">
                          <Icon type="upload" /> {t('lbl_upload_image')}
                        </Button>
                      </Upload>
                    </ImgCrop>
                  </Col>
                  <Col md={{span: 24}} lg={{span: 12}} className="px-0">
                    <Col span={24} >
                      <Form.Item label={t('lbl_email_address')}>
                        {getFieldDecorator('email', {
                          initialValue: userInfo && userInfo.email,
                          rules: [{ required: true, message: t("required", {field: t('lbl_email_address')}) }],
                        })(
                          <Input
                            placeholder={t('lbl_email_address')}
                            allowClear
                            disabled
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col span={24} >
                      <Form.Item label={t('lbl_firstname')}>
                        {getFieldDecorator('firstname', {
                          initialValue: userInfo && userInfo.firstname,
                          rules: [{ required: true, message: t("required", {field: t('lbl_firstname')}) }],
                        })(
                          <Input
                            placeholder={t('lbl_firstname')}
                            allowClear
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col span={24} >
                      <Form.Item  label={t('lbl_lastname')}>
                        {getFieldDecorator('lastname', {
                          initialValue: userInfo && userInfo.lastname,
                          rules: [{ required: true, message: t("required", {field: t('lbl_lastname')}) }],
                        })(
                          <Input
                            placeholder={t('lbl_lastname')}
                            allowClear
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Col>
                  <Row gutter={24} className="px-2" style={{clear: 'both'}}> 
                    <Col md={{span: 24}} lg={{span: 12}} >
                      <Form.Item label={t('lbl_nickname')}>
                        {getFieldDecorator('nickname', {
                          initialValue: profile ? profile.nickname: '',
                          rules: [{ required: true, message: t("required", {field: t('lbl_nickname')}) }],
                        })(
                          <Input
                            placeholder={t('lbl_nickname')}
                            allowClear
                          />,
                        )}
                      </Form.Item>
                    </Col> 
                    <Col md={{span: 24}} lg={{span: 12}} >
                      <Form.Item label={t('lbl_identification_no')}>
                        {getFieldDecorator('identificationNo', {
                          initialValue: profile ? profile.identificationNo: '',
                          rules: [{ required: true, message: t("required", {field: t('lbl_identification_no')}) }],
                        })(
                          <Input
                            placeholder={t('lbl_identification_no')}
                            allowClear
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col md={{span: 24}} lg={{span: 12}} >
                      <Form.Item label={t('lbl_date_of_birth')}>
                        {getFieldDecorator('dateOfBirth', {
                          initialValue: profile ? moment(new Date(new Date(profile.dateOfBirth).toUTCString()).toLocaleString().split(',')[0], 'M/DD/YYYY'): '',
                          rules: [{ required: true, message: t("required_select", {field: t('lbl_date_of_birth')}) }],
                        })(
                          <DatePicker className="w-100" allowClear placeholder={t('lbl_select_date_of_birth')} format="DD MMMM YYYY" />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col md={{span: 24}} lg={{span: 12}} >
                      <Form.Item label={t('lbl_address')}>
                        {getFieldDecorator('address', {
                          initialValue: profile ? profile.address: '',
                          rules: [{ required: true, message: t("required", {field: t('lbl_address')}) }],
                        })(
                          <TextArea
                            placeholder={t('lbl_address_phd')}
                            autoSize={{ minRows: 2, maxRows: 6 }}
                            allowClear
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col md={{span: 24}} lg={{span: 12}} >
                      <Form.Item className=" thailand-address" label={t('lbl_subdistrict')}>
                        {getFieldDecorator('subdistrict', {
                          initialValue: profile ? profile.subdistrict: '',
                          rules: [{ required: true, message: t("required", {field: t('lbl_subdistrict')}) }],
                          onChange: this.changeAddress,
                        })(
                          <InputAddress
                            address="subdistrict"
                            onSelect={this.selectAddress}
                            placeholder={t('lbl_subdistrict')}
                            style={{width: '100%'}}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col md={{span: 24}} lg={{span: 12}} >
                      <Form.Item className=" thailand-address" label={t('lbl_district')}>
                        {getFieldDecorator('district', {
                          initialValue: profile ? profile.district: '',
                          rules: [{ required: true, message: t("required", {field: t('lbl_district')}) }],
                          onChange: () => this.changeAddress
                        })(
                          <InputAddress
                            address="district"
                            onSelect={this.selectAddress}
                            placeholder={t('lbl_district')}
                            style={{width: '100%'}}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col md={{span: 24}} lg={{span: 12}} >
                      <Form.Item className="thailand-address" label={t('lbl_province')}>
                        {getFieldDecorator('province', {
                          initialValue: profile ? profile.province: '',
                          rules: [{ required: true, message: t("required", {field: t('lbl_province')}) }],
                          onChange: () => this.changeAddress
                        })(
                          <InputAddress
                            address="province"
                            onSelect={this.selectAddress}
                            placeholder={t('lbl_province')}
                            style={{width: '100%'}}
                            // allowClear
                            // disabled
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col md={{span: 24}} lg={{span: 12}} > 
                      <Form.Item className="thailand-address" label={t('lbl_zipcode')}>
                        {getFieldDecorator('zipcode', {
                          initialValue: profile ? profile.zipcode: '',
                          rules: [{ required: true, message: t("required", {field: t('lbl_zipcode')}) }],
                          onChange: () => this.changeAddress
                        })(
                          <InputAddress
                            address="zipcode"
                            onSelect={this.selectAddress}
                            placeholder={t('lbl_zipcode')}
                            style={{width: '100%'}}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col md={{span: 24}} lg={{span: 12}} >
                      <Form.Item label={t('lbl_nationality')}>
                        {getFieldDecorator('nationality', {
                          initialValue: profile ? profile.nationality: '',
                          rules: [{ required: true, message: t("required", {field: t('lbl_nationality')}) }],
                        })(
                          <Input
                            placeholder={t('lbl_nationality')}
                            allowClear
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col md={{span: 24}} lg={{span: 12}} >
                      <Form.Item label={t('lbl_race')}>
                        {getFieldDecorator('race', {
                          initialValue: profile ? profile.race: '',
                          rules: [{ required: true, message: t("required", {field: t('lbl_race')}) }],
                        })(
                          <Input
                            placeholder={t('lbl_race')}
                            allowClear
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col md={{span: 24}} lg={{span: 12}} >
                      <Form.Item label={t('lbl_religion')}>
                        {getFieldDecorator('religion', {
                          initialValue: profile ? profile.religion: '',
                          rules: [{ required: true, message: t("required", {field: t('lbl_religion')}) }],
                        })(
                          <Input
                            placeholder={t('lbl_religion')}
                            allowClear
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col md={{span: 24}} lg={{span: 12}} >
                      <Form.Item label={t('lbl_blood_type')}>
                        {getFieldDecorator('bloodType', {
                          initialValue: profile ? profile.bloodType: '',
                          rules: [{ required: true, message: t("required_select", {field: t('lbl_blood_type')}) }],
                        })(
                          <Select
                            placeholder={t('lbl_blood_type')}
                            onChange={(val) => {
                              this.props.form.setFieldsValue({
                                bloodType: val
                              })
                            }}
                            allowClear
                          >
                            <Option value="A">A</Option>
                            <Option value="B">B</Option>
                            <Option value="O">O</Option>
                            <Option value="AB">AB</Option>
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col md={{span: 24}} lg={{span: 12}} >
                      <Form.Item label={t('lbl_weight')}>
                        {getFieldDecorator('weight', {
                          initialValue: profile ? profile.weight: '',
                          rules: [{ required: true, message: t("required", {field: t('lbl_weight')}) }],
                        })(
                          <Input
                            placeholder={t('lbl_weight')}
                            allowClear
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col md={{span: 24}} lg={{span: 12}} >
                      <Form.Item label={t('lbl_height')}>
                        {getFieldDecorator('height', {
                          initialValue: profile ? profile.height: '',
                          rules: [{ required: true, message: t("required", {field: t('lbl_height')}) }],
                        })(
                          <Input
                            placeholder={t('lbl_height')}
                            allowClear
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col md={{span: 24}} lg={{span: 12}} >
                      <Form.Item label={t('lbl_school')}>
                        {getFieldDecorator('school', {
                          initialValue: profile ? profile.school: '',
                          rules: [{ required: true, message: t("required_select", {field: t('lbl_school')}) }],
                        })(
                          <AutoComplete className="w-100" backfill={true} allowClear optionLabelProp="text" onSelect={this.selectSchool} onSearch={this.searchSchool} placeholder={t('lbl_school')}>
                            {childrenSchool}
                          </AutoComplete>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col md={{ span: 24 }} lg={{ span: 12 }} >
                      <Form.Item label={t('lbl_curriculum')}>
                        {getFieldDecorator('curriculum', {
                          initialValue: profile ? profile.curriculum: '',
                          rules: [{ required: true, message: t("required", {field: t('lbl_curriculum')}) }],
                        })(
                          <Input
                            placeholder={t('lbl_curriculum')}
                            allowClear
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </div>
                )}
                { currentProfile === '2' && (
                  <div>
                    <Title level={4} className="mb-4">{t('lbl_contact_info')}</Title>
                    <Row gutter={24} className="px-3">
                      <Col md={{span: 24}} lg={{span: 12}} className="mb-3">
                        <Row gutter={24} className="d-flex align-items-start">
                          <Col className="float-left">
                            <TelSvg />
                          </Col>
                          <Col className="d-inline-block">
                            <Form.Item label={t('lbl_phone_no')} style={{display: 'inline-grid'}}>
                              {getFieldDecorator('phoneNo', {
                                initialValue: profile ? profile.phoneNo: '',
                                rules: [{ required: true, message: t("required", {field: t('lbl_phone_no')}) }],
                              })(
                                <Input
                                  placeholder={t('lbl_phone_no')}
                                  allowClear
                                />,
                              )}
                            </Form.Item>
                          </Col>
                        </Row>
                      </Col>
                      <Col md={{span: 24}} lg={{span: 12}} className="mb-3">
                        <Row gutter={24} className="d-flex align-items-start">
                          <Col className="float-left text-left">
                            <FacebookSvg />
                          </Col>
                          <Col className="d-inline-block">
                            <Form.Item label={t('lbl_facebook')} style={{display: 'inline-grid'}}>
                              {getFieldDecorator('facebook', {
                                initialValue: profile ? profile.facebook: '',
                              })(
                                <Input
                                  placeholder={t('lbl_facebook')}
                                  allowClear
                                />,
                              )}
                            </Form.Item>
                          </Col>
                        </Row>
                      </Col>
                      <Col md={{span: 24}} lg={{span: 12}} className="mb-3">
                        <Row gutter={24} className="d-flex align-items-start">
                          <Col className="float-left text-left">
                            <LineIcon />
                          </Col>
                          <Col className="d-inline-block">
                            <Form.Item label={t('lbl_line')} style={{display: 'inline-grid'}}>
                              {getFieldDecorator('line', {
                                initialValue: profile ? profile.line: '',
                              })(
                                <Input
                                  placeholder={t('lbl_line')}
                                  allowClear
                                />,
                              )}
                            </Form.Item>
                          </Col>
                        </Row>
                      </Col>
                      <Col md={{span: 24}} lg={{span: 12}} className="mb-3">
                        <Row gutter={24} className="d-flex align-items-start">
                          <Col className="float-left text-left">
                            <InstagramIcon />
                          </Col>
                          <Col className="d-inline-block">
                            <Form.Item label={t('lbl_instagram')} style={{display: 'inline-grid'}}>
                              {getFieldDecorator('instagram', {
                                initialValue: profile ? profile.instagram: '',
                              })(
                                <Input
                                  placeholder={t('lbl_instagram')}
                                  allowClear
                                />,
                              )}
                            </Form.Item>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </div>
                )}
                { currentProfile === '3' && (
                  <div>
                    <Title level={4} className="mb-4">{t('lbl_family_info')}</Title>
                    <Row gutter={24} className="mt-3 px-3">
                      <Col md={{ span: 24 }} lg={{ span: 12 }} >
                        <Form.Item label={t('lbl_father_name')}>
                          {getFieldDecorator('fatherName', {
                            initialValue: profile ? profile.fatherName: '',
                            rules: [{ required: true, message: t("required", {field: t('lbl_father_name')}) }],
                          })(
                            <Input
                              placeholder={t('lbl_father_name')}
                              allowClear
                            />,
                          )}
                        </Form.Item>
                      </Col>
                      <Col md={{ span: 24 }} lg={{ span: 12 }} >
                        <Form.Item label={t('lbl_father_occupation')}>
                          {getFieldDecorator('fatherOccupation', {
                            initialValue: profile ? profile.fatherOccupation: '',
                            rules: [{ required: true, message: t("required", {field: t('lbl_father_occupation')}) }],
                          })(
                            <Input
                              placeholder={t('lbl_father_occupation')}
                              allowClear
                            />,
                          )}
                        </Form.Item>
                      </Col>
                      <Col md={{ span: 24 }} lg={{ span: 12 }} >
                        <Form.Item label={t('lbl_father_phone_no')}>
                          {getFieldDecorator('fatherPhoneNo', {
                            initialValue: profile ? profile.fatherPhoneNo: '',
                            rules: [{ required: true, message: t("required", {field: t('lbl_father_phone_no')}) }],
                          })(
                            <Input
                              placeholder={t('lbl_father_phone_no')}
                              allowClear
                            />,
                          )}
                        </Form.Item>
                      </Col>
                      <Col md={{ span: 24 }} lg={{ span: 12 }} >
                        <Form.Item label={t('lbl_father_age')}>
                          <Row gutter={8}>
                            <Col span={12}>
                              {getFieldDecorator('fatherAge', {
                                initialValue: profile ? profile.fatherAge: '',
                                rules: [{ required: true, message: t("required", {field: t('lbl_father_age')}) }],
                              })(
                                <Input
                                  placeholder={t('lbl_father_age')}
                                  allowClear
                                />,
                              )}
                            </Col>
                            <Col span={9}>
                              <Text className="px-3">{t('lbl_year')}</Text>
                            </Col>
                          </Row>
                        </Form.Item>
                      </Col>
                    </Row>
                    <Divider type="horizontal" className="my-2" dashed/>
                    <Row gutter={24} className="px-3">
                      <Col md={{ span: 24 }} lg={{ span: 12 }} >
                        <Form.Item label={t('lbl_mother_name')}>
                          {getFieldDecorator('motherName', {
                            initialValue: profile ? profile.motherName: '',
                            rules: [{ required: true, message: t("required", {field: t('lbl_mother_name')}) }],
                          })(
                            <Input
                              placeholder={t('lbl_mother_name')}
                              allowClear
                            />,
                          )}
                        </Form.Item>
                      </Col>
                      <Col md={{ span: 24 }} lg={{ span: 12 }} >
                        <Form.Item label={t('lbl_mother_occupation')}>
                          {getFieldDecorator('motherOccupation', {
                            initialValue: profile ? profile.motherOccupation: '',
                            rules: [{ required: true, message: t("required", {field: t('lbl_mother_occupation')}) }],
                          })(
                            <Input
                              placeholder={t('lbl_mother_occupation')}
                              allowClear
                            />,
                          )}
                        </Form.Item>
                      </Col>
                      <Col md={{ span: 24 }} lg={{ span: 12 }} >
                        <Form.Item label={t('lbl_mother_phone_no')}>
                          {getFieldDecorator('motherPhoneNo', {
                            initialValue: profile ? profile.motherPhoneNo: '',
                            rules: [{ required: true, message: t("required", {field: t('lbl_mother_phone_no')}) }],
                          })(
                            <Input
                              placeholder={t('lbl_mother_phone_no')}
                              allowClear
                            />,
                          )}
                        </Form.Item>
                      </Col>
                      <Col md={{ span: 24 }} lg={{ span: 12 }} >
                        <Form.Item label={t('lbl_mother_age')}>
                          <Row gutter={8}>
                            <Col span={12}>
                              {getFieldDecorator('motherAge', {
                                initialValue: profile ? profile.motherAge: '',
                                rules: [{ required: true, message: t("required", {field: t('lbl_mother_age')}) }],
                              })(
                                <Input
                                  placeholder={t('lbl_mother_age')}
                                  allowClear
                                />,
                              )}
                            </Col>
                            <Col span={9}>
                              <Text className="px-3">{t('lbl_year')}</Text>
                            </Col>
                          </Row>
                        </Form.Item>
                      </Col>
                    </Row>
                  </div>
                )}
                <Row gutter={24} className="px-2">
                  <Col sm={{ span: 24 }} md={{ span: 24 }} > 
                    <Form.Item className="text-sm-center text-md-left">
                      <Button htmlType="submit" type="primary" className="mt-3 mr-2" >
                        {t('lbl_save')}
                      </Button>
                      <Button type="basic" className="mt-3" onClick={() => this.props.history.go(-1)}>
                        {t('lbl_cancel')}
                      </Button>
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Col>
          </Row>
        </Card>
      </Row>  
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userInfo: state.auth.user,
    currentProfile: state.currentProfile.key
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onCurrentUser: (token) => dispatch(currentUser(token)),
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
    loadingStart: (text) => dispatch(loadingStart(text)),
    loadingEnd: () => dispatch(loadingEnd()),
    setCurrentProfile: (key) => dispatch(setCurrentProfile(key))
  }
}

export default Form.create({ name: 'profile' })(withTranslation()(connect(mapStateToProps, mapDispatchToProps)(EditProfile)))
