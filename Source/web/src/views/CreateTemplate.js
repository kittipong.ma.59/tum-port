import React, { Component } from 'react'
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import SwipeableViews from 'react-swipeable-views';
import i18n from 'i18next';
import * as jsPDF from 'jspdf'
import * as jsZip from 'jszip'
import { Base64 } from 'js-base64'
import fs from 'fs';
import { saveAs } from 'file-saver';
import print from 'print-js';
import useImage from 'use-image';

import portServices from '../services/port'
import storageServices from '../services/storage';
import googleServices from '../services/google';

// import store
import { connect } from 'react-redux'
import { loadingStart, loadingEnd } from '../store/actions/all'
import { logOut } from '../store/actions/auth';

// Import Scss
import '../scss/design.scss'
import 'react-dropdown-modal/lib/dropdown.css';

// Import UI
import {
    Affix,
    Button,
    Divider,
    Drawer,
    Dropdown,
    Layout,
    List,
    message,
    Menu,
    Modal,
    Icon,
    Input,
    PageHeader,
    Select,
    Tooltip,
    Typography,
    Row,
    Col,
    Tabs,
    Collapse,
    Upload,
    Slider,
    Empty,
    InputNumber
} from 'antd';


import Avatar from 'react-avatar';
import ImgCrop from 'antd-img-crop';
import reactcss from 'reactcss';
import { SketchPicker, CirclePicker } from 'react-color';
import tinycolor from 'tinycolor2';
import { NotificationManager } from 'react-notifications';
import GoogleLogin from 'react-google-login';
import { Scrollbars } from 'react-custom-scrollbars';
import DropdownModal from 'react-dropdown-modal';
import IconButton from '@material/react-icon-button';
import '@material/react-icon-button/dist/icon-button.css';
import cloneDeep from 'clone-deep';

// Import Canvas
import Konva from 'konva';
import { Stage, Layer, Rect, Circle, Line, Text as Font, Path, Ellipse, Star, Ring, RegularPolygon, Label } from 'react-konva';
import URLImage from '../components/URLImage';
import Transformer from '../components/Transformer';

const { Header, Sider, Content } = Layout;


// Component Header Function

let history = [];
let historyStep = 0;


class CreateTemplate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            template: null,
            theme: null,
            accessToken: null,
            getwidth: window.innerWidth,
            language: i18n.language,
            lastedWidth: window.innerWidth,
            resizeWidth: window.innerWidth,
            width: window.innerWidth,
            height: window.innerHeight,
            paperWidth: 793.701,
            paperHeight: 1122.52,
            scale: 1,
            percentScale: 100,
            sizeMobile: false,
            page: 0,
            isDrawerOpenOption: false,
            accountVisible: false,
            isModalChangeLanguage: false,
            isModalGoogleDrive: false,
            isModalFileDownload: false,
            collapsed: true,
            selectedKey: "",
            visible: false,
            font: ["Arial", "Kanit", "Mali", "Russo One", "Prompt", "Athiti", "Bai Jamjuree", "Chakra Petch", "Charm", "Charmonman", "Chonburi", "Fahkwang", "Itim", "K2D", "KoHo", "Kodchasan", "Krub", "Maitree", "Mitr", "Niramit", "Pattaya", "Pridi", "Sarabun", "Sriracha", "Srisakdi", "Taviraj", "Thasadith", "Trirong"],
            displayColorPicker: false,
            displayFillPicker: false,
            displayStrokePicker: false,
            displayShadowPicker: false,
            color: "#ffffff",
            FileList: [],
            selectedShapeName: '',
            selectedPage: -1,
            selectedElementIndex: -1,
            selectedType: '',
            fileName: '',
            fileType: 'PDF',
            tabEditFontKey: "1",
            headerFont: 'Arial',
            subheaderFont: 'Arial',
            titleFont: 'Arial',
            subtileFont: 'Arial',
            bodyFont: 'Arial',
            headerSize: 8,
            subheaderSize: 8,
            titleSize: 8,
            subtileSize: 8,
            bodySize: 8,
            isHeaderSize: false,
            isSubheaderSize: false,
            isTitleSize: false,
            isSubtileSize: false,
            isBodySize: false,
            textEdit: {
                visible: false,
                textX: 0,
                fill: "black",
                textY: 0,
                textValue: "hello",
                fontSize: 100,
                width: 400,
                height: 200,
                fontStyle: "normal",
                fontFamily: 'Arial',
                align: "left",
                id: 0
            },
            ContextMenu: {
                visible: false,
                x: 0,
                y: 0
            },
            selectKeyVisible: false,school: String,
            key: ['image','title','description','education1','education2','education3','education4', 'activities2', 'activities3', 'certificate2', 'certificate3', 'photoUrl', 'displayName', 'birth', 'address','nickname','identificationNo','nationality','race','religion','bloodType','weight','height','school','curriculum', 'educationLevel', 'gpax', 'startYear', 'endYear','phoneNo','facebook','line','instagram','fatherName','fatherOccupation','fatherPhoneNo','fatherAge','motherName','motherOccupation','motherPhoneNo','motherAge']
        };
    }

    fitSizePaper = () => {
        const { paperWidth, width, height, paperHeight, sizeMobile } = this.state;
        let zoomSize = 0;
        if (width <= paperWidth) {
            zoomSize = Math.ceil((paperWidth - (paperWidth - (width - 25))) * 100 / paperWidth) | 0
            this.setState({ sizeMobile: true })
        } else {
            zoomSize = Math.ceil((paperHeight - (paperHeight - (height - 150))) * 100 / paperHeight) | 0
        }
        this.setState({
            percentScale: 100,
            scale: 1
        })
    }

    componentDidMount = async () => {
        const { loadingStart, loadingEnd, currentUserDriveGoogle } = this.props;
        loadingStart(null);
        this.fitSizePaper()
        window.addEventListener('resize', this.handleResize);
        window.addEventListener('scroll', this.handleScroll);
        try {

            const id = this.props.match.params.id;
            const theme = await portServices.getAllTheme();
            if (theme.data) {
                this.setState({ theme: theme.data })
            }
            const respone = await portServices.findTemplateById(id);
            if (respone.data) {
                let template = respone.data;
                for (let i = 0; i < template.elements.length; i++) {
                    for (let j = 0; j < template.elements[i].length; j++) {
                        if (template.elements[i][j].key == 'photoUrl') {
                            template.elements[i][j].src = require('../assets/images/avatar.png')
                        }
                        if (template.elements[i][j].key == 'imageActivities') {
                            template.elements[i][j].src = require('../assets/images/emtry-image-4-3.png')
                        }
                        if (template.elements[i][j].key == 'imageCertificate') {
                            template.elements[i][j].src = require('../assets/images/emtry-image-10-7.png')
                        }
                        if (template.elements[i][j].fillType) {
                            template.elements[i][j].fill = template.theme[template.elements[i][j].fillType]
                        }

                        if (template.elements[i][j].strokeType) {
                            template.elements[i][j].stroke = template.theme[template.elements[i][j].strokeType]
                        }
                    }
                    if (i == 0) {
                        template.elements[i].forEach(item => {
                            if (item.fontType == 'header' && !this.state.isHeaderSize) {
                                this.setState({ isHeaderSize: true, headerSize: item.fontSize, headerFont: item.fontFamily ? item.fontFamily : 'Arial' })
                            } else if (item.fontType == 'subheader' && !this.state.isSubheaderSize) {
                                this.setState({ isSubheaderSize: true, subheaderSize: item.fontSize, subheaderFont: item.fontFamily ? item.fontFamily : 'Arial' })
                            } else if (item.fontType == 'title' && !this.state.isTitleSize) {
                                this.setState({ isTitleSize: true, titleSize: item.fontSize, titleFont: item.fontFamily ? item.fontFamily : 'Arial' })
                            } else if (item.fontType == 'subtitle' && !this.state.isSubtileSize) {
                                this.setState({ isSubtileSize: true, subtileSize: item.fontSize, subtileFont: item.fontFamily ? item.fontFamily : 'Arial' })
                            } else if (item.fontType == 'body' && !this.state.isBodySize) {
                                this.setState({ isBodySize: true, bodySize: item.fontSize, bodyFont: item.fontFamily ? item.fontFamily : 'Arial' })
                            }
                        });
                    }
                }
                history = [template];
                this.setState({ template: history[0], fileName: template.name })
            } else {
                this.props.history.go(-(this.props.history.length));
                this.props.history.replace('/')
            }
        } catch (err) {
            console.log(err)
        } finally {
            loadingEnd();
        }
    }

    async componentDidUpdate(prevProps, prevState) {
        const { template, page } = this.state;
        if (prevState.page != page) {
            await this.setState({
                isHeaderSize: false,
                isSubheaderSize: false,
                isTitleSize: false,
                isSubtileSize: false,
                isBodySize: false,
            })
            for (let j = 0; j < template.elements[page].length; j++) {
                if (template.elements[page][j].type === 'bg') {
                    this.setState({ color: template.elements[page][j].fill })
                }
            }
            template.elements[page].forEach(item => {
                if (item.fontType == 'header' && !this.state.isHeaderSize) {
                    this.setState({ isHeaderSize: true, headerSize: item.fontSize, headerFont: item.fontFamily ? item.fontFamily : 'Arial' })
                } else if (item.fontType == 'subheader' && !this.state.isSubheaderSize) {
                    this.setState({ isSubheaderSize: true, subheaderSize: item.fontSize, subheaderFont: item.fontFamily ? item.fontFamily : 'Arial' })
                } else if (item.fontType == 'title' && !this.state.isTitleSize) {
                    this.setState({ isTitleSize: true, titleSize: item.fontSize, titleFont: item.fontFamily ? item.fontFamily : 'Arial' })
                } else if (item.fontType == 'subtitle' && !this.state.isSubtileSize) {
                    this.setState({ isSubtileSize: true, subtileSize: item.fontSize, subtileFont: item.fontFamily ? item.fontFamily : 'Arial' })
                } else if (item.fontType == 'body' && !this.state.isBodySize) {
                    this.setState({ isBodySize: true, bodySize: item.fontSize, bodyFont: item.fontFamily ? item.fontFamily : 'Arial' })
                }
            });
        }
    }

    handleResize = (e) => {
        const { width, height, lastedWidth } = this.state;
        if (lastedWidth >= 576 && width < 576) {
            window.location.reload();
        } else if (lastedWidth < 576 && width >= 576) {
            window.location.reload();
        }
        this.setState({
            resizeWidth: window.innerWidth,
            width: window.innerWidth,
            height: window.innerHeight
        })
    }

    handleScroll = (e) => {
        const { sizeMobile, paperHeight, scale, height } = this.state;
        const page = Math.floor((window.scrollY + 150) / (paperHeight * scale + 50));
        let scalePage = 0;
        if (page === 0) {
            scalePage = (window.scrollY + 150)
        } else {
            scalePage = ((((window.scrollY) / page) - (paperHeight * scale + 50)) * page) + 150
        }
        if (sizeMobile === false) {
            this.setState({
                page: page
            });
            if ((((paperHeight * scale + 50) - scalePage)) < height) {
                if ((((paperHeight * scale + 50) - scalePage)) + 225 < (height / 2)) {
                    this.setState({
                        page: page + 1
                    });
                }
            }

        }

    }

    handleUndo = () => {
        if (historyStep === 0) {
            return;
        }
        historyStep -= 1;
        const previous = history[historyStep];
        this.setState({
            template: previous
        });
    };

    handleRedo = () => {
        if (historyStep === history.length - 1) {
            return;
        }
        historyStep += 1;
        const next = history[historyStep];
        this.setState({
            template: next
        });
    };

    handleSaveHistory = (data) => {
        history = history.slice(0, historyStep + 1);
        history = history.concat([data]);
        historyStep += 1;
    }

    changeScalePaper = (scaleNew) => {
        const { width, paperWidth } = this.state;
        this.setState({
            scale: scaleNew,
            percentScale: scaleNew * 100
        })
    }

    changeLanguage = (lng) => {
        i18n.changeLanguage(lng);
        this.setState({
            language: lng,
            isModalChangeLanguage: false
        });
    }

    handleChangePage = page => {
        this.setState({
            page,
        })
    }

    handleCtrlWheel = (e) => {
        e.preventDefault()
        if (e.ctrlKey) {
            const { scale } = this.state;
            const scaleBy = 0.01;
            const scaleNew = e.deltaY < 0 ? scale + scaleBy : scale - scaleBy;
            if (scaleNew >= 0.25 && scaleNew <= 1) {
                this.setState({
                    scale: scaleNew,
                    percentScale: Math.ceil(scaleNew * 100) | 0
                })
            }
        }
    }

    setProjectName = e => {
        const { template } = this.state;
        template.name = e.target.value;
        this.setState({
            template,
            fileName: e.target.value
        })
    }

    setName = async () => {
        const { template } = this.state;
        const res = await portServices.setNamePortfolio(template._id, template.name)
        console.log(res.data)
    }
    downloadDocument = async (fileType) => {
        const { loadingStart, loadingEnd, t } = this.props;
        const { template } = this.state;
        this.setState({ isModalFileDownload: false })
        loadingStart(t('lbl_downloading'));
        await this.setState({ selectedShapeName: '', isDrawerOpenOption: false })
        const pdf = new jsPDF(new jsPDF('p', 'mm', [297, 210]));
        const zip = new jsZip();
        var w = pdf.internal.pageSize.getWidth();
        var h = pdf.internal.pageSize.getHeight();
        for (let i = 0; i < template.elements.length; i++) {
            const imgData = this.refs['paperPage' + i].getStage().toDataURL()
            if (fileType === 'PDF') {
                if (i != 0) pdf.addPage("a4", "portrait")
                pdf.addImage(imgData, 'PNG', 0, 0, w, h);
            }
            else if (fileType == 'PNG') {
                await zip.file(template.name + (i + 1) + ".png", imgData.split('base64,')[1], { base64: true });
            } else if (fileType == 'JPG') {
                await zip.file(template.name + (i + 1) + ".jpg", imgData.split('base64,')[1], { base64: true });
            }
        }
        if (fileType === 'PDF') {
            await pdf.save(template.name + ".pdf", { returnPromise: true });
        } else {
            const content = await zip.generateAsync({ type: 'blob' })
            saveAs(content, template.name + '.zip');
        }
        loadingEnd();
    }
    printDocument = async () => {
        const { project_name, template } = this.state;
        const { loadingStart, loadingEnd, t } = this.props;
        await this.setState({ selectedShapeName: '', isDrawerOpenOption: false })
        loadingStart(null);
        const doc = new jsPDF(new jsPDF('p', 'mm', [297, 210]));
        var w = doc.internal.pageSize.getWidth();
        var h = doc.internal.pageSize.getHeight();
        for (let i = 0; i < template.elements.length; i++) {
            const imgData = this.refs['paperPage' + i].getStage().toDataURL()
            if (i != 0) doc.addPage("a4", "portrait")
            doc.addImage(imgData, 'PNG', 0, 0, w, h);
        }
        print(doc.output('bloburl'))
        loadingEnd()
    }

    handleLogOut = () => {
        const { onLogOut } = this.props;
        onLogOut();
    }

    handleSave = async () => {
        const { project_name, template } = this.state;
        const { loadingStart, loadingEnd, t } = this.props;
        loadingStart(t('lbl_saving'))
        try {
            await this.setState({ selectedShapeName: '', isDrawerOpenOption: false })
            const pdf = new jsPDF(new jsPDF('p', 'mm', [297, 210]));
            var w = pdf.internal.pageSize.getWidth();
            var h = pdf.internal.pageSize.getHeight();
            let images = [];
            template.image.forEach(async (item, index) => {
                await storageServices.removeFile(item);
            });
            for (let i = 0; i < template.elements.length; i++) {
                const imgData = this.refs['paperPage' + i].getStage().toDataURL();
                if (i != 0) pdf.addPage("a4", "portrait")
                pdf.addImage(imgData, 'PNG', 0, 0, w, h);

                for (let j = 0; j < template.elements[i].length; j++) {
                    if (template.elements[i][j].src)
                        delete template.elements[i][j].src;
                }

                let arr = imgData.split(','), mime = arr[0].match(/:(.*?);/)[1]
                let bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
                while (n--) {
                    u8arr[n] = bstr.charCodeAt(n);
                }
                const file = new File([u8arr], 'filename', { type: 'image/png' });
                const formData = new FormData();
                formData.append("file", file);
                const fileResponse = await storageServices.uploadFile(formData);
                images.push(fileResponse.data.file.id);
            }
            template.image = images;
            const response = await portServices.updateTemplate(template);
            message.success(t('lbl_successfully_saved'));
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loadingEnd()
        }
    }

    handleSaveToDrive = async () => {
        const { template, fileName, fileType } = this.state;
        const { loadingStart, loadingEnd, t, userDrive } = this.props;
        loadingStart(t('lbl_saving'))
        try {
            await this.setState({ selectedShapeName: '', isDrawerOpenOption: false })
            const pdf = new jsPDF(new jsPDF('p', 'mm', [297, 210]));
            const zip = new jsZip();
            var w = pdf.internal.pageSize.getWidth();
            var h = pdf.internal.pageSize.getHeight();
            for (let i = 0; i < template.elements.length; i++) {
                const imgData = this.refs['paperPage' + i].getStage().toDataURL();
                if (fileType === 'PDF') {
                    if (i != 0) pdf.addPage("a4", "portrait")
                    pdf.addImage(imgData, 'PNG', 0, 0, w, h);
                } else if (fileType == 'PNG') {
                    await zip.file(template.name + (i + 1) + ".png", imgData.split('base64,')[1], { base64: true });
                } else if (fileType == 'JPG') {
                    await zip.file(template.name + (i + 1) + ".jpg", imgData.split('base64,')[1], { base64: true });
                }
            }
            let url;
            if (fileType === 'PDF') {
                const doc = pdf.output('blob');
                url = new Blob([doc], { type: 'application/pdf' })
                // let arr = base64.split(','), mime = arr[0].match(/:(.*?);/)[1]
                // let bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
                // while(n--){
                //     u8arr[n] = bstr.charCodeAt(n);
                // }
                // const file = new File([u8arr], template.name, {type: mime});
            }
            else {
                url = await zip.generateAsync({ type: 'blob' })
            }
            const formData = new FormData();
            formData.append('accessToken', this.state.accessToken);
            formData.append('file', url, fileType == 'PDF' ? fileName : fileName + '_' + fileType);
            const response = await googleServices.uploadDrive(formData);
            console.log(response)
            message.success(t('lbl_successfully_saved'));
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loadingEnd()
        }
    }

    MenuSidebarClick = e => {
        this.setState({
            selectedKey: e.key,
            collapsed: false
        })
    }
    MenuSidebarHide = () => {
        this.setState({
            selectedKey: null,
            collapsed: true
        })
    }

    getBase64 = async file => {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        })
    }

    changeColorBackground = (color) => {
        const { page } = this.state;
        let template = this.state.template;
        for (let i = 0; i < template.elements.length; i++) {
            if (i == page) {
                template.elements[i].forEach(item => {
                    if (item.type === 'bg') {
                        item.fill = color;
                    }
                })
            }
        }
        this.setState({
            template: template,
            color: color
        })
    }

    addElement = (data) => {
        const { page } = this.state;
        let template = this.state.template;
        for (let i = 0; i < template.elements.length; i++) {
            if (i == page) {
                template.elements[i].push(data)
            }
        }
        this.setState({
            template: template,
            visible: false
        })
    }

    responseGoogleDrive = async (response) => {
        await this.setState({ accessToken: response.accessToken, isDrawerOpenOption: false })
        await this.setState({ isModalGoogleDrive: true })
    }

    changeTheme = async (id) => {
        const { template } = this.state;
        const { loadingStart, loadingEnd } = this.props;
        loadingStart(null)
        try {
            template.themeId = id;
            const theme = this.state.theme.filter(k => k._id == template.themeId)[0];
            template.theme = theme;
            for (let i = 0; i < template.elements.length; i++) {
                template.elements[i].forEach(item => {
                    if (item.fillType) {
                        item.fill = theme[item.fillType]
                    }

                    if (item.strokeType) {
                        item.stroke = theme[item.strokeType]
                    }
                })

            }
            this.setState({ template })
        } catch (err) {
            console.log(err);
        } finally {
            loadingEnd()
        }
    }

    onNewPage = async (page) => {
        const { template } = this.state;
        const data = [
            {
                "type": "bg",
                "x": 0,
                "y": 0,
                "width": 793.701,
                "height": 1122.52,
                "fillType": "white",
                "fill": "#ffffff",
                "name": "Bg"+Date.now()
            }
        ];
        template.elements.splice(page+1, 0, data);
        await this.setState({template})
        window.scrollTo(0, this.refs['Page'+(page+1)].offsetTop)
    };
    onCopyPage = async (page) => {
        const { template } = this.state;
        let newArray = [];
        for (let i = 0; i < template.elements[page].length; i++) {
            let item = Object.assign({}, template.elements[page][i]);
            const newName = item.type.charAt(0).toUpperCase() + item.type.slice(1)+Date.now()+i;
            item.name = newName;
            newArray.push(item);
        }
        template.elements.splice(page+1, 0, newArray);
        await this.setState({template})
        window.scrollTo(0, this.refs['Page'+(page+1)].offsetTop)
    };
    onDeletePage = (page) => {
        const { template } = this.state;
        template.elements.splice(page, 1);
        if(template.elements.length == 0) {
            const data = [
                {
                    "type": "bg",
                    "x": 0,
                    "y": 0,
                    "width": 793.701,
                    "height": 1122.52,
                    "fillType": "white",
                    "fill": "#ffffff",
                    "name": "Bg"+Date.now()
                }
            ];
            template.elements.splice(page+1, 0, data);
        }
        this.setState({template})
    };

    setPage(key, page) {
        const { template } = this.state;
        if(key != undefined) {
            template[key] = page;
        } else {
            if(template.frontCoverPage == page) {
                template.frontCoverPage = -1;
            } else if(template.profilePage == page) {
                template.profilePage = -1;
            } else if(template.educationPage == page) {
                template.educationPage = -1;
            } else if(template.activityPage == page) {
                template.activityPage = -1;
            } else if(template.certificatePage == page) {
                template.certificatePage = -1;
            }
        }

        this.setState({template});
    }

    setDefaultPage = ( page ) => {
        const { template } = this.state;
        if(template.frontCoverPage == page) {
            return 'frontCoverPage';
        } else if(template.profilePage == page) {
            return 'profilePage';
        } else if(template.educationPage == page) {
            return 'educationPage';
        } else if(template.activityPage == page) {
            return 'activityPage';
        } else if(template.certificatePage == page) {
            return 'certificatePage';
        }
    }

    render() {
        const { t, loading, userInfo, userDrive } = this.props;
        const { ContextMenu, textEdit, resizeWidth, accountVisible, language, user, project_name, paperWidth, paperHeight, scale, percentScale, sizeMobile, elements, collapsed, selectedKey, visible, visibleSetting, font, FileList, selectedShapeName, selectedType, fileName, fileType } = this.state;
        let { template, selectedPage, selectedElementIndex } = this.state;
        const imageTitle = (
            <div className="cursor-pointer" onClick={() => { this.props.history.go(-(this.props.history.length)); this.props.history.replace('/') }}>
                <img className="header-design-banner mr-2" src={require('../assets/logo.png')} style={{ height: 40 }} />
                <Typography.Text className="sm-d-none" strong style={{ fontSize: 16, color: '#fff' }}>Tum Port</Typography.Text>
            </div>
        )

        const styles = reactcss({
            'default': {
                popover: {
                    position: 'absolute',
                    zIndex: 2
                },
                cover: {
                    position: 'fixed',
                    top: '0px',
                    right: '0px',
                    bottom: '0px',
                    left: '0px',
                }
            }
        })

        // component SVG Icon

        const SvgGoogleDrive = () => (
            <img src={require("../assets/socials/drive_google.svg")} />
        );

        const SvgGmail = () => (
            <img src="socials/gmail.svg" />
        );

        const SvgFacebook = () => (
            <img src="socials/facebook.svg" />
        );

        const SvgUndo = () => (
            <svg t="1582233994798" class="icon" viewBox="0 0 1259 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="15747" width="20" height="20"><path d="M1254.656237 991.615106c-2.916117 18.665609-17.066051 32.384894-36.494527 32.384894h-1.341168c-19.908343-0.627519-32.495632-15.601841-34.18132-34.796535-1.341168-14.100717-80.802278-359.592901-632.354421-359.285293v196.868654a39.373731 39.373731 0 0 1-39.373731 39.373731c-2.916117 0-5.475409-1.058169-8.194657-1.648775l0.061521 4.76176-9.843433-7.97318a38.795429 38.795429 0 0 1-11.935162-9.695782L14.371412 472.39864a37.343523 37.343523 0 0 1 0.123043-58.974466L490.596686 3.12529v3.309854c6.004494-3.691287 12.685724-6.42284 20.277471-6.42284a39.373731 39.373731 0 0 1 39.373731 39.373731v196.868654c228.85981 7.02575 446.239717 93.377263 561.075664 223.421314 187.04983 211.621499 145.289067 519.130337 143.332685 531.939103z" fill="currentColor" p-id="15748"></path></svg>
        );

        const SvgRedo = () => (
            <svg t="1582234047927" class="icon" viewBox="0 0 1259 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="16484" width="20" height="20"><path d="M148.551586 459.652619c114.870091-130.028612 332.293974-216.378044 561.062141-223.415928v-196.863909a39.372782 39.372782 0 0 1 39.372782-39.372782c7.591564 0 14.272633 2.706879 20.276982 6.422685V3.112911L1245.440375 413.426513a37.342623 37.342623 0 0 1 0.12304 58.973044l-466.653592 379.209105a38.794494 38.794494 0 0 1-11.934875 9.695547l-9.843195 7.972989 0.06152-4.761646c-2.719183 0.590592-5.278414 1.648735-8.19446 1.648735a39.372782 39.372782 0 0 1-39.372782-39.372782v-196.863908c-551.538849-0.356816-630.98574 345.12704-632.33918 359.276633-1.697951 19.194231-14.272633 34.168192-34.180496 34.795696h-1.316527c-19.366487 0.036912-33.565296-13.718954-36.493647-32.396417-1.956335-12.808458-43.666876-320.309884 143.255405-531.95089z" fill="currentColor" p-id="16485"></path></svg>
        );

        const SvgSave = () => (
            <img src="actions/save.svg" />
        );

        const BarSvg = () => (
            <svg width="18" height="18" fill="currentColor" t="1580461746390" class="icon" viewBox="0 0 1258 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="7958">
                <path d="M26.91643 0h1259.279059v192.813176H26.91643zM26.91643 403.757176h1259.279059v192.813177H26.91643zM26.91643 831.849412h1259.279059v192.813176H26.91643z" p-id="7959"></path>
            </svg>
        );

        const HideCollapsedSvg = () => (
            <svg width="50" height="50" t="1581761529647" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4306">
                <path d="M587.017481 4.645926l-189.629629 33.526518A113.777778 113.777778 0 0 0 303.407407 150.186667V873.813333a113.777778 113.777778 0 0 0 93.980445 112.033186l189.629629 33.526518A113.777778 113.777778 0 0 0 720.592593 907.301926V116.698074A113.777778 113.777778 0 0 0 587.017481 4.645926z" fill="#293038" fill-opacity="1" p-id="4307" data-spm-anchor-id="a313x.7781069.0.i0" class="selected"></path><path d="M352.44563 605.866667a18.962963 18.962963 0 0 1 0-26.81363L419.460741 512l-67.034074-67.072A18.962963 18.962963 0 1 1 379.259259 418.133333l80.459852 80.459852a18.962963 18.962963 0 0 1 0 26.81363L379.259259 605.866667a18.962963 18.962963 0 0 1-26.813629 0z" fill="#FFFFFF" p-id="4308"></path>
            </svg>
        );

        const TemplateSvg = () => (
            <svg t="1583079389402" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3863" width="20" height="20"><path d="M0 0v455.111111h455.111111V0H0z m398.222222 56.888889v341.333333H56.888889V56.888889h341.333333zM0 568.888889v455.111111h455.111111v-455.111111H0z m398.222222 56.888889v341.333333H56.888889v-341.333333h341.333333zM967.111111 0h-398.222222v1024h455.111111V0h-56.888889z m-341.333333 967.111111V56.888889h341.333333v910.222222h-341.333333z" fill="currentColor" p-id="3864"></path></svg>
        );

        const ShapeSvg = () => (
            <svg viewBox="0 0 24 24" fill="currentColor" width="26" height="26"><path fill-rule="evenodd" d="M14.9291 7C14.4439 3.60771 11.5265 1 8 1C4.13401 1 1 4.13401 1 8C1 11.5265 3.60771 14.4439 7 14.9291V21C7 22.1046 7.89543 23 9 23H21C22.1046 23 23 22.1046 23 21V9C23 7.89543 22.1046 7 21 7H14.9291ZM9 14.9291V21H21V9H14.9291C14.4906 12.0657 12.0657 14.4906 9 14.9291ZM12.9468 7.96889C12.9468 10.718 10.7181 12.9467 7.96899 12.9467C5.21984 12.9467 2.99121 10.718 2.99121 7.96889C2.99121 5.21974 5.21984 2.99111 7.96899 2.99111C10.7181 2.99111 12.9468 5.21974 12.9468 7.96889Z"></path></svg>
        );

        const IconSvg = () => (
            <svg t="1581791264066" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="26741" width="20" height="20">
                <path d="M597.344 874.656a42.656 42.656 0 0 1 0-85.312h383.52a42.656 42.656 0 0 1 0 85.312H597.344z m0 149.344a42.656 42.656 0 0 1 0-85.312h383.52a42.656 42.656 0 0 1 0 85.312H597.344zM128 533.344c-23.552 0-42.656 19.104-42.656 42.656v320c0 23.552 19.104 42.656 42.656 42.656h213.344c23.552 0 42.656-19.104 42.656-42.656V576c0-23.552-19.104-42.656-42.656-42.656H128zM128 448h213.344a128 128 0 0 1 128 128v320a128 128 0 0 1-128 128H128a128 128 0 0 1-128-128V576a128 128 0 0 1 128-128zM128 85.344c-23.552 0-42.656 19.104-42.656 42.656v106.656c0 23.552 19.104 42.656 42.656 42.656h213.344c23.552 0 42.656-19.104 42.656-42.656V128c0-23.552-19.104-42.656-42.656-42.656H128zM128 0h213.344a128 128 0 0 1 128 128v106.656a128 128 0 0 1-128 128H128a128 128 0 0 1-128-128V128a128 128 0 0 1 128-128z m554.656 85.344C659.104 85.344 640 104.448 640 128v448c0 23.552 19.104 42.656 42.656 42.656H896c23.552 0 42.656-19.104 42.656-42.656V128c0-23.552-19.104-42.656-42.656-42.656h-213.344z m0-85.344H896a128 128 0 0 1 128 128v448a128 128 0 0 1-128 128h-213.344a128 128 0 0 1-128-128V128a128 128 0 0 1 128-128z" fill="currentColor" p-id="26742"></path>
            </svg>
        );

        const FontSvg = () => (
            <svg fill="currentColor" t="1583079553827" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4612" width="20" height="20"><path d="M446.2 304.5l-104.6 277c13.5 0 41.6 0.4 84 1.2 42.5 0.8 75.4 1.2 98.8 1.2 7.8 0 19.5-0.4 35.1-1.2-35.7-103.8-73.5-196.5-113.3-278.2zM0 984.6L1.2 936c9.4-2.9 20.9-5.4 34.5-7.7 13.5-2.3 25.2-4.4 35.1-6.5 9.9-2 20-5 30.4-8.9 10.4-3.9 19.6-9.9 27.4-17.8 7.8-8 14.2-18.3 19.1-31.1l145.8-379.1L465.8 39.3h78.8c3.3 5.8 5.5 10.1 6.8 12.9l126.2 295.4c13.6 32 35.3 84.8 65.2 158.5C772.7 579.7 796.1 636 813 675c6.1 13.9 18 43.6 35.7 88.9 17.6 45.3 32.4 79.9 44.3 103.7 8.2 18.5 15.4 30.2 21.5 35.1 7.8 6.1 25.9 12.2 54.2 18.2 28.3 6 45.5 10.2 51.7 12.6 2.5 15.6 3.7 27.3 3.7 35.1 0 1.7-0.1 4.3-0.3 8-0.2 3.7-0.3 6.3-0.3 8-25.8 0-64.8-1.6-116.9-4.9-52.1-3.3-91.3-4.9-117.5-4.9-31.2 0-75.3 1.4-132.3 4.3-57 2.9-93.5 4.5-109.5 4.9 0-17.6 0.8-33.6 2.5-48l80.6-17.3c0.4 0 3-0.5 7.7-1.5s7.9-1.7 9.5-2.2c1.7-0.4 4.6-1.3 8.9-2.8 4.3-1.4 7.4-2.8 9.2-4 1.8-1.3 4.1-2.9 6.8-4.9 2.7-2 4.5-4.3 5.6-6.8 1-2.5 1.5-5.4 1.5-8.6 0-6.6-6.4-26.4-19.1-59.4-12.7-33-27.5-69.4-44.3-109.2-16.8-39.8-25.4-60.3-25.8-61.6l-276.9-1.2c-10.7 23.8-26.4 63.9-47.1 120.3-20.7 56.4-31.1 89.8-31.1 100 0 9 2.9 16.7 8.6 23.1 5.7 6.4 14.7 11.4 26.8 15.1 12.1 3.7 22.1 6.5 29.9 8.3 7.8 1.8 19.5 3.6 35.1 5.2 15.6 1.7 24 2.5 25.2 2.5 0.4 7.8 0.6 19.7 0.6 35.7 0 3.7-0.4 9.2-1.2 16.6-23.8 0-59.6-2-107.4-6.1-47.8-4.1-83.6-6.1-107.4-6.1-3.3 0-8.7 0.8-16.3 2.5-7.6 1.7-12 2.5-13.2 2.5-33 5.7-71.6 8.6-116 8.5z" p-id="4613"></path></svg>
        );

        const FontSmallSvg = () => (
            <svg t="1583086149613" fill="currentColor" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4490" width="16" height="16"><path d="M928 832h-46.82L619.76 107.38A64 64 0 0 0 559.16 64h-94.32a64 64 0 0 0-60.6 43.38L142.82 832H96a32 32 0 0 0-32 32v64a32 32 0 0 0 32 32h256a32 32 0 0 0 32-32v-64a32 32 0 0 0-32-32h-39.16l46.6-128h305.12l46.6 128H672a32 32 0 0 0-32 32v64a32 32 0 0 0 32 32h256a32 32 0 0 0 32-32v-64a32 32 0 0 0-32-32zM417.7 544L512 285.02 606.3 544z" p-id="4491"></path></svg>
        );

        const FontLargeSvg = () => (
            <svg t="1583086149613" fill="currentColor" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4490" width="24" height="24"><path d="M928 832h-46.82L619.76 107.38A64 64 0 0 0 559.16 64h-94.32a64 64 0 0 0-60.6 43.38L142.82 832H96a32 32 0 0 0-32 32v64a32 32 0 0 0 32 32h256a32 32 0 0 0 32-32v-64a32 32 0 0 0-32-32h-39.16l46.6-128h305.12l46.6 128H672a32 32 0 0 0-32 32v64a32 32 0 0 0 32 32h256a32 32 0 0 0 32-32v-64a32 32 0 0 0-32-32zM417.7 544L512 285.02 606.3 544z" p-id="4491"></path></svg>
        );

        const ThemeSvg = () => (
            <svg fill="currentColor" t="1583079833271" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="5344" width="24" height="24"><path d="M512 96C229.696 96 0 325.696 0 608c0 90.368 30.304 174.496 85.344 236.896 55.264 62.624 129.152 97.12 208.128 97.12 81.568 0 161.536-36.832 231.264-106.592l2.272-2.496c65.792-81.472 132.896-121.056 205.088-121.056 46.72 0 89.216 15.872 126.688 29.92 30.336 11.328 56.576 21.12 81.216 21.12C1024 762.912 1024 654.336 1024 608c0-282.304-229.696-512-512-512z m428 602.912c-13.088 0-35.296-8.288-58.784-17.088-40.48-15.136-90.848-33.952-149.12-33.952-92.352 0-175.328 46.944-253.76 143.456-57.184 56.704-121.056 86.688-184.832 86.688-60.352 0-117.216-26.784-160.128-75.456C88.64 751.872 64 682.784 64 608 64 360.96 264.96 160 512 160s448 200.96 448 448c0 27.328-1.952 90.912-20 90.912z m-203.296-182.848a64 64 0 1 0 128 0 64 64 0 1 0-128 0z m-343.68-202.688a64 64 0 1 0 128 0 64 64 0 1 0-128 0z m215.68 26.688a64 64 0 1 0 128 0 64 64 0 1 0-128 0z m-381.312 112a64 64 0 1 0 128 0 64 64 0 1 0-128 0zM182.4 698.752a96 96 0 1 0 192 0 96 96 0 1 0-192 0z" p-id="5345"></path></svg>
        );

        const LayoutSvg = () => (
            <svg fill="currentColor" t="1583080155721" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="6762" width="22" height="22"><path d="M810.666667 85.333333H213.333333C140.8 85.333333 85.333333 140.8 85.333333 213.333333v597.333334c0 72.533333 55.466667 128 128 128h597.333334c72.533333 0 128-55.466667 128-128V213.333333c0-72.533333-55.466667-128-128-128zM213.333333 170.666667h597.333334c25.6 0 42.666667 17.066667 42.666666 42.666666v128H170.666667V213.333333c0-25.6 17.066667-42.666667 42.666666-42.666666zM170.666667 810.666667v-384h170.666666v426.666666H213.333333c-25.6 0-42.666667-17.066667-42.666666-42.666666z m640 42.666666h-384V426.666667h426.666666v384c0 25.6-17.066667 42.666667-42.666666 42.666666z" p-id="6763"></path></svg>
        );

        const TextSvg = () => (
            <svg fill="currentColor" t="1581791661810" class="icon" viewBox="0 0 1290 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="30792" width="20" height="20">
                <path d="M1055.007 498.625h-65.201L810.047 970.558h67.344l45.391-129.179h196.508l48.133 129.179h67.343zM941.98 788.058l70.682-196.146q4.257-11.588 7.61-32.9h1.53q3.964 23.455 7.317 32.9l71.294 196.146zM515.73 53.442H78.797v173.346h46.867c10.948-75.913 55.366-110.19 119.802-110.19h160.13v692.216q0 77.707-20.198 95.026t-91.59 17.389h-3.2v49.315h450.204v-49.315h-3.2q-71.378 0-91.577-17.403T625.837 808.8V116.599h160.116c64.436 0 108.868 34.276 119.802 110.19h46.867V53.441z" p-id="30793"></path>
            </svg>
        );

        const ImageSvg = () => (
            <svg t="1581791984366" fill="currentColor" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="31552" width="20" height="">
                <path d="M959.87712 128c0.04096 0.04096 0.08192 0.08192 0.12288 0.12288l0 767.77472c-0.04096 0.04096-0.08192 0.08192-0.12288 0.12288l-895.77472 0c-0.04096-0.04096-0.08192-0.08192-0.12288-0.12288l0-767.77472c0.04096-0.04096 0.08192-0.08192 0.12288-0.12288l895.77472 0zM960 64l-896 0c-35.20512 0-64 28.79488-64 64l0 768c0 35.20512 28.79488 64 64 64l896 0c35.20512 0 64-28.79488 64-64l0-768c0-35.20512-28.79488-64-64-64l0 0z" p-id="31553"></path>
                <path d="M832 288.01024c0 53.02272-42.98752 96.01024-96.01024 96.01024s-96.01024-42.98752-96.01024-96.01024 42.98752-96.01024 96.01024-96.01024 96.01024 42.98752 96.01024 96.01024z" p-id="31554"></path><path d="M896 832l-768 0 0-128 224.01024-384 256 320 64 0 224.01024-192z" p-id="31555"></path>
            </svg>
        );

        const BackgroundSvg = () => (
            <svg t="1581792380932" fill="currentColor" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="32490" width="20" height="20">
                <path d="M296.4 834.2c1.6 1.6 4.1 1.6 5.7 0l39.6-39.6c1.6-1.6 1.6-4.1 0-5.7L229.7 677c-1.6-1.6-4.1-1.6-5.7 0l-39.6 39.6c-1.6 1.6-1.6 4.1 0 5.7l112 111.9z m242.2-5.2c1.6 1.6 4.1 1.6 5.7 0l39.6-39.6c1.6-1.6 1.6-4.1 0-5.7l-349-348.9c-1.6-1.6-4.1-1.6-5.7 0l-39.6 39.6c-1.6 1.6-1.6 4.1 0 5.7l349 348.9z m239.9-7.6c1.6 1.6 4.1 1.6 5.7 0l39.6-39.6c1.6-1.6 1.6-4.1 0-5.7L242.5 194.9c-1.6-1.6-4.1-1.6-5.7 0l-39.6 39.6c-1.6 1.6-1.6 4.1 0 5.7l581.3 581.2z m7.5-239.9c1.6 1.6 4.1 1.6 5.7 0l39.6-39.6c1.6-1.6 1.6-4.1 0-5.7L482.4 187.4c-1.6-1.6-4.1-1.6-5.7 0L437.1 227c-1.6 1.6-1.6 4.1 0 5.7L786 581.5z m5.2-242.2c1.6 1.6 4.1 1.6 5.7 0l39.6-39.6c1.6-1.6 1.6-4.1 0-5.7L724.6 182.2c-1.6-1.6-4.1-1.6-5.7 0l-39.6 39.6c-1.6 1.6-1.6 4.1 0 5.7l111.9 111.8zM911 97H113c-8.8 0-16 7.2-16 16v798c0 8.8 7.2 16 16 16h798c8.8 0 16-7.2 16-16V113c0-8.8-7.2-16-16-16z m-48 766H161V161h702v702z" p-id="32491"></path>
            </svg>
        );

        const RemoveSvg = () => (
            <svg fill="currentColor" t="1582229597141" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4100" width="18" height="18">
                <path d="M978.622061 168.242424H786.327273c0-75.734626-59.010586-137.354343-131.563313-137.354343H368.486141c-72.579879 0-131.582707 61.619717-131.582707 137.354343H44.600889c-15.169939 0-27.469576 12.298343-27.469576 27.469576 0 15.172525 12.299636 27.472162 27.469576 27.472162h934.021172a27.463111 27.463111 0 0 0 23.792484-13.733495 27.452768 27.452768 0 0 0 0-27.469576 27.46699 27.46699 0 0 0-23.792484-13.738667zM368.486141 85.828525h286.277819c42.254222 0 76.621576 36.979071 76.621575 82.413899H291.846465c0-45.436121 34.363475-82.413899 76.639676-82.413899z m472.782869 192.302546a27.472162 27.472162 0 0 0-27.470869 27.470868V855.027071c0 45.432242-36.977778 82.412606-82.412606 82.412606H291.846465c-45.44 0-82.419071-36.981657-82.419071-82.412606V305.601939c0-15.171232-12.298343-27.470869-27.470869-27.470868-15.169939 0-27.470869 12.299636-27.470868 27.470868V855.027071c0 75.738505 61.618424 137.354343 137.360808 137.354343h439.53907c75.734626 0 137.354343-61.615838 137.354344-137.354343V305.601939a27.464404 27.464404 0 0 0-27.470869-27.470868z m0 0" p-id="4101"></path>
                <path d="M429.199515 800.08404V305.601939c0-15.171232-12.298343-27.470869-27.469576-27.470868-15.172525 0-27.472162 12.299636-27.472161 27.470868v494.482101c0 15.172525 12.299636 27.468283 27.472161 27.468283 15.171232 0.001293 27.469576-12.294465 27.469576-27.468283z m219.773414 0V305.601939c0-15.171232-12.306101-27.470869-27.477333-27.470868-15.172525 0-27.470869 12.299636-27.470869 27.470868v494.482101c0 15.172525 12.298343 27.468283 27.470869 27.468283 15.172525 0.001293 27.477333-12.294465 27.477333-27.468283z m0 0" p-id="4102"></path>
            </svg>
        );
        const KeyValueSvg = () => (
            <svg fill="currentColor" t="1584307499459" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2072" width="18" height="18"><path d="M64 960h206.8V753.2H64V960z m68.9-137.9h68.9V891h-68.9v-68.9zM64 615.4h206.8V408.6H64v206.8z m68.9-137.9h68.9v68.9h-68.9v-68.9zM64 270.8h206.8V64H64v206.8z m68.9-137.9h68.9v68.9h-68.9v-68.9zM339.7 960H960V753.2H339.7V960z m68.9-137.9h482.5V891H408.6v-68.9zM339.7 64v206.8H960V64H339.7z m551.4 137.8H408.6v-68.9h482.5v68.9zM339.7 615.4H960V408.6H339.7v206.8z m68.9-137.9h482.5v68.9H408.6v-68.9z" p-id="2073"></path></svg>
        );
        const LockSvg = () => (
            <svg t="1582230588147" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="13369" width="20" height="20">
                <path d="M768 384H256c-70.4 0-128 57.6-128 128v320c0 70.4 57.6 128 128 128h512c70.4 0 128-57.6 128-128V512c0-70.4-57.6-128-128-128z m64 448c0 38.4-25.6 64-64 64H256c-38.4 0-64-25.6-64-64V512c0-38.4 25.6-64 64-64h512c38.4 0 64 25.6 64 64v320z" fill="currentColor" p-id="13370"></path>
                <path d="M512 627.2m-64 0a64 64 0 1 0 128 0 64 64 0 1 0-128 0Z" fill="currentColor" p-id="13371"></path>
                <path d="M512 780.8c-19.2 0-32-12.8-32-32v-128c0-19.2 12.8-32 32-32s32 12.8 32 32v128c0 19.2-12.8 32-32 32zM320 448V320c0-108.8 83.2-192 192-192s192 83.2 192 192v128h64V320c0-140.8-115.2-256-256-256S256 179.2 256 320v128h64z" fill="currentColor" p-id="13372"></path>
            </svg>
        );

        const UnlockSvg = () => (
            <svg t="1582231041985" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="14262" width="22" height="22"><path d="M583.111111 684.657778a71.111111 71.111111 0 1 1-99.555555-65.137778v-62.862222a28.444444 28.444444 0 0 1 56.888888 0v62.862222a71.111111 71.111111 0 0 1 42.666667 65.137778zM867.555556 440.888889v426.666667a56.888889 56.888889 0 0 1-56.888889 56.888888H213.333333a56.888889 56.888889 0 0 1-56.888889-56.888888V440.888889a56.888889 56.888889 0 0 1 56.888889-56.888889h85.333334v-71.111111a213.333333 213.333333 0 0 1 420.835555-48.64l-80.497778 33.422222A128 128 0 0 0 384 312.888889v71.111111h426.666667a56.888889 56.888889 0 0 1 56.888889 56.888889z m-85.333334 60.728889A32 32 0 0 0 750.364444 469.333333H273.635556A32 32 0 0 0 241.777778 501.617778v305.208889A32 32 0 0 0 273.635556 839.111111h476.728888A32 32 0 0 0 782.222222 806.826667z" fill="currentColor" p-id="14263"></path></svg>
        );

        const OpacitySvg = () => (
            <svg t="1582231138965" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="15014" width="22" height="22"><path d="M128 85.344h85.344C236.896 85.344 256 104.448 256 128v85.344C256 236.896 236.896 256 213.344 256H128a42.656 42.656 0 0 1-42.656-42.656V128c0-23.552 19.104-42.656 42.656-42.656z m0 341.312h85.344c23.552 0 42.656 19.104 42.656 42.656v85.344c0 23.552-19.104 42.656-42.656 42.656H128a42.656 42.656 0 0 1-42.656-42.656v-85.344c0-23.552 19.104-42.656 42.656-42.656zM128 768h85.344C236.896 768 256 787.104 256 810.656V896c0 23.552-19.104 42.656-42.656 42.656H128A42.656 42.656 0 0 1 85.344 896v-85.344C85.344 787.104 104.448 768 128 768z" fill="#444444" p-id="15015"></path><path d="M469.344 85.344h85.344c23.552 0 42.656 19.104 42.656 42.656v85.344c0 23.552-19.104 42.656-42.656 42.656h-85.344a42.656 42.656 0 0 1-42.656-42.656V128c0-23.552 19.104-42.656 42.656-42.656z m0 341.312h85.344c23.552 0 42.656 19.104 42.656 42.656v85.344c0 23.552-19.104 42.656-42.656 42.656h-85.344a42.656 42.656 0 0 1-42.656-42.656v-85.344c0-23.552 19.104-42.656 42.656-42.656z m0 341.344h85.344c23.552 0 42.656 19.104 42.656 42.656V896c0 23.552-19.104 42.656-42.656 42.656h-85.344A42.656 42.656 0 0 1 426.688 896v-85.344c0-23.552 19.104-42.656 42.656-42.656z" fill="#444444" opacity=".45" p-id="15016"></path><path d="M810.656 85.344H896c23.552 0 42.656 19.104 42.656 42.656v85.344C938.656 236.896 919.552 256 896 256h-85.344A42.656 42.656 0 0 1 768 213.344V128c0-23.552 19.104-42.656 42.656-42.656z m0 341.312H896c23.552 0 42.656 19.104 42.656 42.656v85.344c0 23.552-19.104 42.656-42.656 42.656h-85.344A42.656 42.656 0 0 1 768 554.656v-85.344c0-23.552 19.104-42.656 42.656-42.656z m0 341.344H896c23.552 0 42.656 19.104 42.656 42.656V896c0 23.552-19.104 42.656-42.656 42.656h-85.344A42.656 42.656 0 0 1 768 896v-85.344c0-23.552 19.104-42.656 42.656-42.656z" fill="#444444" opacity=".15" p-id="15017"></path><path d="M298.656 256H384c23.552 0 42.656 19.104 42.656 42.656V384c0 23.552-19.104 42.656-42.656 42.656H298.656A42.656 42.656 0 0 1 256 384V298.656C256 275.104 275.104 256 298.656 256z m0 341.344H384c23.552 0 42.656 19.104 42.656 42.656v85.344c0 23.552-19.104 42.656-42.656 42.656H298.656A42.656 42.656 0 0 1 256 725.344V640c0-23.552 19.104-42.656 42.656-42.656z" fill="#444444" opacity=".7" p-id="15018"></path><path d="M640 256h85.344C748.896 256 768 275.104 768 298.656V384c0 23.552-19.104 42.656-42.656 42.656H640A42.656 42.656 0 0 1 597.344 384V298.656C597.344 275.104 616.448 256 640 256z m0 341.344h85.344c23.552 0 42.656 19.104 42.656 42.656v85.344c0 23.552-19.104 42.656-42.656 42.656H640a42.656 42.656 0 0 1-42.656-42.656V640c0-23.552 19.104-42.656 42.656-42.656z" fill="#444444" opacity=".3" p-id="15019"></path></svg>
        );

        const StrokeSvg = () => (
            <svg t="1582293403120" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="7012" width="20" height="20"><path d="M128 832h768v128H128z" fill="currentColor" p-id="7013" data-spm-anchor-id="a313x.7781069.0.i5" class=""></path><path d="M361.088 513.472l-51.456-51.392a8 8 0 0 1 0-11.328L409.088 351.36 377.6 319.936a8 8 0 0 1 0-11.328L592.64 93.632a8 8 0 0 1 11.264 0l311.168 311.168a8 8 0 0 1 0 11.328L700.096 631.04a8 8 0 0 1-11.328 0l-37.888-37.888-99.392 99.392a8 8 0 0 1-11.328 0l-40.448-40.448-34.432 34.56a32 32 0 0 1-22.656 9.344H216.96a16 16 0 0 1-11.328-27.328l155.456-155.2z m237.184-318.016L479.488 314.304l214.976 214.912 118.784-118.784-214.976-214.976zM411.456 456.384l134.4 134.4 50.88-50.944-134.4-134.4-50.88 50.944z" fill="currentColor" p-id="7014"></path></svg>
        );

        const ShadowSvg = () => (
            <svg fill="currentColor" t="1582297419093" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="1170" width="20" height="20"><path d="M512 981.333333C252.8 981.333333 42.666667 771.2 42.666667 512S252.8 42.666667 512 42.666667s469.333333 210.133333 469.333333 469.333333-210.133333 469.333333-469.333333 469.333333zM130.346667 469.333333a388.224 388.224 0 0 0 0 85.333334H512v-85.333334H130.346667z m62.314666-170.666666a382.634667 382.634667 0 0 0-42.816 85.333333H512v-85.333333H192.661333zM512 128a382.4 382.4 0 0 0-241.386667 85.333333H512V128zM270.613333 810.666667c65.962667 53.376 149.930667 85.333333 241.386667 85.333333v-85.333333H270.613333z m-120.746666-170.666667a382.634667 382.634667 0 0 0 42.794666 85.333333H512v-85.333333H149.845333z" p-id="1171"></path>
            </svg>
        );

        const FillColorSvg = () => (
            <svg fill="currentColor" t="1582387132862" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2763" width="20" height="20"><path d="M0 0h1024v1024H0z" p-id="2764"></path></svg>
        );

        const CircleSvg = () => (
            <svg t="1584378733211" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="1748" width="20" height="20"><path d="M512 512m-440.32 0a440.32 440.32 0 1 0 880.64 0 440.32 440.32 0 1 0-880.64 0Z" fill="currentColor" p-id="1749"></path></svg>
        );

        const MoreSvg = () => (
            <svg t="1582496002992" fill="currentColor" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3583" width="22" height="22"><path d="M265.1 509.8c-0.1 45.5-36.8 82.2-82.4 82.1-45.4-0.1-82.3-37-82.2-82.5 0-45.3 37.1-82.2 82.6-82.1 45.4 0.1 82 36.9 82 82.5z m577-82.4c45.5 0 82.5 36.8 82.5 82.2 0.1 45.4-36.8 82.4-82.3 82.4-45.6 0-82.4-36.6-82.4-82.2s36.6-82.4 82.2-82.4zM512.4 592c-45.4 0-82.6-37.3-82.4-82.4 0.3-45.5 37.1-82.2 82.6-82.2 45.4 0 82.5 37.1 82.3 82.4-0.1 45.4-37 82.2-82.5 82.2z" p-id="3584"></path></svg>
        );

        const PdfSvg = () => (
            <svg fill="currentColor" t="1582834429674" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3868" width="20" height="20"><path d="M497.018384 770.968065c-12.294276 0-20.234523 1.087578-24.936206 2.16586v159.362675c4.710978 1.089437 12.299854 1.089437 19.156242 1.089436 49.857539 0.351371 82.386338-27.098348 82.386338-85.284686 0.35323-50.597463-29.271644-77.333285-76.606374-77.333285zM284.141307 770.252308c-11.195544 0-18.78442 1.078282-22.770346 2.156565v71.921424c4.7054 1.078282 10.490942 1.450104 18.410739 1.450103 29.293953 0 47.353321-14.822662 47.353321-39.75515 0.001859-22.400383-15.545855-35.772942-42.993714-35.772942z" p-id="3869"></path><path d="M883.253496 245.387286c-0.059491-4.696105-1.548636-9.334578-4.774188-13.000737L681.036539 6.867542c-0.039041-0.06321-0.115265-0.08366-0.156165-0.141292-1.176815-1.314389-2.528386-2.398248-3.980348-3.353829-0.431313-0.278866-0.883076-0.529846-1.334839-0.784544a20.580317 20.580317 0 0 0-3.959899-1.658323c-0.35323-0.107828-0.706461-0.260275-1.078282-0.356949A19.567104 19.567104 0 0 0 665.938729 0H180.706156C158.567906 0 140.549439 18.020327 140.549439 40.158576v943.682848c0 22.147545 18.018468 40.158576 40.156717 40.158576h662.585829c22.138249 0 40.158576-18.011031 40.158576-40.158576V247.636806c-0.001859-0.754798-0.118983-1.494722-0.197065-2.24952zM359.293859 862.389665c-18.78442 17.704278-46.611537 25.666835-79.140337 25.666834-7.217054 0-13.740662-0.371821-18.780702-1.078282v87.078725h-54.564798V733.74316c16.975508-2.881616 40.839009-5.058631 74.451668-5.058631 33.960312 0 58.175185 6.508735 74.431219 19.509472 15.547714 12.292417 25.999615 32.5288 25.999615 56.37185 0 23.861642-7.930952 44.098024-22.396665 57.823814z m232.371676 82.765596c-25.627794 21.312806-64.656032 31.430067-112.342134 31.430067-28.554028 0-48.779256-1.803334-62.534791-3.606668V733.74316c20.240101-3.234847 46.631988-5.058631 74.44981-5.058631 46.23042 0 76.230834 8.313928 99.731809 26.018206 25.30245 18.793715 41.205253 48.784834 41.205253 91.784126-0.02045 46.628269-17.001536 78.785248-40.509947 98.6684z m225.546893-169.489232h-93.607909v55.646798h87.450546v44.823077h-87.450546v97.921038H668.3314V730.508313h148.881028v45.157716zM180.706156 681.831308V40.158576h465.154215v205.459239c0 11.082138 8.981347 20.078358 20.078358 20.078359H843.291985l0.02045 416.135134H180.706156z" p-id="3870"></path><path d="M717.997451 431.563865c-1.176815-0.109687-29.470568-2.691987-72.901173-2.691987-13.606806 0-27.312145 0.265852-40.842728 0.775247-85.766195-64.362294-156.014423-128.774783-193.622303-164.873068 0.686011-3.974771 1.158224-7.116663 1.377598-9.529784 4.960098-52.361756-0.554014-87.710822-16.334116-105.067447-10.332918-11.342413-25.508811-15.116401-41.333532-10.793977-9.827241 2.574864-28.018606 12.108366-33.843189 31.515586-6.430652 21.446661 3.905984 47.476022 31.061965 77.660488 0.431313 0.45734 9.646908 10.113544 26.391886 26.475547-10.883214 51.893261-39.372174 163.876587-53.194636 217.651265-32.46559 17.34733-59.515601 38.243696-80.456585 62.215024l-1.372021 1.569087-0.886794 1.883275c-2.156564 4.530644-12.465314 28.048351-4.725851 46.9406 3.534163 8.589076 10.156303 14.863563 19.152523 18.1579l2.411262 0.648829s2.175156 0.470354 5.999339 0.470354c16.750556 0 58.097102-8.802873 80.274393-90.529228l5.37282-20.706737c77.411367-37.626472 174.176041-49.764583 244.314581-53.146299 36.077836 26.754413 71.979056 51.331811 106.764812 73.096379l1.135915 0.658124c1.68621 0.862626 16.940185 8.393869 34.804347 8.410601 25.529261 0 44.176107-15.666697 51.118013-42.969546l0.351372-1.866544c1.940908-15.603487-1.979949-29.667634-11.333118-40.662394-19.702819-23.160758-56.388582-25.179749-59.68478-25.291295z m-452.493695 162.874528a1.849812 1.849812 0 0 1-0.308611-0.557732c-1.66576-4.010094 0.33278-13.736944 3.273887-20.862901 12.627057-14.118061 27.784358-27.077897 45.27484-38.745655-17.033141 55.137403-41.803886 59.904154-48.240116 60.166288z m108.147988-366.335227c-26.157639-29.117338-25.765368-43.553306-24.358023-48.413012 2.309011-8.12244 12.731167-11.191826 12.820403-11.219713 5.250119-1.427794 8.436629-1.147069 11.273627 1.966936 6.415779 7.047876 11.926173 28.317922 9.749158 67.333147-6.175954-6.2057-9.485165-9.667358-9.485165-9.667358z m-13.51571 247.954713l0.451763-1.725251-0.059491 0.02045c13.097411-51.298347 31.985941-126.411858 42.846845-174.661269l0.392272 0.373681 0.039041-0.230529c35.137127 33.088391 89.014056 81.486531 153.523219 131.823719l-0.725052 0.029745 1.067128 0.804994c-60.770498 5.132995-133.353764 17.113082-197.535725 43.56446z m399.098275 16.828639c-4.627318 17.003395-13.528724 19.327279-21.686486 19.327279-9.470292 0-18.587354-3.941307-20.665836-4.89317-23.686886-14.846831-47.920349-31.024782-72.373187-48.333071h0.587478c41.92101 0 69.842942 2.545118 70.939815 2.623201 7.001398 0.260275 29.158238 3.532304 38.706613 14.757593 3.744242 4.400507 5.177614 9.659921 4.491603 16.518168z" p-id="3871"></path></svg>
        );

        const PngSvg = () => (
            <svg fill="currentColor" t="1582834558427" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4219" width="20" height="20"><path d="M904 200H712V8H104v448h64V72h480v192h192v688H104v64h800z" p-id="4220"></path><path d="M168 752h28C246.4 752 288 710.4 288 660S246.4 568 196 568H104v288h64V752z m0-120h28c15.2 0 28 12.8 28 28S211.2 688 196 688H168v-56zM468.8 856H528V568h-64v143.2L387.2 568H328v288h64V712.8zM664 632c17.6 0 32 14.4 32 32h64c0-52.8-43.2-96-96-96s-96 43.2-96 96v96c0 52.8 43.2 96 96 96s96-43.2 96-96v-64H664v64h32c0 17.6-14.4 32-32 32s-32-14.4-32-32V664c0-17.6 14.4-32 32-32z" p-id="4221"></path></svg>
        );

        const JpgSvg = () => (
            <svg fill="currentColor" t="1582834700517" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4656" width="20" height="20"><path d="M895.999996 352.000007v599.999994q0 30-20.5 50.999999t-50.499999 21H201.000003q-30 0-51.499999-21T128.000004 952.000001V72.000009q0-30 21.5-50.999999T201.000003 0.00001h345.999997a72.929999 72.929999 0 0 1 28.999999 6 12 12 0 0 1 3 2c0.66 0 2 0.67 4 2a125.189999 125.189999 0 0 1 15 11l277.999997 280.999997q20 20 20 50z m-71.999999 31.999999H584.999999q-30 0-51.499999-21T512 312.000007V72.000009H200.000003v879.999992h623.999994zM351.000002 577.000004h44.999999v122.999999q0 29-15.5 47.5T335.000002 766.000003q-41 0-61-34l30-23q10 19 25 19 11 0 16.5-7t5.5-25z m225.999997 60q0 32-20.999999 47.999999t-52 16h-22v62h-43.999999V577.000004h63.999999q74.999999 0 74.999999 60z m-42.999999 1q0-25-34-25h-18v52.999999h19q33 0 33-27.999999z m49.999999-325.999997h200.999998L583.999999 110.000009z m168.999999 344.999997v86.999999q-22 22-61 22-41 0-66.499999-24.5T599.999999 671.000003q0-45 26-70.999999t64.999999-26q35 0 59 24l-23 29q-16-15-34-15-21 0-33.999999 15.5T645.999999 670.000003q0 57.999999 49.999999 58 12 0 18-5v-30h-30v-35.999999z" p-id="4657"></path></svg>
        );

        const BringToFrontSvg = () => (
            <svg t="1584236254346" fill="currentColor" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="5281" width="18" height="18"><path d="M1008.336 623.712l-140.176-73.32-73.424 38.464 114.544 59.936-375.64 196.36a45.08 45.08 0 0 1-41.68 0L116.136 648.736l114.592-59.944-73.424-38.352-140.224 73.328a28.16 28.16 0 0 0-14.976 25.072 28.16 28.16 0 0 0 14.976 24.96l444.28 232.272a111.072 111.072 0 0 0 102.744 0l444.232-232.272a28.192 28.192 0 0 0 14.968-24.96 28.32 28.32 0 0 0-14.968-25.032v-0.096z m0 0" p-id="5282"></path><path d="M17.08 400.312L461.36 632.592a111.072 111.072 0 0 0 102.744 0l444.232-232.28a28.208 28.208 0 0 0 14.968-24.968 28.192 28.192 0 0 0-14.968-25.024L564.104 118a110.744 110.744 0 0 0-102.744-0.04L17.08 350.272a28.288 28.288 0 0 0 0 50.04z m0 0" p-id="5283"></path></svg>
        );

        const BringToBackSvg = () => (
            <svg t="1584236254346" transform="rotate(180)" fill="currentColor" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="5281" width="18" height="18"><path d="M1008.336 623.712l-140.176-73.32-73.424 38.464 114.544 59.936-375.64 196.36a45.08 45.08 0 0 1-41.68 0L116.136 648.736l114.592-59.944-73.424-38.352-140.224 73.328a28.16 28.16 0 0 0-14.976 25.072 28.16 28.16 0 0 0 14.976 24.96l444.28 232.272a111.072 111.072 0 0 0 102.744 0l444.232-232.272a28.192 28.192 0 0 0 14.968-24.96 28.32 28.32 0 0 0-14.968-25.032v-0.096z m0 0" p-id="5282"></path><path d="M17.08 400.312L461.36 632.592a111.072 111.072 0 0 0 102.744 0l444.232-232.28a28.208 28.208 0 0 0 14.968-24.968 28.192 28.192 0 0 0-14.968-25.024L564.104 118a110.744 110.744 0 0 0-102.744-0.04L17.08 350.272a28.288 28.288 0 0 0 0 50.04z m0 0" p-id="5283"></path></svg>
        );

        const CloseSvg = () => (
            <svg viewBox="0 0 14 12" width="14" height="12" fill="currentColor"><path fill-rule="evenodd" d="M6.11017 11.85L6.82486 11.15C6.92014 11.0561 6.97373 10.9283 6.97373 10.795C6.97373 10.6617 6.92014 10.5339 6.82486 10.44L2.34549 5.99997L6.82486 1.55997C6.91954 1.46781 6.9729 1.34166 6.9729 1.20997C6.9729 1.07828 6.91954 0.952138 6.82486 0.859974L6.11017 0.149974C6.01567 0.055318 5.88703 0.0020752 5.75283 0.0020752C5.61863 0.0020752 5.48999 0.055318 5.39549 0.149974L0.221574 5.27997C0.0817733 5.4217 0.00244128 5.61156 0.00012207 5.80997V6.18997C0.000298277 6.38882 0.0799529 6.57946 0.221574 6.71997L5.39549 11.85C5.48999 11.9446 5.61863 11.9979 5.75283 11.9979C5.88703 11.9979 6.01567 11.9446 6.11017 11.85ZM13.1365 11.85L13.8512 11.15C13.9465 11.0561 14.0001 10.9283 14.0001 10.795C14.0001 10.6617 13.9465 10.5339 13.8512 10.44L9.37186 5.99997L13.8512 1.55997C13.9459 1.46781 13.9993 1.34166 13.9993 1.20997C13.9993 1.07828 13.9459 0.952138 13.8512 0.859974L13.1365 0.149974C13.042 0.055318 12.9134 0.0020752 12.7792 0.0020752C12.645 0.0020752 12.5164 0.055318 12.4219 0.149974L7.24794 5.27997C7.10814 5.4217 7.02881 5.61156 7.02649 5.80997V6.18997C7.02667 6.38882 7.10632 6.57946 7.24794 6.71997L12.4219 11.85C12.5164 11.9446 12.645 11.9979 12.7792 11.9979C12.9134 11.9979 13.042 11.9446 13.1365 11.85Z"></path></svg>
        );

        const IconGoogleDrive = props => <Icon component={SvgGoogleDrive} {...props} />;
        const IconBar = props => <Icon component={BarSvg} {...props} />;
        const HideCollapsedIcon = props => <Icon component={HideCollapsedSvg} {...props} />;
        const TemplateIcon = props => <Icon component={TemplateSvg} {...props} />;
        const ShapeIcon = props => <Icon component={ShapeSvg} {...props} />;
        const IconIcon = props => <Icon component={IconSvg} {...props} />;
        const FontIcon = props => <Icon component={FontSvg} {...props} />;
        const FontSmallIcon = props => <Icon component={FontSmallSvg} {...props} />;
        const FontLargeIcon = props => <Icon component={FontLargeSvg} {...props} />;
        const ThemeIcon = props => <Icon component={ThemeSvg} {...props} />;
        const LayoutIcon = props => <Icon component={LayoutSvg} {...props} />;
        const TextIcon = props => <Icon component={TextSvg} {...props} />;
        const ImageIcon = props => <Icon component={ImageSvg} {...props} />;
        const BackgroundIcon = props => <Icon component={BackgroundSvg} {...props} />;
        const RemoveIcon = props => <Icon component={RemoveSvg} {...props} />;
        const KeyValueIcon = props => <Icon component={KeyValueSvg} {...props} />;
        const LockIcon = props => <Icon component={LockSvg} {...props} />;
        const UnlockIcon = props => <Icon component={UnlockSvg} {...props} />;
        const OpacityIcon = props => <Icon component={OpacitySvg} {...props} />;
        const IconUndo = props => <Icon component={SvgUndo} {...props} />;
        const IconRedo = props => <Icon component={SvgRedo} {...props} />;
        const StrokeIcon = props => <Icon component={StrokeSvg} {...props} />;
        const ShadowIcon = props => <Icon component={ShadowSvg} {...props} />;
        const FillColorIcon = props => <Icon component={FillColorSvg} {...props} />;
        const MoreIcon = props => <Icon component={MoreSvg} {...props} />;
        const PdfIcon = props => <Icon component={PdfSvg} {...props} />;
        const PngIcon = props => <Icon component={PngSvg} {...props} />;
        const JpgIcon = props => <Icon component={JpgSvg} {...props} />;
        const IconSave = props => <Icon component={SvgSave} {...props} />;
        const BringToFrontIcon = props => <Icon component={BringToFrontSvg} {...props} />;
        const BringToBackIcon = props => <Icon component={BringToBackSvg} {...props} />;
        const CloseIcon = props => <Icon component={CloseSvg} {...props} />;
        const CircleIcon = props => <Icon component={CircleSvg} {...props} />;

        // Component Function

        const { SubMenu } = Menu;
        const { Title, Text } = Typography;
        const menuChangeOpacity = (
            <Menu className="row align-items-center px-2" style={{ minWidth: 250 }}>
                <div>
                    <span>{t('lbl_opacity')}</span>
                </div>
                <div class="col-7">
                    <Slider step={1} value={template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].opacity ? template.elements[selectedPage][selectedElementIndex].opacity * 100 : 100} onChange={value => changeOpacity(value)} tooltipVisible={false} />
                </div>
                <div >
                    <span>{template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].opacity ? Math.trunc(template.elements[selectedPage][selectedElementIndex].opacity * 100) : 100}%</span>
                </div>
            </Menu>
        );

        const menuStroke = (
            <Menu>
                { template && template.theme.primary && (
                    <Menu.Item className="d-flex align-items-center" onClick={() => changeStrokeColor('primary', template.theme.primary)}>
                        <CircleIcon style={{border: '1px solid #ddd', borderRadius: '50%', color: template.theme.primary}} />
                        {template.theme.primary}
                    </Menu.Item>
                )}
                { template && template.theme.secondary && (
                    <Menu.Item className="d-flex align-items-center" onClick={() => changeStrokeColor('secondary', template.theme.secondary)}>
                        <CircleIcon style={{border: '1px solid #ddd', borderRadius: '50%', color: template.theme.secondary}} />
                        {template.theme.secondary}
                    </Menu.Item>
                )}
                { template && template.theme.info && (
                    <Menu.Item className="d-flex align-items-center" onClick={() => changeStrokeColor('info', template.theme.info)}>
                        <CircleIcon style={{border: '1px solid #ddd', borderRadius: '50%', color: template.theme.info}} />
                        {template.theme.info}
                    </Menu.Item>
                )}
                { template && template.theme.body && (
                    <Menu.Item className="d-flex align-items-center" onClick={() => changeStrokeColor('body', template.theme.body)}>
                        <CircleIcon style={{border: '1px solid #ddd', borderRadius: '50%', color: template.theme.body}} />
                        {template.theme.body}
                    </Menu.Item>
                )}
                { template && template.theme.dark && (
                    <Menu.Item className="d-flex align-items-center" onClick={() => changeStrokeColor('dark', template.theme.dark)}>
                        <CircleIcon style={{border: '1px solid #ddd', borderRadius: '50%', color: template.theme.dark}} />
                        {template.theme.dark}
                    </Menu.Item>
                )}
                { template && template.theme.light && (
                    <Menu.Item className="d-flex align-items-center" onClick={() => changeStrokeColor('light', template.theme.light)}>
                        <CircleIcon style={{border: '1px solid #ddd', borderRadius: '50%', color: template.theme.light}} />
                        {template.theme.light}
                    </Menu.Item>
                )}
                { template && template.theme.white && (
                    <Menu.Item className="d-flex align-items-center" onClick={() => changeStrokeColor('white', template.theme.white)}>
                        <CircleIcon style={{border: '1px solid #ddd', borderRadius: '50%', color: template.theme.white}} />
                        {template.theme.white}
                    </Menu.Item>
                )}
            </Menu>
        );
        
        const changeStrokeWidth = (value) => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            template.elements[selectedPage][selectedElementIndex].strokeWidth = value;
            if (!template.elements[selectedPage][selectedElementIndex].stroke) {
                template.elements[selectedPage][selectedElementIndex].stroke = '#000000';
            }
            this.setState({ template });
        }

        const menuChangeStroke = (
            <Menu style={{ minWidth: 270 }}>
                <Menu.Item disabled className="p-0 m-0">
                    <List className="px-3">
                        <List.Item className="border-0 p-0">
                            <Row gutter={24} className="mx-0">
                                <Col span={18} className="text-left px-0"><p>{t('lbl_stroke_color')}</p></Col>
                                <Col span={6} className="px-2">
                                    <Dropdown overlay={menuStroke}>
                                        <Button type="basic" style={{ backgroundColor: template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].stroke ? template.elements[selectedPage][selectedElementIndex].stroke : '#000000' }} onClick={() => this.setState({ displayStrokePicker: !this.state.displayStrokePicker })}><Icon type="bg-colors" style={{ fontSize: 22, color: tinycolor(template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].stroke ? template.elements[selectedPage][selectedElementIndex].stroke : '#000000').isLight() ? '#000000' : '#ffffff' }} /></Button>
                                    </Dropdown>
                                </Col>
                            </Row>
                        </List.Item>
                        <List.Item className="border-0 p-0">
                            <Row gutter={24} className="mx-0 w-100">
                                <Col span={10} className="px-0"><p>{t('lbl_stroke_width')}</p></Col>
                                <Col span={12} className="px-0">
                                    <Slider
                                        min={0}
                                        max={50}
                                        tooltipVisible={false}
                                        className="mt-1 mb-3 "
                                        value={template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].strokeWidth ? template.elements[selectedPage][selectedElementIndex].strokeWidth : 0}
                                        onChange={value => changeStrokeWidth(value)}
                                    />
                                </Col>
                                <Col span={2} className="px-0 text-right">
                                    <p className="mb-0">{template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].strokeWidth ? template.elements[selectedPage][selectedElementIndex].strokeWidth : 0}</p>
                                </Col>
                            </Row>
                        </List.Item>
                    </List>
                </Menu.Item>
            </Menu>
        );
        const menuChangeShadow = (
            <Menu style={{ minWidth: 270 }}>
                <Menu.Item disabled className="p-0 m-0">
                    <List className="px-3">
                        <List.Item className="border-0 p-0">
                            <Row gutter={24} className="mx-0 mt-2">
                                <Col span={18} className="px-0"><p>{t('lbl_shadow_color')}</p></Col>
                                <Col span={6} className="px-0">
                                    <Button type="basic" style={{ backgroundColor: template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].shadowColor ? template.elements[selectedPage][selectedElementIndex].shadowColor : '#000000' }} onClick={() => this.setState({ displayShadowPicker: !this.state.displayShadowPicker })}><Icon type="bg-colors" style={{ fontSize: 22, color: tinycolor(template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].shadowColor ? template.elements[selectedPage][selectedElementIndex].shadowColor : '#000000').isLight() ? '#000000' : '#ffffff' }} /></Button>
                                    {this.state.displayShadowPicker ?
                                        <div style={styles.popover}>
                                            <div style={styles.cover} onClick={() => this.setState({ displayShadowPicker: false })} />
                                            <SketchPicker color={template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].shadowColor ? template.elements[selectedPage][selectedElementIndex].shadowColor : '#000000'} onChange={(color) => changeShadowColor(color.hex)} />
                                        </div>
                                        : null
                                    }
                                </Col>
                            </Row>
                        </List.Item>
                        <List.Item className="border-0 p-0">
                            <Row gutter={24} className="mx-0 w-100">
                                <Col span={6} className="px-0"><p>{t('lbl_shadow_blur')}</p></Col>
                                <Col span={18} className="px-2">
                                    <Slider
                                        min={0}
                                        max={50}
                                        tooltipVisible={false}
                                        value={template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].shadowBlur ? template.elements[selectedPage][selectedElementIndex].shadowBlur : 0}
                                        onChange={value => changeShadowBlur(value)}
                                        className="mt-1 mb-3 "
                                    />
                                </Col>
                            </Row>
                        </List.Item>
                        <List.Item className="border-0 p-0">
                            <Row gutter={24} className="mx-0 w-100">
                                <Col span={6} className="px-0"><p>{t('X')}</p></Col>
                                <Col span={18} className="px-2">
                                    <Slider
                                        min={-50}
                                        max={50}
                                        tooltipVisible={false}
                                        className="mt-1 mb-3 "
                                        value={template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].shadowOffsetX ? template.elements[selectedPage][selectedElementIndex].shadowOffsetX : 0}
                                        onChange={value => changeShadowOffsetX(value)}
                                    />
                                </Col>
                            </Row>
                        </List.Item>
                        <List.Item className="border-0 p-0">
                            <Row gutter={24} className="mx-0 w-100">
                                <Col span={6} className="px-0"><p>{t('Y')}</p></Col>
                                <Col span={18} className="px-2">
                                    <Slider
                                        min={-50}
                                        max={50}
                                        tooltipVisible={false}
                                        className="mt-1 mb-3 "
                                        value={template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].shadowOffsetY ? template.elements[selectedPage][selectedElementIndex].shadowOffsetY : 0}
                                        onChange={value => changeShadowOffsetY(value)}
                                    />
                                </Col>
                            </Row>
                        </List.Item>
                        <List.Item className="border-0 p-0">
                            <Row gutter={24} className="mx-0 w-100">
                                <Col span={6} className="px-0"><p>{t('lbl_opacity')}</p></Col>
                                <Col span={18} className="px-2">
                                    <Slider
                                        min={0}
                                        max={100}
                                        tooltipVisible={false}
                                        step={1}
                                        value={template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].shadowOpacity ? template.elements[selectedPage][selectedElementIndex].shadowOpacity * 100 : 100}
                                        className="mt-1 mb-3 "
                                        onChange={value => changeShadowOpacity(value)}
                                    />
                                </Col>
                            </Row>
                        </List.Item>
                    </List>
                </Menu.Item>
            </Menu>
        );
        const menuChangeSpacing = (
            <Menu style={{ minWidth: 310 }}>
                <Menu.Item disabled className="p-0 m-0">
                    <List className="px-3">
                        <List.Item className="border-0 p-0">
                            <Row gutter={24} className="mx-0 w-100 mt-2">
                                <Col span={6} className="px-0"><p>{t('lbl_letter_spacing')}</p></Col>
                                <Col span={15} className="px-2">
                                    <Slider
                                        min={-50}
                                        max={200}
                                        marks={{ 0: '' }}
                                        value={template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].letterSpacing ? template.elements[selectedPage][selectedElementIndex].letterSpacing : 0}
                                        tooltipVisible={false}
                                        onChange={value => changeLetterSpacing(value)}
                                        className="mt-1 mb-3 "
                                    />
                                </Col>
                                <Col span={2} className="px-0">
                                    {template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].letterSpacing ? template.elements[selectedPage][selectedElementIndex].letterSpacing : 0}
                                </Col>
                            </Row>
                        </List.Item>
                        <List.Item className="border-0 p-0">
                            <Row gutter={24} className="mx-0 w-100">
                                <Col span={6} className="px-0"><p>{t('lbl_line_height')}</p></Col>
                                <Col span={15} className="px-2">
                                    <Slider
                                        min={0}
                                        max={20}
                                        step={0.01}
                                        value={template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].lineHeight ? template.elements[selectedPage][selectedElementIndex].lineHeight : 1}
                                        tooltipVisible={false}
                                        onChange={value => changeLinehightSpacing(value)}
                                        className="mt-1 mb-3 "
                                    />
                                </Col>
                                <Col span={2} className="px-0">
                                    {template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].lineHeight ? template.elements[selectedPage][selectedElementIndex].lineHeight : 1}
                                </Col>
                            </Row>
                        </List.Item>
                    </List>
                </Menu.Item>
            </Menu>
        );

        const MenuLanguage = (
            <Menu selectable={false}>
                <Menu.Item className="py-2 d-flex align-items-center" onClick={() => this.changeLanguage('th-TH')}>
                    <img src={require("../assets/flags/th-TH.svg")} style={{ width: 25, alignSelf: 'center' }} />&nbsp;
                {t('lbl_thai')}
                </Menu.Item>
                <Menu.Item className="py-2 d-flex align-items-center" onClick={() => this.changeLanguage('en-US')}>
                    <img src={require("../assets/flags/en-US.svg")} style={{ width: 25 }} />&nbsp;
                {t('lbl_english')}
                </Menu.Item>
            </Menu>
        );

        const listAccount = (
            <List
                itemLayout="horizontal"
            >
                <List.Item>
                    <List.Item.Meta
                        avatar={<Avatar name={userInfo && (userInfo.firstname + ' ' + userInfo.lastname)} round={true} size={50} src={userInfo && userInfo.photoUrl} />}
                        title={userInfo && (userInfo.firstname + ' ' + userInfo.lastname)}
                        description={userInfo && userInfo.email}
                    />
                </List.Item>
            </List>
        );

        const MenuAccount = (
            <Menu>
                <Menu.ItemGroup title={listAccount}>
                    <Menu.Item key="/profile" className="d-flex align-items-center">
                        <Link to="/profile">
                            <Icon type="setting" style={{ fontSize: 18 }} className="mr-2" />
                            {t('lbl_account_setting')}
                        </Link>
                    </Menu.Item>
                    {(userInfo && userInfo.providerId === 'password') &&
                        <Menu.Item key="/changePassword" className="d-flex align-items-center">
                            <Link to="/changePassword">
                                <Icon type="lock" style={{ fontSize: 18 }} className="mr-2" />
                                {t('lbl_change_password')}
                            </Link>
                        </Menu.Item>
                    }
                    <Menu.Item key="logout" className="d-flex align-items-center" onClick={this.handleLogOut}>
                        <Icon type="logout" style={{ fontSize: 18 }} />
                        {t("lbl_sign_out")}
                    </Menu.Item>
                </Menu.ItemGroup>
            </Menu>
        );

        const menuSizeZoomable = (
            <Menu>
                <Menu.Item key="size_100" className="d-flex align-items-center" onClick={() => this.changeScalePaper(1)}>
                    {t('100%')}
                </Menu.Item>
                <Menu.Item key="size_75" className="d-flex align-items-center" onClick={() => this.changeScalePaper(0.75)}>
                    {t('75%')}
                </Menu.Item>
                <Menu.Item key="size_50" className="d-flex align-items-center" onClick={() => this.changeScalePaper(0.5)}>
                    {t('50%')}.
                </Menu.Item>
            </Menu>
        );
        const MenuDownload = (
            <Menu selectable={false}>
                <Menu.Item key="pdfFile" className="d-flex align-items-center" onClick={() => this.downloadDocument('PDF')}>
                    <PdfIcon />
                    PDF
                </Menu.Item>
                <Menu.Item key="pngFile" className="d-flex align-items-center" onClick={() => this.downloadDocument('PNG')}>
                    <PngIcon />
                    PNG
                </Menu.Item>
                <Menu.Item key="jpgFile" className="d-flex align-items-center" onClick={() => this.downloadDocument('JPG')}>
                    <JpgIcon />
                    JPG
                </Menu.Item>
            </Menu>
        );

        const MenuGoogleDrive = (
            <Menu className="p-5">
                <p style={{ fontSize: 18 }} className="text-center">
                    <IconGoogleDrive className="position-relative mr-2" style={{ top: -4 }} />
                    {t('lbl_save_to_google_drive')}
                </p>
                <p style={{ fontSize: 14 }} className="mt-5">{t('lbl_file_name')}</p>
                <Input
                    placeholder={t('lbl_file_name')}
                    value={fileName}
                    onChange={e => this.setState({ fileName: e.target.value })}
                />
                <p style={{ fontSize: 14 }} className="mt-4">{t('lbl_file_type')}</p>
                <Select value={fileType} style={{ width: 150 }} onChange={value => this.setState({ fileType: value })}>
                    <Select.Option value="PDF">PDF</Select.Option>
                    <Select.Option value="PNG">PNG</Select.Option>
                    <Select.Option value="JPG">JPG</Select.Option>
                </Select>
                <div className="mt-4">
                    <Button disabled={fileName == '' || fileType == '' ? true : false} type="primary" className="mr-2" onClick={() => { this.setState({ isModalGoogleDrive: false }); this.handleSaveToDrive() }}>{t('lbl_save')}</Button>
                    <Button type="basic" onClick={() => this.setState({ isModalGoogleDrive: false, accessToken: null })}>{t('lbl_cancel')}</Button>
                </div>
            </Menu>
        );
        
        const MenuSelectKey = ([
            <p style={{ fontSize: 18 }} >
                <KeyValueIcon className="position-relative mr-2" style={{top: -7}}/>
                {t('lbl_select_key')}
            </p>,
            <Scrollbars style={{height: 300}}>
                <Menu selectedKeys={template && selectedPage >= 0 && template.elements[selectedPage][selectedElementIndex].key && template.elements[selectedPage][selectedElementIndex].key}>
                    {this.state.key.map(item => (
                        <Menu.Item 
                            key={item} 
                            className="py-0"
                            onClick={e => {
                                const { template } = this.state;
                                if(template && selectedPage >= 0 ) {
                                    template.elements[selectedPage][selectedElementIndex].key = e.key;
                                    this.setState({template, selectKeyVisible: false})
                                }
                            }}
                        >
                            {item}
                        </Menu.Item>
                    )) }
                </Menu>
            </Scrollbars>
        ]);

        const MenuMore = (
            <Menu selectable={false}>
                <Menu.Item className="d-flex align-items-center" onClick={() => this.handleSave()}>
                    <Icon type="save" style={{ fontSize: 22 }} />
                    {t("lbl_save")}
                </Menu.Item>
                <Menu.Item className="d-flex align-items-center" onClick={() => this.setState({ isModalFileDownload: true, isDrawerOpenOption: false })}>
                    <Icon type="cloud-download" style={{ fontSize: 22 }} />
                    {t("lbl_download")}
                </Menu.Item>
                <Modal
                    visible={this.state.isModalFileDownload}
                    title={[<Icon type="cloud-download" style={{ fontSize: 22 }} className="mr-2" />, t("lbl_download")]}
                    onCancel={() => this.setState({ isModalFileDownload: false })}
                    footer={null}
                    closable={true}
                    bodyStyle={{ padding: 0 }}
                >
                    {MenuDownload}
                </Modal>
                <Menu.Item className="d-flex align-items-center p-0 m-0" >
                    <GoogleLogin
                        clientId="112794756574-8gjk6lscsnkak7da0tpsgpmrej3tfaob.apps.googleusercontent.com"
                        render={renderProps => (
                            <Button type="link" className="text-left" style={{ color: 'rgba(0, 0, 0, 0.65)' }} onClick={renderProps.onClick} block >
                                <IconGoogleDrive className="position-relative mr-1" style={{ top: -3 }} />
                                {t("lbl_save_to_google_drive")}
                            </Button>
                        )}
                        onSuccess={this.responseGoogleDrive}
                        onFailure={res => console.log(res)}
                        cookiePolicy={'single_host_origin'}
                        scope="https://www.googleapis.com/auth/drive"
                    />
                </Menu.Item>
                <Menu.Item className="d-flex align-items-center" onClick={() => this.setState({ isModalChangeLanguage: true, isDrawerOpenOption: false })}>
                    <Icon type="global" style={{ fontSize: 22 }} />
                    {t("lbl_language")}
                </Menu.Item>
            </Menu>
        );

        const inputProjectName = (
            <Input
                placeholder={t('lbl_untitled')}
                suffix={
                    <Tooltip title={t('lbl_project_name')}>
                        <Icon type="edit" style={{ color: 'rgba(0,0,0,.45)' }} />
                    </Tooltip>
                }
                value={template ? template.name : ''}
                onChange={this.setProjectName}
                onBlur={this.setName}
            />
        );

        const sidebar = ([
            // <Sider theme='light' trigger={null} width={80} className="position-fixed disign-sidebar" style={{minHeight: '100vh', zIndex: 3, left: 0}}>
            //     <Menu theme="light" selectedKeys={selectedKey} inlineCollapsed={true} onClick={this.MenuSidebarClick}>
            //         <Menu.Item key="1" className="text-center position-relative" style={{minHeight: 66}}>
            //             <TemplateIcon className="position-relative mr-0" style={{fontSize: 20, top: -10}}/>
            //             <p className="m-0 position-relative" style={{opacity: 1, lineHeight: 0, top: -7}}>{t('lbl_template')}</p>
            //         </Menu.Item>
            //         <Menu.Item key="2" className="text-center position-relative" style={{minHeight: 66}}>
            //             <FontIcon className="position-relative mr-0" style={{fontSize: 20, top: -10}}/>
            //             <p className="m-0 position-relative" style={{opacity: 1, lineHeight: 0, top: -7}}>{t('lbl_fonts')}</p>
            //         </Menu.Item>
            //         <Menu.Item key="3" className="text-center position-relative" style={{minHeight: 66}}>
            //             <ThemeIcon className="position-relative mr-0" style={{fontSize: 20, top: -10}}/>
            //             <p className="m-0 position-relative" style={{opacity: 1, lineHeight: 0, top: -7}}>{t('lbl_theme')}</p>
            //         </Menu.Item>
            //         <Menu.Item key="4" className="text-center position-relative" style={{minHeight: 66}}>
            //             <LayoutIcon className="position-relative mr-0" style={{fontSize: 20, top: -10}}/>
            //             <p className="m-0 position-relative" style={{opacity: 1, lineHeight: 0, top: -7}}>{t('lbl_layout')}</p>
            //         </Menu.Item>
            //     </Menu>
            // </Sider>,
            // <Sider theme='light' trigger={null} width={sizeMobile ? visible ? 280 : 0 : 300} collapsible collapsed={collapsed && !sizeMobile} className="position-fixed p-3" style={{minHeight: '100vh', zIndex: 2, left: 0, border: '1px solid #ddd', marginLeft: collapsed ? 0 : 80}}>
            //      {!collapsed && !sizeMobile &&<Icon type="close" onClick={this.MenuSidebarHide} className="position-absolute" style={{fontSize: 22, cursor: 'pointer', top: 18, right: 12, zIndex: 2}}/>}
            //     { selectedKey === "1" &&
            //         <div>
            //             <h5 className="font-weight-bold">{t('lbl_template')}</h5>
            //         </div>
            //     }
            //     { selectedKey === "2" &&
            //         <div>
            //             <h5 className="font-weight-bold">{t('lbl_fonts')}</h5>
            //             <Tabs className="tabs-edit" activeKey={this.state.tabEditFontKey} onChange={key => this.setState({tabEditFontKey: key})}>
            //                 <Tabs.TabPane tab={t('lbl_font')} key="1">
            //                     <Row gutter={24} className="mx-0 px-1">
            //                         {this.state.isHeaderSize && [
            //                             <p className="text-dark mb-1">{t('lbl_header_font')}</p>,
            //                             <Col span={24} className="px-0">
            //                                 <Select
            //                                     className="w-100"
            //                                     showSearch
            //                                     optionFilterProp="children"
            //                                     notFoundContent={
            //                                         <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} className="m-0" description={t('lbl_no_font')} />
            //                                     }
            //                                     filterOption={(input, option) =>
            //                                         option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            //                                     }
            //                                     value={this.state.headerFont}
            //                                     onChange={value => changeFontsFamily('header', value)}
            //                                 >
            //                                     {this.state.font.map(item =>
            //                                         <Select.Option value={item}>{item}</Select.Option>
            //                                     )}
            //                                 </Select>
            //                             </Col>
            //                         ]}
            //                         {this.state.isSubheaderSize && [
            //                             <Col span={24} className="px-0 mt-3">
            //                                 <p className="text-dark mb-1">{t('lbl_subheader_font')}</p>
            //                             </Col>,
            //                             <Col span={24} className="px-0">
            //                                 <Select
            //                                     className="w-100"
            //                                     showSearch
            //                                     optionFilterProp="children"
            //                                     notFoundContent={
            //                                         <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} className="m-0" description={t('lbl_no_font')} />
            //                                     }
            //                                     filterOption={(input, option) =>
            //                                         option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            //                                     }
            //                                     value={this.state.subheaderFont}
            //                                     onChange={value => changeFontsFamily('subheader', value)}
            //                                 >
            //                                     {this.state.font.map(item =>
            //                                         <Select.Option value={item}>{item}</Select.Option>
            //                                     )}
            //                                 </Select>
            //                             </Col>
            //                         ]}
            //                         {this.state.isTitleSize && [
            //                             <Col span={24} className="px-0 mt-3">
            //                                 <p className="text-dark mb-1">{t('lbl_title_font')}</p>
            //                             </Col>,
            //                             <Col span={24} className="px-0">
            //                                 <Select
            //                                     className="w-100"
            //                                     showSearch
            //                                     optionFilterProp="children"
            //                                     notFoundContent={
            //                                         <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} className="m-0" description={t('lbl_no_font')} />
            //                                     }
            //                                     filterOption={(input, option) =>
            //                                         option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            //                                     }
            //                                     value={this.state.titleFont}
            //                                     onChange={value => changeFontsFamily('title', value)}
            //                                 >
            //                                     {this.state.font.map(item =>
            //                                         <Select.Option value={item}>{item}</Select.Option>
            //                                     )}
            //                                 </Select>
            //                             </Col>
            //                         ]}
            //                         {this.state.isSubtileSize && [
            //                             <Col span={24} className="px-0 mt-3">
            //                                 <p className="text-dark mb-1">{t('lbl_subtitle_font')}</p>
            //                             </Col>,
            //                             <Col span={24} className="px-0">
            //                                 <Select
            //                                     className="w-100"
            //                                     showSearch
            //                                     optionFilterProp="children"
            //                                     notFoundContent={
            //                                         <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} className="m-0" description={t('lbl_no_font')} />
            //                                     }
            //                                     filterOption={(input, option) =>
            //                                         option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            //                                     }
            //                                     value={this.state.subtileFont}
            //                                     onChange={value => changeFontsFamily('subtitle', value)}
            //                                 >
            //                                     {this.state.font.map(item =>
            //                                         <Select.Option value={item}>{item}</Select.Option>
            //                                     )}
            //                                 </Select>
            //                             </Col>
            //                         ]}
            //                         {this.state.isBodySize && [
            //                             <Col span={24} className="px-0 mt-3">
            //                                 <p className="text-dark mb-1">{t('lbl_body_font')}</p>
            //                             </Col>,
            //                             <Col span={24} className="px-0">
            //                                 <Select
            //                                     className="w-100"
            //                                     showSearch
            //                                     optionFilterProp="children"
            //                                     notFoundContent={
            //                                         <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} className="m-0" description={t('lbl_no_font')} />
            //                                     }
            //                                     filterOption={(input, option) =>
            //                                         option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            //                                     }
            //                                     value={this.state.bodyFont}
            //                                     onChange={value => changeFontsFamily('body', value)}
            //                                 >
            //                                     {this.state.font.map(item =>
            //                                         <Select.Option value={item}>{item}</Select.Option>
            //                                     )}
            //                                 </Select>
            //                             </Col>
            //                         ]}
            //                     </Row>
            //                 </Tabs.TabPane>
            //                 <Tabs.TabPane tab={t('lbl_font_size')} key="2">
            //                     <Row gutter={24} className="mx-0 px-1">
            //                         {this.state.isHeaderSize && [
            //                             <p className="text-dark mb-1">{t('lbl_header_size')}: <span className="font-weight-bold">{this.state.headerSize+'px'}</span></p>,
            //                             <div className="row align-items-center w-100 mx-0">
            //                                 <div>
            //                                     <FontSmallIcon />
            //                                 </div>
            //                                 <div class="col-9">
            //                                     <Slider value={this.state.headerSize} dots={true} step={1} min={8} max={150} onChange={value => changeFontsSize('header', value)} tooltipVisible={false}/>
            //                                 </div>
            //                                 <div >
            //                                     <FontLargeIcon />
            //                                 </div>
            //                             </div>
            //                         ]}
            //                         {this.state.isSubheaderSize && [
            //                             <Col span={24} className="px-0 mt-3">
            //                                 <p className="text-dark mb-1">{t('lbl_subheader_size')}: <span className="font-weight-bold">{this.state.subheaderSize+'px'}</span></p>
            //                             </Col>,
            //                             <Col span={24} className="px-0">
            //                                 <div className="row align-items-center w-100 mx-0">
            //                                     <div>
            //                                         <FontSmallIcon />
            //                                     </div>
            //                                     <div class="col-9">
            //                                         <Slider value={this.state.subheaderSize} dots={true} step={1} min={8} max={150} onChange={value => changeFontsSize('subheader', value)} tooltipVisible={false}/>
            //                                     </div>
            //                                     <div >
            //                                         <FontLargeIcon />
            //                                     </div>
            //                                 </div>
            //                             </Col>
            //                         ]}
            //                         {this.state.isTitleSize && [
            //                             <Col span={24} className="px-0 mt-3">
            //                                 <p className="text-dark mb-1">{t('lbl_title_size')}: <span className="font-weight-bold">{this.state.titleSize+'px'}</span></p>
            //                             </Col>,
            //                             <Col span={24} className="px-0">
            //                                 <div className="row align-items-center w-100 mx-0">
            //                                     <div>
            //                                         <FontSmallIcon />
            //                                     </div>
            //                                     <div class="col-9">
            //                                         <Slider value={this.state.titleSize} dots={true} step={1} min={8} max={150} onChange={value => changeFontsSize('title', value)} tooltipVisible={false}/>
            //                                     </div>
            //                                     <div >
            //                                         <FontLargeIcon />
            //                                     </div>
            //                                 </div>
            //                             </Col>
            //                         ]}
            //                         {this.state.isSubtileSize && [
            //                             <Col span={24} className="px-0 mt-3">
            //                                 <p className="text-dark mb-1">{t('lbl_subtitle_size')}: <span className="font-weight-bold">{this.state.subtileSize+'px'}</span></p>
            //                             </Col>,
            //                             <Col span={24} className="px-0">
            //                                 <div className="row align-items-center w-100 mx-0">
            //                                     <div>
            //                                         <FontSmallIcon />
            //                                     </div>
            //                                     <div class="col-9">
            //                                         <Slider value={this.state.subtileSize} dots={true} step={1} min={8} max={150} onChange={value => changeFontsSize('subtitle', value)} tooltipVisible={false}/>
            //                                     </div>
            //                                     <div >
            //                                         <FontLargeIcon />
            //                                     </div>
            //                                 </div>
            //                             </Col>
            //                         ]}
            //                         {this.state.isBodySize && [
            //                             <Col span={24} className="px-0 mt-3">
            //                                 <p className="text-dark mb-1">{t('lbl_body_size')}: <span className="font-weight-bold">{this.state.bodySize+'px'}</span></p>
            //                             </Col>,
            //                             <Col span={24} className="px-0">
            //                                 <div className="row align-items-center w-100 mx-0">
            //                                     <div>
            //                                         <FontSmallIcon />
            //                                     </div>
            //                                     <div class="col-9">
            //                                         <Slider value={this.state.bodySize} dots={true} step={1} min={8} max={150} onChange={value => changeFontsSize('body', value)} tooltipVisible={false}/>
            //                                     </div>
            //                                     <div >
            //                                         <FontLargeIcon />
            //                                     </div>
            //                                 </div>
            //                             </Col>
            //                         ]}
            //                     </Row>
            //                 </Tabs.TabPane>
            //             </Tabs>
            //         </div>
            //     }
            //     { selectedKey === "3" &&
            //         <div>
            //             <h5 className="font-weight-bold">{t('lbl_theme')}</h5>
            //             <Row gutter={24} type="flex" className="mx-0">
            //                 {  this.state.theme &&  this.state.theme.map(item => (
            //                     <Col span={6} className="p-3">
            //                         <Button shape="circle" size="large" style={{background: item.primary}} onClick={() => this.changeTheme(item._id)}/>
            //                     </Col>
            //                 ))}
            //             </Row>
            //         </div>
            //     }
            //     { selectedKey === "4" &&
            //         <div>
            //             <h5 className="font-weight-bold">{t('lbl_layout')}</h5>
            //         </div>
            //     }
            // </Sider>,
            <Sider theme='dark' trigger={null} width={80} className="position-fixed create-template-sidebar" style={{ minHeight: '100vh', zIndex: 3, left: 0 }}>
                <Menu theme="dark" selectedKeys={selectedKey} inlineCollapsed={true} onClick={this.MenuSidebarClick}>
                    <Menu.Item key="0" className="text-center position-relative" style={{ minHeight: 66 }}>
                        <ThemeIcon className="position-relative mr-0" style={{ fontSize: 20, top: -10 }} />
                        <p className="m-0 position-relative" style={{ opacity: 1, lineHeight: 0, top: -7 }}>{t('lbl_theme')}</p>
                    </Menu.Item>
                    <Menu.Item key="1" className="text-center position-relative" style={{ minHeight: 66 }}>
                        <ShapeIcon className="position-relative mr-0" style={{ fontSize: 20, top: -10 }} />
                        <p className="m-0 position-relative" style={{ opacity: 1, lineHeight: 0, top: -7, fontSize: this.props.i18n.language != 'en-US' && 12 }}>{t('lbl_objects')}</p>
                    </Menu.Item>
                    <Menu.Item key="2" className="text-center position-relative" style={{ minHeight: 66 }}>
                        <IconIcon className="position-relative mr-0" style={{ fontSize: 20, top: -10 }} />
                        <p className="m-0 position-relative" style={{ opacity: 1, lineHeight: 0, top: -7 }}>{t('lbl_icon')}</p>
                    </Menu.Item>
                    <Menu.Item key="3" className="text-center position-relative" style={{ minHeight: 66 }}>
                        <TextIcon className="position-relative mr-0" style={{ fontSize: 20, top: -10 }} />
                        <p className="m-0 position-relative" style={{ opacity: 1, lineHeight: 0, top: -7 }}>{t('lbl_text')}</p>
                    </Menu.Item>
                    <Menu.Item key="4" className="text-center position-relative" style={{ minHeight: 66 }}>
                        <ImageIcon className="position-relative mr-0" style={{ fontSize: 20, top: -10 }} />
                        <p className="m-0 position-relative" style={{ opacity: 1, lineHeight: 0, top: -7 }}>{t('lbl_image')}</p>
                    </Menu.Item>
                    {/* <Menu.Item key="4" className="text-center position-relative" style={{minHeight: 66}}>
                        <BackgroundIcon className="position-relative mr-0" style={{fontSize: 20, top: -10}}/>
                        <p className="m-0 position-relative" style={{opacity: 1, lineHeight: 0, top: -7, fontSize: this.props.i18n.language === 'en-US' && 12}}>{t('lbl_background')}</p>
                    </Menu.Item> */}
                </Menu>
            </Sider>,
            <Sider theme='light' trigger={null} width={sizeMobile ? visible ? 220 : 0 : 280} collapsible collapsed={collapsed && !sizeMobile} className="position-fixed" style={{ minHeight: '100vh', zIndex: 2, left: 0, marginLeft: collapsed ? 0 : 80 }}>
                <div className="position-relative" style={{ minHeight: '100vh', zIndex: 1, padding: '16px 8px', border: '1px solid #dddddd' }} >
                    {!collapsed && !sizeMobile && <CloseIcon onClick={this.MenuSidebarHide} className="position-absolute text-muted" style={{ fontSize: 22, cursor: 'pointer', top: 24, right: 18, zIndex: 2 }} />}
                    {selectedKey === "0" && [
                        <p className="font-weight-bold mt-1 pl-3" style={{ fontSize: 16 }}>{t('lbl_theme')}</p>,
                        <Scrollbars  className="position-relative d-flex p-3" style={{borderTopLeftRadius: 8, borderTopRightRadius: 8, height: 'calc(100vh - 124px)'}}>
                            <Row gutter={24} type="flex" className="mx-0">
                                {  this.state.theme &&  this.state.theme.map(item => (
                                <Col span={6} className="p-3">
                                    <Button shape="circle" size="large" style={{background: item.primary}} onClick={() => this.changeTheme(item._id)}/>
                                </Col>
                                ))}
                            </Row>
                        </Scrollbars>
                    ]}
                    {selectedKey === "1" && [
                        <p className="font-weight-bold mt-1 pl-3" style={{ fontSize: 16 }}>{t('lbl_objects')}</p>,
                        <Scrollbars  className="position-relative d-flex p-3" style={{borderTopLeftRadius: 8, borderTopRightRadius: 8, height: 'calc(100vh - 124px)'}}>
                            <Row gutter={24} className="mx-0 px-3">
                                <p>{t('lbl_shape')}</p>
                                <Col span={12} className="p-2">
                                    <img src={require('../assets/shapes/circle.png')} className="w-100 cursor-pointer" style={{filter: 'brightness(0)'}}
                                        onClick={() => {
                                            const data = {
                                                type: 'circle',
                                                name: `Circle${Date.now()}`,
                                                x: 270,
                                                y: 270,
                                                radius: 100,
                                                fillType: 'body',
                                                fill: '#000000',
                                                draggable: true
                                            }
                                            this.addElement(data);
                                        }}
                                    />
                                </Col>
                                <Col span={12} className="p-2">
                                    <img src={require('../assets/shapes/times-circle.png')} className="w-100 cursor-pointer" style={{filter: 'brightness(0)'}}
                                        onClick={() => {
                                            const data = {
                                                type: 'circle',
                                                name: `Circle${Date.now()}`,
                                                x: 270,
                                                y: 270,
                                                radius: 100,
                                                strokeType: 'body',
                                                stroke: '#000000',
                                                strokeWidth: 7,
                                                draggable: true
                                            }
                                            this.addElement(data);
                                        }}
                                    />
                                </Col>
                                <Col span={12} className="p-2">
                                    <img src={require('../assets/shapes/ellipse.png')} className="w-100 cursor-pointer" style={{filter: 'brightness(0)'}}
                                        onClick={() => {
                                            const data = {
                                                type: 'ellipse',
                                                name: `Ellipse${Date.now()}`,
                                                x: 270,
                                                y: 270,
                                                width: 250,
                                                height: 170,
                                                fillType: 'body',
                                                fill: '#000000',
                                                draggable: true
                                            }
                                            this.addElement(data);
                                        }}
                                    />
                                </Col>
                                <Col span={12} className="p-2">
                                    <img src={require('../assets/shapes/times-ellipse.png')} className="w-100 cursor-pointer" style={{filter: 'brightness(0)'}}
                                        onClick={() => {
                                            const data = {
                                                type: 'ellipse',
                                                name: `Ellipse${Date.now()}`,
                                                x: 270,
                                                y: 270,
                                                width: 250,
                                                height: 170,
                                                strokeType: 'body',
                                                stroke: '#000000',
                                                strokeWidth: 7,
                                                draggable: true
                                            }
                                            this.addElement(data);
                                        }}
                                    />
                                </Col>
                                <Col span={12} className="p-2">
                                    <img src={require('../assets/shapes/square.png')} className="w-100 cursor-pointer" style={{filter: 'brightness(0)'}}
                                        onClick={() => {
                                            const data = {
                                                type: 'rect',
                                                name: `React${Date.now()}`,
                                                x: 270,
                                                y: 270,
                                                width: 200,
                                                height: 200,
                                                fillType: 'body',
                                                fill: '#000000',
                                                draggable: true
                                            }
                                            this.addElement(data);
                                        }}
                                    />
                                </Col>
                                <Col span={12} className="p-2">
                                    <img src={require('../assets/shapes/times-square.png')} className="w-100 cursor-pointer" style={{filter: 'brightness(0)'}}
                                        onClick={() => {
                                            const data = {
                                                type: 'rect',
                                                name: `React${Date.now()}`,
                                                x: 270,
                                                y: 270,
                                                width: 200,
                                                height: 200,
                                                strokeType: 'body',
                                                stroke: '#000000',
                                                strokeWidth: 7,
                                                draggable: true
                                            }
                                            this.addElement(data);
                                        }}
                                    />
                                </Col>
                                <Col span={12} className="p-2">
                                    <img src={require('../assets/shapes/rounded.png')} className="w-100 cursor-pointer" style={{filter: 'brightness(0)'}}
                                        onClick={() => {
                                            const data = {
                                                type: 'rect',
                                                name: `React${Date.now()}`,
                                                x: 270,
                                                y: 270,
                                                width: 200,
                                                height: 120,
                                                cornerRadius: 15,
                                                fillType: 'body',
                                                fill: '#000000',
                                                draggable: true
                                            }
                                            this.addElement(data);
                                        }}
                                    />
                                </Col>
                                <Col span={12} className="p-2">
                                    <img src={require('../assets/shapes/times-rounded.png')} className="w-100 cursor-pointer" style={{filter: 'brightness(0)'}}
                                        onClick={() => {
                                            const data = {
                                                type: 'rect',
                                                name: `React${Date.now()}`,
                                                x: 270,
                                                y: 270,
                                                width: 200,
                                                height: 120,
                                                cornerRadius: 15,
                                                strokeType: 'body',
                                                stroke: '#000000',
                                                strokeWidth: 7,
                                                draggable: true
                                            }
                                            this.addElement(data);
                                        }}
                                    />
                                </Col>
                                <Col span={12} className="p-2">
                                    <img src={require('../assets/shapes/star.png')} className="w-100 cursor-pointer" style={{filter: 'brightness(0)'}}
                                        onClick={() => {
                                            const data = {
                                                type: 'star',
                                                name: `Star${Date.now()}`,
                                                x: 270,
                                                y: 270,
                                                numPoints: 5,
                                                innerRadius: 75,
                                                outerRadius: 150,
                                                fillType: 'body',
                                                fill: '#000000',
                                                draggable: true
                                            }
                                            this.addElement(data);
                                        }}
                                    />
                                </Col>
                                <Col span={12} className="p-2">
                                    <img src={require('../assets/shapes/times-star.png')} className="w-100 cursor-pointer" style={{filter: 'brightness(0)'}}
                                        onClick={() => {
                                            const data = {
                                                type: 'star',
                                                name: `Star${Date.now()}`,
                                                x: 270,
                                                y: 270,
                                                numPoints: 5,
                                                innerRadius: 75,
                                                outerRadius: 150,
                                                strokeType: 'body',
                                                stroke: '#000000',
                                                strokeWidth: 10,
                                                draggable: true
                                            }
                                            this.addElement(data);
                                        }}
                                    />
                                </Col>
                                <Col span={12} className="p-2">
                                    <img src={require('../assets/shapes/ring.png')} className="w-100 cursor-pointer" style={{filter: 'brightness(0)'}}
                                        onClick={() => {
                                            const data = {
                                                type: 'ring',
                                                name: `Ring${Date.now()}`,
                                                x: 270,
                                                y: 270,
                                                innerRadius: 80,
                                                outerRadius: 120,
                                                fillType: 'body',
                                                fill: '#000000',
                                                draggable: true
                                            }
                                            this.addElement(data);
                                        }}
                                    />
                                </Col>
                                <Col span={12} className="p-2">
                                    <img src={require('../assets/shapes/triangle.png')} className="w-100 cursor-pointer" style={{filter: 'brightness(0)'}}
                                        onClick={() => {
                                            const data = {
                                                type: 'polygon',
                                                name: `Polygon${Date.now()}`,
                                                x: 270,
                                                y: 270,
                                                sides: 3,
                                                radius: 70,
                                                fillType: 'body',
                                                fill: '#000000',
                                                draggable: true
                                            }
                                            this.addElement(data);
                                        }}
                                    />
                                </Col>
                                <Col span={12} className="p-2">
                                    <img src={require('../assets/shapes/times-triangle.png')} className="w-100 cursor-pointer" style={{filter: 'brightness(0)'}}
                                        onClick={() => {
                                            const data = {
                                                type: 'polygon',
                                                name: `Polygon${Date.now()}`,
                                                x: 270,
                                                y: 270,
                                                sides: 3,
                                                radius: 70,
                                                strokeType: 'body',
                                                stroke: '#000000',
                                                strokeWidth: 7,
                                                draggable: true
                                            }
                                            this.addElement(data);
                                        }}
                                    />
                                </Col>
                                <Col span={12} className="p-2">
                                    <img src={require('../assets/shapes/hexagon.png')} className="w-100 cursor-pointer" style={{filter: 'brightness(0)'}}
                                        onClick={() => {
                                            const data = {
                                                type: 'polygon',
                                                name: `Polygon${Date.now()}`,
                                                x: 270,
                                                y: 270,
                                                sides: 6,
                                                radius: 70,
                                                fillType: 'body',
                                                fill: '#000000',
                                                draggable: true
                                            }
                                            this.addElement(data);
                                        }}
                                    />
                                </Col>
                                {/* <Col span={12} className="px-2 d-flex align-items-center cursor-pointer justify-content-center font-weight-bold" style={{ height: 70 }}
                                    onClick={() => {
                                        const data = {
                                            type: 'text',
                                            name: `Text${Date.now()}`,
                                            text: 'ข้อความ',
                                            x: 270,
                                            y: 270,
                                            width: 250,
                                            fontStyle: 'bold',
                                            fontFamily: "Arial",
                                            verticalAlign: 'middle',
                                            align: 'center',
                                            fontSize: 30,
                                            fill: '#000',
                                            draggable: true
                                        }
                                        this.addElement(data);
                                    }}
                                >
                                    ข้อความ
                                </Col> */}
                            </Row>
                        </Scrollbars>
                    ]}
                    {selectedKey === "3" && [
                        <p className="font-weight-bold mt-1 pl-3" style={{ fontSize: 16 }}>{t('lbl_text')}</p>,
                        <Scrollbars  className="position-relative d-flex p-3" style={{borderTopLeftRadius: 8, borderTopRightRadius: 8, height: 'calc(100vh - 124px)'}}>
                            <Row gutter={24} type="flex" className="mx-0 p-2">
                                <Button 
                                    block
                                    className="mb-2 font-weight-bold"
                                    style={{fontSize: 24, height: 40}}
                                    onClick={() => {
                                        const data = {
                                            type: 'text',
                                            name: `Text${Date.now()}`,
                                            text: 'ข้อความ',
                                            x: 270,
                                            y: 270,
                                            width: 250,
                                            fontStyle: 'bold',
                                            fontFamily: "Arial",
                                            verticalAlign: 'middle',
                                            align: 'center',
                                            fontSize: 36,
                                            fillType: 'body',
                                            fill: '#000',
                                            draggable: true
                                        }
                                        this.addElement(data);
                                    }}
                                >
                                    {t('lbl_add_heading_text')}
                                </Button>
                                <Button 
                                    block
                                    className="mb-2"
                                    style={{fontSize: 18, height: 35}}
                                    onClick={() => {
                                        const data = {
                                            type: 'text',
                                            name: `Text${Date.now()}`,
                                            text: 'ข้อความ',
                                            x: 270,
                                            y: 270,
                                            width: 250,
                                            fontStyle: 'bold',
                                            fontFamily: "Arial",
                                            verticalAlign: 'middle',
                                            align: 'center',
                                            fontSize: 28,
                                            fillType: 'body',
                                            fill: '#000',
                                            draggable: true
                                        }
                                        this.addElement(data);
                                    }}
                                >
                                    {t('lbl_add_subheading_text')}
                                </Button>
                                <Button 
                                    block
                                    className="mb-2"
                                    style={{fontSize: 14}}
                                    onClick={() => {
                                        const data = {
                                            type: 'text',
                                            name: `Text${Date.now()}`,
                                            text: 'ข้อความ',
                                            x: 270,
                                            y: 270,
                                            width: 250,
                                            fontFamily: "Arial",
                                            verticalAlign: 'middle',
                                            align: 'center',
                                            fontSize: 20,
                                            fillType: 'body',
                                            fill: '#000',
                                            draggable: true
                                        }
                                        this.addElement(data);
                                    }}
                                >
                                    {t('lbl_add_body_text')}
                                </Button>
                            </Row>
                        </Scrollbars>
                    ]}
                    {selectedKey === "4" && [
                        <p className="font-weight-bold mt-1 pl-3" style={{ fontSize: 16 }}>{t('lbl_image')}</p>,
                        <Row gutter={24} className="mx-0">
                            <Col span={24} className="px-0 p-2">
                                <img src={require('../assets/images/image-placefolder.png')} className="w-100 cursor-pointer"/>
                            </Col>
                        </Row>
                    ]}
                </div>
            </Sider>
        ])

        const changeFontsSize = (type, value) => {
            const { template, page } = this.state;
            template.elements[page].forEach(item => {
                if (type === 'header' && item.fontType === 'header') {
                    item.fontSize = value
                    this.setState({ headerSize: value })
                } else if (type === 'subheader' && item.fontType === 'subheader') {
                    item.fontSize = value
                    this.setState({ subheaderSize: value })
                } else if (type === 'title' && item.fontType === 'title') {
                    item.fontSize = value
                    this.setState({ titleSize: value })
                } else if (type === 'subtitle' && item.fontType === 'subtitle') {
                    item.fontSize = value
                    this.setState({ subtileSize: value })
                } else if (type === 'body' && item.fontType === 'body') {
                    item.fontSize = value
                    this.setState({ bodySize: value })
                }
            })
            this.setState({ template });
        }

        const changeFontsFamily = (type, value) => {
            const { template, page } = this.state;
            template.elements[page].forEach(item => {
                if (type === 'header' && item.fontType === 'header') {
                    item.fontFamily = value
                    this.setState({ headerFont: value })
                } else if (type === 'subheader' && item.fontType === 'subheader') {
                    item.fontFamily = value
                    this.setState({ subheaderFont: value })
                } else if (type === 'title' && item.fontType === 'title') {
                    item.fontFamily = value
                    this.setState({ titleFont: value })
                } else if (type === 'subtitle' && item.fontType === 'subtitle') {
                    item.fontFamily = value
                    this.setState({ subtileFont: value })
                } else if (type === 'body' && item.fontType === 'body') {
                    item.fontFamily = value
                    this.setState({ bodyFont: value })
                }
            })
            this.setState({ template });
        }

        const changeFontFamily = (value) => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            template.elements[selectedPage][selectedElementIndex].fontFamily = value;
            this.setState({ template });
        }

        const changeFontSize = (value) => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            template.elements[selectedPage][selectedElementIndex].fontSize = value;
            this.setState({ template });
        }

        const changeConnerRadius = (value) => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            template.elements[selectedPage][selectedElementIndex].cornerRadius = value;
            this.setState({ template });
        }

        const changeInnerRadius = (value) => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            template.elements[selectedPage][selectedElementIndex].innerRadius = value;
            this.setState({ template });
        }

        const changeSides = (value) => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            template.elements[selectedPage][selectedElementIndex].sides = value;
            this.setState({ template });
        }

        const changeFillColor = (name, color) => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            template.elements[selectedPage][selectedElementIndex].fillType = name;
            template.elements[selectedPage][selectedElementIndex].fill = color;
            this.setState({ template });
        }
        
        const changeStrokeColor = (name, color) => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            template.elements[selectedPage][selectedElementIndex].strokeType = name;
            template.elements[selectedPage][selectedElementIndex].stroke = color;
            if (!template.elements[selectedPage][selectedElementIndex].strokeWidth) {
                template.elements[selectedPage][selectedElementIndex].strokeWidth = 1;
            }
            this.setState({ template });
        }

        const changeShadowColor = (color) => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            template.elements[selectedPage][selectedElementIndex].shadowColor = color;
            this.setState({ template });
        }
        const changeShadowBlur = (value) => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            template.elements[selectedPage][selectedElementIndex].shadowBlur = value;
            this.setState({ template });
        }
        const changeShadowOffsetX = (value) => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            template.elements[selectedPage][selectedElementIndex].shadowOffsetX = value;
            this.setState({ template });
        }
        const changeShadowOffsetY = (value) => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            template.elements[selectedPage][selectedElementIndex].shadowOffsetY = value;
            this.setState({ template });
        }
        const changeShadowOpacity = (value) => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            template.elements[selectedPage][selectedElementIndex].shadowOpacity = value / 100;
            this.setState({ template });
        }

        const changeTextBold = () => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            if (template.elements[selectedPage][selectedElementIndex].fontStyle) {
                let str = template.elements[selectedPage][selectedElementIndex].fontStyle;
                let arr = str.split(' ')
                let strNew = '';
                let isItalic = false;
                arr.forEach(item => {
                    if (item.toLowerCase() != 'bold' && item != '') {
                        strNew += item + ' '
                    } else if (item.toLowerCase() == 'bold' && item != '') {
                        isItalic = true;
                    }
                })
                if (!isItalic) { strNew += 'bold' }
                template.elements[selectedPage][selectedElementIndex].fontStyle = strNew
            } else {
                template.elements[selectedPage][selectedElementIndex].fontStyle = 'bold'
            }
            this.setState({ template });
        }

        const changeTextItalic = () => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            if (template.elements[selectedPage][selectedElementIndex].fontStyle) {
                let str = template.elements[selectedPage][selectedElementIndex].fontStyle;
                let arr = str.split(' ')
                let strNew = '';
                let isItalic = false;
                arr.forEach(item => {
                    if (item.toLowerCase() != 'italic' && item != '') {
                        strNew += item + ' '
                    } else if (item.toLowerCase() == 'italic' && item != '') {
                        isItalic = true;
                    }
                })
                if (!isItalic) { strNew += 'italic' }
                template.elements[selectedPage][selectedElementIndex].fontStyle = strNew
            } else {
                template.elements[selectedPage][selectedElementIndex].fontStyle = 'italic'
            }
            this.setState({ template });
        }

        const changeTextUnderline = () => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            if (template.elements[selectedPage][selectedElementIndex].textDecoration === 'underline') {
                template.elements[selectedPage][selectedElementIndex].textDecoration = ''
            } else {
                template.elements[selectedPage][selectedElementIndex].textDecoration = 'underline'
            }
            this.setState({ template });
        }

        const changeTextAlign = () => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            if (template.elements[selectedPage][selectedElementIndex].align === 'center') {
                template.elements[selectedPage][selectedElementIndex].align = 'right'
            } else if (template.elements[selectedPage][selectedElementIndex].align === 'right') {
                template.elements[selectedPage][selectedElementIndex].align = 'left'
            } else {
                template.elements[selectedPage][selectedElementIndex].align = 'center'
            }
            this.setState({ template });
        }

        const changeTextVerticalAlign = () => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            if (template.elements[selectedPage][selectedElementIndex].verticalAlign === 'middle') {
                template.elements[selectedPage][selectedElementIndex].verticalAlign = 'bottom'
            } else if (template.elements[selectedPage][selectedElementIndex].verticalAlign === 'bottom') {
                template.elements[selectedPage][selectedElementIndex].verticalAlign = 'top'
            } else {
                template.elements[selectedPage][selectedElementIndex].verticalAlign = 'middle'
            }
            this.setState({ template });
        }

        const changeOpacity = (value) => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            template.elements[selectedPage][selectedElementIndex].opacity = value / 100
            this.setState({ template });
        }

        const deleteElement = () => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            if(template && selectedPage >=0 && selectedElementIndex >= 0 ) {
                if(template && template.elements[selectedPage]) {
                    const newElements = template.elements[selectedPage].filter((item, index) => index != selectedElementIndex)
                    template.elements[selectedPage] = newElements;
                    this.setState({ template, selectedShapeName: '', selectedType: null, selectedPage: -1, selectedElementIndex: -1 });
                }
            }
            this.setState({ template });
        }

        const changeDraggable = () => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            if(template && selectedPage >=0 && selectedElementIndex >= 0 ) {
                if (template.elements[selectedPage][selectedElementIndex].draggable) {
                    template.elements[selectedPage][selectedElementIndex].draggable = false
                } else {
                    template.elements[selectedPage][selectedElementIndex].draggable = true
                }
            }
            this.setState({ template });
        }

        const changeLetterSpacing = (value) => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            template.elements[selectedPage][selectedElementIndex].letterSpacing = value
            this.setState({ template });
        }
        const changeLinehightSpacing = (value) => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            template.elements[selectedPage][selectedElementIndex].lineHeight = value
            this.setState({ template });
        }

        const brintToFrontElement = () => {
            const { template, selectedPage, selectedElementIndex } = this.state;
            template.elements[selectedPage].push(template.elements[selectedPage].splice(selectedElementIndex, 1)[0]);
            this.setState({ template });
        }

        const brintToBackElement = () => {
            const { template, selectedPage, selectedElementIndex, selectedType } = this.state;
            template.elements[selectedPage].splice(1, 0, template.elements[selectedPage].splice(selectedElementIndex, 1)[0]);
            this.setState({ template });
        }

        const makeCopyElement = async () => {
            const { template, selectedPage, selectedElementIndex, selectedType } = this.state;
            let newArray = Object.assign({}, template.elements[selectedPage][selectedElementIndex]);
            await template.elements[selectedPage].splice(selectedElementIndex, 0, newArray)
            let newName = await selectedType.charAt(0).toUpperCase() + selectedType.slice(1)+Date.now();
            template.elements[selectedPage][selectedElementIndex+1].name = newName;
            template.elements[selectedPage][selectedElementIndex+1].x = newArray.x + 10;
            template.elements[selectedPage][selectedElementIndex+1].y = newArray.y + 10;
            this.setState({ template, selectedElementIndex: selectedElementIndex + 1, selectedShapeName: newName });
        }

        const menuFill = (
            <Menu>
                { template && template.theme.primary && (
                    <Menu.Item className="d-flex align-items-center" onClick={() => changeFillColor('primary', template.theme.primary)}>
                        <CircleIcon style={{border: '1px solid #ddd', borderRadius: '50%', color: template.theme.primary}} />
                        {template.theme.primary}
                    </Menu.Item>
                )}
                { template && template.theme.secondary && (
                    <Menu.Item className="d-flex align-items-center" onClick={() => changeFillColor('secondary', template.theme.secondary)}>
                        <CircleIcon style={{border: '1px solid #ddd', borderRadius: '50%', color: template.theme.secondary}} />
                        {template.theme.secondary}
                    </Menu.Item>
                )}
                { template && template.theme.info && (
                    <Menu.Item className="d-flex align-items-center" onClick={() => changeFillColor('info', template.theme.info)}>
                        <CircleIcon style={{border: '1px solid #ddd', borderRadius: '50%', color: template.theme.info}} />
                        {template.theme.info}
                    </Menu.Item>
                )}
                { template && template.theme.body && (
                    <Menu.Item className="d-flex align-items-center" onClick={() => changeFillColor('body', template.theme.body)}>
                        <CircleIcon style={{border: '1px solid #ddd', borderRadius: '50%', color: template.theme.body}} />
                        {template.theme.body}
                    </Menu.Item>
                )}
                { template && template.theme.dark && (
                    <Menu.Item className="d-flex align-items-center" onClick={() => changeFillColor('dark', template.theme.dark)}>
                        <CircleIcon style={{border: '1px solid #ddd', borderRadius: '50%', color: template.theme.dark}} />
                        {template.theme.dark}
                    </Menu.Item>
                )}
                { template && template.theme.light && (
                    <Menu.Item className="d-flex align-items-center" onClick={() => changeFillColor('light', template.theme.light)}>
                        <CircleIcon style={{border: '1px solid #ddd', borderRadius: '50%', color: template.theme.light}} />
                        {template.theme.light}
                    </Menu.Item>
                )}
                { template && template.theme.white && (
                    <Menu.Item className="d-flex align-items-center" onClick={() => changeFillColor('white', template.theme.white)}>
                        <CircleIcon style={{border: '1px solid #ddd', borderRadius: '50%', color: template.theme.white}} />
                        {template.theme.white}
                    </Menu.Item>
                )}
            </Menu>
        );

        

        return (
            <Layout className="design-page">
                <PageHeader
                    style={{
                        boxShadow: '0 0 4px 4px rgba(0, 0, 0, 0.08)',
                        position: "fixed",
                        top: 0,
                        zIndex: 5,
                        backgroundColor: '#030852',
                        height: 63,
                        display: 'flex'
                    }}
                    className="w-100"
                    backIcon={<Icon type="home" style={{ color: '#fff' }} onClick={() => { this.props.history.go(-(this.props.history.length)); this.props.history.replace('/') }} />}
                    onBack={() => null}
                    title={!sizeMobile ? imageTitle : null}
                    subTitle={
                        template && ([
                            <div className="mx-2 text-white">{t('lbl_page') + ' ' + (this.state.page + 1) + '/' + template.elements.length}</div>
                        ])
                    }
                    extra={[
                        !sizeMobile ? [
                            <Tooltip placement="bottom" title={sizeMobile ? t('lbl_save') : ''}>
                                <Button ghost icon="save" className="header-design-button" onClick={this.handleSave}>{t('lbl_save')}</Button>
                            </Tooltip>,
                            // <Divider type="vertical" className="sm-d-none"/>,
                            // <Tooltip placement="bottom" title={sizeMobile? t('lbl_download'): ''}>
                            //     <Dropdown overlay={MenuDownload} placement="bottomRight" trigger={['click']}>
                            //         <Button ghost icon="cloud-download" className="header-design-button">{t('lbl_download')}</Button>
                            //     </Dropdown>
                            // </Tooltip>,
                            // <GoogleLogin
                            //     clientId="112794756574-8gjk6lscsnkak7da0tpsgpmrej3tfaob.apps.googleusercontent.com"
                            //     render={renderProps => (
                            //         <Button ghost className="header-design-button"  onClick={renderProps.onClick}>
                            //             <IconGoogleDrive/>
                            //             {t("lbl_save_to_google_drive")}
                            //         </Button>
                            //     )}
                            //     onSuccess={this.responseGoogleDrive}
                            //     onFailure={res => console.log(res)}
                            //     cookiePolicy={'single_host_origin'}
                            //     scope="https://www.googleapis.com/auth/drive"
                            // />,
                            // <Tooltip placement="bottom" title={sizeMobile?t('lbl_print'): ''}>
                            //     <Button ghost icon="printer" className="header-design-button sm-d-none"  onClick={this.printDocument}>{t('lbl_print')}</Button>
                            // </Tooltip>
                        ] : [
                                <Button ghost className="header-design-button" onClick={() => this.setState({ isDrawerOpenOption: true })}><MoreIcon /></Button>,
                                <Drawer
                                    title={inputProjectName}
                                    placement="bottom"
                                    closable={false}
                                    onClose={() => this.setState({ isDrawerOpenOption: false })}
                                    visible={this.state.isDrawerOpenOption}
                                    className="drawer-design-page"
                                >
                                    {MenuMore}
                                </Drawer>
                            ],
                        !sizeMobile ? ([
                            <Divider type="vertical" />,
                            <Dropdown overlay={MenuLanguage} placement="bottomRight">
                                <a className="ant-dropdown-link text-dark d-inline-flex align-items-center mx-0" href="#">
                                    <img className="header-design-lang" src={require('../assets/flags/' + language + '.png')} style={{ width: 30 }} />
                                    <span className="sm-d-none text-white ml-1">{t('lbl_' + language)}</span>
                                </a>
                            </Dropdown>,
                            <Divider type="vertical" className="sm-d-none" />,
                            <Dropdown overlay={MenuAccount} placement="bottomRight">
                                <a className="ant-dropdown-link text-dark mx-0 header-design-dropdown" href="#">
                                    <Avatar className="header-design-avatar" src={userInfo && userInfo.photoUrl} name={userInfo && (userInfo.firstname + ' ' + userInfo.lastname)} round={true} size={35} /> &nbsp;
                                <span className="sm-d-none mx-1 text-white">{userInfo && (userInfo.firstname + ' ' + userInfo.lastname)}</span>
                                    <Icon className="header-design-icon-dropdown" type="down" style={{ color: "#fff" }} />
                                </a>
                            </Dropdown>
                        ])
                            : ([
                                <Button ghost className="header-design-button" onClick={() => this.setState({ accountVisible: true })}><Icon type="user" style={{ fontSize: 22 }} /></Button>,
                                <Drawer
                                    title={null}
                                    placement="bottom"
                                    closable={false}
                                    onClose={() => this.setState({ accountVisible: false })}
                                    visible={this.state.accountVisible}
                                    className="drawer-design-page"
                                >
                                    {MenuAccount}
                                </Drawer>
                            ])
                    ]}
                    ghost={false}
                />
                <Layout style={{ marginTop: '3.9rem', minHeight: '90vh' }}>
                    {sizeMobile ?
                        <Drawer
                            title={null}
                            placement="left"
                            closable={false}
                            onClose={() => this.setState({ visible: false })}
                            visible={visible}
                            width={270}
                            className="drawer-sidebar-design"
                        >
                            {sidebar}
                        </Drawer>
                        : userInfo && sidebar
                    }
                    <Layout>
                        <Modal
                            visible={this.state.isModalGoogleDrive}
                            title={null}
                            onCancel={() => this.setState({ isModalGoogleDrive: false, accessToken: null })}
                            maskClosable={false}
                            footer={null}
                            closable={true}
                            bodyStyle={{ padding: 0 }}
                        >
                            {MenuGoogleDrive}
                        </Modal>
                        <Modal
                            visible={this.state.selectKeyVisible}
                            title={null}
                            onCancel={() => this.setState({ selectKeyVisible: false })}
                            footer={null}
                            closable={true}
                            className="p-0"
                        >
                            {MenuSelectKey}
                        </Modal>
                        <Header className="d-flex align-items-center p-2 position-fixed" style={{ overflowX: 'auto', overflowY: 'hidden', marginLeft: !sizeMobile ? collapsed ? '5rem' : '22.5rem' : 0, background: '#fff', left: 0, right: 0, zIndex: 1, bottom: sizeMobile && 0, height: sizeMobile ? 64 : 45, boxShadow: '0 1px 0 rgba(14, 19, 24, .15)' }}>
                            <div className="d-flex justify-content-between" style={{ minWidth: sizeMobile ? 'max-content' : '100%' }}>
                                <div className="d-flex align-items-center" >
                                    {/* <div className="d-inline-flex align-items-center" style={{minWidth: sizeMobile && 'max-content'}}>
                                        <Tooltip placement={sizeMobile? "top": 'bottom'} title={t('lbl_undo')} className="mx-2">
                                            <IconUndo onClick={() => this.handleUndo()} className="cursor-pointer position-relative text-dark" style={{fontSize: 18}}/>
                                        </Tooltip>
                                        <Tooltip placement={sizeMobile? "top": 'bottom'} title={t('lbl_redo')} className="mx-2">
                                            <IconRedo onClick={() => this.handleRedo()} className="cursor-pointer position-relative text-dark" style={{fontSize: 18}}/>
                                        </Tooltip>
                                        <Divider type="vertical" style={{height: 24}}/>
                                    </div> */}
                                    <div className="align-items-center" style={{ display: (selectedShapeName !== '') ? 'flex' : 'none', minWidth: 'max-content' }}>
                                        {selectedType && selectedType === "text" && template.elements[selectedPage][selectedElementIndex].draggable && [
                                            <Select
                                                showSearch
                                                style={{ minWidth: 150 }}
                                                optionFilterProp="children"
                                                value={template && selectedElementIndex >= 0 ? template.elements[selectedPage][selectedElementIndex].fontFamily : 'Arial'}
                                                notFoundContent={
                                                    <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} className="m-0" description={t('lbl_no_font')} />
                                                }
                                                filterOption={(input, option) =>
                                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                }
                                                onChange={value => changeFontFamily(value)}
                                            >
                                                {this.state.font.map(item =>
                                                    <Select.Option value={item}>{item}</Select.Option>
                                                )}
                                            </Select>,
                                            <InputNumber
                                                min={1}
                                                max={200}
                                                value={template && selectedElementIndex >= 0 ? template.elements[selectedPage][selectedElementIndex].fontSize : 12}
                                                className="mx-2 edit-font-size"
                                                onChange={value => changeFontSize(value)}
                                            />,
                                            <Tooltip placement={sizeMobile ? "top" : 'bottom'} title={null} className="mx-2">
                                                <Dropdown overlay={menuFill}>
                                                    <Icon type="font-colors" className="cursor-pointer position-relative" style={{fontSize: 20, color: template && selectedElementIndex >=0 ? template.elements[selectedPage][selectedElementIndex].fill : '#000'}}/>
                                                </Dropdown>
                                            </Tooltip>,
                                            <Button.Group className="mx-2">
                                                <Tooltip placement={sizeMobile ? "top" : 'bottom'} title={t('lbl_bold')}>
                                                    <Button onClick={() => changeTextBold()} className="px-1" type={template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].fontStyle && template.elements[selectedPage][selectedElementIndex].fontStyle.includes("bold") ? 'primary' : 'basic'}>
                                                        <Icon type="bold" onClick={e => e.preventDefault()} className="cursor-pointer position-relative " style={{ fontSize: 18, top: -3 }} />
                                                    </Button>
                                                </Tooltip>
                                                <Tooltip placement={sizeMobile ? "top" : 'bottom'} title={t('lbl_italic')}>
                                                    <Button onClick={() => changeTextItalic()} className="px-1" type={template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].fontStyle && template.elements[selectedPage][selectedElementIndex].fontStyle.includes("italic") ? 'primary' : 'basic'}>
                                                        <Icon type="italic" onClick={e => e.preventDefault()} className="cursor-pointer position-relative " style={{ fontSize: 18, top: -3 }} />
                                                    </Button>
                                                </Tooltip>
                                                <Tooltip placement={sizeMobile ? "top" : 'bottom'} title={t('lbl_underline')}>
                                                    <Button onClick={() => changeTextUnderline()} className="px-1" type={template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].textDecoration === 'underline' ? 'primary' : 'basic'}>
                                                        <Icon type="underline" onClick={e => e.preventDefault()} className="cursor-pointer position-relative" style={{ fontSize: 18, top: -3 }} />
                                                    </Button>
                                                </Tooltip>
                                            </Button.Group>,
                                            <Tooltip placement={sizeMobile ? "top" : 'bottom'} title={t('lbl_text_alignment')} className="mx-2">
                                                <Icon type={template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].align ? 'align-' + template.elements[selectedPage][selectedElementIndex].align : 'align-left'} onClick={e => changeTextAlign()} className="cursor-pointer position-relative text-dark" style={{ fontSize: 20 }} />
                                            </Tooltip>,
                                            <Tooltip placement={sizeMobile ? "top" : 'bottom'} title={t('lbl_vertical_alignment')} className="mx-2">
                                                <Icon type={template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].verticalAlign ? 'vertical-align-' + template.elements[selectedPage][selectedElementIndex].verticalAlign : 'vertical-align-top'} onClick={e => changeTextVerticalAlign()} className="cursor-pointer position-relative text-dark" style={{ fontSize: 20 }} />
                                            </Tooltip>,
                                            <Divider type="vertical" style={{ height: 24 }} />
                                        ]}
                                        {selectedType != "text" && template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].draggable && [
                                            <span className="text-dark mx-1">{t('lbl_color')}: </span>,
                                            <Tooltip placement={sizeMobile ? "top" : 'bottom'} title={null} className="mx-2">
                                                <Dropdown overlay={menuFill}>
                                                    <FillColorIcon onClick={e => this.setState({ displayFillPicker: true })} className="cursor-pointer position-relative" style={{ fontSize: 20, top: -1, color: template && selectedElementIndex >= 0 ? template.elements[selectedPage][selectedElementIndex].fill : '#000' }} />
                                                </Dropdown>
                                            </Tooltip>,
                                            selectedType == "rect" && template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].draggable && [
                                                <span className="text-dark mx-1">{t('lbl_conner_radius')}: </span>,
                                                <InputNumber
                                                    min={0}
                                                    max={200}
                                                    value={template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].cornerRadius ? template.elements[selectedPage][selectedElementIndex].cornerRadius : 0}
                                                    className="mx-2 edit-font-size"
                                                    onChange={value => changeConnerRadius(value)}
                                                />
                                            ],
                                            selectedType == "ring" && template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].draggable && [
                                                <span className="text-dark mx-1">{t('lbl_radius')}: </span>,
                                                <InputNumber
                                                    min={0}
                                                    max={200}
                                                    value={template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].innerRadius ? template.elements[selectedPage][selectedElementIndex].innerRadius : 0}
                                                    className="mx-2 edit-font-size"
                                                    onChange={value => changeInnerRadius(value)}
                                                />
                                            ],
                                            selectedType == "polygon" && template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].draggable && [
                                                <span className="text-dark mx-1">{t('lbl_sides_number')}: </span>,
                                                <InputNumber
                                                    min={3}
                                                    max={10}
                                                    value={template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].sides ? template.elements[selectedPage][selectedElementIndex].sides : 3}
                                                    className="mx-2 edit-font-size"
                                                    onChange={value => changeSides(value)}
                                                />
                                            ],
                                            <Divider type="vertical" style={{ height: 24 }} />
                                        ]}
                                        {template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].draggable && [
                                            <Tooltip placement={sizeMobile ? "top" : 'bottom'} title={null} className="ml-2 mr-1">
                                                <Dropdown placement={sizeMobile ? "topRight" : "bottomRight"} overlay={menuChangeStroke} trigger={['click']}>
                                                    <Button onClick={e => e.preventDefault()} className="px-2">
                                                        <StrokeIcon onClick={e => e.preventDefault()} className="cursor-pointer position-relative text-dark" style={{ fontSize: 18, top: -3 }} />
                                                        {t('lbl_stroke')}
                                                    </Button>
                                                </Dropdown>
                                            </Tooltip>
                                            ,
                                            <Tooltip placement={sizeMobile ? "top" : 'bottom'} title={null} className="mx-1">
                                                <Dropdown placement={sizeMobile ? "topRight" : "bottomRight"} overlay={menuChangeShadow} trigger={['hover']}>
                                                    <Button onClick={e => e.preventDefault()} className="px-2">
                                                        <ShadowIcon className="cursor-pointer position-relative text-dark" style={{ fontSize: 18, top: -3 }} />
                                                        {t('lbl_shadow')}
                                                    </Button>
                                                </Dropdown>
                                            </Tooltip>
                                        ]}
                                        {selectedType && selectedType === "text" && template && selectedElementIndex >= 0 && template.elements[selectedPage][selectedElementIndex].draggable &&
                                            <Tooltip placement={sizeMobile ? "top" : 'bottom'} title={null} className="mx-1">
                                                <Dropdown placement={sizeMobile ? "topRight" : "bottomRight"} overlay={menuChangeSpacing} trigger={['hover']}>
                                                    <Button onClick={e => e.preventDefault()} className="cursor-pointer position-relative text-dark px-2">{t('lbl_spacing')}</Button>
                                                </Dropdown>
                                            </Tooltip>
                                        }
                                    </div>
                                </div>
                                <div style={{ display: (selectedShapeName !== '') ? '' : 'none', minWidth: sizeMobile && 'max-content' }}>
                                    <Tooltip placement={sizeMobile ? "top" : 'bottom'} title={null} className="mx-2">
                                        <Dropdown placement={sizeMobile ? "topRight" : "bottomRight"} overlay={menuChangeOpacity} trigger={['hover']}>
                                            <OpacityIcon onClick={e => e.preventDefault()} className="cursor-pointer position-relative text-dark" style={{ fontSize: 18, top: -3 }} />
                                        </Dropdown>
                                    </Tooltip>
                                    {template && selectedElementIndex >= 0 && !template.elements[selectedPage][selectedElementIndex].draggable ?
                                        <Tooltip placement={sizeMobile ? "top" : 'bottom'} title={t('lbl_unlock')} className="mx-2">
                                            <LockIcon onClick={() => changeDraggable()} className="cursor-pointer position-relative text-dark" style={{ fontSize: 18, top: -3 }} />
                                        </Tooltip>
                                        :
                                        <Tooltip placement={sizeMobile ? "top" : 'bottom'} title={t('lbl_lock')} className="mx-2">
                                            <UnlockIcon onClick={() => changeDraggable()} className="cursor-pointer position-relative text-dark" style={{ fontSize: 18, top: -3 }} />
                                        </Tooltip>
                                    }
                                    <Tooltip placement={sizeMobile ? "top" : 'bottom'} title={t('lbl_delete')} className="mx-2">
                                        <RemoveIcon onClick={() => deleteElement()} className="cursor-pointer position-relative text-dark" style={{ fontSize: 18, top: -3 }} />
                                    </Tooltip>
                                </div>
                            </div>
                        </Header>
                        <Content
                            className="bg-transparent"
                            style={{
                                marginLeft: !sizeMobile ? collapsed ? '0rem' : '24rem' : 0
                            }}
                        >
                            <div className="position-fixed sm-d-show" style={{ right: -20, top: 150, zIndex: 100 }}>
                                <Button type="primary" onClick={() => this.setState({ visible: true })} shape="round" size='large' className="shadow-sm position-relative">
                                    <Icon type="setting" spin className="position-relative" style={{ fontSize: 18, top: -2, left: -9 }} />
                                </Button>
                            </div>
                            <Layout className="container-design" style={{ marginTop: sizeMobile ? 0 : '2rem', marginLeft: collapsed ? 80 : 0 }} >
                                {template && (
                                    Array(template.elements && template.elements.length).fill(1).map((el, i) =>
                                        <div ref={`Page${i}`} className="d-flex justify-content-center pt-5" style={{ transform: 'scale(' + scale + ')', transformOrigin: 'top center', height: percentScale+'%' }} key={'page_panel_' + i}>
                                            <div className="position-relative">
                                                <div className="row justify-content-between mx-0">
                                                    <div className="d-flex align-items-center">
                                                        <h5 className="text-secondary mb-0">{t('lbl_page')+' '+(i+1)} </h5>
                                                        <Divider type="vertical" />
                                                        <Select defaultValue={this.setDefaultPage(i)} style={{ width: 150 }} allowClear={true} onChange={val => {
                                                            this.setPage(val, i);
                                                        }}>
                                                            <Select.Option key={`frontCoverPage${i}`} value='frontCoverPage'>{t('lbl_front_cover')}</Select.Option>
                                                            <Select.Option key={`profilePage${i}`} value='profilePage'>{t('lbl_profile')}</Select.Option>
                                                            <Select.Option key={`educationPage${i}`} value='educationPage'>{t('lbl_education')}</Select.Option>
                                                            <Select.Option key={`activityPage${i}`} value='activityPage'>{t('lbl_activities')}</Select.Option>
                                                            <Select.Option key={`certificatePage${i}`} value='certificatePage'>{t('lbl_certificate')}</Select.Option>
                                                        </Select>
                                                    </div>
                                                    <div>
                                                        <IconButton size="small" className="d-inline-flex align-items-center mr-1" style={{outline: 'none'}} onClick={() => this.onNewPage(i)}>
                                                            <Icon type="plus" />
                                                        </IconButton>
                                                        
                                                        <IconButton className="d-inline-flex align-items-center mr-1" onClick={()=> this.onCopyPage(i)}>
                                                            <Icon type="copy" />
                                                        </IconButton>
                                                        <IconButton size="small" className="d-inline-flex align-items-center" style={{outline: 'none'}}  onClick={() => this.onDeletePage(i)}>
                                                            <Icon type="delete" />
                                                        </IconButton>
                                                    </div>
                                                </div>
                                                {ContextMenu.visible &&
                                                    <Menu selectable={false} style={{ position: 'absolute', top: ContextMenu.y, left: ContextMenu.x, zIndex: 1030, border: '1px solid #eee', boxShadow: '1px 1px 2px' }}>
                                                        <Menu.Item className="my-0" key="0" onClick={() => {
                                                            ContextMenu.visible = false;
                                                            this.setState({ ContextMenu });
                                                            deleteElement();
                                                        }}>
                                                            <RemoveIcon className="position-relative" style={{ top: -3 }} />
                                                            {t('lbl_delete')}
                                                        </Menu.Item>
                                                        <Menu.Item className="my-0" key="1" onClick={() => {
                                                            ContextMenu.visible = false;
                                                            this.setState({ ContextMenu });
                                                            makeCopyElement();
                                                        }}>
                                                            <Icon type="copy" className="position-relative" style={{fontSize: 18, top: -3 }} />
                                                            {t('lbl_make_a_copy')}
                                                        </Menu.Item>
                                                        <Menu.Item className="my-0" key="2" onClick={() => {
                                                            ContextMenu.visible = false;
                                                            this.setState({ ContextMenu, selectKeyVisible: true });
                                                        }}>
                                                            <KeyValueIcon className="position-relative" style={{fontSize: 18, top: -3 }} />
                                                            {t('lbl_select_key')}
                                                        </Menu.Item>
                                                        { template && selectedPage >= 0 && template.elements[selectedPage][selectedElementIndex].key &&
                                                            <Menu.Item className="my-0" key="3" onClick={() => {
                                                                const { template } = this.state;
                                                                ContextMenu.visible = false;
                                                                this.setState({ ContextMenu});
                                                                if(template && selectedPage >= 0) {
                                                                    template.elements[selectedPage][selectedElementIndex].key = null
                                                                    this.setState({template});
                                                                }
                                                            }}>
                                                                <RemoveIcon className="position-relative" style={{fontSize: 18, top: -3 }} />
                                                                {t('lbl_delete_key')}
                                                            </Menu.Item>
                                                        }
                                                        <Divider className="my-0" />
                                                        <Menu.Item className="my-0" key="4" onClick={() => {
                                                            ContextMenu.visible = false;
                                                            this.setState({ ContextMenu });
                                                            brintToFrontElement();
                                                        }}>
                                                            <BringToFrontIcon className="position-relative" style={{ top: -3 }} />
                                                            {t('lbl_bring_to_front')}
                                                        </Menu.Item>
                                                        <Menu.Item className="my-0" key="5" onClick={() => {
                                                            ContextMenu.visible = false;
                                                            this.setState({ ContextMenu });
                                                            brintToBackElement();
                                                        }}>
                                                            <BringToBackIcon className="position-relative" style={{ top: -3 }} />
                                                            {t('lbl_bring_to_back')}
                                                        </Menu.Item>
                                                    </Menu>
                                                }
                                                <Stage
                                                    width={paperWidth}
                                                    height={paperHeight}
                                                    ref={"paperPage" + i}
                                                    onClick={e => {
                                                        if (selectedPage >= 0 && selectedType == 'text' && textEdit.visible) {
                                                            template.elements[selectedPage][selectedElementIndex].text = textEdit.textValue;
                                                            template.elements[selectedPage][selectedElementIndex].visible = true;
                                                        }
                                                        textEdit.visible = false;
                                                        ContextMenu.visible = false
                                                        this.setState({ ContextMenu, selectedShapeName: e.target.name().substring(0, 2) !== "Bg" ? e.target.name() : "" })
                                                    }
                                                    }
                                                >
                                                    <Layer>
                                                        {
                                                            template.elements && template.elements[i].map((item, index) => {
                                                                switch (item.type) {
                                                                    case 'bg': return <Rect {...item} key={`${i}${index}`} />
                                                                    case 'rect': return (
                                                                        <Rect
                                                                            key={`${i}${index}`}
                                                                            // name={`Rect${i}${index}`}
                                                                            {...item}
                                                                            onContextMenu={e => {
                                                                                if (template.elements[i][index].draggable) {
                                                                                    e.evt.preventDefault();
                                                                                    ContextMenu.visible = true;
                                                                                    ContextMenu.x = e.evt.offsetX;
                                                                                    ContextMenu.y = e.evt.offsetY;
                                                                                    this.setState({ ContextMenu });
                                                                                }
                                                                            }}
                                                                            onClick={() => {
                                                                                if (selectedPage >= 0 && selectedType == 'text' && textEdit.visible) {
                                                                                    template.elements[selectedPage][selectedElementIndex].text = textEdit.textValue;
                                                                                    template.elements[selectedPage][selectedElementIndex].visible = true;
                                                                                }
                                                                                textEdit.visible = false;
                                                                                this.setState({ textEdit, template, selectedPage: i, selectedElementIndex: index, selectedType: item.type })
                                                                            }}
                                                                            onDragEnd={e => {
                                                                                template.elements[i][index].x = e.target.x();
                                                                                template.elements[i][index].y = e.target.y();
                                                                                this.setState({ template })
                                                                            }}
                                                                            onTransformEnd={e => {
                                                                                const shape = e.target;
                                                                                // template.elements[i][index].scaleX = shape.scaleX();
                                                                                // template.elements[i][index].scaleY = shape.scaleY();
                                                                                template.elements[i][index].width = shape.width() * shape.scaleX();
                                                                                template.elements[i][index].height = shape.height() * shape.scaleY();
                                                                                template.elements[i][index].rotation = shape.rotation();
                                                                                shape.scaleX(1);
                                                                                shape.scaleY(1);
                                                                                const pos = template
                                                                                history = history.slice(0, historyStep + 1);
                                                                                history = history.concat([pos]);
                                                                                historyStep += 1;
                                                                                this.setState({ template })
                                                                            }}
                                                                        />
                                                                    )
                                                                    case 'circle': return (
                                                                        <Circle
                                                                            key={`${i}${index}`}
                                                                            // name={`Circle${i}${index}`} 
                                                                            {...item}
                                                                            onContextMenu={e => {
                                                                                if (template.elements[i][index].draggable) {
                                                                                    e.evt.preventDefault();
                                                                                    ContextMenu.visible = true;
                                                                                    ContextMenu.x = e.evt.offsetX;
                                                                                    ContextMenu.y = e.evt.offsetY;
                                                                                    this.setState({ ContextMenu });
                                                                                }
                                                                            }}
                                                                            onClick={() => {
                                                                                textEdit.visible = false;
                                                                                if (selectedPage >= 0 && selectedType == 'text' && textEdit.visible) {
                                                                                    template.elements[selectedPage][selectedElementIndex].text = textEdit.textValue;
                                                                                    template.elements[selectedPage][selectedElementIndex].visible = true;
                                                                                }
                                                                                this.setState({ textEdit, template, selectedPage: i, selectedElementIndex: index, selectedType: item.type })
                                                                            }}

                                                                            onDragEnd={e => {
                                                                                template.elements[i][index].x = e.target.x();
                                                                                template.elements[i][index].y = e.target.y();
                                                                            }}
                                                                            onTransformEnd={e => {
                                                                                const shape = e.target;
                                                                                // template.elements[i][index].scaleX = shape.scaleX();
                                                                                // template.elements[i][index].scaleY = shape.scaleY();
                                                                                template.elements[i][index].width = shape.width() * shape.scaleX();
                                                                                template.elements[i][index].height = shape.height() * shape.scaleY();
                                                                                template.elements[i][index].rotation = shape.rotation();
                                                                                shape.scaleX(1);
                                                                                shape.scaleY(1);
                                                                            }}

                                                                        />
                                                                    )
                                                                    case 'ellipse': return (
                                                                        <Ellipse
                                                                            key={`${i}${index}`}
                                                                            // name={`Ellipse${i}${index}`} 
                                                                            {...item}
                                                                            onContextMenu={e => {
                                                                                if (template.elements[i][index].draggable) {
                                                                                    e.evt.preventDefault();
                                                                                    ContextMenu.visible = true;
                                                                                    ContextMenu.x = e.evt.offsetX;
                                                                                    ContextMenu.y = e.evt.offsetY;
                                                                                    this.setState({ ContextMenu });
                                                                                }
                                                                            }}
                                                                            onClick={() => {
                                                                                textEdit.visible = false;
                                                                                if (selectedPage >= 0 && selectedType == 'text' && textEdit.visible) {
                                                                                    template.elements[selectedPage][selectedElementIndex].text = textEdit.textValue;
                                                                                    template.elements[selectedPage][selectedElementIndex].visible = true;
                                                                                }
                                                                                this.setState({ textEdit, template, selectedPage: i, selectedElementIndex: index, selectedType: item.type })
                                                                            }}

                                                                            onDragEnd={e => {
                                                                                template.elements[i][index].x = e.target.x();
                                                                                template.elements[i][index].y = e.target.y();
                                                                            }}
                                                                            onTransformEnd={e => {
                                                                                const shape = e.target;
                                                                                // template.elements[i][index].scaleX = shape.scaleX();
                                                                                // template.elements[i][index].scaleY = shape.scaleY();
                                                                                template.elements[i][index].width = shape.width() * shape.scaleX();
                                                                                template.elements[i][index].height = shape.height() * shape.scaleY();
                                                                                template.elements[i][index].rotation = shape.rotation();
                                                                                shape.scaleX(1);
                                                                                shape.scaleY(1);
                                                                            }}

                                                                        />
                                                                    )
                                                                    case 'star': return (
                                                                        <Star
                                                                            {...item}
                                                                            key={`${i}${index}`}
                                                                            // name={`Star${i}${index}`}
                                                                            onContextMenu={e => {
                                                                                if (template.elements[i][index].draggable) {
                                                                                    e.evt.preventDefault();
                                                                                    ContextMenu.visible = true;
                                                                                    ContextMenu.x = e.evt.offsetX;
                                                                                    ContextMenu.y = e.evt.offsetY;
                                                                                    this.setState({ ContextMenu });
                                                                                }
                                                                            }}
                                                                            onClick={() => {
                                                                                textEdit.visible = false;
                                                                                if (selectedPage >= 0 && selectedType == 'text' && textEdit.visible) {
                                                                                    template.elements[selectedPage][selectedElementIndex].text = textEdit.textValue;
                                                                                    template.elements[selectedPage][selectedElementIndex].visible = true;
                                                                                }
                                                                                this.setState({ textEdit, template, selectedPage: i, selectedElementIndex: index, selectedType: item.type })
                                                                            }}

                                                                            onDragEnd={e => {
                                                                                template.elements[i][index].x = e.target.x();
                                                                                template.elements[i][index].y = e.target.y();
                                                                            }}
                                                                            onTransformEnd={e => {
                                                                                const shape = e.target;
                                                                                // template.elements[i][index].scaleX = shape.scaleX();
                                                                                // template.elements[i][index].scaleY = shape.scaleY();
                                                                                template.elements[i][index].width = shape.width() * shape.scaleX();
                                                                                template.elements[i][index].height = shape.height() * shape.scaleY();
                                                                                template.elements[i][index].rotation = shape.rotation();
                                                                                shape.scaleX(1);
                                                                                shape.scaleY(1);
                                                                            }}

                                                                        />
                                                                    )
                                                                    case 'ring': return (
                                                                        <Ring
                                                                            {...item}
                                                                            key={`${i}${index}`}
                                                                            // name={`Ring${i}${index}`}
                                                                            onContextMenu={e => {
                                                                                if (template.elements[i][index].draggable) {
                                                                                    e.evt.preventDefault();
                                                                                    ContextMenu.visible = true;
                                                                                    ContextMenu.x = e.evt.offsetX;
                                                                                    ContextMenu.y = e.evt.offsetY;
                                                                                    this.setState({ ContextMenu });
                                                                                }
                                                                            }}
                                                                            onClick={() => {
                                                                                textEdit.visible = false;
                                                                                if (selectedPage >= 0 && selectedType == 'text' && textEdit.visible) {
                                                                                    template.elements[selectedPage][selectedElementIndex].text = textEdit.textValue;
                                                                                    template.elements[selectedPage][selectedElementIndex].visible = true;
                                                                                }
                                                                                this.setState({ textEdit, template, selectedPage: i, selectedElementIndex: index, selectedType: item.type })
                                                                            }}

                                                                            onDragEnd={e => {
                                                                                template.elements[i][index].x = e.target.x();
                                                                                template.elements[i][index].y = e.target.y();
                                                                            }}
                                                                            onTransformEnd={e => {
                                                                                const shape = e.target;
                                                                                // template.elements[i][index].scaleX = shape.scaleX();
                                                                                // template.elements[i][index].scaleY = shape.scaleY();
                                                                                template.elements[i][index].width = shape.width() * shape.scaleX();
                                                                                template.elements[i][index].height = shape.height() * shape.scaleY();
                                                                                template.elements[i][index].rotation = shape.rotation();
                                                                                shape.scaleX(1);
                                                                                shape.scaleY(1);
                                                                            }}

                                                                        />
                                                                    )
                                                                    case 'polygon': return (
                                                                        <RegularPolygon
                                                                            {...item}
                                                                            key={`${i}${index}`}
                                                                            // name={`RegularPolygon${i}${index}`}
                                                                            onContextMenu={e => {
                                                                                if (template.elements[i][index].draggable) {
                                                                                    e.evt.preventDefault();
                                                                                    ContextMenu.visible = true;
                                                                                    ContextMenu.x = e.evt.offsetX;
                                                                                    ContextMenu.y = e.evt.offsetY;
                                                                                    this.setState({ ContextMenu });
                                                                                }
                                                                            }}
                                                                            onClick={() => {
                                                                                textEdit.visible = false;
                                                                                if (selectedPage >= 0 && selectedType == 'text' && textEdit.visible) {
                                                                                    template.elements[selectedPage][selectedElementIndex].text = textEdit.textValue;
                                                                                    template.elements[selectedPage][selectedElementIndex].visible = true;
                                                                                }
                                                                                this.setState({ textEdit, template, selectedPage: i, selectedElementIndex: index, selectedType: item.type })
                                                                            }}

                                                                            onDragEnd={e => {
                                                                                template.elements[i][index].x = e.target.x();
                                                                                template.elements[i][index].y = e.target.y();
                                                                            }}
                                                                            onTransformEnd={e => {
                                                                                const shape = e.target;
                                                                                // template.elements[i][index].scaleX = shape.scaleX();
                                                                                // template.elements[i][index].scaleY = shape.scaleY();
                                                                                template.elements[i][index].width = shape.width() * shape.scaleX();
                                                                                template.elements[i][index].height = shape.height() * shape.scaleY();
                                                                                template.elements[i][index].rotation = shape.rotation();
                                                                                shape.scaleX(1);
                                                                                shape.scaleY(1);
                                                                            }}

                                                                        />
                                                                    )
                                                                    case 'line': return (
                                                                        <Line
                                                                            {...item}
                                                                            key={`${i}${index}`}
                                                                            // name={`Line${i}${index}`}
                                                                            onContextMenu={e => {
                                                                                if (template.elements[i][index].draggable) {
                                                                                    e.evt.preventDefault();
                                                                                    ContextMenu.visible = true;
                                                                                    ContextMenu.x = e.evt.offsetX;
                                                                                    ContextMenu.y = e.evt.offsetY;
                                                                                    this.setState({ ContextMenu });
                                                                                }
                                                                            }}
                                                                            onClick={() => {
                                                                                this.setState({ selectedPage: i, selectedElementIndex: index, selectedType: item.type })
                                                                            }}
                                                                            onDragStart={e => {
                                                                                e.target.moveToTop();
                                                                            }}
                                                                            onDragEnd={e => {
                                                                                template.elements[i][index].x = e.target.x();
                                                                                template.elements[i][index].y = e.target.y();
                                                                            }}
                                                                            onTransformEnd={e => {
                                                                                const shape = e.target;
                                                                                template.elements[i][index].scaleX = shape.scaleX();
                                                                                template.elements[i][index].scaleY = shape.scaleY();
                                                                                template.elements[i][index].rotation = shape.rotation();
                                                                            }}
                                                                        />
                                                                    )
                                                                    case 'text': return (
                                                                        <Font
                                                                            {...item}
                                                                            key={`${i}${index}`}
                                                                            // name={`Font${i}${index}`}
                                                                            onContextMenu={e => {
                                                                                if (template.elements[i][index].draggable) {
                                                                                    e.evt.preventDefault();
                                                                                    ContextMenu.visible = true;
                                                                                    ContextMenu.x = e.evt.offsetX;
                                                                                    ContextMenu.y = e.evt.offsetY;
                                                                                    this.setState({ ContextMenu });
                                                                                }
                                                                            }}
                                                                            onClick={() => {
                                                                                textEdit.visible = false;
                                                                                if (selectedPage >= 0 && selectedType == 'text' && textEdit.visible) {
                                                                                    template.elements[selectedPage][selectedElementIndex].text = textEdit.textValue;
                                                                                    template.elements[selectedPage][selectedElementIndex].visible = true;
                                                                                }
                                                                                this.setState({ textEdit, template, selectedPage: i, selectedElementIndex: index, selectedType: item.type })
                                                                            }}
                                                                            onDblClick={e => {
                                                                                if (template.elements[i][index].draggable) {
                                                                                    this.setState({ selectedShapeName: '' })
                                                                                    const absPos = e.target.getAbsolutePosition();
                                                                                    const { textEdit } = this.state;
                                                                                    textEdit.visible = true;
                                                                                    textEdit.textX = item.x;
                                                                                    textEdit.fill = item.fill;
                                                                                    textEdit.textY = item.y;
                                                                                    textEdit.textValue = item.text;
                                                                                    textEdit.fontSize = item.fontSize;
                                                                                    textEdit.width = item.width;
                                                                                    textEdit.height = item.height;
                                                                                    textEdit.fontStyle = item.fontStyle;
                                                                                    textEdit.fontFamily = item.fontFamily;
                                                                                    textEdit.align = item.align
                                                                                    item.visible = false;
                                                                                    this.setState({ textEdit })
                                                                                }
                                                                            }}
                                                                            onDragEnd={e => {
                                                                                template.elements[i][index].x = e.target.x();
                                                                                template.elements[i][index].y = e.target.y();
                                                                            }}
                                                                            onTransformEnd={e => {
                                                                                const shape = e.target;
                                                                                // template.elements[i][index].scaleX = shape.scaleX();
                                                                                // template.elements[i][index].scaleY = shape.scaleY();
                                                                                template.elements[i][index].width = shape.width() * shape.scaleX();
                                                                                template.elements[i][index].height = shape.height() * shape.scaleY();
                                                                                template.elements[i][index].rotation = shape.rotation();
                                                                                shape.scaleX(1);
                                                                                shape.scaleY(1);
                                                                            }}
                                                                        />
                                                                    )
                                                                    case 'image': return (
                                                                        <URLImage
                                                                            {...item}
                                                                            key={`${i}${index}`}
                                                                            name={`Image${i}${index}`}
                                                                            onContextMenu={e => {
                                                                                if (template.elements[i][index].draggable) {
                                                                                    e.evt.preventDefault();
                                                                                    ContextMenu.visible = true;
                                                                                    ContextMenu.x = e.evt.offsetX;
                                                                                    ContextMenu.y = e.evt.offsetY;
                                                                                    this.setState({ ContextMenu });
                                                                                }
                                                                            }}
                                                                            onClick={() => {
                                                                                textEdit.visible = false;
                                                                                if (selectedPage >= 0 && selectedType == 'text' && textEdit.visible) {
                                                                                    template.elements[selectedPage][selectedElementIndex].text = textEdit.textValue;
                                                                                    template.elements[selectedPage][selectedElementIndex].visible = true;
                                                                                }
                                                                                this.setState({ textEdit, template, selectedPage: i, selectedElementIndex: index, selectedType: item.type })
                                                                            }}

                                                                            onDragEnd={e => {
                                                                                template.elements[i][index].x = e.target.x();
                                                                                template.elements[i][index].y = e.target.y();
                                                                            }}
                                                                            onTransformEnd={e => {
                                                                                const shape = e.target;
                                                                                // template.elements[i][index].scaleX = shape.scaleX();
                                                                                // template.elements[i][index].scaleY = shape.scaleY();
                                                                                template.elements[i][index].width = shape.width() * shape.scaleX();
                                                                                template.elements[i][index].height = shape.height() * shape.scaleY();
                                                                                template.elements[i][index].rotation = shape.rotation();
                                                                                shape.scaleX(1);
                                                                                shape.scaleY(1);
                                                                            }}

                                                                        />
                                                                    )
                                                                    case 'path': return (
                                                                        <Path {...item}
                                                                            {...item}
                                                                            key={`${i}${index}`}
                                                                            // name={`Path${i}${index}`}
                                                                            onContextMenu={e => {
                                                                                if (template.elements[i][index].draggable) {
                                                                                    e.evt.preventDefault();
                                                                                    ContextMenu.visible = true;
                                                                                    ContextMenu.x = e.evt.offsetX;
                                                                                    ContextMenu.y = e.evt.offsetY;
                                                                                    this.setState({ ContextMenu });
                                                                                }
                                                                            }}
                                                                            onClick={() => {
                                                                                textEdit.visible = false;
                                                                                if (selectedPage >= 0 && selectedType == 'text' && textEdit.visible) {
                                                                                    template.elements[selectedPage][selectedElementIndex].text = textEdit.textValue;
                                                                                    template.elements[selectedPage][selectedElementIndex].visible = true;
                                                                                }
                                                                                this.setState({ textEdit, template, selectedPage: i, selectedElementIndex: index, selectedType: item.type })
                                                                            }}

                                                                            onDragEnd={e => {
                                                                                template.elements[i][index].x = e.target.x();
                                                                                template.elements[i][index].y = e.target.y();
                                                                            }}
                                                                            onTransformEnd={e => {
                                                                                const shape = e.target;
                                                                                // template.elements[i][index].scaleX = shape.scaleX();
                                                                                // template.elements[i][index].scaleY = shape.scaleY();
                                                                                template.elements[i][index].width = shape.width() * shape.scaleX();
                                                                                template.elements[i][index].height = shape.height() * shape.scaleY();
                                                                                template.elements[i][index].rotation = shape.rotation();
                                                                                shape.scaleX(1);
                                                                                shape.scaleY(1);
                                                                            }}
                                                                        />
                                                                    )

                                                                }
                                                            })
                                                        }
                                                        {template && selectedPage >= 0 && template.elements[selectedPage][selectedElementIndex].draggable &&
                                                            <Transformer selectedShapeName={selectedShapeName} />
                                                        }
                                                    </Layer>
                                                </Stage>
                                                <textarea
                                                    value={textEdit.textValue}
                                                    style={{
                                                        display: textEdit.visible ? "block" : "none",
                                                        position: "absolute",
                                                        top: textEdit.textY + "px",
                                                        left: textEdit.textX + "px",
                                                        color: textEdit.fill && textEdit.fill,
                                                        fontFamily: textEdit.fontFamily && textEdit.fontFamily + "!important",
                                                        fontSize: textEdit.fontSize && textEdit.fontSize + "px",
                                                        textAlign: textEdit.align && textEdit.align,
                                                        width: textEdit.width && textEdit.width,
                                                        height: 'auto',
                                                        border: 'none',
                                                        background: 'none',
                                                        outline: 'none',
                                                        lineHeight: 1,
                                                        padding: 0,
                                                        margin: 0
                                                    }}
                                                    onChange={e => {
                                                        textEdit.textValue = e.target.value;
                                                        this.setState({
                                                            textEdit
                                                        })
                                                    }}
                                                    onKeyDown={e => {
                                                        if (e.keyCode === 13) {

                                                            textEdit.visible = false;
                                                            template.elements[selectedPage][selectedElementIndex].text = textEdit.textValue;
                                                            template.elements[selectedPage][selectedElementIndex].visible = true;
                                                            this.setState({
                                                                textEdit,
                                                                template
                                                            });
                                                        }
                                                    }
                                                    }
                                                />
                                            </div>
                                        </div>
                                    )
                                )}
                                {/* <div className="design-change-zoomable p-1">
                                    <Dropdown className="sm-d-none" overlay={menuSizeZoomable} placement="topCenter" trigger={["click"]}>
                                        <Button type="primary" shape="round">{percentScale}%</Button>
                                    </Dropdown>
                                </div> */}
                            </Layout>

                        </Content>
                    </Layout>
                </Layout>
            </Layout>
        );
    }
}

const mapStateToProps = (state) => ({
    userInfo: state.auth.user,
    loading: state.loading.loading
})

const mapDispatchToProps = dispatch => {
    return {
        loadingStart: (text) => dispatch(loadingStart(text)),
        loadingEnd: () => dispatch(loadingEnd()),
        onLogOut: () => dispatch(logOut()),
    }
}

export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(CreateTemplate))