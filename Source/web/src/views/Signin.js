import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next';

// Import Store
import { signin, signinFacebook, signinGoogle } from '../store/actions/auth'
import { setCurrentPath } from '../store/actions/all'

import {
    Button,
    Form,
    Input,
    Icon,
    Typography
} from 'antd';

import { Link } from 'react-router-dom';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import GoogleLogin from 'react-google-login';


export class Signin extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount= () => {
        const { onSetCurrentPath, location, t } = this.props
        onSetCurrentPath(location.pathname)
        document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
    }

    componentWillReceiveProps = (nextProps) => {
        const { location, t } = nextProps;
        document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err,values) => {
            if (!err) {
                const { onSignin } = this.props;
                onSignin(values)
            }
        });
    }

    responseFacebook = (response) => {
        const { onSigninFacebook } = this.props;
        onSigninFacebook(response.accessToken)
    }
    responseFailFacebook = (response) => {
        console.log(response);
    }
    responseGoogle = (response) => {
        const { onSigninGoogle } = this.props;
        onSigninGoogle(response.accessToken)
    }
    responseFailGoogle = (response) => {
        console.log(response);
    }

    render() {
        const { getFieldDecorator } = this.props.form;

        const { t } = this.props;
        const { Text } = Typography;

        const SvgFacebook = () => (
            <img src="socials/facebook.svg"/>  
        );
        const IconFacebook = props => <Icon component={SvgFacebook} {...props} />;

        return (
            <div className="container" >
                <div className="row d-flex justify-content-center align-items-center align-items-center" style={{minHeight: '100vh'}}>
                    <div className="col-md-4 p-3 ">
                        <h2 className="mb-5 font-weight-bold text-center">{t('lbl_sign_in')}</h2>
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Item className="mb-0 px-3">
                                {getFieldDecorator('email', {
                                    rules: [{ required: true, message: t("required", {field: t('lbl_email_address')}) }],
                                })(
                                    <Input
                                        placeholder={t('lbl_email_address')}
                                        prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                        size="large"
                                        allowClear
                                        // value={signinData.email}
                                        id="email"
                                        // onChange={ this.onChangeText }
                                    />,
                                )}
                            </Form.Item>
                            <Form.Item className="mb-0 px-3">
                                {getFieldDecorator('password', {
                                    rules: [
                                        { required: true, message: t("required", {field: t('lbl_password')}) },
                                        // { min: 8, message: t("min", {field: t('lbl_password'), number: 8}) }
                                    ],
                                })(
                                    <Input.Password
                                        placeholder={t('lbl_password')}
                                        prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                        size="large"
                                        allowClear
                                        id="password"
                                        // value={signinData.password}
                                        // onChange={ this.onChangeText }
                                        className="mt-4"
                                    />,
                                )}
                            </Form.Item>
                            <Form.Item className="mb-0 px-3">
                                <Button htmlType="submit" block type="primary" shape="round"  size="large" className="mt-4 " >
                                    {t('lbl_login')}
                                </Button>
                            </Form.Item>
                        </Form>
                        <Link to="/forgotPassword">
                            <Button type="link" block className="mt-2   ">
                                {t('lbl_forgot_password')}?
                            </Button>
                        </Link>
                        <Text className="text-center d-block mt-4 ">{t('lbl_login_with_social')}</Text>
                        <div className="row justify-content-center ">
                            <FacebookLogin
                                appId="2394769104096765"
                                callback={this.responseFacebook}
                                onFailure={this.responseFailFacebook}
                                fields="name,email,picture"
                                scope="public_profile"
                                render={renderProps => (
                                    <Button onClick={renderProps.onClick } type="primary" style={{width: 120}} shape="round" className="d-flex justify-content-center mt-3 align-items-center mx-1" ><IconFacebook />Facebook</Button>
                                )}
                            />
                            <GoogleLogin
                                clientId="112794756574-8gjk6lscsnkak7da0tpsgpmrej3tfaob.apps.googleusercontent.com"
                                render={renderProps => (
                                    <Button onClick={renderProps.onClick} type="danger" style={{width: 120}} shape="round" className="d-flex justify-content-center mt-3 align-items-center mx-1" icon="google" >Google</Button>
                                )}
                                onSuccess={this.responseGoogle}
                                onFailure={this.responseFailGoogle}
                                cookiePolicy={'single_host_origin'}
                            />
                        </div>
                        <div className="text-center d-block-sm mt-3">
                            {t('lbl_no_account')} &nbsp; <Link to="/register"><Button type="link" className="px-0">{t('lbl_sign_up')}</Button></Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = dispatch => {
    return {
        onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
        onSignin: (signinData) => dispatch(signin(signinData)) ,
        onSigninFacebook: (token) => dispatch(signinFacebook(token)),
        onSigninGoogle: (token) => dispatch(signinGoogle(token))
    }
}

export default Form.create({ name: 'signin' })(withTranslation()(connect(mapStateToProps, mapDispatchToProps)(Signin)));