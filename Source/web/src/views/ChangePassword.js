import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next';

// Import Store
import { setCurrentPath } from '../store/actions/all'
import { changePassword } from '../store/actions/auth'

import authService from '../services/auth' 

import {
  Button,
  Divider,
  Form,
  Input,
  Icon,
  Card,
  Row,
  Col,
  Typography
} from 'antd';

import { Link } from 'react-router-dom';

export class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sizeMobile: 576,
      resizeWidth: window.innerWidth,
    }
  }

  componentDidMount= () => {
    window.addEventListener('resize', this.handleResize);
    const { onSetCurrentPath, location, t } = this.props
    onSetCurrentPath(location.pathname);
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
  }

  componentWillReceiveProps = (nextProps) => {
    const { location, t } = nextProps;
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
  }

  handleResize = (e) => {
    e.preventDefault();
    this.setState({
      resizeWidth: window.innerWidth
    })
  }

  handleSubmit = e => {
    e.preventDefault();
    const { onChangePassword, userInfo } = this.props;
    const { email } = this.state;
    this.props.form.validateFields((err,values) => {
      if (!err) {
        onChangePassword(userInfo.email, values.current_password, values.password)
      }
    });
  }

  compareToFirstPassword = (rule, value, callback) => {
    const { form, t } = this.props;
    if (value && value !== form.getFieldValue('password')) {
        callback(t('confirmed'));
    } else {
        callback();
    }
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { t } = this.props;
    const { resizeWidth, sizeMobile } = this.state;
    const { Title ,Text } = Typography;

    return (
      <Row style={{padding: resizeWidth < sizeMobile ? '0 12px' : '0 24px'}} className="py-3">
        <Row gutter={24}>
          <Col md={{span: 24}} lg={{span: 24}}>
            <Card title={null} bordered={false} className="mb-4 shadow-sm position-relative" >
              <Title level={4}>{t('lbl_change_password')}</Title>
              <Divider className="my-0" style={{backgroundColor: '#1890ff', height: 2}}/>
              <Row gutter={24} type={resizeWidth >= sizeMobile ? "flex" : ''} justify="center" className=" mx-0 mt-3" style={{padding: '0!important'}}>
                <Col md={{span: 24}} lg={{span: 10}}>
                  <Form onSubmit={this.handleSubmit} hasFeedback >
                    <Form.Item className="mb-0 mt-3">
                        {getFieldDecorator('current_password', {
                            rules: [
                              { required: true, message: t("required", {field: t('lbl_current_password')}) },
                            ],
                        })(
                          <Input.Password
                            placeholder={t('lbl_current_password')}
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            size="large"
                            allowClear
                            id="password"
                          />,
                        )}
                    </Form.Item>
                    <Form.Item className="mb-0 mt-2">
                        {getFieldDecorator('password', {
                            rules: [
                              { required: true, message: t("required", {field: t('lbl_new_password')}) },
                              { min: 8, message: t("min", {field: t('lbl_new_password'), number: 8}) }
                            ],
                        })(
                          <Input.Password
                            placeholder={t('lbl_new_password')}
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            size="large"
                            allowClear
                            id="password"
                          />,
                        )}
                    </Form.Item>
                    <Form.Item className="mb-0 mt-2">
                        {getFieldDecorator('confirm_password', {
                            rules: [
                              { required: true, message: t("required", {field: t('lbl_passsword_confirm')}) },
                              { validator: this.compareToFirstPassword }
                            ],
                        })(
                          <Input.Password
                            placeholder={t('lbl_passsword_confirm')}
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            size="large"
                            allowClear
                            id="confirm_password"
                          />,
                        )}
                    </Form.Item>
                    <Form.Item className="mb-0 px-3">
                      <Button htmlType="submit" block type="primary" shape="round"  size="large" className="mt-4 " loading={this.state.loading}>
                          {t('lbl_change_password')}
                      </Button>
                    </Form.Item>
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </Row>
    )
  }
}

const mapStateToProps = (state) => ({
  userInfo: state.auth.user
})

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
    onChangePassword: (email, current_password, password) => dispatch(changePassword(email, current_password, password))
  }
}

export default Form.create({ name: 'changePassword' })(withTranslation()(connect(mapStateToProps, mapDispatchToProps)(ChangePassword)))
