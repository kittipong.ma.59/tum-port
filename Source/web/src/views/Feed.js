import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next';

// Import Store
import { setCurrentPath, loadingStart, loadingEnd, getNotifications } from '../store/actions/all'
import { currentUser } from '../store/actions/auth'
import feedServices from '../services/feed';
import authServices from '../services/auth';

import config from '../config';

import '../scss/feed.scss'

import ReactSelect from 'react-select'

import {
  Button,
  Row,
  Col,
  Empty,
  Card,
  List,
  Divider,
  Modal,
  Select,
  Icon,
  Typography,
  Tooltip,
  Tabs,
  DatePicker
} from 'antd';

import SocketIO from 'socket.io-client';
import cheerio from 'cheerio';
import moment from 'moment';
let Parser = require('rss-parser');
let parser = new Parser();

export class Feed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sizeMobile: 576,
      resizeWidth: window.innerWidth,
      feed: [],
      filterVisable: false,
      selectUniversity: [],
      selectYear: [
        { value: 'TCAS 63 / รับตรง 63', label: '2563' },
        { value: 'TCAS 64 / รับตรง 64', label: '2564' },
      ],
      selectType: [
        { value: 'รับตรง/TCAS', label: 'รับตรง/TCAS' },
        { value: 'ครูคืนถิ่น', label: 'ครูคืนถิ่น' },
        { value: 'ทุนการศึกษา', label: 'ทุนการศึกษา' },
        { value: 'คอร์สติวฟรี', label: 'คอร์สติว' },
      ],
      selectCategory: [
        { value: "รับตรง TCAS รอบ 1", label: 'รับตรง TCAS รอบ 1' },
        { value: "รับตรง TCAS รอบ 2", label: 'รับตรง TCAS รอบ 2' },
        { value: "รับตรง TCAS รอบ 3", label: 'รับตรง TCAS รอบ 3' },
        { value: "รับตรง TCAS รอบ 4", label: 'รับตรง TCAS รอบ 4' },
        { value: "รับตรง TCAS รอบ 5", label: 'รับตรง TCAS รอบ 5' },
        { value: "รับตรงไม่เข้าร่วมระบบ TCAS", label: 'รับตรงไม่เข้าร่วมระบบ TCAS' },
        { value: "รับตรงเด็กซิ่ว", label: 'รับตรงเด็กซิ่ว' },
        { value: "รับตรงวุฒิเทียบเท่า", label: 'รับตรงวุฒิเทียบเท่า' },
        { value: "รับตรงไม่ใช้เกรดขั้นต่ำ", label: 'รับตรงไม่ใช้เกรดขั้นต่ำ' },
        { value: "รับตรงใช้ GAT-PAT", label: 'รับตรงใช้ GAT-PAT' },
        { value: "รับตรงไม่ใช้ GAT-PAT", label: 'รับตรงไม่ใช้ GAT-PAT' },
        { value: "รับตรงใช้ 9 วิชาสามัญ", label: 'รับตรงใช้ 9 วิชาสามัญ' },
        { value: "รับตรงไม่ใช้ 9 วิชาสามัญ", label: 'รับตรงไม่ใช้ 9 วิชาสามัญ' },
        { value: "รับตรงสอบข้อเขียน", label: 'รับตรงสอบข้อเขียน' },
        { value: "รับตรงสอบสัมภาษณ์อย่างเดียว", label: 'รับตรงสอบสัมภาษณ์อย่างเดียว' },
        { value: "รับตรงผู้จบปวช.", label: 'รับตรงผู้จบปวช.' },
        { value: "รับตรงผู้จบปวส.", label: 'รับตรงผู้จบปวส.' },
        { value: "รับน้องม.6", label: 'รับน้องม.6' }
      ],
      filter: {
        year: null,
        university: null,
        type: null,
        category: null
      },
      startDate: null,
      endDate: null
    }
  }

  // fetchDataEnttrong = async () => {
  //   const { userInfo } = this.props;
  //   const response = await feedServices.getEnttrong();
  //   for (let i = 0; i < response.data.length; i++) {
  //     response.data[i].readed = response.data[i].read_by.filter(k => k.userId == userInfo._id).length > 0 ? true : false;
  //   }
  //   this.setState({
  //     notifData: response.data
  //   })
  // }
  // fetchDataSangfans = async () => {
  //   const { userInfo } = this.props;
  //   let count = 0;
  //   const response = await feedServices.getSangfans();
  //   for (let i = 0; i < response.data.length; i++) {
  //     response.data[i].readed = response.data[i].read_by.filter(k => k.userId == userInfo._id).length > 0 ? true : false;
  //   }
  //   this.setState({
  //     sangfansData: response.data
  //   })
  // }

  componentDidMount= async () => {
    const { onSetCurrentPath, location, loadingStart, loadingEnd, t, userInfo, getNotifications, onCurrentUser } = this.props;
    loadingStart(null)
    onSetCurrentPath(location.pathname)
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
    const university = await feedServices.getUniversity();
    const selectUniversity = university.data.map(item =>  ({ value: item.university, label: item.university }) )
    this.socket = SocketIO(config.socket);
    this.socket.on('feedChange', async () => {
      await getNotifications(userInfo._id);
      await authServices.readNewFeed(userInfo._id);
      const token = localStorage.getItem('TOKEN');
      if(token){
        await onCurrentUser(token);
      }
      this.filterFeed();
    })
    await getNotifications(userInfo._id)
    await authServices.readNewFeed(userInfo._id);
    const token = localStorage.getItem('TOKEN');
    if(token){
      await onCurrentUser(token);
    }
    this.setState({selectUniversity, feed: this.props.feed.data})
    loadingEnd()
  }

  componentWillReceiveProps = (nextProps) => {
    const { location, t } = nextProps;
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
  }

  handleResize = (e) => {
    e.preventDefault();
    this.setState({
      resizeWidth: window.innerWidth
    })
  }

  linkToDetail =async (id) => {
    const { userInfo, getNotifications } = this.props;
    await feedServices.updateReadBy(id, userInfo._id);
    getNotifications(userInfo._id);
    // const enttrongLink = 'https://www.enttrong.com';
    // const sangfansLink = 'https://www.sangfans.com';
    // const feedURL = id.includes(enttrongLink) ? enttrongLink : sangfansLink;
    // if(feedURL == enttrongLink) {
      
    // } else {
    //   await feedServices.updateReadBy(id, userInfo._id);
    //   getNotifications(userInfo._id);
    // }
    this.props.history.push(`/feed/feedDetail`, { id: id})
  }
  selectFilter = (item, name) => {
    const { filter, selectYear, selectUniversity, selectType, selectCategory, getNotifications, userInfo } = this.state;
    if(item) {
      if(name == 'year') {
        filter.year = item.value;
      } else if(name == 'university') {
        filter.university = item.value;
      } else if(name == 'type') {
        filter.type = item.value;
      } else if(name == 'category') {
        filter.category = item.value;
      }
    } else {
      if(name == 'year') {
        filter.year = null;
      } else if(name == 'university') {
        filter.university = null;
      } else if(name == 'type') {
        filter.type = null;
      } else if(name == 'category') {
        filter.category = null;
      }
    }
    this.setState({filter})
    this.filterFeed()
  }

  filterFeed = async () => {
    const { filter, startDate, endDate } = this.state;
    const filterData = Object.values(filter).filter(val => val != null);
    const defaultFeed = this.props.feed.data.map(obj => Object.assign({}, obj));
    defaultFeed.forEach(item => {
      if(item.filters && filterData.length > 0) {
        for (let i = 0; i < filterData.length; i++) {
          if(item.filters.includes(filterData[i])) {
            item.status = true;
          } else {
            item.status = false;
            break;
          }
        }
      }
    });
    let result = defaultFeed.filter(k => k.status != false)
    if(startDate && endDate) {
      result = result.filter(k => moment(k.pubDate) >= moment(startDate).startOf('day') && moment(k.pubDate) <= moment(endDate).endOf('day'))
    } else if(startDate) {
      result = result.filter(k => moment(k.pubDate) >= moment(startDate).startOf('day'))
    } else if(endDate) {
      result = result.filter(k => moment(k.pubDate) <= moment(endDate).endOf('day'))
    }
    this.setState({feed: result});
  }

  render() {
    const { resizeWidth, sizeMobile } = this.state;
    const { userInfo, t, i18n } = this.props;
    
    const TimeSvg = () => (
      <svg fill="currentColor" t="1583369362220" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="5773" width="12" height="12"><path d="M505.984 975.317333c-259.2 0-469.333333-210.133333-469.333333-469.333333s210.133333-469.333333 469.333333-469.333333 469.333333 210.133333 469.333333 469.333333S765.226667 975.317333 505.984 975.317333zM505.984 124.672c-210.602667 0-381.354667 170.709333-381.354667 381.354667 0 210.602667 170.709333 381.354667 381.354667 381.354667 210.602667 0 381.354667-170.752 381.354667-381.354667C887.338667 295.381333 716.586667 124.672 505.984 124.672zM696.661333 550.016l-88.021333 0-58.666667 0-43.989333 0 0 0c-24.32 0-43.989333-19.712-43.989333-43.989333L461.994667 242.005333c0-24.277333 19.669333-43.989333 43.989333-43.989333 24.277333 0 43.989333 19.712 43.989333 43.989333l0 219.989333 58.666667 0 88.021333 0c24.277333 0 43.989333 19.712 43.989333 43.989333S720.938667 550.016 696.661333 550.016z" p-id="5774"></path></svg>
    );

    const DotSvg = () => (
      <svg t="1583442693367" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3884" width="8" height="8">
        <path d="M512 512m-512 0a512 512 0 1 0 1024 0 512 512 0 1 0-1024 0Z" fill="currentColor" p-id="3885"></path>
      </svg>
    );

    const TimeIcon = props => <Icon component={TimeSvg} {...props} />;
    const DotIcon = props => <Icon component={DotSvg} {...props} />;
    const colorList = [
      { value: 'ocean', label: 'Ocean', color: '#00B8D9', isFixed: true },
      { value: 'blue', label: 'Blue', color: '#0052CC', isDisabled: true },
      { value: 'purple', label: 'Purple', color: '#5243AA' },
      { value: 'red', label: 'Red', color: '#FF5630', isFixed: true },
      { value: 'orange', label: 'Orange', color: '#FF8B00' },
      { value: 'yellow', label: 'Yellow', color: '#FFC400' },
      { value: 'green', label: 'Green', color: '#36B37E' },
      { value: 'forest', label: 'Forest', color: '#00875A' },
      { value: 'slate', label: 'Slate', color: '#253858' },
      { value: 'silver', label: 'Silver', color: '#666666' },
    ];
    

    return (
      <Row style={{padding: resizeWidth < sizeMobile ? '0 12px' : '0 30px'}} className="pt-0 pb-3 home-page">
        <Row gutter={24}>
          <Col sm={{span: 12}} xs={{span: 24}}>
            <Typography.Title level={4} className="mb-1">{t('lbl_feed')}</Typography.Title> 
            <p style={{fontSize: 14}} className="text-muted">{t('lbl_feed_title')}</p>
          </Col>
          <Col sm={{span: 12}} xs={{span: 24}} className="text-right">
            <Button className="mb-3" onClick={() => this.setState({ filterVisable: !this.state.filterVisable })}>
              <Icon type={this.state.filterVisable ? 'close' : 'filter'} style={{position: 'relative', top: -3}}/>
              {t('lbl_filter')}
            </Button>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={24}>
            <Tabs key="tab-feed" defaultActiveKey="2" className="tabs-feed" type="card" tabBarStyle={{display: 'none'}}>
              <Card className="border-0 mb-3" style={{display: this.state.filterVisable ? '' : 'none', borderRadius: 10, boxShadow: '0 0 3px 3px rgba(0, 0, 0, 0.03)'}}>
                <Row gutter={24}>
                  <Col sm={{span: 12}} xs={{span:24}} className="mb-2">
                    <label htmlFor="filterYear" className="font-weight-bold" style={{fontSize: 16}}>{t('lbl_academic_year')}</label>
                    <ReactSelect 
                      id="filterYear"
                      placeholder={'-- '+t('lbl_all_academic_year')+' --'}
                      onChange={item => this.selectFilter(item, 'year')}
                      options={this.state.selectYear}
                      isClearable={true}
                      isSearchable={true}
                      styles={{
                        control: styles => ({ ...styles, backgroundColor: 'white', height: 35, minHeight: 'auto'}),
                      }}
                    />
                  </Col>
                  <Col sm={{span: 12}} xs={{span:24}} className="mb-2">
                    <label htmlFor="filterUniversity" className="font-weight-bold" style={{fontSize: 16}}>{t('lbl_educational_institution')}</label>
                    <ReactSelect 
                      id="filterUniversity"
                      options={this.state.selectUniversity}
                      onChange={item => this.selectFilter(item, 'university')}
                      placeholder={'-- '+t('lbl_all_institutions')+' --'}
                      isClearable={true}
                      isSearchable={true}
                      styles={{
                        control: styles => ({ ...styles, backgroundColor: 'white', height: 35, minHeight: 'auto'}),
                      }}
                    />
                  </Col>
                  <Col sm={{span: 12}} xs={{span:24}} className="mb-2">
                    <label htmlFor="filterType" className="font-weight-bold" style={{fontSize: 16}}>{t('lbl_type')}</label>
                    <ReactSelect 
                      id="filterType"
                      placeholder={'-- '+t('lbl_all_type')+' --'}
                      onChange={item => this.selectFilter(item, 'type')}
                      options={this.state.selectType} 
                      isClearable={true}
                      isSearchable={true}
                      styles={{
                        control: base => ({
                          ...base,
                          height: 35,
                          minHeight: 35
                        })
                      }}
                    />
                  </Col>
                  <Col sm={{span: 12}} xs={{span:24}} className="mb-2">
                    <label htmlFor="filterCategory" className="font-weight-bold" style={{fontSize: 16}}>{t('lbl_categories')}</label>
                    <ReactSelect 
                      id="filterCategory"
                      placeholder={'-- '+t('lbl_all_category')+' --'}
                      onChange={item => this.selectFilter(item, 'category')}
                      options={this.state.selectCategory} 
                      isClearable={true}
                      isSearchable={true}
                      styles={{
                        control: base => ({
                          ...base,
                          height: 35,
                          minHeight: 35
                        })
                      }}
                    />
                  </Col>
                  <Col sm={{span: 12}} xs={{span:24}} className="mb-2">
                    <label htmlFor="filterStartDate" className="font-weight-bold" style={{fontSize: 16}}>{t('lbl_start_date')}</label>
                    <DatePicker 
                      id="filterStartDate" 
                      className="w-100"
                      size="default"
                      locale={i18n.language == 'en-US' ?'en': 'th'}
                      inputReadOnly={true}
                      format="LL"
                      placeholder={t('lbl_start_date')}
                      disabledDate={current => {
                        const { endDate } = this.state;
                        if(endDate) {
                          return  current && current > moment(endDate).endOf("day")
                        } else {
                          return  current && current > moment().endOf("day")
                        }
                      }}
                      onChange={async (date) => {
                        await this.setState({startDate: date})
                        this.filterFeed()
                      }}
                    />
                  </Col>
                  <Col sm={{span: 12}} xs={{span:24}} className="mb-2">
                    <label htmlFor="filterEndDate" className="font-weight-bold" style={{fontSize: 16}}>{t('lbl_end_date')}</label>
                    <DatePicker 
                      id="filterEndDate" 
                      className="w-100"
                      size="default"
                      locale={i18n.language == 'en-US' ?'en': 'th'}
                      inputReadOnly={true}
                      format="LL"
                      placeholder={t('lbl_end_date')}
                      disabledDate={current => {
                        const { startDate } = this.state;
                        if(startDate) {
                          return  current && current > moment() || current < moment(startDate).startOf('day')
                        } else {
                          return  current && current > moment()
                        }
                      }}
                      onChange={async (date) => {
                        await this.setState({endDate: date})
                        this.filterFeed()
                      }}
                    />
                  </Col>
                </Row>
              </Card>
              <List
                className="noti-feed p-0"
                itemLayout="horizontal"
                locale={{emptyText: (<Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={t('lbl_no_data')} />)}}
                dataSource={this.state.feed}
                pagination={{
                  onChange: page => {
                    window.scrollTo({
                      top: 0,
                      behavior: "smooth"
                    });
                  },
                  pageSize: 20,
                }}
                renderItem={(item, index) => (
                  <Card 
                    title={[
                      <p className="mb-0 cursor-pointer hover-link-title" onClick={() => this.linkToDetail(item._id)}>
                        {!item.readed && 
                          <DotIcon className="position-relative" style={{color: 'red', top: -7}}/>
                        }
                        {item.title}
                      </p>,
                      <p className="mb-0 text-muted" style={{fontSize: 12}}>
                        <TimeIcon className="position-relative mr-1" style={{top: -3}}/>
                        {moment(item.date).locale(i18n.language == 'en-US' ?'en': 'th').add(543, 'years').format('LLL')}
                        <Divider type="vertical" />
                        <a className="btn-link text-dark" href={item.meta.link == 'https://www.enttrong.com/' ? 'https://www.enttrong.com/': 'https://www.sangfans.com/'} target='_blank'>{item.meta.link == 'https://www.enttrong.com/' ? 'Enttrong': 'Sangfans'}</a>
                        <Divider type="vertical" className="sm-d-none"/>
                        {resizeWidth < sizeMobile ? 
                          <p>
                            <Icon type="eye" className="position-relative mr-1" style={{top: -3}}/>
                            {item.readed ? t('lbl_reader'): t('lbl_no_reader')}
                          </p>
                        : [
                          <Icon type="eye" className="position-relative mr-1" style={{top: -3}}/>,
                          item.readed ? t('lbl_reader'): t('lbl_no_reader')
                        ]}
                      </p>,
                    ]}
                    bordered={false} 
                    className="mx-1 mt-1 mb-4" 
                    style={{borderRadius: 10, boxShadow: '0 0 3px 3px rgba(0, 0, 0, 0.03)'}}
                  >
                    <Card.Meta
                      avatar={<img src={item.logo} className={item.meta.link == 'https://www.enttrong.com/' ? "w-60" : "w-100"}/>}
                      description={[
                        <p className="mb-0">{item.detail}</p>
                      ]}
                    />
                    <div className="action-detail">
                      <Button type="link" className="py-0" onClick={() => this.linkToDetail(item._id)}>{t('lbl_see_detail')}</Button>
                    </div>
                  </Card>
                )}
              />
            </Tabs>
          </Col>
        </Row>
      </Row>
    )
  }
}

const mapStateToProps = (state) => ({
  userInfo: state.auth.user,
  feed: state.feed
})

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
    loadingStart: (text) => dispatch(loadingStart(text)) ,
    loadingEnd: () => dispatch(loadingEnd()) ,
    getNotifications: (userId) => dispatch(getNotifications(userId)),
    onCurrentUser: (token) => dispatch(currentUser(token)),
  }
}

export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(Feed))
