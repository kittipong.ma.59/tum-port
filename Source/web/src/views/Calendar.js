import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next';

// Import Store
import { setCurrentPath } from '../store/actions/all'

import googleService from '../services/google';

import {
  Button,
  Row,
  Col,
  Empty,
  Card,
  Divider,
  Select,
  Icon,
  Typography,
  Tooltip
} from 'antd';

export class Template extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sizeMobile: 576,
      resizeWidth: window.innerWidth,
    }
  }

  componentDidMount= () => {
    const { onSetCurrentPath, location, t } = this.props
    onSetCurrentPath(location.pathname)
    window.addEventListener('resize', this.handleResize);
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
    googleService.connect();
  }

  componentWillReceiveProps = (nextProps) => {
    const { location, t } = nextProps;
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
  }

  handleResize = (e) => {
    e.preventDefault();
    this.setState({
      resizeWidth: window.innerWidth
    })
  }

  render() {
    const {t} = this.props;
    const {resizeWidth, sizeMobile, template} = this.state;
    const { Title } = Typography;
    const { Option } = Select;

    return (
      <Row style={{padding: resizeWidth < sizeMobile ? '0 12px' : '0 30px'}} className="py-3 home-page">
        <Row gutter={24}>
          <Col span={24}>
            <Title level={4}>{t('Test Calendar')}</Title>
          </Col>
        </Row>
      </Row>
    )
  }
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
  }
}

export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(Template))
