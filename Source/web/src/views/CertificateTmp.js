import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next'

// Import Scss
import '../scss/information.scss'

// Import Store
import { setCurrentPath } from '../store/actions/all'

// Import Services
import infoService from '../services/info';

import {
  Button,
  Divider,
  Icon,
  Card,
  Table,
  Typography,
  Modal,
  Upload,
  Form,
  Empty
} from 'antd';

export class Certificate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resizeWidth: window.innerWidth,
      sizeMobile: 576,
      dataSource: [],
      previewVisible: false
    }
  }

  componentDidMount= async () => {
    window.addEventListener('resize', this.handleResize);
    const { onSetCurrentPath, location, userInfo } = this.props
    onSetCurrentPath(location.pathname)
    const { dataSource } = this.state;
    this.setState({
      dataSource: [
        {
          _id: '1',
          image: 'https://scontent.fubp1-1.fna.fbcdn.net/v/t1.0-9/11406795_824179700984252_3165897189238338852_n.jpg?_nc_cat=108&_nc_oc=AQmf0kGG8A1lgU3a2S9ehK8G1X0-m7-MKQ0CCLxUJYD8b5BdtSLHzyiuvdiLt3_EYjxhLsCsB7I51_9A1JN_ZGhV&_nc_ht=scontent.fubp1-1.fna&oh=d9bda48a0dfc31db5d164908e8616cf5&oe=5E8F34CE',
          title: "กิจกรรมวันแม่แห่งชาติ",
          description: 'ได้รับหน้าที่เป็นนักร้องเพลงวันแม่ ในวันแม่แห่งชาติ ประจำปีการศึกษา 2560',
        },
        {
          _id: '2',
          image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTSHfbzxzaVWZtiBq2SSuqoXfFvI7SkwAvyoP7ntNvVgdf0EQuG',
          title: "กิจกรรมกีฬาสี 2559",
          description: 'ได้รับหน้าที่เป็นพี่คุมน้องแสตนเชียร์ และได้ับมอบหมายให้เป็นเชียร์ลีดเดอร์ ซึ่งเป็นการฝึกทักษะ ทางด้านความคิดสร้างสรรค์ ความมีวินัย และความสามัคคี ในหมู่คณะ',
        },
      ]
    })
  }

  handleResize = (e) => {
    e.preventDefault();
    this.setState({
      resizeWidth: window.innerWidth
    })
  }

  render() {
    const { t } = this.props;
    const { dataSource, resizeWidth, sizeMobile, previewVisible, imagePreview } = this.state;
    const { Title } = Typography;

    const columns = [
      {
        title: t('lbl_image'),
        dataIndex: 'image',
        key: 'image',
        width: resizeWidth < sizeMobile ? '25%' : '15%',
        align: 'center',
        render: text => ([
          text ? <img onClick={() => this.setState({previewVisible: true, imagePreview: text})} src={text} className="w-100" style={{cursor: 'pointer'}}/>
          : <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} className="m-0" description={t('lbl_no_image')} />
        ])
      }, 
      {
        title: t('lbl_title'),
        dataIndex: 'title',
        key: 'title',
        width: resizeWidth < sizeMobile ? '40%' : '20%',
      },
      {
        title: t('lbl_description'),
        dataIndex: 'description',
        key: 'description',
        className: 'sm-d-none',
        width: '40%'
      },
      {
        title: null,
        key: 'action',
        width: '25%',
        render: () => (
          <span>
            <Button size={resizeWidth < sizeMobile ? "small" : 'default'} icon="form" className="d-inline-flex align-items-center m-1"><span className="text-action">{t('lbl_edit')}</span></Button>
            <Button size={resizeWidth < sizeMobile ? "small" : 'default'} type="danger" icon="delete" className="d-inline-flex align-items-center m-1"><span className="text-action">{t('lbl_delete')}</span></Button>
          </span>
        )
      },
    ]

    return (
      <div className="container activities-page px-md-4">
        <Card title={null} bordered={false} className="mb-4 shadow-sm position-relative" bodyStyle={{padding: resizeWidth < sizeMobile ?'.75rem': '1.5rem'}}>
          <Title level={4}>{t('lbl_certificate')}</Title>
          <Divider className="my-0" style={{backgroundColor: '#1890ff', height: 2}}/>
          <div className="text-right my-2">
            <Button type="primary">{t('lbl_add_activities')}</Button>
          </div>
          <Table 
            columns={columns} 
            dataSource={dataSource} 
            pagination={{ pageSize: 5 }} 
            locale={{emptyText: (<Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={t('lbl_no_data')} />)}}
            expandedRowRender={resizeWidth < sizeMobile ? record => <p style={{ margin: 0 }}>{record.description}</p> : false}
          ></Table>
          <Modal visible={previewVisible} footer={null} onCancel={() => this.setState({previewVisible: false})}>
            <img  style={{ width: '100%' }} src={imagePreview} />
          </Modal>
        </Card>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userInfo: state.auth.user
})

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
  }
}

export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(Certificate))
