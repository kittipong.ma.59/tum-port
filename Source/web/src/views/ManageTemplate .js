import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next';
import { NotificationManager } from 'react-notifications';

import '../scss/home.scss'

import moment from 'moment';

// Import Store
import { loadingStart, loadingEnd, setCurrentPath } from '../store/actions/all'


import infoServices from '../services/info'
import portServices from '../services/port'
import storageService from '../services/storage'

import {
  Form,
  Button,
  Row,
  Col,
  Empty,
  Card,
  Divider,
  Select,
  Icon,
  Typography,
  Tooltip,
  Input,
  Modal
} from 'antd';

import ImageGallery from 'react-image-gallery';

export class ManageTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sizeMobile: 576,
      resizeWidth: window.innerWidth,
      previewVisible: false,
      createVisible: false,
      selectedTemplateId: '',
      imageShow: [],
      template: []
    }
  }

  componentDidMount= async () => {
    const { onSetCurrentPath, location, t, loadingStart, loadingEnd } = this.props
    loadingStart(null);
    onSetCurrentPath(location.pathname)
    window.addEventListener('resize', this.handleResize);
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())

    const response = await portServices.findAllTemplates();
    let templates = response.data;
    for (let i = 0; i < templates.length; i++) {
      for (let j = 0; j < templates[i].image.length; j++) {
        const fileResponse = await storageService.getFile(templates[i].image[j]);
        templates[i].image[j] = 'data:' + fileResponse.data.file.contentType + ';base64,' + fileResponse.data.file.url
      }
    }
    this.setState({ template: templates })
    loadingEnd();
  }

  componentWillReceiveProps = (nextProps) => {
    const { location, t } = nextProps;
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
  }

  handleResize = (e) => {
    e.preventDefault();
    this.setState({
      resizeWidth: window.innerWidth
    })
  }

  handleShowImage = (index) => {
    const { template } = this.state;
    let images = [];
    (template[index].image).forEach(key => {
      images.push({original: key, thumbnail: key}) 
    })
    this.setState({imageShow: images})
    this.setState({previewVisible: true})
  }

  handleDelete = (index) => {
    const { t } = this.props;
    const { template } = this.state;
    Modal.confirm({
      title: t('lbl_confirm_delete'),
      okText: t('lbl_ok'),
      cancelText: t('lbl_cancel'),
      onOk: async () => {
        const id = template[index]._id;
        await portServices.deleteTemplate(id);
        await template[index].image.forEach(async (item, index) => {
          await storageService.removeFile(item);
        });
        this.setState({
          template: template.filter(item => item._id != id)
        })
      },
      onCancel() {},
    });
  }
  createTemplate = async () => {
    const { loadingStart, loadingEnd, userInfo, t } = this.props;
    loadingStart();
    try {
      const theme = await portServices.getAllTheme()
      const data = {
        image: [],
        theme: theme.data[0],
        elements: [
          {
            type: "bg",
            x: 0,
            y: 0,
            width: 793.701,
            height: 1122.52,
            fill: "#ffffff"
          },
        ]
      }
      const response = await portServices.createTemplate(data);
      this.props.history.push(`/designTemplate/${response.data._id}`)
    } catch (err) {
      console.log(err)
    } finally {
      loadingEnd();
    }
  }

  render() {
    const {t} = this.props;
    const { getFieldDecorator } = this.props.form;
    const {resizeWidth, sizeMobile, template, previewVisible, imageShow, createVisible, selectedTemplateId} = this.state;
    const { Title } = Typography;
    const { Option } = Select;

    const AddSvg = () => (
      <svg t="1581171216570" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="6708" width="40" height="40">
        <path d="M512 512m-448 0a448 448 0 1 0 896 0 448 448 0 1 0-896 0Z" fill="#1890ff" p-id="6709" data-spm-anchor-id="a313x.7781069.0.i10" class="selected"></path>
        <path d="M448 298.666667h128v426.666666h-128z" fill="#FFFFFF" p-id="6710"></path>
        <path d="M298.666667 448h426.666666v128H298.666667z" fill="#FFFFFF" p-id="6711"></path>
      </svg>
    );

    const AddIcon = props => <Icon component={AddSvg} {...props} />;

    return (
      <Row style={{padding: resizeWidth < sizeMobile ? '0 12px' : '0 30px'}} className="py-3 home-page">
        <Modal width={resizeWidth < sizeMobile ? resizeWidth-30 : 400} style={{top: resizeWidth < sizeMobile ? null : 24}} bodyStyle={{padding: 0}} visible={previewVisible} footer={null} onCancel={() => this.setState({previewVisible: false, imageShow: []})}>
          <ImageGallery 
            items={imageShow}
            infinite={false} 
            showIndex={true}
            showBullets={true}
            showThumbnails={false}
          />
        </Modal>
        <Row gutter={24}>
          <Col span={24}>
            <Title level={4}>{t('lbl_manage_template')}</Title>
          </Col>
        </Row>
        {template.length == 0 ?
        <Row gutter={24}>
          <Col span={24} style={{minHeight: 400}} className="d-flex align-items-center justify-content-center">
            <Empty
              image="https://gw.alipayobjects.com/mdn/miniapp_social/afts/img/A*pevERLJC9v0AAAAAAAAAAABjAQAAAQ/original"
              imageStyle={{
                height: 150,
              }}
              description={t('lbl_no_data')}
            />
          </Col>
        </Row>
        :
        <Row gutter={24}>
          {template.map((item, index) => (
              <Col xs={{span: 12}} md={{span: 8}} lg={{span: 6}} className="p-3 mt-2">
                <Card title={null} bordered={false} className="w-100 shadow-sm" headStyle={{fontWeight: 'bold'}} bodyStyle={{padding: 8}}>
                  <div className="image-item-photo">
                    <img src={item.image[0]} className="w-100" style={{marginBottom: 4}}/>
                    <div className="image-item-hover ">
                      <div className="image-item-action p-0 m-0">
                        <Button onClick={() => this.props.history.push(`/designTemplate/${item._id}`)} className="my-2 button-action" style={{minWidth: 100}} type="basic" ghost={true}>{t('lbl_edit')}</Button>
                        <Button onClick={() => this.handleDelete(index)} className="my-2 button-action" style={{minWidth: 100}} type="basic" ghost={true}>{t('lbl_delete')}</Button>
                        <Button onClick={() => this.handleShowImage(index)} className="my-2 button-action" style={{minWidth: 100}} type="basic" ghost={true}>{t('lbl_preview')}</Button>
                      </div>
                    </div>
                  </div>
                  <strong className="mb-0" style={{fontSize: 14}}>{item.name}</strong>
                </Card>
              </Col>
            )
          )}
          <Col xs={{span: 12}} md={{span: 8}} lg={{span: 6}} className="p-3 mt-2">
            <Card title={null} bordered={false} className="w-100 shadow-sm" headStyle={{fontWeight: 'bold'}} bodyStyle={{padding: 8}}>
              <div className="image-item-photo">
                <img src={require('../assets/images/card_create.jpg')} className="w-100" style={{marginBottom: 4}}/>
                <div className="card-create-hover" onClick={() => this.createTemplate()}>
                  <div className="card-create-action w-100 text-center">
                    <AddIcon />
                    <p className="mt-2">{t('lbl_create_a_new')}</p>
                  </div>
                </div>
              </div>
            </Card>
          </Col>
        </Row>
        }
      </Row>
    )
  }
}

const mapStateToProps = (state) => ({
  userInfo: state.auth.user
})

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
    loadingStart: (text) => dispatch(loadingStart(text)),
    loadingEnd: () => dispatch(loadingEnd()),
  }
}

export default Form.create({ name: 'template' })(withTranslation()(connect(mapStateToProps, mapDispatchToProps)(ManageTemplate)))
