import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next'
import Cropper from 'react-cropper';

// Import Store
import { loadingStart, loadingEnd, setCurrentPath } from '../store/actions/all'

// Import Services
import infoService from '../services/info';
import storageService from '../services/storage';

// Import Scss
import '../scss/information.scss'
import 'cropperjs/dist/cropper.css';

import {
  Button,
  Card,
  Col,
  Row,
  Form,
  Input,
  Icon,
  Upload,
  Divider,
  Typography,
  Empty,
  Modal
} from 'antd';

import ImgCrop from 'antd-img-crop';
import { NotificationManager } from 'react-notifications';

export class AddCertificate extends Component {
  constructor(props) {
    super(props);
    this.cropper = React.createRef();
    this.state = {
      resizeWidth: window.innerWidth,
      sizeMobile: 576,
      activitiesFileList: [],
      activityImage: null,
      cropVisible: false
    }
    this.cropperImage = this.cropperImage.bind(this);
  }

  componentDidMount= async () => {
    const { onSetCurrentPath, location, t } = this.props
    onSetCurrentPath(location.pathname)
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
    window.addEventListener('resize', this.handleResize);
  }

  componentWillReceiveProps = (nextProps) => {
    const { location, t } = nextProps;
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
  }

  handleResize = (e) => {
    e.preventDefault();
    this.setState({
      resizeWidth: window.innerWidth
    })
  }

  handleSubmit = async e => {
    e.preventDefault();
    const { loadingStart, loadingEnd, userInfo, t } = this.props;
    const { activitiesFileList } = this.state;
    loadingStart();
    try {
      const { validateFields } = this.props.form;
      const values = await validateFields();
      if(values){
        values.userId = userInfo._id
        if (activitiesFileList.length !== 0 && activitiesFileList[0]) {
          let formData = new FormData();
          formData.append("file", activitiesFileList[0]);
          const response = await storageService.uploadFile(formData);
          values.imageId = response.data.file.id;
          values.status = true;
        } else {
          values.status = false;
        }
        await infoService.addCertificate(values);
        NotificationManager.success(t('lbl_successfully_saved'),null,3000);
        window.scrollTo(0,0)
        this.props.history.go(-1);
      }
    }
    catch(err) {
      console.log(err);
    }
    finally {
      loadingEnd();
    }
  }

  getBase64Avatar =async file => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () =>resolve(reader.result);
      reader.onerror = error => reject(error);
    })
  }

  selectedImage = async (e) => {
    const file = e.target.files[0];
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/jpg' || file.type === 'image/png';
    if(!isJpgOrPng) {
      NotificationManager.error(this.props.t("jpg/png-only"), null ,3000);
    } else {
      let fileList = [];
      let image = null;
      fileList = [{ ...file, status: 'done' }]
      image = await this.getBase64Avatar(file);
      this.setState({
        activitiesFileList: fileList,
        activityImage: image,
        cropVisible: true
      });
    }
  }

  cropperImage = () => {
    let fileList = [];
    let image = this.cropper.current.getCroppedCanvas().toDataURL();
    let arr = image.split(','), mime = arr[0].match(/:(.*?);/)[1]
    let bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }
    let file = new File([u8arr], 'filename', { type: mime });
    fileList = [file]
    this.setState({
      activitiesFileList: fileList,
      activityImage: image,
      cropVisible: false
    });
  }

  render() {
    const { resizeWidth, sizeMobile, activitiesFileList, activityImage } = this.state;
    const { userInfo, t } = this.props;
    const { getFieldDecorator } = this.props.form;
    const { Title } = Typography;

    return (
      <Row style={{padding: resizeWidth < sizeMobile ? '0 12px' : '0 24px'}} className="py-3">
        <Row gutter={24}>
          <Col md={{span: 24}} lg={{span: 24}}>
            <Card title={null} bordered={false} className="mb-4 px-3 shadow-sm position-relative" >
              <Title level={4}>{t('/certificate/addcertificate')}</Title>
              <Divider className="mt-0 mb-3" style={{backgroundColor: '#1890ff', height: 2}}/>
              <Row gutter={24} >
                <Col md={{span: 24}} lg={{span: 12}} className="text-center px-4">
                  {/* <ImgCrop modalTitle={t('lbl_edit_image')} modalWidth={resizeWidth <= sizeMobile ? 300 : 520} width={375} height={375} beforeCrop={(file) => {
                    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/jpg' || file.type === 'image/png';
                    if(!isJpgOrPng) {
                      NotificationManager.error(t("jpg/png-only"), null ,3000);
                    }
                    return isJpgOrPng
                  }} contain={true}> */}
                    {/* <Upload.Dragger
                      name="activities"
                      fileList={activitiesFileList}
                      action={() => {}}
                      style={{ minHeight: 350 }}
                      showUploadList={{ showDownloadIcon: false, showPreviewIcon: false }}
                      onChange={async ({file}) => {
                        let fileList = [];
                        let image = null;
                        if (file.status !== 'removed') {
                          fileList = [{ ...file, status: 'done' }]
                          image = await this.getBase64Avatar(file.originFileObj);
                        }
                        this.setState({
                          activitiesFileList: fileList,
                          activityImage: image
                        });
                      }}
                    >
                      {
                        activityImage
                        ? <img src={activityImage} className="w-100"/>
                        : ([
                        <p className="ant-upload-drag-icon">
                          <Icon type="inbox" />
                        </p>,
                        <p className="ant-upload-text">{t('lbl_click_and_drag')}</p>
                        ])
                      }
                    </Upload.Dragger> */}
                  {/* </ImgCrop> */}
                  { activityImage && !this.state.cropVisible
                    ? <div style={{borderRadius: 4, border: '1px dashed #d9d9d9', background: '#fafafa'}} className="py-3">
                        <img src={activityImage} className="w-100"/>
                      </div>
                    : <div style={{borderRadius: 4, border: '1px dashed #d9d9d9', background: '#fafafa'}} className="py-3">
                        <Empty  className="m-0" description={false} className="py-4"/>
                      </div>
                  }
                  <label for="file" className="rounded mt-3 mx-2" style={{border: '1px solid #ccc', display: 'inline-block', padding: '4px 12px', cursor: 'pointer'}}>
                    <Icon type="cloud-upload" style={{position: 'relative', top: -3, fontSize: 18}}/> {t('lbl_choose_image')}
                  </label>
                  <input ref={this.files} id="file" type="file" style={{display: 'none'}} onChange={this.selectedImage}/>
                  { activityImage && activitiesFileList && !this.state.cropVisible &&
                    <Button onClick={() => this.setState({activitiesFileList: [], activityImage: null})} type="danger" style={{border: '1px solid #ccc', display: 'inline-block'}} className="mx-2 mt-3"><Icon type="delete" style={{position: 'relative', top: -3, fontSize: 18}} /> {t('lbl_delete_image')}</Button>
                  }
                  <Modal title={t('lbl_edit_image')} cancelText={t('lbl_cancel')} okText={t('lbl_crop')} visible={this.state.cropVisible} onCancel={() => this.setState({activitiesFileList: [], activityImage: null, cropVisible: false})}
                    onOk={this.cropperImage}
                  >
                    <Cropper
                      ref={this.cropper}
                      preview=".img-preview"
                      src={activityImage}
                      aspectRatio={10 / 7}
                      guides={false}
                      style={{height: 400, width: '100%'}}
                    />
                  </Modal>
                </Col>
                <Col md={{span: 24}} lg={{span: 12}}>
                  <Form onSubmit={this.handleSubmit} className="my-3" hideRequiredMark={true} labelCol={{ span: 24 }} wrapperCol={{ span: 24 }}>
                    <Form.Item label={t('lbl_title')}>
                      {getFieldDecorator('title', {
                        validateTrigger: ['onBlur'],
                        rules: [
                          { 
                            required: true, 
                            whitespace: true,
                            message: t("required", {field: t('lbl_title')}) }
                        ],
                      })(
                        <Input
                          placeholder={t('lbl_title')}
                          allowClear
                        />,
                      )}
                    </Form.Item>
                    <Form.Item label={t('lbl_description')}>
                      {getFieldDecorator('description', {
                        validateTrigger: ['onBlur'],
                        rules: [
                          { 
                            required: true, 
                            whitespace: true,
                            message: t("required", {field: t('lbl_description')}) }
                        ],
                      })(
                        <Input.TextArea
                          placeholder={t('lbl_description')}
                          autoSize={{ minRows: 6 }}
                          allowClear
                        />,
                      )}
                    </Form.Item>
                    <Form.Item className="text-sm-center text-md-left">
                      <Button htmlType="submit" type="primary" className="mr-2">
                        {t('lbl_save')}
                      </Button>
                      <Button type="basic" className="mt-3" onClick={() => this.props.history.go(-1)}>
                        {t('lbl_cancel')}
                      </Button>
                    </Form.Item>
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </Row>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userInfo: state.auth.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
    loadingStart: (text) => dispatch(loadingStart(text)),
    loadingEnd: () => dispatch(loadingEnd()),
  }
}

export default Form.create({ name: 'certificate' })(withTranslation()(connect(mapStateToProps, mapDispatchToProps)(AddCertificate)));
