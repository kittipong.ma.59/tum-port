import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

// Import Store
import { signup } from '../store/actions/auth'
import { setCurrentPath } from '../store/actions/all'

import {
    Button,
    Card,
    Form,
    Icon,
    Input,
    Select,
    Col,
    Row
} from 'antd';

export class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount= () => {
        const { onSetCurrentPath, location, t } = this.props
        onSetCurrentPath(location.pathname)
        document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
    }

    componentWillReceiveProps = (nextProps) => {
        const { location, t } = nextProps;
        document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err,values) => {
            if (!err) {
                const { onSignup } = this.props;
                onSignup(values)
            }
        });
    }

    compareToFirstPassword = (rule, value, callback) => {
        const { form, t } = this.props;
        if (value && value !== form.getFieldValue('password')) {
            callback(t('confirmed'));
        } else {
            callback();
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form;

        const { t } = this.props;

        const { Option } = Select;

        return (
            <div className="container" >
                <div className="row d-flex justify-content-center align-items-center align-items-center" style={{minHeight: '100vh'}}>
                    <div className="col-md-5 p-3">
                        <h2 className="mb-4 font-weight-bold text-center">{t('lbl_create_account')}</h2>
                        <Card title={null} bordered={false} headStyle={{border: 0}}>
                            <Form onSubmit={this.handleSubmit}>
                                <Row>
                                    <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                                        <Form.Item className="px-2 mb-2">
                                            {getFieldDecorator('firstname', {
                                                rules: [{ required: true, message: t("required", {field: t('lbl_firstname')}) }],
                                            })(
                                                <Input
                                                    size="large"
                                                    placeholder={t('lbl_firstname')}
                                                    allowClear
                                                />,
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                                        <Form.Item className="px-2 mb-2">
                                            {getFieldDecorator('lastname', {
                                                rules: [{ required: true, message: t("required", {field: t('lbl_lastname')}) }],
                                            })(
                                                <Input
                                                    size="large"
                                                    placeholder={t('lbl_lastname')}
                                                    allowClear
                                                />,
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Form.Item className="px-2 mb-2" >
                                    {getFieldDecorator('email', {
                                        rules: [{ required: true, message: t("required", {field: t('lbl_email_address')}) }],
                                    })(
                                        <Input
                                            size="large"
                                            placeholder={t('lbl_email_address')}
                                            prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                            allowClear
                                        />,
                                    )}
                                </Form.Item>
                                <Form.Item className="px-2 mb-2" >
                                    {getFieldDecorator('password', {
                                        rules: [
                                            { required: true, message: t("required", {field: t('lbl_password')}) },
                                            { min: 8, message: t("min", {field: t('lbl_password'), number: 8}) }
                                        ],
                                    })(
                                        <Input.Password
                                            size="large"
                                            placeholder={t('lbl_password')}
                                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                            allowClear
                                        />,
                                    )}
                                </Form.Item>
                                <Form.Item className="px-2 mb-2" hasFeedback>
                                    {getFieldDecorator('confirm_password', {
                                        rules: [
                                            { required: true, message: t("required", {field: t('lbl_passsword_confirm')}) },
                                            {
                                                validator: this.compareToFirstPassword,
                                            },
                                        ],
                                    })(
                                        <Input.Password
                                            size="large"
                                            placeholder={t('lbl_passsword_confirm')}
                                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                            allowClear
                                        />,
                                    )}
                                </Form.Item>
                                <Form.Item className="px-3 mb-2"
                                >
                                    <Button htmlType="submit" block type="primary"  size="large" className="mt-4 " >
                                        {t('lbl_sign_up')}
                                    </Button>
                                </Form.Item>
                            </Form>
                            <div className="text-center d-block-sm mt-3">
                                {t('lbl_has_account')} &nbsp; <Link to="/signin"><Button type="link" className="px-0">{t('lbl_login')}</Button></Link>
                            </div>
                        </Card>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = dispatch => {
    return {
        onSignup: (signupData) => dispatch(signup(signupData)) ,
        onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
    }
}

export default Form.create({ name: 'signup' })(withTranslation()(connect(mapStateToProps, mapDispatchToProps)(Signup)))

