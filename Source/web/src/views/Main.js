import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next';

// Import Store
import { setCurrentPath } from '../store/actions/all'
import { currentUser } from '../store/actions/auth'

// Import Service
import authService from '../services/auth'

export class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidMount= async () => {
    const { onSetCurrentPath, location, onCurrentUser } = this.props
    onSetCurrentPath(location.pathname)
    const token = localStorage.getItem('TOKEN');
    if(token){
      const response = await authService.getChkTokenExpired(token);
      if(response.data){
        localStorage.removeItem('TOKEN');
        this.props.history.push('/signin');
      }else{
        onCurrentUser(token);
      }
    }else{
      this.props.history.push('/signin');
    }
  }

  render() {
    return (
      <div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userInfo: state.auth.user
})

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
    onCurrentUser: (token) => dispatch(currentUser(token))
  }
}

export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(Main))
