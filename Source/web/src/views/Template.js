import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next';
import { NotificationManager } from 'react-notifications';

import '../scss/home.scss'

import moment from 'moment';

// Import Store
import { loadingStart, loadingEnd, setCurrentPath } from '../store/actions/all'


import infoServices from '../services/info'
import portServices from '../services/port'
import storageService from '../services/storage'

import {
  Form,
  Button,
  Row,
  Col,
  Empty,
  Card,
  Divider,
  Select,
  Icon,
  Typography,
  Tooltip,
  Input,
  Modal
} from 'antd';

import ImageGallery from 'react-image-gallery';

export class Template extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sizeMobile: 576,
      resizeWidth: window.innerWidth,
      previewVisible: false,
      createVisible: false,
      selectedTemplateId: '',
      imageShow: [],
      template: []
    }
  }

  componentDidMount= async () => {
    const { onSetCurrentPath, location, t, loadingStart, loadingEnd } = this.props
    loadingStart(null);
    onSetCurrentPath(location.pathname)
    window.addEventListener('resize', this.handleResize);
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())

    const response = await portServices.findAllTemplates();
    let templates = response.data;
    for (let i = 0; i < templates.length; i++) {
      for (let j = 0; j < templates[i].image.length; j++) {
        const fileResponse = await storageService.getFile(templates[i].image[j]);
        templates[i].image[j] = 'data:' + fileResponse.data.file.contentType + ';base64,' + fileResponse.data.file.url
      }
    }
    this.setState({ template: templates })
    loadingEnd();
  }

  componentWillReceiveProps = (nextProps) => {
    const { location, t } = nextProps;
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
  }

  handleResize = (e) => {
    e.preventDefault();
    this.setState({
      resizeWidth: window.innerWidth
    })
  }

  handleShowImage = (index) => {
    const { template } = this.state;
    let images = [];
    (template[index].image).forEach(key => {
      images.push({original: key, thumbnail: key}) 
    })
    this.setState({imageShow: images})
    this.setState({previewVisible: true})
  }

  handleShowCreate = (index) => {
    const { template } = this.state;
    this.setState({createVisible: true, selectedTemplateId: template[index]._id})
  }

  handleSubmit = async e => {
    e.preventDefault();
    const { loadingStart, loadingEnd, userInfo, t } = this.props;
    loadingStart();
    try {
      const { validateFields, getFieldValue } = this.props.form;
      const values = await validateFields();
      if(values) {
        this.setState({createVisible: false})
        const responeProfile = await infoServices.getProfile(userInfo._id);
        const responeEducation = await infoServices.getEducation(userInfo._id);
        const responeActivities = await infoServices.getActivities(userInfo._id);
        const responeCertificate = await infoServices.getCertificate(userInfo._id);
        const responeTemplate = await portServices.findTemplateById(this.state.selectedTemplateId);
        let profile = responeProfile.data;
        let education = responeEducation.data;
        let activities = responeActivities.data.filter(k => k.status === true);
        let certificate = responeCertificate.data.filter(k => k.status === true);
        let template = responeTemplate.data;
        let theme = responeTemplate.data.theme;
        let elements = responeTemplate.data.elements;
        let elementsNew = [];
        if(education.length === 0) {
          elements = elements.filter((item, index) => index != template.educationPage);
          template.frontCoverPage = template.frontCoverPage > 0 && template.frontCoverPage > template.educationPage ? template.frontCoverPage-1 : template.frontCoverPage;
          template.profilePage = (template.profilePage > 0) && (template.profilePage > template.educationPage) ? template.profilePage-1 : template.profilePage;
          template.educationPage = -1;
          template.activityPage = template.activityPage > 0 && template.activityPage > template.educationPage ? template.activityPage-1 : template.activityPage;
          template.certificatePage = template.certificatePage > 0 && template.certificatePage > template.educationPage ? template.certificatePage-1 : template.certificatePage;
        }
        if(activities.length === 0) {
          elements = elements.filter((item, index) => index != template.activityPage);
          template.frontCoverPage = template.frontCoverPage > 0 && template.frontCoverPage > template.activityPage ? template.frontCoverPage-1 : template.frontCoverPage;
          template.profilePage = template.profilePage > 0 && template.profilePage > template.activityPage ? template.profilePage-1 : template.profilePage;
          template.educationPage = template.educationPage > 0 && template.educationPage > template.activityPage ? template.educationPage-1 : template.educationPage;
          template.activityPage = -1
          template.certificatePage = template.certificatePage > 0 && template.certificatePage > template.activityPage ? template.certificatePage-1 : template.certificatePage;
        }
        if(certificate.length === 0) {
          elements = elements.filter((item, index) => index != template.certificatePage);
          template.frontCoverPage = template.frontCoverPage > 0 && template.frontCoverPage > template.certificatePage ? template.frontCoverPage-1 : template.frontCoverPage;
          template.profilePage = template.profilePage > 0 && template.profilePage > template.certificatePage ? template.profilePage-1 : template.profilePage;
          template.educationPage = template.educationPage > 0 && template.educationPage > template.certificatePage ? template.educationPage-1 : template.educationPage;
          template.activityPage = template.activityPage > 0 && template.activityPage > template.certificatePage ? template.activityPage-1 : template.activityPage;
          template.certificatePage = -1;
        }
        let titleActivity = 0;
        let imageActivity = 0;
        let descriptionActivities = 0;
        let locationActivity = 0;
        let dateActivities = 0;
        let titleCertificate = 0;
        let imageCertificate = 0;
        let descriptionCertificate = 0;
        
        for (let i = 0; i < elements.length; i++) {
          if(i == template.frontCoverPage || i == template.profilePage) {
            elements[i].forEach((item, index) => {
              if(item.key && item.key === 'displayName') {
                item.text = userInfo.firstname+' '+userInfo.lastname
              } else if(item.key && item.key === 'birth') {
                item.text = profile.dateOfBirth ? moment(profile.dateOfBirth).add(543, 'years').format('LL') : '-'
              } else if(item.key && item.key === 'address') {
                item.text = profile.address ? `${profile.address}, ${profile.subdistrict}, ${profile.district}, ${profile.province}, ${profile.zipcode}`: '-' 
              } else if(item.key && item.key === 'email') {
                item.text = userInfo && userInfo.email ? userInfo.email : '-' 
              } 
              else if(item.key && item.key === 'photoUrl') {
                if(userInfo.photoId) {
                  item.imageId = userInfo.photoId
                } else if(userInfo.photoUrl) {
                  item.photoUrl = userInfo.photoUrl
                } else {
                  item.src = ''
                }
              } 
              else if(item.key && userInfo[item.key]) {
                item.text = userInfo[item.key];
              }else if(item.key && profile[item.key]) {
                item.text = profile[item.key] ? profile[item.key] : '-';
              } else if(item.key && !profile[item.key]) {
                item.text = '-';
              }
            })
            elementsNew.push(elements[i])
          } else if (i == template.educationPage && education.length > 0) {
            let school = 0;
            let educationLevel = 0;
            let curriculum = 0;
            let gpax = 0;
            let startYear = 0;
            let endYear = 0;
            for ( let j = 0 ;j < elements[i].length; j++ ){
              if(elements[i][j].key) {
                if(elements[i][j].key == 'school') {
                  elements[i][j].text = education[school] && education[school].school ? education[school].school : '';
                  school++;
                } else if(elements[i][j].key == 'educationLevel') {
                  elements[i][j].text = education[educationLevel] && education[educationLevel].educationLevel ? education[educationLevel].educationLevel : '';
                  educationLevel++;
                } else if(elements[i][j].key == 'curriculum') {
                  elements[i][j].text = education[curriculum] && education[curriculum].curriculum ? education[curriculum].curriculum : '';
                  curriculum++;
                } else if(elements[i][j].key == 'gpax') {
                  elements[i][j].text = education[gpax] && education[gpax].gpax ? education[gpax].gpax : '';
                  gpax++;
                } else if(elements[i][j].key == 'startYear') {
                  elements[i][j].text = education[startYear] && education[startYear].startYear ? education[startYear].startYear : '';
                  startYear++;
                } else if(elements[i][j].key == 'endYear') {
                  elements[i][j].text = education[endYear] && education[endYear].endYear  ? education[endYear].endYear : '';
                  endYear++;
                }
              }
            }
            for ( let j = elements[i].length -1; j >=0 ; j-- ){
              if(elements[i][j].key) {
                if(elements[i][j].key == 'education1') {
                  const position = 0;
                  if(position >= education.length) {
                    elements[i].splice(j, 1);
                  }
                } else if(elements[i][j].key == 'education2') {
                  const position = 1;
                  if(position >= education.length) {
                    elements[i].splice(j, 1);
                  }
                } else if(elements[i][j].key == 'education3') {
                  const position = 2;
                  if(position >= education.length) {
                    elements[i].splice(j, 1);
                  }
                } else if(elements[i][j].key == 'education4') {
                  const position = 3;
                  if(position >= education.length) {
                    elements[i].splice(j, 1);
                  }
                }
              }
            }
            elementsNew.push(elements[i])
          }
          else if (i == template.activityPage && activities.length > 0) {
            let tmp = [];
            let count = 0;
            for (let j = 0; j < elements[i].length; j++) {
              if(elements[i][j].key) {
                if(elements[i][j].key == 'title') {
                  elements[i][j].text = activities[titleActivity] && activities[titleActivity].title ? activities[titleActivity].title : '';
                  titleActivity++;
                } else if(elements[i][j].key == 'description') {
                  elements[i][j].text = activities[descriptionActivities] && activities[descriptionActivities].description ? activities[descriptionActivities].description : '';
                  descriptionActivities++;
                } else if(elements[i][j].key == 'imageActivities') {
                  elements[i][j].imageId = activities[imageActivity] && activities[imageActivity].imageId ? activities[imageActivity].imageId : '';
                  imageActivity++;
                } else if(elements[i][j].key == 'location') {
                  elements[i][j].text = activities[locationActivity] && activities[locationActivity].location ? activities[locationActivity].location : '';
                  locationActivity++;
                } else if(elements[i][j].key == 'date') {
                  elements[i][j].text = activities[dateActivities] && activities[dateActivities].date ? moment(activities[dateActivities].date).add(543, 'years').format('LL') : '';
                  dateActivities++;
                }
              }
              let itemTmp = Object.assign({}, elements[i][j]);
              tmp.push(itemTmp)
            }
            elementsNew.push(tmp)
            if(activities.length > titleActivity && activities.length > descriptionActivities && activities.length > imageActivity) {
              count += template.activityCount;
              if((activities.length % count) <= template.activityCount) {
                for (let k = 1; k <= template.activityCount; k++) {
                  if(k > (activities.length % count) && (activities.length % count) != 0) {
                    for ( let j = elements[i].length -1; j >=0 ; j-- ){
                      if(elements[i][j].key && elements[i][j].key == `activities${k}`) {
                        elements[i].splice(j, 1);
                      }
                    }
                  }
                }
              }
              i--;
            } else {
              for (let k = 1; k <= template.activityCount; k++) {
                if((k > activities.length) && (activities.length != 0)) {
                  for ( let j = elementsNew[i].length -1; j >=0 ; j-- ){
                    if(elementsNew[i][j].key && elementsNew[i][j].key == `activities${k}`) {
                      elementsNew[i].splice(j, 1);
                    }
                  }
                }
              }
            }
          } else if (i == template.certificatePage && certificate.length > 0) {
            let tmp = [];
            let count = 0;
            for (let j = 0; j < elements[i].length; j++) {
              if(elements[i][j].key) {
                if(elements[i][j].key == 'title') {
                  elements[i][j].text = certificate[titleCertificate] && certificate[titleCertificate].title ? certificate[titleCertificate].title : '';
                  titleCertificate++;
                } else if(elements[i][j].key == 'description') {
                  elements[i][j].text = certificate[descriptionCertificate] && certificate[descriptionCertificate].description ? certificate[descriptionCertificate].description : '';
                  descriptionCertificate++;
                } else if(elements[i][j].key == 'imageCertificate') {
                  elements[i][j].imageId = certificate[imageCertificate] && certificate[imageCertificate].imageId ? certificate[imageCertificate].imageId : '';
                  imageCertificate++;
                }
              }
              let itemTmp = Object.assign({}, elements[i][j]);
              tmp.push(itemTmp)
            }
            elementsNew.push(tmp)
            if(certificate.length > titleCertificate && certificate.length > descriptionCertificate && certificate.length > imageCertificate) {
              count += template.certificateCount;
              if((certificate.length % count) <= template.certificateCount) {
                for (let k = 1; k <= template.certificateCount; k++) {
                  if(k > (certificate.length % count) && (certificate.length % count) != 0) {
                    for ( let j = elements[i].length -1; j >=0 ; j-- ){
                      if(elements[i][j].key && elements[i][j].key == `certificate${k}`) {
                        elements[i].splice(j, 1);
                      }
                    }
                  }
                }
              }
              i--;
            } else {
              for (let k = 1; k <= template.certificateCount; k++) {
                if((k > certificate.length) && (certificate.length != 0)) {
                  for ( let j = elementsNew[i].length -1; j >=0 ; j-- ){
                    if(elementsNew[i][j].key && elementsNew[i][j].key == `certificate${k}`) {
                      elementsNew[i].splice(j, 1);
                    }
                  }
                }
              }
            }
          } else {
            elementsNew.push(elements[i])
          }
        }
        console.log(elementsNew)
        const data = {
          image: [],
          userId: userInfo._id,
          name: values.name,
          theme: theme,
          elements: elementsNew
        }
        const response = await portServices.createPortfolio(data);
        this.props.history.push(`/design/${response.data._id}`)
        this.setState({selectedTemplateId: ''})
        this.props.form.setFieldsValue({
          name: ''
        });
      }
    } catch(err) {
      if(err && err.response) {
        NotificationManager.error(t(err.response.data.code), null ,3000);
        this.props.form.setFieldsValue({
          name: ''
        });
      }
      
    } finally {
      loadingEnd();
    }
  }

  render() {
    const {t} = this.props;
    const { getFieldDecorator } = this.props.form;
    const {resizeWidth, sizeMobile, template, previewVisible, imageShow, createVisible, selectedTemplateId} = this.state;
    const { Title } = Typography;
    const { Option } = Select;

    return (
      <Row style={{padding: resizeWidth < sizeMobile ? '0 12px' : '0 30px'}} className="py-3 home-page">
        <Modal width={resizeWidth < sizeMobile ? resizeWidth-30 : 400} style={{top: resizeWidth < sizeMobile ? null : 24}} bodyStyle={{padding: 0}} visible={previewVisible} footer={null} onCancel={() => this.setState({previewVisible: false, imageShow: []})}>
          <ImageGallery 
            items={imageShow}
            infinite={false} 
            showIndex={true}
            showBullets={true}
            showThumbnails={false}
          />
        </Modal>
        <Modal title={t('lbl_create_portfolio')} onOk={this.handleSubmit} cancelText={t('lbl_cancel')} okText={t('lbl_create')} visible={createVisible} onCancel={() => {this.setState({createVisible: false, selectedTemplateId: ''}); this.props.form.setFieldsValue({name: ''});}}>
          <Form layout="vertical">
            <Form.Item label={t('lbl_name')} >
              {getFieldDecorator('name', {
                initialValue: '',
                rules: [
                  { required: true, message: t("required", {field: t('lbl_portfolio_name')}) }
                ],
              })(
              <Input size="large" placeholder={t('lbl_untitled')} />
            )}</Form.Item>
          </Form>
        </Modal>
        <Row gutter={24}>
          <Col span={24}>
            <Title level={4}>{t('lbl_select_template')}</Title>
          </Col>
        </Row>
        {template.length == 0 ?
        <Row gutter={24}>
          <Col span={24} style={{minHeight: 400}} className="d-flex align-items-center justify-content-center">
            <Empty
              image="https://gw.alipayobjects.com/mdn/miniapp_social/afts/img/A*pevERLJC9v0AAAAAAAAAAABjAQAAAQ/original"
              imageStyle={{
                height: 150,
              }}
              description={t('lbl_no_data')}
            />
          </Col>
        </Row>
        :
        <Row gutter={24}>
          {template.map((item, index) => (
              <Col xs={{span: 12}} md={{span: 8}} lg={{span: 6}} className="p-3 mt-2">
                <Card title={null} bordered={false} className="w-100 shadow-sm" headStyle={{fontWeight: 'bold'}} bodyStyle={{padding: 8}}>
                  <div className="image-item-photo">
                    <img src={item.image[0]} className="w-100" style={{marginBottom: 4}}/>
                    <div className="image-item-hover ">
                      <div className="image-item-action p-0 m-0">
                        <Button onClick={() => this.handleShowCreate(index)} className="my-2 button-action" style={{minWidth: 100}} type="basic" ghost={true}>{t('lbl_use_template')}</Button>
                        <Button onClick={() => this.handleShowImage(index)} className="my-2 button-action" style={{minWidth: 100}} type="basic" ghost={true}>{t('lbl_preview')}</Button>
                      </div>
                    </div>
                  </div>
                  <strong className="mb-0" style={{fontSize: 14}}>{item.name}</strong>
                </Card>
              </Col>
            )
          )}
        </Row>
        }
      </Row>
    )
  }
}

const mapStateToProps = (state) => ({
  userInfo: state.auth.user
})

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
    loadingStart: (text) => dispatch(loadingStart(text)),
    loadingEnd: () => dispatch(loadingEnd()),
  }
}

export default Form.create({ name: 'portfolio' })(withTranslation()(connect(mapStateToProps, mapDispatchToProps)(Template)))
