import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next';
import { NotificationManager } from 'react-notifications';

// Import Store
import { forgotPassword } from '../store/actions/auth'
import { setCurrentPath } from '../store/actions/all'

import {
  Button,
  Form,
  Input,
  Icon,
  Result
} from 'antd';

import { Link } from 'react-router-dom';

export class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      loading: false,
      sendEmailSuccess: false
    }
  }

  componentDidMount= () => {
    const { onSetCurrentPath, location, t } = this.props
    onSetCurrentPath(location.pathname)
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
  }

  componentWillReceiveProps = (nextProps) => {
    const { location, t } = nextProps;
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err,values) => {
      if (!err) {
        const { onForgotPassword, t } = this.props;
        this.setState({
          email: values.email,
          loading: true
        })
        onForgotPassword(values.email)
          .then(()=>{
            this.setState({
              sendEmailSuccess: true,
              loading:false
            })
          })
          .catch(err => {
            this.setState({
              loading:false
            })
            NotificationManager.error(t(err.response.data.code), null ,3000);
          })
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { t } = this.props;
    const { sendEmailSuccess, email } = this.state;

    return (
      <div className="container" >
        <div className="row d-flex justify-content-center align-items-center align-items-center" style={{minHeight: '100vh'}}>
          {(sendEmailSuccess === false)? 
          <div className="col-md-4 p-3 ">
            <h2 className="mb-5 font-weight-bold text-center">{t('lbl_forgot_password')}</h2>
            <Form onSubmit={this.handleSubmit} hasFeedback>
              <Form.Item className="mb-0 px-3">
                  {getFieldDecorator('email', {
                      rules: [{ required: true, message: t("required", {field: t('lbl_email_address')}) }],
                  })(
                    <Input
                      placeholder={t('lbl_email_address')}
                      prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                      size="large"
                      allowClear
                      id="email"
                    />,
                  )}
              </Form.Item>
              <Form.Item className="mb-0 px-3">
                <Button htmlType="submit" block type="primary" shape="round"  size="large" className="mt-4 " loading={this.state.loading}>
                    {t('lbl_reset_password')}
                </Button>
              </Form.Item>
            </Form>
            <div className="text-center d-block-sm mt-3">
              <Link to="/signin"><Button type="link" className="px-0">{t('lbl_back_to_login')}</Button></Link>
            </div>
          </div>
          :
          <Result
            status="success"
            title={t('lbl_successfully')+'!'}
            subTitle={t('lbl_wording_send_email_reset_password', {email: email})}
            extra={[
              <Link to="/signin">
                <Button type="primary" key="go_login">
                  {t('lbl_back_to_login')}
                </Button>
              </Link>,
              <Link to="/forgotPassword">
                <Button key="go_back" onClick={() => this.setState({sendEmailSuccess: false})}>{t('lbl_go_back')}</Button>,
              </Link>
            ]}
          />
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
    onForgotPassword: (email) => dispatch(forgotPassword(email))
  }
}

export default Form.create({ name: 'forgotPassword' })(withTranslation()(connect(mapStateToProps, mapDispatchToProps)(ForgotPassword)))
