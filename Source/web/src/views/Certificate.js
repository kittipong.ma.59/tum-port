import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next'
import { Link } from 'react-router-dom';

// Import Scss
import '../scss/information.scss'

// Import Store
import { setCurrentPath, loadingStart, loadingEnd } from '../store/actions/all'

// Import Services
import infoService from '../services/info';
import storageService from '../services/storage';

import {
  Button,
  Divider,
  Icon,
  Card,
  Table,
  Typography,
  Modal,
  Upload,
  Form,
  Empty
} from 'antd';

import { NotificationManager } from 'react-notifications';
import reactcss from 'reactcss';


export class Certificate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resizeWidth: window.innerWidth,
      sizeMobile: 576,
      dataSource: [],
      previewVisible: false
    }
  }

  componentDidMount= async () => {
    window.addEventListener('resize', this.handleResize);
    const { onSetCurrentPath, location, userInfo, t } = this.props;
    onSetCurrentPath(location.pathname);
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
    if(userInfo) {
      const response = await infoService.getCertificate(userInfo._id);
      let dataSource = [];
      let data = response.data;
      if(data.length > 0) {
        for (let i=0; i<data.length;i++) {
          if (data[i].imageId) {
            const fileResponse = await storageService.getFile(data[i].imageId);
            data[i].imageUrl = `data:${fileResponse.data.file.contentType};base64,${fileResponse.data.file.url}`;
          }
          dataSource.push(data[i]);
        }
        this.setState({ dataSource });
      }
    }
  }

  componentWillReceiveProps = (nextProps) => {
    const { location, t } = nextProps;
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
  }

  handleResize = (e) => {
    e.preventDefault();
    this.setState({
      resizeWidth: window.innerWidth
    })
  }

  showConfirmDelete = (id) => {
    const { t } = this.props;
    const { dataSource } = this.state;
    Modal.confirm({
      title: t('lbl_confirm_delete'),
      okText: t('lbl_ok'),
      cancelText: t('lbl_cancel'),
      onOk: async () => {
        const imageId = dataSource.find(item => item._id == id).imageId;
        if (imageId)
          await storageService.removeFile(imageId);
        await infoService.removeCertificate(id);
        this.setState({
          dataSource: dataSource.filter(item => item._id != id)
        })
      },
      onCancel() {},
    });
  }

  async changeStatus(id, status) {
    const { loadingStart, loadingEnd, t } = this.props;
    loadingStart(null)
    const { dataSource } = this.state;
    const index = dataSource.findIndex((obj => obj._id == id))
    if(dataSource[index].imageId) {
      dataSource[index].status = status;
      await infoService.changeStatusCertificate(id, status)
      this.setState({ dataSource })
    } else {
      NotificationManager.error(t('image'), t('cannot-be-displayed') ,1500);
    }
    loadingEnd()
  } 

  render() {
    const { t } = this.props;
    const { dataSource, resizeWidth, sizeMobile, previewVisible, imagePreview } = this.state;
    const { Title } = Typography;
    
    const styles = reactcss({
      'default': {
        badge: {
          border: 'none',
          cursor: 'pointer',
          outline: 'inherit'
        }
      }
    })

    const columns = [
      {
        title: t('lbl_image'),
        dataIndex: 'imageUrl',
        key: 'image',
        width: resizeWidth < sizeMobile ? '25%' : '15%',
        align: 'center',
        render: imageUrl => (
          imageUrl ? <img onClick={() => this.setState({previewVisible: true, imagePreview: imageUrl})} src={imageUrl} className="w-100" style={{cursor: 'pointer'}}/>
          : <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} className="m-0" description={t('lbl_no_image')} />
        )
      }, 
      {
        title: t('lbl_title'),
        dataIndex: 'title',
        key: 'title',
        width: resizeWidth < sizeMobile ? '40%' : '20%',
      },
      {
        title: t('lbl_description'),
        dataIndex: 'description',
        key: 'description',
        className: 'sm-d-none',
        width: '40%'
      },
      {
        title: t('lbl_status'),
        key: 'status',
        className: 'sm-d-none text-center',
        width: '10%',
        render: (item) => (
          item.status && item.status == true ? <button style={styles.badge} class="badge badge-success p-2 cursor-pointer" onClick={() => this.changeStatus(item._id, false)}>{t('lbl_show')}</button> : <button style={styles.badge} class="badge badge-danger p-2 cursor-pointer" onClick={() => this.changeStatus(item._id, true)}>{t('lbl_hide')}</button>
        )
      },
      {
        title: null,
        key: 'action',
        width: '25%',
        render: (item) => (
          <span>
            <Button onClick={() => this.props.history.push(`/certificate/editCertificate`, { id: item._id})} size={resizeWidth < sizeMobile ? "small" : 'default'} icon="form" className="d-inline-flex align-items-center m-1"><span className="text-action">{t('lbl_edit')}</span></Button>
            <Button onClick={() => this.showConfirmDelete(item._id)} size={resizeWidth < sizeMobile ? "small" : 'default'} type="danger" icon="delete" className="d-inline-flex align-items-center m-1"><span className="text-action">{t('lbl_delete')}</span></Button>
          </span>
        )
      },
    ]

    return (
      <div className="activities-page px-md-4" style={{padding: resizeWidth < sizeMobile ? '0 16px': ''}}>
        <Card title={null} bordered={false} className="mb-4 shadow-sm position-relative" bodyStyle={{padding: resizeWidth < sizeMobile ?'.75rem': '1.5rem'}}>
          <Title level={4}>{t('lbl_certificate')}</Title>
          <Divider className="my-0" style={{backgroundColor: '#1890ff', height: 2}}/>
          <div className="text-right my-2">
            <Link to="/certificate/addCertificate"><Button type="primary">{t('/certificate/addcertificate')}</Button></Link>
          </div>
          <Table 
            columns={columns} 
            dataSource={dataSource} 
            pagination={{ pageSize: 5 }} 
            locale={{emptyText: (<Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={t('lbl_no_data')} />)}}
            expandedRowRender={resizeWidth < sizeMobile ? record => [
              <p style={{ margin: 0 }}>{record.description}</p>,
              <div className="mt-1">
                {record.status && record.status == true ? <button style={styles.badge} class="badge badge-success p-2 cursor-pointer" onClick={() => this.changeStatus(record._id, false)}>{t('lbl_show')}</button> : <button style={styles.badge} class="badge badge-danger p-2 cursor-pointer" onClick={() => this.changeStatus(record._id, true)}>{t('lbl_hide')}</button>}
              </div>
            ] : false}
          ></Table>
          <Modal visible={previewVisible} footer={null} onCancel={() => this.setState({previewVisible: false})}>
            <img  style={{ width: '100%' }} src={imagePreview} />
          </Modal>
        </Card>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userInfo: state.auth.user
})

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
    loadingStart: (text) => dispatch(loadingStart(text)),
    loadingEnd: () => dispatch(loadingEnd())
  }
}

export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(Certificate))
