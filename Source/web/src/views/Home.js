import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next';

import '../scss/home.scss'

// Import Store
import { setCurrentPath, loadingStart, loadingEnd } from '../store/actions/all'
import { logOut } from '../store/actions/auth';

import portServices from '../services/port';
import storageService from '../services/storage';

import {
  Button,
  Row,
  Col,
  Empty,
  Card,
  Divider,
  Modal,
  Select,
  Icon,
  Typography,
  Tooltip
} from 'antd';

import ImageGallery from 'react-image-gallery';

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sizeMobile: 576,
      resizeWidth: window.innerWidth,
      previewVisible: false,
      imageShow: [],
      portfolio: []
    }
  }

  componentDidMount= async () => {
    const { onSetCurrentPath, location, t, userInfo, loadingStart, loadingEnd } = this.props;
    loadingStart(null);
    onSetCurrentPath(location.pathname)
    window.addEventListener('resize', this.handleResize);
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())

    if(userInfo && !userInfo.admin) {
      const response = await portServices.findPortfolioByUserId(userInfo._id);
      if(response.data.length > 0) {
        let portfolio = response.data;
        for (let i = 0; i < portfolio.length; i++) {
          portfolio[i].photo = [];
          for (let j = 0; j < portfolio[i].image.length; j++) {
            const fileResponse = await storageService.getFile(portfolio[i].image[j]);
            portfolio[i].photo.push('data:' + fileResponse.data.file.contentType + ';base64,' + fileResponse.data.file.url)
          }
        }
        this.setState({portfolio: portfolio})
      }
    } else {
      this.props.history.go(-(this.props.history.length));
      this.props.history.replace('/manageTemplate')
    }
    loadingEnd();
  }

  componentWillReceiveProps = (nextProps) => {
    const { location, t } = nextProps;
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
  }

  handleResize = (e) => {
    e.preventDefault();
    this.setState({
      resizeWidth: window.innerWidth
    })
  }

  handleShowImage = (index) => {
    const { portfolio } = this.state;
    let images = [];
    (portfolio[index].photo).forEach(key => {
      images.push({original: key, thumbnail: key}) 
    })
    this.setState({imageShow: images})
    this.setState({previewVisible: true})
  }

  handleDelete = (index) => {
    const { t } = this.props;
    const { portfolio } = this.state;
    Modal.confirm({
      title: t('lbl_confirm_delete'),
      okText: t('lbl_ok'),
      cancelText: t('lbl_cancel'),
      onOk: async () => {
        const id = portfolio[index]._id;
        await portServices.deletePortfolio(id);
        for (let i = 0; i < portfolio[index].image.length; i++) {
          await storageService.removeFile(portfolio[index].image[i]);
        }
        this.setState({
          portfolio: portfolio.filter(item => item._id != id)
        })
      },
      onCancel() {},
    });
  }

  

  render() {
    const {t} = this.props;
    const {resizeWidth, sizeMobile, portfolio, previewVisible, imageShow} = this.state;
    const { Title } = Typography;
    const { Option } = Select;

    const AddSvg = () => (
      <svg t="1581171216570" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="6708" width="40" height="40">
        <path d="M512 512m-448 0a448 448 0 1 0 896 0 448 448 0 1 0-896 0Z" fill="#1890ff" p-id="6709" data-spm-anchor-id="a313x.7781069.0.i10" class="selected"></path>
        <path d="M448 298.666667h128v426.666666h-128z" fill="#FFFFFF" p-id="6710"></path>
        <path d="M298.666667 448h426.666666v128H298.666667z" fill="#FFFFFF" p-id="6711"></path>
      </svg>
    );

    const AddIcon = props => <Icon component={AddSvg} {...props} />;

    return (
      <Row style={{padding: resizeWidth < sizeMobile ? '0 12px' : '0 30px'}} className="py-3 home-page">
        <Modal width={resizeWidth < sizeMobile ? resizeWidth-30 : 400} style={{top: resizeWidth < sizeMobile ? null : 24}} bodyStyle={{padding: 0}} visible={previewVisible} footer={null} onCancel={() => this.setState({previewVisible: false, imageShow: []})}>
          <ImageGallery 
            items={imageShow}
            infinite={false} 
            showIndex={true}
            showBullets={true}
            showThumbnails={false}
          />
        </Modal>
        <Row gutter={24}>
          <Col span={24}>
            <Title level={4}>{t('lbl_my_portfolio')}</Title>
          </Col>
        </Row>
        {portfolio.length == 0 ?
        <Row gutter={24}>
          <Col span={24} style={{minHeight: 400}} className="d-flex align-items-center justify-content-center">
            <Empty
              image="https://gw.alipayobjects.com/mdn/miniapp_social/afts/img/A*pevERLJC9v0AAAAAAAAAAABjAQAAAQ/original"
              imageStyle={{
                height: 150,
              }}
              description={([
                <h6 style={{fontSize: 18}}>{t('lbl_create_now_portfolio')}</h6>,
                <p>{t('lbl_no_portfolio')}</p>
              ])}
            >
              <Button type="primary" onClick={() => this.props.history.push('/template')}>{t('lbl_create_now')}</Button>
            </Empty>
          </Col>
        </Row>
        :
        <Row gutter={24}>
          {portfolio.map((item, index) => (
              <Col xs={{span: 12}} md={{span: 8}} lg={{span: 6}} className="p-3 mt-2">
                <Card title={null} bordered={false} className="w-100 shadow-sm" headStyle={{fontWeight: 'bold'}} bodyStyle={{padding: 8}}>
                  <div className="image-item-photo">
                    <img src={item.photo[0]} className="w-100" style={{marginBottom: 4}}/>
                    <div className="image-item-hover ">
                      <div className="image-item-action p-0 m-0">
                        <Button onClick={() => this.props.history.push(`/design/${item._id}`)} className="my-2 button-action" style={{minWidth: 100}} type="basic" ghost={true}>{t('lbl_open')}</Button>
                        <Button onClick={() => this.handleShowImage(index)} className="my-2 button-action" style={{minWidth: 100}} type="basic" ghost={true}>{t('lbl_preview')}</Button>
                        <Button onClick={() => this.handleDelete(index)} className="my-2 button-action" style={{minWidth: 100}} type="basic" ghost={true}>{t('lbl_delete')}</Button>
                      </div>
                    </div>
                  </div>
                  <strong className="mb-0" style={{fontSize: 14}}>{item.name}</strong>
                </Card>
              </Col>
            )
          )}
          <Col xs={{span: 12}} md={{span: 8}} lg={{span: 6}} className="p-3 mt-2">
            <Card title={null} bordered={false} className="w-100 shadow-sm" headStyle={{fontWeight: 'bold'}} bodyStyle={{padding: 8}}>
              <div className="image-item-photo">
                <img src={require('../assets/images/card_create.jpg')} className="w-100" style={{marginBottom: 28}}/>
                <div className="card-create-hover" onClick={() => this.props.history.push('/template')}>
                  <div className="card-create-action w-100 text-center">
                    <AddIcon />
                    <p className="mt-2">{t('lbl_create_a_new')}</p>
                  </div>
                </div>
              </div>
            </Card>
          </Col>
        </Row>
        }
      </Row>
    )
  }
}

const mapStateToProps = (state) => ({
  userInfo: state.auth.user
})

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
    loadingStart: (text) => dispatch(loadingStart(text)) ,
    loadingEnd: () => dispatch(loadingEnd()) ,
    onLogOut: () => dispatch(logOut()),
  }
}

export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(Home))
