import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next';

// Import Store
import { setCurrentPath } from '../store/actions/all'
import { resetPassword } from '../store/actions/auth'

import authService from '../services/auth' 

import {
  Button,
  Form,
  Input,
  Icon,
  Result,
  Typography
} from 'antd';

import { Link } from 'react-router-dom';

export class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      loading: false,
      error: false,
      codeError: ''
    }
  }

  componentDidMount= async () => {
    try {
      const { onSetCurrentPath, location, t } = this.props
      onSetCurrentPath(location.pathname)
      document.title = 'Tum Port | '+t('lbl_reset_password')
      const token = this.props.match.params.token;
      const response = await authService.getUserForResetPassword(token);
      this.setState({
        email: response.data.email
      });
    } catch(err) {
      this.setState({
        error: true,
        codeError: err.response.data.code
      });
    }
  }

  componentWillReceiveProps = (nextProps) => {
    const { location, t } = nextProps;
    document.title = 'Tum Port | '+t(('lbl_reset_password').toLowerCase())
  }

  handleSubmit = e => {
    e.preventDefault();
    const { onResetPassword } = this.props;
    const { email } = this.state;
    this.props.form.validateFields((err,values) => {
      if (!err) {
        onResetPassword(email, values.password)
      }
    });
  }

  compareToFirstPassword = (rule, value, callback) => {
    const { form, t } = this.props;
    if (value && value !== form.getFieldValue('password')) {
        callback(t('confirmed'));
    } else {
        callback();
    }
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { t } = this.props;
    const { email ,error, codeError } = this.state;
    const { Text } = Typography;

    return (
      <div className="container" >
        <div className="row d-flex justify-content-center align-items-center align-items-center" style={{minHeight: '100vh'}}>
          <div className="col-md-4 p-3 ">
            {error ? (
              <Result
                status= "500"
                title= "400"
                subTitle={t(codeError)}
                extra={<Link to="/"><Button type="primary">{t('lbl_back_to_home')}</Button></Link>}
              />
            ) : [
              <h2 className="mb-3 font-weight-bold text-center">{t('lbl_reset_password')}</h2>,
              <Text className="px-3">{email}</Text>,
              <Form onSubmit={this.handleSubmit} hasFeedback>
                <Form.Item className="mb-0 px-3 mt-2">
                    {getFieldDecorator('password', {
                        rules: [
                          { required: true, message: t("required", {field: t('lbl_new_password')}) },
                          { min: 8, message: t("min", {field: t('lbl_new_password'), number: 8}) }
                        ],
                    })(
                      <Input.Password
                        placeholder={t('lbl_new_password')}
                        prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        size="large"
                        allowClear
                        id="password"
                      />,
                    )}
                </Form.Item>
                <Form.Item className="mb-0 px-3 mt-3">
                    {getFieldDecorator('confirm_password', {
                        rules: [
                          { required: true, message: t("required", {field: t('lbl_passsword_confirm')}) },
                          { validator: this.compareToFirstPassword }
                        ],
                    })(
                      <Input.Password
                        placeholder={t('lbl_passsword_confirm')}
                        prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        size="large"
                        allowClear
                        id="confirm_password"
                      />,
                    )}
                </Form.Item>
                <Form.Item className="mb-0 px-3">
                  <Button htmlType="submit" block type="primary" shape="round"  size="large" className="mt-4 " loading={this.state.loading}>
                      {t('lbl_reset_password')}
                  </Button>
                </Form.Item>
              </Form>
            ]}
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
    onResetPassword: (email, password) => dispatch(resetPassword(email, password))
  }
}

export default Form.create({ name: 'resetPassword' })(withTranslation()(connect(mapStateToProps, mapDispatchToProps)(ResetPassword)))
