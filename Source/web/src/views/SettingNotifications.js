import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next';

// Import Store
import { setCurrentPath, loadingStart, loadingEnd } from '../store/actions/all'
import { logOut } from '../store/actions/auth';


import feedServices from '../services/feed';

import {
  Button,
  Row,
  Col,
  Empty,
  Card,
  Divider,
  Modal,
  Form,
  Select,
  Icon,
  Typography,
  Tooltip
} from 'antd';

import { NotificationManager } from 'react-notifications';

export class SettingNotifications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sizeMobile: 576,
      resizeWidth: window.innerWidth,
      setting: null,
      university: [],
      category: [
        "รับตรง/TCAS",
        "TCAS 63 / รับตรง 63",
        "TCAS 64 / รับตรง 64",
        "รับตรง TCAS รอบ 1",
        "รับตรง TCAS รอบ 2",
        "รับตรง TCAS รอบ 3",
        "รับตรง TCAS รอบ 4",
        "รับตรง TCAS รอบ 5",
        "รับตรงไม่เข้าร่วมระบบ TCAS",
        "รับตรงเด็กซิ่ว",
        "รับตรงวุฒิเทียบเท่า",
        "รับตรงไม่ใช้เกรดขั้นต่ำ",
        "รับตรงใช้ GAT-PAT",
        "รับตรงไม่ใช้ GAT-PAT",
        "รับตรงใช้ 9 วิชาสามัญ",
        "รับตรงไม่ใช้ 9 วิชาสามัญ",
        "รับตรงสอบข้อเขียน",
        "รับตรงสอบสัมภาษณ์อย่างเดียว",
        "รับตรงผู้จบปวช.",
        "รับตรงผู้จบปวส.",
        "ครูคืนถิ่น",
        "รับน้องม.6",
        "ทุนการศึกษา",
        "คอร์สติวฟรี",
      ]
    }
  }

  componentDidMount= async () => {
    const { onSetCurrentPath, location, t, userInfo, loadingStart, loadingEnd } = this.props;
    loadingStart(null);
    onSetCurrentPath(location.pathname)
    window.addEventListener('resize', this.handleResize);
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
    const university = await feedServices.getUniversity();
    const setting = await feedServices.getSetting(userInfo._id);
    this.setState({university: university.data, setting: setting.data})
    loadingEnd();
  }

  componentWillReceiveProps = (nextProps) => {
    const { location, t } = nextProps;
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
  }

  handleResize = (e) => {
    e.preventDefault();
    this.setState({
      resizeWidth: window.innerWidth
    })
  }

  handleSubmit = async e => {
    e.preventDefault();
    const { loadingStart, loadingEnd, userInfo, t } = this.props;
    loadingStart();
    try {
      const { validateFields } = this.props.form;
      const values = await validateFields();
      if(values){
        await feedServices.settingNotification(userInfo._id, values)
        NotificationManager.success(t('lbl_successfully_saved'),null,3000);
        window.scrollTo(0,0)
        this.props.history.push('/')
      }
    }
    catch(err) {
      console.log(err);
    }
    finally {
      loadingEnd();
    }
  }
  

  render() {
    const {t} = this.props;
    const {resizeWidth, sizeMobile, university, category, setting} = this.state;
    const { getFieldDecorator } = this.props.form;

    return (
      <Row style={{padding: resizeWidth < sizeMobile ? '0 12px' : '0 30px'}} className="py-3 ">
          <Typography.Title level={4}>{t('/settingnotifications')}</Typography.Title>
          <Card title={null} bordered={false} className="mb-4 shadow-sm position-relative" bodyStyle={{padding: resizeWidth < sizeMobile ?'.75rem': '1.5rem'}}>
            <Form onSubmit={this.handleSubmit} className="my-3" hideRequiredMark={true} labelCol={{ span: 24 }} wrapperCol={{ span: 24 }}>
              <Row gutter={24}>
                <Col span={24} className="mb-2">
                  <h6>{t('lbl_educational_institution')}</h6>
                </Col>
                <Col span={24} className="mb-4">
                  <Form.Item>
                    {getFieldDecorator('university', {
                      initialValue: setting && setting.university ? setting.university : undefined,
                      validateTrigger: ['onBlur'],
                      rules: [],
                    })(
                        <Select
                          mode="multiple"
                          placeholder={'-- '+t('lbl_all_institutions')+' --'}
                          className="w-100"
                          notFoundContent={(<Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={t('lbl_no_data')} />)}
                        >
                          {
                            university.map(item => <Select.Option value={item.university}>{item.university}</Select.Option>)
                          }
                        </Select>,
                    )}
                  </Form.Item>
                </Col>
                <Col span={24} className="mb-2">
                  <h6>{t('lbl_category')}</h6>
                </Col>
                <Col span={24} className="mb-4">
                <Form.Item>
                    {getFieldDecorator('category', {
                      initialValue: setting && setting.category ? setting.category : undefined,
                      validateTrigger: ['onBlur'],
                      rules: [],
                    })(
                        //<Icon type="down" className="position-absolute" style={{zIndex: 1, right: 8, top: 9, cursor: 'pointer'}}/>
                        <Select
                          mode="multiple"
                          placeholder={'-- '+t('lbl_all_category')+' --'}
                          className="w-100"
                          notFoundContent={(<Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={t('lbl_no_data')} />)}
                        >
                          {
                            category.map(item => <Select.Option value={item}>{item}</Select.Option>)
                          }
                        </Select>
                    )}
                  </Form.Item>
                </Col>
                <Col span={24} className="text-sm-center text-md-left">
                  <Form.Item>
                    <Button htmlType="submit" type="primary">{t('lbl_save')}</Button>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </Card>
      </Row>
    )
  }
}

const mapStateToProps = (state) => ({
  userInfo: state.auth.user
})

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
    loadingStart: (text) => dispatch(loadingStart(text)) ,
    loadingEnd: () => dispatch(loadingEnd()) ,
    onLogOut: () => dispatch(logOut()),
  }
}

export default Form.create({ name: 'feed' })(withTranslation()(connect(mapStateToProps, mapDispatchToProps)(SettingNotifications)))
