import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next';

// Import Store
import { setCurrentPath, loadingStart, loadingEnd } from '../store/actions/all'
import feedServices from '../services/feed';

import config from '../config';

import '../scss/feed.scss'

import {
  Button,
  BackTop,
  Row,
  Col,
  Empty,
  Card,
  List,
  Divider,
  Modal,
  Select,
  Icon,
  Typography,
  Tooltip,
  Tabs
} from 'antd';

import SocketIO from 'socket.io-client';
import cheerio from 'cheerio';
import moment from 'moment';
let Parser = require('rss-parser');
let parser = new Parser();

export class FeedDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sizeMobile: 576,
      resizeWidth: window.innerWidth,
      feed: null,
      feedURL: ''
    }
  }

  fetchDataNotification = async (id) => {
    var listDetail = [];
    const enttrongLink = 'https://www.enttrong.com';
    const sangfansLink = 'https://www.sangfans.com';
    const feedURL = id.includes(enttrongLink) ? enttrongLink : sangfansLink;
    if(feedURL == enttrongLink) {
      const response = await feedServices.getFeedById(id);
      const fetchUrl = await feedServices.fetchUrl(response.data._id);
      const $ = cheerio.load(fetchUrl.data);
      await $('.entry').children().each(function(index, item){
        if($(this).text() != 'ไม่อยากพลาดข่าวรับตรง กรุณาปฏิบัติดังนี้ (คลิ๊ก)' && index != 0) {
          if($(this).children("img").attr("src") != undefined){
            let path = $(this).children("img").attr("src")
            listDetail.push({type: 'image', text: path})
          }
          if(item.name === 'p'){
            listDetail.push({type: 'title', text: $(this).text()})
          }
          else if(item.name === 'ul'){
            listDetail.push({type: 'description', text: $(this).html()})
          }
        }
      });
      if($('.pagination-single').children().length > 0) {
        for (let i = 0; i < $('.pagination-single').children().length; i++) {
          const item = $('.pagination-single').children()[i];
          if(item.name == 'a') {
            const fetchUrlPage = await feedServices.fetchUrl(response.data.link+'/'+$(item).text());
            const $$ = cheerio.load(fetchUrlPage.data);
            $$('.entry').children().each(function(i, it){
              if($$(this).children("img").attr("src") != undefined){
                let path = $$(this).children("img").attr("src")
                listDetail.push({type: 'image', text: path})
              }
            })
          }
        }
      }
      
      response.data.listDetail = listDetail
      this.setState({
        feed: response.data,
        feedURL: feedURL
      })
    } else {
      const response = await feedServices.getFeedById(id);
      const fetchUrl = await feedServices.fetchUrl(response.data._id);
      const $ = cheerio.load(fetchUrl.data);
      await $('.entry').children().each(function(index, item){
        if($(this).children("img").attr("src") != undefined){
          const path = 'https://www.sangfans.com' + $(this).children("img").attr("src")
          if(path != response.data.logo) {
            listDetail.push({type: 'image', text: path})
          }
        }
        else if(item.name == 'h4') {
          listDetail.push({type: 'title', text: $(this).text()})
        } else if(item.name == 'p' && $(this).text() != response.data.detail && $(this).attr('style') != "text-align: center;" && $(this).text() != '\xa0') {
          listDetail.push({type: 'description', text: $(this).html()})
        }
      })
      console.log(listDetail)
      response.data.listDetail = listDetail
      this.setState({
        feed: response.data,
        feedURL: feedURL
      })
    }
  }

  componentDidMount= async () => {
    const { onSetCurrentPath, location, loadingStart, loadingEnd, t } = this.props;
    loadingStart(null)
    onSetCurrentPath(location.pathname)
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
    const id = location.state.id;
    if(id) {
      await this.fetchDataNotification(id)
    } else {
      this.props.history.go(-(this.props.history.length));
      this.props.history.replace('/')
    }
    loadingEnd()
  }
  
  componentWillReceiveProps = async (nextProps) => {
    const { location, t } = nextProps;
    const { loadingStart, loadingEnd } = this.props;
    document.title = 'Tum Port | '+t((location.pathname).toLowerCase())
    if(this.props.location.state.id != location.state.id) {
      loadingStart(null)
      const id = location.state.id;
      if(id) {
        await this.fetchDataNotification(id)
      } else {
        this.props.history.go(-(this.props.history.length));
        this.props.history.replace('/')
      }
      loadingEnd()
    }
  }

  handleResize = (e) => {
    e.preventDefault();
    this.setState({
      resizeWidth: window.innerWidth
    })
  }

  render() {
    const { resizeWidth, sizeMobile, feed } = this.state;
    const { userInfo, t, i18n } = this.props;
    
    const TimeSvg = () => (
      <svg fill="currentColor" t="1583369362220" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="5773" width="12" height="12"><path d="M505.984 975.317333c-259.2 0-469.333333-210.133333-469.333333-469.333333s210.133333-469.333333 469.333333-469.333333 469.333333 210.133333 469.333333 469.333333S765.226667 975.317333 505.984 975.317333zM505.984 124.672c-210.602667 0-381.354667 170.709333-381.354667 381.354667 0 210.602667 170.709333 381.354667 381.354667 381.354667 210.602667 0 381.354667-170.752 381.354667-381.354667C887.338667 295.381333 716.586667 124.672 505.984 124.672zM696.661333 550.016l-88.021333 0-58.666667 0-43.989333 0 0 0c-24.32 0-43.989333-19.712-43.989333-43.989333L461.994667 242.005333c0-24.277333 19.669333-43.989333 43.989333-43.989333 24.277333 0 43.989333 19.712 43.989333 43.989333l0 219.989333 58.666667 0 88.021333 0c24.277333 0 43.989333 19.712 43.989333 43.989333S720.938667 550.016 696.661333 550.016z" p-id="5774"></path></svg>
    );

    const DotSvg = () => (
      <svg t="1583442693367" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3884" width="8" height="8">
        <path d="M512 512m-512 0a512 512 0 1 0 1024 0 512 512 0 1 0-1024 0Z" fill="currentColor" p-id="3885"></path>
      </svg>
    );

    const BackTopSvg = () => (
      <svg t="1583476474103" fill="currentColor" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3869" width="20" height="20"><path d="M554.666667 320V810.666667c0 25.6-17.066667 42.666667-42.666667 42.666666s-42.666667-17.066667-42.666667-42.666666V320l-179.2 179.2c-17.066667 17.066667-46.933333 17.066667-64 0-17.066667-17.066667-17.066667-46.933333 0-64l251.733334-251.733333c17.066667-17.066667 46.933333-17.066667 64 0l251.733333 251.733333c17.066667 17.066667 17.066667 46.933333 0 64-17.066667 17.066667-46.933333 17.066667-64 0L554.666667 320z" p-id="3870"></path></svg>
    );

    const TimeIcon = props => <Icon component={TimeSvg} {...props} />;
    const DotIcon = props => <Icon component={DotSvg} {...props} />;
    const BackTopIcon = props => <Icon component={BackTopSvg} {...props} />;

    return (
      feed ?
      <Row style={{padding: resizeWidth < sizeMobile ? '0 12px' : '0 24px'}} className="pt-0 pb-3 home-page">
        <BackTop>
          <Button type="primary" shape="circle-outline"><BackTopIcon className="position-relative" style={{top: -2}}/></Button>
        </BackTop>
        <Row gutter={24}>
          <Col span={24}>
            { this.state.feedURL == 'https://www.enttrong.com' ?
            <Card title={null} bordered={false} className="mb-4 px-md-3 shadow-sm position-relative" >
              <Typography.Title level={4} className="mb-0">{feed.title}</Typography.Title>
              <Typography.Text>
                <TimeIcon className="position-relative mr-1" style={{top: -3}}/>
                {moment(feed.date).locale(i18n.language == 'en-US' ?'en': 'th').add(543, 'years').format('LLL')}
              </Typography.Text>
              <Divider className="mt-2"/>
              <div className="text-center mb-3">
                  <img src={feed.logo} />
              </div>
              <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                {feed.detail}
              </p>
              {feed.listDetail && feed.listDetail &&
                feed.listDetail.map(item => {
                  if(item.type === 'title') 
                    return <h6 className="font-weight-bold">{item.text}</h6>
                  else if(item.type === 'description') 
                    return <p className="feed-description-ul"><ul dangerouslySetInnerHTML={{__html: item.text}}></ul></p>
                  else if(item.type === 'image')
                    return <div className="d-block"><img src={item.text} style={{width: resizeWidth < sizeMobile ? resizeWidth-70 : '50%'}}/></div>
                })
              }
            </Card>
            :
            <Card title={null} bordered={false} className="mb-4 px-md-3 shadow-sm position-relative" >
              <Typography.Title level={4} className="mb-0">{feed.title}</Typography.Title>
              <Typography.Text>
                <TimeIcon className="position-relative mr-1" style={{top: -3}}/>
                {moment(feed.date).locale(i18n.language == 'en-US' ?'en': 'th').add(543, 'years').format('LLL')}
              </Typography.Text>
              <Divider className="mt-2"/>
              <div className="text-center mb-3">
                  <img src={feed.logo} className={resizeWidth < sizeMobile? 'w-100' : "w-60"}/>
              </div>
              <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                {feed.detail}
              </p>
              {feed.listDetail && feed.listDetail &&
                feed.listDetail.map(item => {
                  if(item.type === 'title') 
                    return <h6 className="font-weight-bold mt-3">{item.text}</h6>
                  else if(item.type === 'description' ) 
                    return <p className="feed-description-ul" dangerouslySetInnerHTML={{__html: item.text}} />
                  else if(item.type === 'image')
                    return <div className="d-block"><img src={item.text} style={{width: resizeWidth < sizeMobile ? resizeWidth-70 : '75%'}}/></div>
                })
              }
            </Card>
            }
          </Col>
        </Row>
      </Row>
      : <React.Fragment />
    )
  }
}

const mapStateToProps = (state) => ({
  userInfo: state.auth.user
})

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentPath: (path) => dispatch(setCurrentPath(path)),
    loadingStart: (text) => dispatch(loadingStart(text)) ,
    loadingEnd: () => dispatch(loadingEnd()) ,
  }
}

export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(FeedDetail))
