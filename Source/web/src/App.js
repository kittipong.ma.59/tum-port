import React, { Component, Fragment } from 'react';
import i18next from 'i18next';
// import { Router, Switch, Route, Redirect } from 'react-router';
import { Router, Route as DefaultRoute, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux'
import {NotificationContainer} from 'react-notifications';
import history from './history';
import authService from './services/auth'

// Import Layout
import DesignLayout from './layouts/DesignLayout'
import HomeLayout from './layouts/HomeLayout';
import UserLayout from './layouts/UserLayout';

// Import View 
import Design from './views/Design';
import Signin from './views/Signin';
import Signup from './views/Signup';
import ForgotPassword from './views/ForgotPassword';
import ResetPassword from './views/ResetPassword';
import Feed from './views/Feed';
import FeedDetail from './views/FeedDetail';
import Home from './views/Home';
import ChangePassword from './views/ChangePassword';
import Profile from './views/Profile';
import Education from './views/Education';
import EditProfile from './views/EditProfile';
import EditEducation from './views/EditEducation';
import Activities from './views/Activities';
import Certificate from './views/Certificate';
import AddActivities from './views/AddActivities';
import AddCertificate from './views/AddCertificate';
import EditActivities from './views/EditActivities';
import EditCertificate from './views/EditCertificate';
import Template from './views/Template';
import CreateTemplate from './views/CreateTemplate';
import ManageTemplate from './views/ManageTemplate ';
import SettingNotifications from './views/SettingNotifications';

// Import Store
import { setDarkMode } from './store/actions/all'
import { currentUser } from './store/actions/auth'

// Import css
import 'bootstrap/dist/css/bootstrap.min.css';
import 'antd/dist/antd.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import 'react-notifications/lib/notifications.css';
import 'react-loading-bar/dist/index.css'
import './App.scss';

import LoadingOverlay from 'react-loading-overlay'
import Loader from 'react-loader-spinner'
import Loading from 'react-loading-bar'
import 'moment/locale/th';

const DefaultHomeLayout = ({ children }) => (
  <Fragment>
    <HomeLayout />
    {children}
  </Fragment>
);

const DefaultDesignLayout = ({ children }) => (
  <Fragment>
    <DesignLayout />
    {children}
  </Fragment>
);

const Route = ({ layout: Layout, component: Component, ...rest }) => (
  <DefaultRoute {...rest} render={props => (
    <Layout>
      <Component {...props} />
    </Layout>
  )}>
  </DefaultRoute>
);

const PrivateRoute = ({ layout: Layout, component: Component, ...rest }) => (
  <DefaultRoute {...rest} render={props => (
    localStorage.getItem('TOKEN')
      ? (<Layout>
        <Component {...props} />
      </Layout>)
    : <Redirect to={{ pathname: '/signin', state: { from: props.location  } }} />
  )} />
);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
    }
  }

  componentDidMount = async () => {
    const { onCurrentUser, setDarkMode } = this.props;
    const token = localStorage.getItem('TOKEN');
    if(token){
      const response = await authService.getChkTokenExpired(token);
      if(response.data){
        localStorage.removeItem('TOKEN');
        window.OneSignal.deleteTag('userId')
      }else{
        await onCurrentUser(token);
      }
    } else {
      window.OneSignal.deleteTag('userId')
    }
    // const isDarkMode = localStorage.getItem('DARK_MODE');
    // await setDarkMode(isDarkMode);
    this.setState({
      isReady: true
    })
  }
  
  render() {
    const { isReady } = this.state;
    const { loading } = this.props;

    return <div>
      <Loading
        show={!isReady}
        color="#52c41a"
      /> {
      !isReady ? <div></div>
      : (
        <div>
          <NotificationContainer/>
          <LoadingOverlay
            active={loading.loading}
            spinner={<Loader type="Triangle" color="#fff" />}
            text={loading.text}
          >
            <Router history={history}>
              <Switch>
                <PrivateRoute layout={UserLayout} exact path="/" name="home" component={Home} />
                <PrivateRoute layout={UserLayout} exact path="/changePassword" name="changePassword" component={ChangePassword} />
                <PrivateRoute layout={UserLayout} exact path="/profile" name="profile" component={Profile} />
                <PrivateRoute layout={UserLayout} exact path="/profile/editProfile" name="editProfile" component={EditProfile} />
                <PrivateRoute layout={UserLayout} exact path="/education" name="educaton" component={Education} />
                <PrivateRoute layout={UserLayout} exact path="/activities" name="activities" component={Activities} />
                <PrivateRoute layout={UserLayout} exact path="/certificate" name="certificate" component={Certificate} />
                <PrivateRoute layout={UserLayout} exact path="/education/editEducation" name="editEducaton" component={EditEducation} />
                <PrivateRoute layout={UserLayout} exact path="/activities/addActivities" name="addActivities" component={AddActivities} />
                <PrivateRoute layout={UserLayout} exact path="/certificate/addCertificate" name="addCertificate" component={AddCertificate} />
                <PrivateRoute layout={UserLayout} exact path="/activities/editActivities" name="editActivities" component={EditActivities} />
                <PrivateRoute layout={UserLayout} exact path="/certificate/editCertificate" name="editCertificate" component={EditCertificate} />
                <PrivateRoute layout={UserLayout} exact path="/template" name="template" component={Template} />
                <PrivateRoute layout={UserLayout} exact path="/manageTemplate" name="manageTemplate" component={ManageTemplate} />
                <PrivateRoute layout={UserLayout} exact path="/feed" name="feed" component={Feed} />
                <PrivateRoute layout={UserLayout} exact path="/feed/feedDetail" name="feedDetail" component={FeedDetail} />
                <PrivateRoute layout={UserLayout} exact path="/settingNotifications" name="settingNotification" component={SettingNotifications}/>
                <PrivateRoute layout={DefaultDesignLayout} exact path="/design/:id" name="design" component={Design}/>
                <PrivateRoute layout={DefaultDesignLayout} exact path="/designTemplate/:id" name="design" component={CreateTemplate}/>
                <Route layout={DefaultHomeLayout} exact path="/signin" name="login" component={Signin} />
                <Route layout={DefaultHomeLayout} exact path="/register" name="register" component={Signup} />
                <Route layout={DefaultHomeLayout} exact path="/forgotPassword" name="forgotPassword" component={ForgotPassword} />
                <Route layout={DefaultHomeLayout} exact path="/resetPassword/:token" name="resetPassword" component={ResetPassword} />
              </Switch>
            </Router>
          </LoadingOverlay>
        </div>
    ) }
    </div>
  }
}


const mapStateToProps = (state) => ({
  loading: state.loading
})

const mapDispatchToProps = dispatch => {
  return {
    onCurrentUser: (token) => dispatch(currentUser(token)),
    setDarkMode: (isDarkMode) => dispatch(setDarkMode(isDarkMode))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
