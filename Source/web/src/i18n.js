import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import detector from 'i18next-browser-languagedetector';
import resources from './locales';

i18n
    .use(detector)
    .use(initReactI18next)
    .init({
        resources,
        lng: i18n.language,
        fallbackLng: "en-US",

        keySeparator: false,

        interpolation: {
            escapeValue: false
        }
    });

export default i18n;