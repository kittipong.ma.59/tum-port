import React, { Component } from 'react'
import { connect } from 'react-redux'
import Navbar from '../components/Navbar'

import { Layout } from 'antd';
const { Header, Content, Footer } = Layout;

class HomeLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }
    render() {

        return (
            <Layout>
                <Navbar/>
                <Content>
                    {this.props.children}
                </Content>
            </Layout>
        );
    }
}

const mapStateToProps = (state) => ({
    darkMode: state.darkMode.dark
})
  
const mapDispatchToProps = dispatch => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeLayout);