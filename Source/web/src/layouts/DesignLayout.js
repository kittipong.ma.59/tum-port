import React, { Component } from 'react'

// Import UI
import {
    Layout
} from 'antd'

class DesignLayout extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Layout>
                <main>{this.props.children}</main>
            </Layout>
        );
    }
}

export default DesignLayout;