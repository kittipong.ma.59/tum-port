import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import reducer from './store/reducers';

export default createStore(reducer, {}, applyMiddleware(reduxThunk));