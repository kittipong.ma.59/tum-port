const mongoose = require('mongoose');
const config = require('../config');
const db = {};

mongoose.connect(config.MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

const conn = mongoose.connection;

// conn.on('error', () => {
//     console.log('Error!!');
// });

// conn.on('open', () => {
//     job.start();
//     console.log('MongoDB Connected');
// });

db.mongoose = mongoose;
db.connection = conn;

module.exports = db;
