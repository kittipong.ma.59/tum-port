exports.setFilterFeed = (arr, universitys) => {
  let filters = [];
  let university = [];
  for (const i in universitys) {
    university.push(universitys[i]._doc.university)
  }
  const category = [
    "รับตรง/TCAS",
    "TCAS 63 / รับตรง 63",
    "TCAS 64 / รับตรง 64",
    "รับตรง TCAS รอบ 1",
    "รับตรง TCAS รอบ 2",
    "รับตรง TCAS รอบ 3",
    "รับตรง TCAS รอบ 4",
    "รับตรง TCAS รอบ 5",
    "รับตรงไม่เข้าร่วมระบบ TCAS",
    "รับตรงเด็กซิ่ว",
    "รับตรงวุฒิเทียบเท่า",
    "รับตรงไม่ใช้เกรดขั้นต่ำ",
    "รับตรงใช้ GAT-PAT",
    "รับตรงไม่ใช้ GAT-PAT",
    "รับตรงใช้ 9 วิชาสามัญ",
    "รับตรงไม่ใช้ 9 วิชาสามัญ",
    "รับตรงสอบข้อเขียน",
    "รับตรงสอบสัมภาษณ์อย่างเดียว",
    "รับตรงผู้จบปวช.",
    "รับตรงผู้จบปวส.",
    "ครูคืนถิ่น",
    "รับน้องม.6",
    "ทุนการศึกษา",
    "คอร์สติวฟรี",
  ]
  for (let i = 0; i < (arr._doc.categories).length; i++) {
    if(university.some(val => (arr._doc.categories[i]).includes(val))) {
      let val = university.find(val => (arr._doc.categories[i]).includes(val))
      if(!filters.includes(val)) { filters.push(val) }
    } else if(university.some(val => (arr._doc.title).includes(val))) {
      let val = university.find(val => (arr._doc.title).includes(val))
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('รับตรง/TCAS') || arr._doc.categories[i].includes('รับตรง TCAS') || arr._doc.categories[i].includes('TCAS 63 / รับตรง 63') || arr._doc.categories[i].includes('TCAS') || arr._doc.categories[i].includes('รับตรง/TCAS63') || arr._doc.categories[i].includes('TCAS 64 / รับตรง 64') || arr._doc.categories[i].includes('รับตรง/TCAS64')) {  
      let val = 'รับตรง/TCAS';
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('TCAS 63 / รับตรง 63') || arr._doc.categories[i].includes('รับตรง/TCAS63')) {
      let val = 'TCAS 63 / รับตรง 63';
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('TCAS 64 / รับตรง 64') || arr._doc.categories[i].includes('รับตรง/TCAS64')) {  
      let val = 'TCAS 64 / รับตรง 64';
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('TCAS รอบที่1') || arr._doc.categories[i].includes('tcas รอบที่1') || arr._doc.categories[i].includes('TCAS รอบ 1')) {  
      let val = 'รับตรง TCAS รอบ 1';
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('TCAS รอบที่2') || arr._doc.categories[i].includes('tcas รอบที่2') || arr._doc.categories[i].includes('TCAS รอบ 2')) {  
      let val = 'รับตรง TCAS รอบ 2';
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('TCAS รอบที่3') || arr._doc.categories[i].includes('tcas รอบที่3') || arr._doc.categories[i].includes('TCAS รอบ 3')) {  
      let val = 'รับตรง TCAS รอบ 3';
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('TCAS รอบที่4') || arr._doc.categories[i].includes('tcas รอบที่4') || arr._doc.categories[i].includes('TCAS รอบ 4')) {  
      let val = 'รับตรง TCAS รอบ 4';
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('TCAS รอบที่5') || arr._doc.categories[i].includes('tcas รอบที่5') || arr._doc.categories[i].includes('TCAS รอบ 5')) {  
      let val = 'รับตรง TCAS รอบ 5';
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('รับตรงไม่เข้าร่วมระบบ TCAS') || arr._doc.categories[i].includes('รับตรง ไม่อยู่ในระบบ TCAS')) {  
      let val = 'รับตรงไม่เข้าร่วมระบบ TCAS';
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('รับตรงเด็กซิ่ว') || arr._doc.categories[i].includes('เด็กซิ่ว')) { 
      let val = 'รับตรงเด็กซิ่ว';
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('รับตรงวุฒิเทียบเท่า')) {  
      let val = 'รับตรงวุฒิเทียบเท่า';
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('รับตรงไม่ใช้เกรดขั้นต่ำ')) {  
      let val = 'รับตรงไม่ใช้เกรดขั้นต่ำ';
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('รับตรงใช้ GAT-PAT') || arr._doc.categories[i].includes('รับตรงใช้คะแนนสอบ GAT-PAT') || arr._doc.categories[i].includes('รับตรงใช้ GAT PAT')) {
      let val = 'รับตรงใช้ GAT-PAT';
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('รับตรงไม่ใช้GAT-PAT') || arr._doc.categories[i].includes('รับตรงไม่ใช้ GAT-PAT') || arr._doc.categories[i].includes('รับตรงไม่ใช้คะแนนสอบGAT-PATและวิชาสามัญ') || arr._doc.categories[i].includes('รับตรงไม่ใช้ GAT PAT')) {  
      let val = 'รับตรงไม่ใช้ GAT-PAT';
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('รับตรงใช้ 9 วิชาสามัญ')) {  
      let val = 'รับตรงใช้ 9 วิชาสามัญ';
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('รับตรงไม่ใช้ 9 วิชาสามัญ') || arr._doc.categories[i].includes('รับตรงไม่ใช้คะแนนสอบGAT-PATและวิชาสามัญ')) {  
      let val = 'รับตรงไม่ใช้ 9 วิชาสามัญ';
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('รับตรงสอบข้อเขียน')) {  
      let val = 'รับตรงสอบข้อเขียน';
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('รับตรงสอบสัมภาษณ์อย่างเดียว') || arr._doc.categories[i].includes('รับตรงสัมภาษณ์อย่างเดียว')) {
      let val = "รับตรงสอบสัมภาษณ์อย่างเดียว";
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('รับตรงผู้จบปวช.')) {  
      let val = "รับตรงผู้จบปวช.";
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('รับตรงผู้จบปวส.')) {  
      let val = "รับตรงผู้จบปวส.";
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('ครูคืนถิ่น.')) {  
      let val = "ครูคืนถิ่น";
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('รับน้องม.6')) {  
      let val = "รับน้องม.6";
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('ทุนการศึกษา') || arr._doc.categories[i].includes('ทุนเรียนต่อ')) {  
      let val = "ทุนการศึกษา";
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('คอร์สติวฟรี')) {  
      let val = "คอร์สติวฟรี";
      if(!filters.includes(val)) { filters.push(val) }
    }
    if(arr._doc.categories[i].includes('รูปแบบการรับ')) {  
      let val = "รูปแบบการรับ";
      if(!filters.includes(val)) { filters.push(val) }
    }
  }

  return filters;
}