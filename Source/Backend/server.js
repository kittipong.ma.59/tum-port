const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');
const socketIO = require('socket.io');
const axios = require('axios');
const cheerio = require('cheerio');
const { setFilterFeed } = require('./functions.js');

const CronJob = require('cron').CronJob;
const FeedParser = require('feedparser');
const request = require('request');

const config = require('./config');

const db = require('./db');
require('./models/user');
require('./models/address');
require('./models/school');
require('./models/profile');
require('./models/education');
require('./models/activities');
require('./models/certificate');
require('./models/template');
require('./models/portfolio');
require('./models/theme');
require('./models/feed');
require('./models/sangfans');
require('./models/university');
require('./models/setting');

const routes = require('./routes');

const app = express();
const port =  process.env.PORT || 1700;

// cron.schedule('0 0-59 * * * *', () => {
//   console.log("Test 1 second")
// })

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/api/v1/auth', routes.authRouter);
app.use('/api/v1/info', routes.infoRouter);
app.use('/api/v1/storage', routes.storageRouter);
app.use('/api/v1/port', routes.portRouter);
app.use('/api/v1/google', routes.googleRouter);
app.use('/api/v1/feed', routes.feedRouter);

const server = app.listen(port, () => console.log(`Server started listening to port ${port}`));

const schoolJson = require('./files/school.json');
const universityJson = require('./files/university.json');
const templateJson = require('./files/template.json');
const themeJson = require('./files/theme.json');

var SchoolModel = mongoose.model('School');
var UniversityModel = mongoose.model('University');
var TemplateModel = mongoose.model('Template');
var ThemeModel = mongoose.model('Theme');

schoolJson.forEach(async (item, index) => {
  await SchoolModel.updateOne(
    {SchoolID: item.SchoolID},
    item,
    { upsert: true }
  )
})

universityJson.forEach(async (item, index) => {
  await UniversityModel.updateOne(
    {university: item.university},
    item,
    { upsert: true }
  )
})

templateJson.forEach(async (item, index) => {
  await TemplateModel.updateOne(
    {_id: item._id},
    item,
    { upsert: true }
  )
})
themeJson.forEach(async (item, index) => {
  await ThemeModel.updateOne(
    {_id: item._id},
    item,
    { upsert: true }
  )
})



const io = socketIO.listen(server);

db.connection.once('open', () => {
  // io.on('connection', socket => {
  //   console.warn('connection');
  // });
  var Feed = mongoose.model('Feed');
  var Sangfans = mongoose.model('Sangfans');
  var University = mongoose.model('University');
  var Setting = mongoose.model('Setting');
  var User = mongoose.model('User');

  let job = new CronJob({
    cronTime: '0 0-59 * * * *',
    onTick: function() {
        const url = ['https://www.enttrong.com/feed', 'https://www.sangfans.com/feed']
        for(let i = 0; i < url.length ; i++ ) {
          const req = request(url[i]);
          const feedparser = new FeedParser();

          const bulk = Feed.collection.initializeUnorderedBulkOp();

          req.on('error',function(err) {
            // throw err;
            console.log(err)
          });
        
          req.on('response',function(res) {
              var stream = this;
        
              if (res.statusCode != 200) {
                return this.emit('error', new Error('Bad status code'));
              } else {
              //   console.log("res OK");
              }
        
              stream.pipe(feedparser);
        
          });
        
          feedparser.on('error',function(err) {
              throw err;
          });
        
          feedparser.on('readable',function() {
        
              var stream = this,
                  meta = this.meta,
                  item;
        
              while ( item = stream.read() ) {
                  item._id = item.guid;
                  delete item.guid;
                  bulk.find({ _id: item._id }).upsert().updateOne({ "$set": item });
              }
        
          });
        
          feedparser.on('end',async function() {
              // console.log('at end');
              const response = await bulk.execute();
              if(response) {

                if(response.result.upserted.length > 0) {
                  const data = response.result.upserted;
                  for (let i = 0; i < data.length; i++) {
                    try {
                      let feed = await Feed.findOne({_id: data[i]._id })
                      const university = await University.find().select('university -_id');
                      const user = await User.find();
                      const filter = await setFilterFeed(feed, university)
                      let userAlert = [];
                      for (let j = 0; j < user.length; j++) {
                        const setting = await Setting.findOne({userId: user[j]._doc._id}).sort({createdAt: 'desc'})
                        let tmpFeed = Object.assign({}, feed);
                        if(setting) {
                          for (let k = 0; k < setting._doc.university.length; k++) {
                            if(filter.includes(setting._doc.university[k])) {
                                tmpFeed._doc.isFilterUniversity = true;
                                break;
                            } else {
                                tmpFeed._doc.isFilterUniversity = false;
                            }
                          }
                          for (let k = 0; k < setting._doc.category.length; k++) {
                            if(filter.includes(setting._doc.category[k])) {
                              tmpFeed._doc.isFilterCategory = true;
                              break;
                            } else {
                              tmpFeed._doc.isFilterCategory = false;
                            }
                          }
                          if(tmpFeed._doc.isFilterUniversity == undefined &&  tmpFeed._doc.isFilterCategory == undefined) {
                            userAlert.push(user[j]._doc._id)
                          } else if((tmpFeed._doc.isFilterUniversity && tmpFeed._doc.isFilterUniversity === true) &&  tmpFeed._doc.isFilterCategory == undefined) {
                            userAlert.push(user[j]._doc._id)
                          } else if((tmpFeed._doc.isFilterCategory && tmpFeed._doc.isFilterCategory === true) &&  tmpFeed._doc.isFilterUniversity == undefined){
                            userAlert.push(user[j]._doc._id)
                          } else if((tmpFeed._doc.isFilterCategory && tmpFeed._doc.isFilterCategory === true) &&  (tmpFeed._doc.isFilterUniversity && tmpFeed._doc.isFilterUniversity === true)){
                            userAlert.push(user[j]._doc._id)
                          }
                        } else {
                          userAlert.push(user[j]._doc._id)
                        }
                      }
                      for (let j = 0; j < userAlert.length; j++) {
                        await User.updateOne(
                          {_id: userAlert[j]}, {readNewFeed: false}, { upsert: true }
                        )
                      }
                      const fetchUrl = await axios.default.get(feed._doc._id);
                      const $ = cheerio.load(fetchUrl.data);
                      const detail = feed._doc._id.includes('https://www.enttrong.com') ? feed._doc.description : $('.entry p').first().text();
                      const logo =  feed._doc._id.includes('https://www.enttrong.com') ? $('.wp-post-image').attr("src") : 'https://www.sangfans.com'+$('.entry .alignnone').attr("src");
                      await Feed.updateOne(
                        {_id: feed._doc._id}, {detail: detail, logo: logo, filters: filter}, { upsert: true }
                      )
                    
                      var headers = {
                        "Content-Type": "application/json; charset=utf-8",
                        "Authorization": `Basic ${config.ONESIGNAL_REST_API_KEY}`
                      };

                      let endpoint = 'https://onesignal.com/api/v1/notifications';
                      const filters = userAlert
                        .map(userId => JSON.stringify({ field: 'tag', key: 'userId', relation: '=', value: userId }))
                        .join('|' + JSON.stringify({ operator: 'OR' }) + '|')
                        .split('|')
                        .map(item => JSON.parse(item));

                      let body = {
                        app_id: config.ONESIGNAL_APP_ID,
                        template_id: 'ed236cdc-d17a-48cb-90e0-24e99cb99199',
                        contents: { en: detail },
                        filters: filters
                      }
                      axios.default.post(endpoint, body, {headers})
                        .then(res => console.log(res.data))
                        .catch(err => console.warn(err.response.data))
                      io.emit('feedChange');
                    } catch(err) {
                      console.log(err)
                    }
                  }
                }
              }
          });
        }
    },
    start: true
  })
  // let job1 = new CronJob({
  //   cronTime: '0 0-59 * * * *',
  //   onTick: function() {
  //       const req = request('https://www.sangfans.com/feed');
  //       const feedparser = new FeedParser();

  //       const bulk = Sangfans.collection.initializeUnorderedBulkOp();

  //       req.on('error',function(err) {
  //         // throw err;
  //         console.log(err)
  //       });
      
  //       req.on('response',function(res) {
  //           var stream = this;
      
  //           if (res.statusCode != 200) {
  //             return this.emit('error', new Error('Bad status code'));
  //           } else {
  //           //   console.log("res OK");
  //           }
      
  //           stream.pipe(feedparser);
      
  //       });
      
  //       feedparser.on('error',function(err) {
  //           throw err;
  //       });
      
  //       feedparser.on('readable',function() {
      
  //           var stream = this,
  //               meta = this.meta,
  //               item;
      
  //           while ( item = stream.read() ) {
  //               item._id = item.guid;
  //               delete item.guid;
  //               bulk.find({ _id: item._id }).upsert().updateOne({ "$set": item });
  //           }
      
  //       });
      
  //       feedparser.on('end',async function() {
  //           // console.log('at end');
  //           const response = await bulk.execute();
  //           if(response) {
  //             if(response.result.upserted.length > 0) {
  //               const data = response.result.upserted;
  //               for (let i = 0; i < data.length; i++) {
  //                 try {
  //                   const sangfans = await Sangfans.findOne({_id: data[i]._id })
  //                   const content = sangfans._doc.title;
  //                   const fetchUrl = await axios.default.get(sangfans._doc._id);
  //                   const $ = cheerio.load(fetchUrl.data);
  //                   const detail = $('.entry p').first().text()
  //                   const logo =  'https://www.sangfans.com'+$('.entry .alignnone').attr("src");
  //                   await Sangfans.updateOne(
  //                     {_id: sangfans._doc._id}, {detail: detail, logo: logo}, { upsert: true }
  //                   )
                    
  //                   var headers = {
  //                     "Content-Type": "application/json; charset=utf-8",
  //                     "Authorization": `Basic ${config.ONESIGNAL_REST_API_KEY}`
  //                   };

  //                   let endpoint = 'https://onesignal.com/api/v1/notifications';
  //                   let body = {
  //                     app_id: config.ONESIGNAL_APP_ID,
  //                     included_segments: ['Subscribed Users'],
  //                     template_id: 'ed236cdc-d17a-48cb-90e0-24e99cb99199',
  //                     contents: { en: content },
  //                   }
  //                   axios.default.post(endpoint, body, {headers})
  //                     .then(res => console.log(res.data))
  //                     .catch(err => console.warn(err.response.data))
                    
  //                     io.emit('feedChange');
  //                 } catch(err) {
  //                   console.log(err)
  //                 }
  //               }
  //             }
  //           }
  //       });
  //   },
  //   start: true
  // })
  job.start()
  console.log('MongoDB Connected');
})


