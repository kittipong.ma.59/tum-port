const passport = require('passport');
const FacebookTokenStrategy = require('passport-facebook-token');
const GoogleTokenStrategy = require('passport-google-token').Strategy;

const config = require('../config');
const userService = require('../services/user');

passport.use(new FacebookTokenStrategy({
    clientID: config.FACEBOOK_CLIENT_ID,
    clientSecret: config.FACEBOOK_CLIENT_SECRET
  }, async (accessToken, refreshToken, profile, done) => {
    try {
      console.log(profile)
      const existingUser = await userService.findUser({facebookId: profile.id})
      if(existingUser)
        return done(null, existingUser)
      const data = {
        providerId: 'facebook',
        firstname: profile._json.first_name,
        lastname: profile._json.last_name,
        email: profile.emails[0].value,
        photoUrl: profile.photos[0].value,
        facebookId: profile.id
      }
      const newUser = await userService.createUser(data);
      done(null, newUser);
    } catch(err) {
      done(err, null, err.message);
    }
  } 
));

passport.use(new GoogleTokenStrategy({
    clientID: config.GOOGLE_CLIENT_ID,
    clientSecret: config.GOOGLE_CLIENT_SECRET
  }, (accessToken, refreshToken, profile, done) => done(null, { ...profile, accessToken, refreshToken }))
);

module.exports = passport;