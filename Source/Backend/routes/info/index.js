const express = require('express');

const router = express.Router();

const infoController = require('../../controllers/info');

router.get('/all/:id', infoController.getAllInfo);

router.get('/customUniversity', infoController.customUniversity);

router.get('/customAddress', infoController.customAddress);

router.get('/customSchool', infoController.customSchool);

router.get('/provinces', infoController.getProvinces);

router.post('/districts', infoController.setDistrict);

router.post('/schools', infoController.setSchool);

router.post('/autoCompleteSchool', infoController.autoCompleteSchool);

router.get('/getProfile', infoController.getProfile);

router.get('/getEducation/:userId', infoController.getEducation);

router.get('/getActivities/:userId', infoController.getActivities);

router.get('/getCertificate/:userId', infoController.getCertificate);

router.get('/getActivitiesById/:id', infoController.getActivitiesById);

router.get('/getCertificateById/:id', infoController.getCertificateById);

router.post('/addEducation', infoController.addEducation);

router.post('/addProfile', infoController.addProfile);

router.post('/addActivities', infoController.addActivities);

router.post('/addCertificate', infoController.addCertificate);

router.put('/updateProfile', infoController.updateProfile);

router.put('/updateEducation', infoController.updateEducation);

router.put('/updateActivities', infoController.updateActivities);

router.put('/updateCertificate', infoController.updateCertificate);

router.put('/changeStatusActivities/:id', infoController.changeStatusActivities);

router.put('/changeStatusCertificate/:id', infoController.changeStatusCertificate);

router.delete('/removeEducation/:id', infoController.removeEducation)

router.delete('/removeActivities/:id', infoController.removeActivities)

router.delete('/removeCertificate/:id', infoController.removeCertificate)

module.exports = router;
