const express = require('express');

const passport = require('../../passport');
const authController = require('../../controllers/auth');
const authorization = require('../../middleware/authorization');

const router = express.Router();

router.post('/login', (req, res) => {
  res.json({hello: req.body.name});
});

router.post('/signup', authController.signup);

router.post('/signin', authController.signin);

router.post('/facebook/token', passport.authenticate('facebook-token', { session: false }), authController.facebookOAuth);

router.post('/google/token', passport.authenticate('google-token', { session: false }), authController.googleOAuth);

router.post('/google/drive', passport.authenticate('google-token', { session: false }), authController.googleDrive);

router.post('/resetPassword', authController.resetPassword);

router.get('/reset', authController.reset);

router.put('/updatePasswordForReset', authController.updatePasswordForReset);

router.put('/updatePasswordForChange', authController.updatePasswordForChange);

router.put('/readNewFeed/:id', authController.readNewFeed);

router.get('/users', authController.findAll);

router.get('/currentUser', authorization, authController.getCurrentUser);
router.get('/userDriveGoogle', authorization, authController.userDriveGoogle);
router.get('/token', authorization, authController.chkTokenExpired);

module.exports = router;