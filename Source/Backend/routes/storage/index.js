const express = require('express');

const storageController = require('../../controllers/storage');
const uploadFile = require('../../middleware/uploadFile');

const router = express.Router();

router.get('/file/:fileId', storageController.getFile);

router.post('/upload', uploadFile.single('file'), storageController.uploadFile);

router.delete('/delete/:id', storageController.deleteFile);

module.exports = router;
