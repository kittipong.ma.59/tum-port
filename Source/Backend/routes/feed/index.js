const express = require('express');

const feedController = require('../../controllers/feed');

const router = express.Router();

router.get('/university', feedController.getUniversity);

router.get('/getId', feedController.getFeedById);

router.get('/sangfansGetId', feedController.getSangfansById);

router.get('/noti/:id', feedController.getNotification);

router.get('/enttrong', feedController.getEnttrong);

router.get('/sangfans', feedController.getSangfans);

router.get('/fetch', feedController.fetchUrl);

router.put('/readBy', feedController.updateReadBy)

router.put('/setting/:id', feedController.settingNotification)

router.get('/setting/:id', feedController.getSetting)

router.get('/category', feedController.getCategory)
module.exports = router;