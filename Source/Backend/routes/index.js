const authRouter = require('./auth');
const infoRouter = require('./info');
const storageRouter = require('./storage');
const portRouter = require('./port');
const googleRouter = require('./google');
const feedRouter = require('./feed');

module.exports = {
  authRouter,
  infoRouter,
  storageRouter,
  portRouter,
  googleRouter,
  feedRouter
};
