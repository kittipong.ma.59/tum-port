const express = require('express');

const portController = require('../../controllers/port');

const router = express.Router();


router.post('/createPort', portController.createPortfolio);

router.post('/createTemplate', portController.createTemplate);

router.put('/update', portController.updateTemplate);

router.put('/updatePort', portController.updatePortfolio);

router.get('/findOne/:id', portController.findTemplateById);

router.post('/setName/:id', portController.setNamePortfolio);

router.get('/findOnePort/:id', portController.findPortfolioById);

router.get('/findPortByUserId/:id', portController.findPortfolioByUserId);

router.get('/findAll', portController.findAllTemplate);

router.delete('/delete/:id', portController.deletePortfolio);

router.delete('/deleteTemplate/:id', portController.deleteTemplate);

router.get('/theme', portController.getAllTheme)

module.exports = router;