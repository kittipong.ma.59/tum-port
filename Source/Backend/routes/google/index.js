const express = require('express');

const googleController = require('../../controllers/google');
const getFile = require('../../middleware/getFile');
const passport = require('../../passport');

const router = express.Router();

router.get('/calendar', googleController.getCalendar);

router.post('/upload/drive',  getFile.single('file'), googleController.uploadDrive);

router.post('/auth/calendar',  passport.authenticate('google-token', { session: false }), googleController.signInCalendar);

module.exports = router;