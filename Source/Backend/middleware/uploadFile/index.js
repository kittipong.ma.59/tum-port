const multer = require('multer');
const GridFsStorage = require('multer-gridfs-storage');

const db = require('../../db');

const storage = new GridFsStorage({
    db: db.connection,
    file: (req, file) => {
        return new Promise((resolve, reject) => {
            const date = Date.now().valueOf();
            const fileInfo = {
                filename: date + '_' + file.originalname,
                bucketName: 'storages'
            };
            resolve(fileInfo);
        });
    }
});

module.exports = multer({ storage });
