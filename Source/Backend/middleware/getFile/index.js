const path = require('path');
const multer = require('multer');
const crypto = require('crypto');
const mime = require('mime-types')

var storage = multer.memoryStorage()

module.exports = multer({ storage });