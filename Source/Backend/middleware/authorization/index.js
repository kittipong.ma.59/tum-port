const JWT = require('jsonwebtoken');
const config = require('../../config')

module.exports = ((req, res, next) => {
  const authorization = req.headers['authorization'];
  if (authorization === undefined)
    return res.sendStatus(401);
  const bearer = authorization.split(' ')[0];
  if (bearer !== 'Bearer')
    return res.sendStatus(401);
  const token = authorization.split(' ')[1];
  if (token === undefined)
    return res.sendStatus(401);
  JWT.verify(token, config.JWT_SECRET, (err, decoded) => {
    if (err) return res.sendStatus(401);
    req.decoded = decoded;
    next();
  });
});
