const mongoose = require('mongoose');
const JWT = require('jsonwebtoken');
const bcryptjs = require('bcryptjs');
const async = require('async');
const crypto = require('crypto');
const nodemailer = require('nodemailer');

const config = require('../../config')
const userService = require('../../services/user');
const User = mongoose.model('User');

const createToken = user => {
  const currentDate = new Date();
  return JWT.sign({
    iss: config.APPLICATION_NAME,
    sub: user._id,
    iat: currentDate.valueOf(),
    exp: currentDate.setDate(currentDate.getDate() + 2).valueOf()
  }, config.JWT_SECRET);
}

exports.signup = async (req, res) => {
  try {
    const signupData = req.body.data;
    const query = {
      providerId: 'password',
      email: signupData.email
    }
    const emailRegex = RegExp(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i);
    const existingUser = await userService.findUser(query);
    if (!emailRegex.test(signupData.email))
      return res.status(400).json({ code: 'auth/invalid-email', message: 'Invalid email format.' });
    else if (existingUser)
      return res.status(400).json({ code: 'auth/email-already-in-use', message: 'Email is already in use by another user.' });
    const passwordHash = await bcryptjs.hash(signupData.password, 10);
    const data = {
      providerId: 'password',
      firstname: signupData.firstname,
      lastname: signupData.lastname,
      email: signupData.email,
      password: passwordHash
    }
    const newUser = await userService.createUser(data)
    const token = createToken(newUser);
    res.status(200).json({ token });
  } catch(err) {
    console.error(err);
    res.sendStatus(401);
  }
}

exports.signin = async (req, res) => {
  try {
    const signinData = req.body.data;
    const emailRegex = RegExp(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i);
    const existingUser = await userService.findUser({providerId: 'password', email: signinData.email});
    if (!emailRegex.test(signinData.email))
      return res.status(400).json({ code: 'auth/invalid-email', message: 'Invalid email format.' });
    else if(!existingUser)
      return res.status(400).json({ code: 'auth/user-not-found', message: 'The email specified is not found.' });
    
    const isMatchPassword = await bcryptjs.compare(signinData.password, existingUser.password )
    if(!isMatchPassword)
      return res.status(400).json({ code: 'auth/wrong-password', message: 'Password is invalid.' });
    const token = createToken(existingUser);
    res.status(200).json({ token });
  } catch(err){
    console.error(err);
    res.sendStatus(401);
  }
}

exports.facebookOAuth = (req, res) => {
  const user = req.user;
  if(!user){
    return res.sendStatus(401);
  }
  const token = createToken(user);
  res.status(200).json({token})
}

exports.googleOAuth = async (req, res) => {
  const profile = req.user;
  const existingUser = await userService.findUser({googleId: profile.id})
  if(existingUser) {
    const token = createToken(existingUser);
    return res.status(200).json({ token });
  }
  const data = {
    providerId: 'google',
    firstname: profile._json.given_name,
    lastname: profile._json.family_name,
    email: profile.emails[0].value,
    photoUrl: profile._json.picture,
    googleId: profile.id
  }
  const newUser = await userService.createUser(data);
  const token = createToken(newUser);
  res.status(200).json({ token });
  
}

exports.googleDrive = (req, res) => {
  const profile = req.user;
  const token = JWT.sign(profile, config.JWT_SECRET);
  res.status(200).json({ token });
}

exports.resetPassword = async (req, res) => {
  const query = {
    providerId: 'password',
    email: req.body.email
  }
  const existingUser = await userService.findUser(query);
  const emailRegex = RegExp(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i);
  if (!emailRegex.test(req.body.email))
    return res.status(400).json({ code: 'auth/invalid-email', message: 'Invalid email format.' });
  else if(!existingUser)
    return res.status(400).json({ code: 'auth/user-not-found', message: 'The email specified is not found.' });
      
  const token = crypto.randomBytes(20).toString('hex');
  await User.updateOne(
    {_id: existingUser._id}, 
    {
      resetPasswordToken: token, 
      resetPasswordExpires: Date.now() + 3600000
    },
    { upsert: true }
  );
  const transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: config.MAILER_EMAIL_ID,
      pass: config.MAILER_PASSWORD
    }
  });

  const mailOptions = {
    from: 'no-reply@senior-prj-62.tumport.com',
    to: `${existingUser.email}`,
    subject: 'Reset you password for TumPort',
    text:
      'Hello\n' +
      'Follow this link to reset your password for your kittipong.ma.59@ubu.ac.th account.\n\n' +
      `http://localhost:3000/resetPassword/${token}` +
      '\n\nIf you didn’t ask to reset your password, you can ignore this email.\n' +
      'Thanks'
  };

  transporter.sendMail(mailOptions, (err, response) => {
    if(err){
      console.log('Error: ', err);
    }else{
      console.log('Response', response);
      res.status(200).json('Recovery email sent')
    }
  });
}

exports.reset = async (req, res, next) => {
  try{
    const query = {
      resetPasswordToken: req.query.token,
      resetPasswordExpires: {
        $gt: Date.now()
      }
    }
    const user = await userService.findUser(query);
    if(!user) {
      res.status(400).json({code:'password-reset-link-expired', massage: 'Password reset link is invalid or has expired.'})
    }else{
      res.status(200).send({
        email: user.email
      })
    }
  } catch(err){
    console.error(err);
    res.sendStatus(401);
  }
}

exports.updatePasswordForReset = async (req, res) => {
  try {
    const existingUser = await userService.findUser({providerId: 'password', email: req.body.data.email});
    if(!existingUser)
      return res.status(400).json({ code: 'auth/user-not-found', message: 'The email specified is not found.' });

    const passwordHash = await bcryptjs.hash(req.body.data.password, 10);
    await User.updateOne(
      {_id: existingUser._id}, 
      {
        password: passwordHash,
        resetPasswordToken: null, 
        resetPasswordExpires: null
      },
      { upsert: true }
    );
    res.status(200).send({message: 'Password Update Success'});
  } catch(err){
    console.error(err);
    res.sendStatus(401);
  }
}

exports.readNewFeed = async (req, res) => {
  try {
    const id = req.params.id
    await User.updateOne(
      {_id: id}, 
      {
        readNewFeed: true
      },
      { upsert: true }
    );
    res.status(200).send("done");
  } catch(err){
    console.error(err);
    res.sendStatus(401);
  }
}

exports.updatePasswordForChange = async (req, res) => {
  try {
    const existingUser = await userService.findUser({providerId: 'password', email: req.body.data.email});
    if(!existingUser)
      return res.status(400).json({ code: 'auth/user-not-found', message: 'The email specified is not found.' });
    const chkCurrentPassword = await bcryptjs.compare(req.body.data.current_password, existingUser.password);
    if(!chkCurrentPassword) {
      return res.status(400).json({ code: 'auth/wrong-current-password', message: 'Current Password is invalid.' });
    }
    const passwordHash = await bcryptjs.hash(req.body.data.password, 10);
    await User.update(
      {_id: existingUser._id}, 
      {
        password: passwordHash,
      },
      { upsert: true }
    );
    res.status(200).send({message: 'Password Update Success'});
  } catch(err) {
    console.error(err);
    res.sendStatus(401);
  }
}

exports.findAll = async (req, res) => {
  const users = await userService.findAllUser();
  res.status(200).json({ users })
}

exports.getCurrentUser = async (req, res) => {
  try {
    const query = {
      _id: req.decoded.sub
    }
    const user = await userService.findUser(query);
    res.status(200).json({ user });
  } catch (error) {
    console.error(error);
    return res.sendStatus(401);
  }
}

exports.userDriveGoogle = async (req, res) => {
  try {
    res.status(200).json({ profile: req.decoded });
  } catch (error) {
    console.error(error);
    return res.sendStatus(401);
  }
}

exports.chkTokenExpired = async (req, res) => {
  try{
    const exp = req.decoded.exp;
    const currentDate = new Date().valueOf()
    res.status(200).send(currentDate > exp);
  } catch(err) {
    console.error(error);
    return res.sendStatus(401);
  }
}
