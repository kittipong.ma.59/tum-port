const exprees = require('express');
const mongoose = require('mongoose');
const axios = require('axios');
const moment = require('moment');
const cheerio = require('cheerio');

const Feed = mongoose.model('Feed');
const Sangfans = mongoose.model('Sangfans');
const University = mongoose.model('University');
const Setting = mongoose.model('Setting');

exports.getUniversity = async (req, res) => {
    try {
        const response = await University.find().sort({ university: 1 });;
        res.status(200).send(response);
    } catch (error) {
        console.error(error);
        return res.sendStatus(404);
    }
}

exports.getSangfans = async (req, res, next) => {
    try {
        const sangfans = await Sangfans.find().sort({ pubDate: -1 });
        res.status(200).send(sangfans);
    } catch (error) {
        console.error(error);
        return res.sendStatus(404);
    }
}

exports.getEnttrong = async (req, res) => {
    try {
        const feed = await Feed.find().sort({ pubDate: -1 });
        res.status(200).send(feed);
    } catch (error) {
        console.error(error);
        return res.sendStatus(404);
    }
}

exports.getNotification = async (req, res) => {
    try {
        const userId = req.params.id;
        let response = await Feed.find().sort({ pubDate: -1 });
        let setting = await Setting.find({userId: userId});
        let feed = [];
        //let sangfans = await Sangfans.find().sort({ pubDate: -1 });
        //let response = enttrong.concat(sangfans)
        response.sort(function (a, b) {
           return new moment(b._doc.pubDate) - new moment(a._doc.pubDate)
        });
        if(setting.length != 0) {
            response.forEach((item, index) => {
                if(item._doc.filters) {
                    const filters = setting.reduce(function (a, b) {
                        var nowA = moment(item._doc.pubDate);
                        var endA = moment(a._doc.createdAt);
                        var nowB = moment(item._doc.pubDate);
                        var endB = moment(b._doc.createdAt);
                        return (( moment.duration(nowA.diff(endA)).asHours() <  moment.duration(nowB.diff(endB)).asHours()) || moment.duration(nowB.diff(endB)).asHours() < 0  ? a : b );
                    });
                    for (let i = 0; i < filters.university.length; i++) {
                        if(item._doc.filters.includes(filters.university[i])) {
                            item._doc.isFilterUniversity = true;
                            break;
                        } else {
                            item._doc.isFilterUniversity = false;
                        }
                    }
                    for (let i = 0; i < filters.category.length; i++) {
                        if(item._doc.filters.includes(filters.category[i])) {
                            item._doc.isFilterCategory = true;
                            break;
                        } else {
                            item._doc.isFilterCategory = false;
                        }
                    }
                }
            })
        }
        response.forEach((item, index) => {
            if(item._doc.isFilterUniversity == undefined &&  item._doc.isFilterCategory == undefined) {
                feed.push(item._doc)
            } else if((item._doc.isFilterUniversity && item._doc.isFilterUniversity === true) &&  item._doc.isFilterCategory == undefined) {
                feed.push(item._doc)
            } else if((item._doc.isFilterCategory && item._doc.isFilterCategory === true) &&  item._doc.isFilterUniversity == undefined){
                feed.push(item._doc)
            } else if((item._doc.isFilterCategory && item._doc.isFilterCategory === true) &&  (item._doc.isFilterUniversity && item._doc.isFilterUniversity === true)){
                feed.push(item._doc)
            }
        })
        res.status(200).send(feed);
    } catch (error) {
        console.error(error);
        return res.sendStatus(404);
    }
}

exports.getFeedById = async (req, res) => {
    try {
        const id = req.headers.id;
        const feed = await Feed.findOne({_id: id});
        res.status(200).send(feed);
    } catch (error) {
        console.error(error);
        return res.sendStatus(404);
    }
}

exports.getSangfansById = async (req, res) => {
    try {
        const id = req.headers.id;
        const response = await Sangfans.findOne({_id: id});
        res.status(200).send(response);
    } catch (error) {
        console.error(error);
        return res.sendStatus(404);
    }
}

exports.fetchUrl = async (req, res) => {
    try {
        const url = req.headers.url;
        const response = await axios.default.get(url);
        res.status(200).send(response.data);
    } catch (error) {
        res.status(200).send('');
    }
}

exports.updateReadBy = async (req, res) => {
    try {
        const id = req.body.id;
        const userId = req.body.userId;
        const feed = await Feed.findOne({_id: id});
        const existingUser = await feed.read_by.some(k => k.userId == userId)
        if(!existingUser) {
            feed.read_by.push({userId: userId})
            feed.save();
        }
        // const sangfans = await Sangfans.findOne({_id: id});
        // if(feed) {
        // } else if(sangfans) {
        //     const existingUser = await sangfans.read_by.some(k => k.userId == userId)
        //     if(!existingUser) {
        //         sangfans.read_by.push({userId: userId})
        //         sangfans.save();
        //     }
        // }
        res.status(200).send('done');
    } catch (error) {
        console.error(error);
        return res.sendStatus(404);
    }
}

exports.settingNotification = async (req, res) => {
    try {
        const userId = req.params.id;
        await Setting.create({
            userId: userId, 
            university: req.body.university,
            category: req.body.category
        })
        res.status(200).send('done');
    } catch (error) {
        console.error(error);
        return res.sendStatus(404);
    }
}

exports.getSetting = async (req, res) => {
    try {
        const userId = req.params.id;
        const response = await Setting.findOne({userId: userId}).sort({createdAt: 'desc'})
        res.status(200).send(response);
    } catch (error) {
        console.error(error);
        return res.sendStatus(404);
    }
}

exports.getCategory = async (req, res) => {
    try {
        const response = await Feed.find({author: "P'Nutkung"}).select('categories -_id');
        let category = [];
        for (let i = 0; i < response.length; i++) {
            for (let j = 0; j < (response[i]._doc.categories).length; j++) {
                if(!category.includes(response[i]._doc.categories[j])) {
                    if(!(response[i]._doc.categories[j]).includes('กลุ่ม') && !(response[i]._doc.categories[j]).includes('คณะ') && !(response[i]._doc.categories[j]).includes('วิทยาลัย') && !(response[i]._doc.categories[j]).includes('สถาบัน') && !(response[i]._doc.categories[j]).includes('โรงเรียน') && !(response[i]._doc.categories[j]).includes('2562') && !(response[i]._doc.categories[j]).includes('2563'))
                        category.push(response[i]._doc.categories[j])
                }
            }
        }
        res.status(200).send(category);
    } catch (error) {
        console.error(error);;;
        return res.sendStatus(404);
    }
}

