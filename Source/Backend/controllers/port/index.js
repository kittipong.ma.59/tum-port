const mongoose = require('mongoose');

const Template = mongoose.model('Template');
const Portfolio = mongoose.model('Portfolio');
const Theme = mongoose.model('Theme');

exports.findTemplateById = async (req, res) => {
    try {
        const id = req.params.id
        const response = await Template.findOne({_id: id})
        res.status(200).send(response);
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.findPortfolioById = async (req, res) => {
    try {
        const id = req.params.id
        const response = await Portfolio.findOne({_id: id})
        res.status(200).send(response);
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.findPortfolioByUserId = async (req, res) => {
    try {
        const id = req.params.id
        const response = await Portfolio.find({userId: id})
        res.status(200).send(response);
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.findAllTemplate = async (req, res) => {
    try {
        const response = await Template.find()
        res.status(200).send(response);
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.createTemplate = async (req, res) => {
    try {
        const response = await Template.create(req.body.data)
        res.status(200).send(response);
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.createPortfolio = async (req, res) => {
    try {
        const data = req.body.data;
        const existingPortfolio = await Portfolio.findOne({userId: data.userId, name: data.name});
        if(existingPortfolio)
            return res.status(400).json({ code: 'portfolio/name-already-in-use', message: 'Name is already use in this user.' });
        const dataAdd = {
            userId: data.userId,
            name: data.name,
            theme: data.theme,
            image: data.image,
            elements: data.elements
        }
        const response = await Portfolio.create(dataAdd)
        res.status(200).send(response);
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.setNamePortfolio = async (req, res) => {
    try {
        const id = req.params.id;
        const name = req.body.name;
        await Portfolio.updateOne(
            {_id: id}, {name: name}, { upsert: true }
        );
        res.status(200).send('done');
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.updateTemplate = async (req, res) => {
    try {
        await Template.updateOne(
            {_id: req.body.data._id}, (req.body.data), { upsert: true }
        );
        res.status(200).send('done');
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.updatePortfolio = async (req, res) => {
    try {
        await Portfolio.updateOne(
            {_id: req.body.data._id}, (req.body.data), { upsert: true }
        );
        res.status(200).send('done');
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.deletePortfolio = async (req, res) => {
    try {
        const id = req.params.id;
        await Portfolio.deleteOne({ _id: id });
        res.status(200).send("done");
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.deleteTemplate = async (req, res) => {
    try {
        const id = req.params.id;
        await Template.deleteOne({ _id: id });
        res.status(200).send("done");
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.getAllTheme = async (req, res) => {
    try {
        const theme = await Theme.find();
        res.status(200).send(theme);
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}