const fs = require('fs');
const readline = require('readline');
const stream = require('stream');
const { google } = require('googleapis');

const config = require('../../config');

const SCOPES = ['https://www.googleapis.com/auth/calendar.readonly'];
const TOKEN_PATH = 'token.json';

exports.getCalendar = async (req, res) => {
  try {
    res.status(200).send('test');
  } catch(error) {
    console.error(error);
    res.sendStatus(200);
  }
}

exports.uploadDrive = async (req, res) => {
  try {
    const { originalname, mimetype, buffer } = req.file;
    const accessToken = req.body.accessToken;
    const oauth2Client = await new google.auth.OAuth2();
    oauth2Client.setCredentials({
      access_token: accessToken
    })

    const drive = await google.drive({
      version: 'v3',
      auth: oauth2Client
    })

    let bufferStream = new stream.PassThrough();
    bufferStream.end(buffer);
    const driveResponse = await drive.files.create({
      requestBody: {
        name: originalname,
        mimeType: mimetype
      },
      media: {
        mimeType: mimetype,
        body: bufferStream
      }
    });
    res.status(200).send('done');
  } catch (err) {
    console.error(err);
    res.sendStatus(401);
  }
}

exports.signInCalendar = async (req, res) => {
  try {
    const profile = req.user;
    const oauth2Client = await new google.auth.OAuth2();

    const url = oauth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: 'https://www.googleapis.com/auth/calendar.readonly'
    })
    oauth2Client.setCredentials({
      access_token: accessToken
    })

    res.status(200).send('done');
  } catch (err) {
    console.error(err);
    res.sendStatus(401);
  }
}