const mongoose = require('mongoose');
const schoolJson = require('../../files/school.json');
const addressJson = require('../../files/address.json');
const universityJson = require('../../files/university.json');
const User = mongoose.model('User');
const School = mongoose.model('School');
const Profile = mongoose.model('Profile');
const Address = mongoose.model('Address');
const Education = mongoose.model('Education');
const Activities = mongoose.model('Activities');
const Certificate = mongoose.model('Certificate');
const University = mongoose.model('University');

exports.customUniversity = async (req, res) => {
    try {
        universityJson.forEach(async item => {
           await University.create(item);
        });
        res.send("done");
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.customAddress = async (req, res) => {
    try {
        addressJson.forEach(async item => {
           await University.create(item);
        });
        res.send("done");
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.customSchool = async (req, res) => {
    try {
        schoolJson.forEach(async item => {
           await School.create(item);
        });
        res.send("done");
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.getProvinces = async (req, res) => {
    try {
        const existingProvince = await School.aggregate([
            { "$sort": {Province: 1} },
            {
                $group: 
                { 
                    "_id": '$Province'
                }
            },
        ]);
        const listProvinces = existingProvince.map(item => item._id)
        listProvinces.sort();
        res.status(200).send(listProvinces);
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.setDistrict = async (req, res) => {
    try {
        const province = req.body.province;
        if(province == ''){
            res.status(200).send([]);
        }
        const existingDistrict = await School.aggregate([
            { $match: {Province: province} },
            { $sort: {District: 1} },
            {
                $group: { 
                    "_id": '$District'
                },
            }
        ]);
        const listDisctrict = existingDistrict.map(item => item._id)
        listDisctrict.sort();
        res.status(200).send(listDisctrict);
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.setSchool = async (req, res) => {
    try {
        const district = req.body.district;
        const province = req.body.province;
        if(district == '' || province == ''){
            res.status(200).send([]);
        }
        const existingSchool = await School.aggregate([
            { 
                $match: {
                    District: district,
                    Province: province
                } 
            },
            { $sort: {SchoolName: 1} },
            {
                $group: { 
                    "_id": '$SchoolName'
                },
            }
        ]);
        const listSchool = existingSchool.map(item => item._id)
        listSchool.sort();
        res.status(200).send(listSchool);
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.autoCompleteSchool = async (req, res) => {
    try {
        const input = req.body.key;
        const school = await School.find({SchoolName: { $regex: `^${input}.*` }}).limit(10)
        res.status(200).send(school);
    } catch(err) {
        console.error(err);
        res.sendStatus(401);
    }
}


exports.getAllInfo = async (req, res) =>{
    try {
        const userId = req.params.id;
        const existingInfo = await Profile.findOne({userId: userId});
        res.status(200).send(existingInfo);
        
    } catch(err){
        console.error(err);
        res.sendStatus(401);
    }    
}

exports.getProfile = async (req, res) =>{
    try {
        const userId = req.headers.userid;
        const existingProfile = await Profile.findOne({userId: userId});
        res.status(200).send(existingProfile);
        
    } catch(err){
        console.error(err);
        res.sendStatus(401);
    }    
}

exports.getEducation = async (req, res) =>{
    try {
        const userId = req.params.userId;
        const existingEducation = await Education.find({userId: userId}).sort({startYear: 1});;
        res.status(200).send(existingEducation);
        
    } catch(err){
        console.error(err);
        res.sendStatus(401);
    }    
}

exports.addProfile = async (req, res) => {
    try {
        const profileData = req.body.data;
        const existingUser = await User.findOne({_id: profileData.userId});
        if(profileData.firstname && profileData.lastname) {
            if(existingUser.firstname != profileData.firstname || existingUser.lastname != profileData.lastname) {
                await User.updateOne(
                    {_id: existingUser._id}, 
                    {
                        firstname: profileData.firstname,
                        lastname: profileData.lastname
                    },
                    { upsert: true }
                );
            }
        }
        if (profileData.photoId) {
            await User.updateOne(
                {_id: existingUser._id},
                {
                    photoId: profileData.photoId ? profileData.photoId : null
                },
                { upsert: true }
            )
        }
        await Profile.create(profileData)
        res.status(200).send("done");
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.addEducation = async (req, res) => {
    try {
        const educationData = req.body.data;
        await Education.create(educationData);
        res.status(200).send("done");
    } catch (err) {
        console.error(err);
        res.sendStatus(401);
    }
}

exports.updateProfile = async (req, res) => {
    try {
        const profileData = req.body.data;
        const existingUser = await User.findOne({_id: profileData.userId})
        if(profileData.firstname && profileData.lastname) {
            if(existingUser.firstname != profileData.firstname || existingUser.lastname != profileData.lastname) {
                await User.updateOne(
                    {_id: existingUser._id}, 
                    {
                    firstname: profileData.firstname,
                    lastname: profileData.lastname,
                    },
                    { upsert: true }
                );
            }
        }
        if (profileData.photoId) {
            await User.updateOne(
                {_id: existingUser._id},
                {
                    photoId: profileData.photoId ? profileData.photoId : null
                },
                { upsert: true }
            )
        }
        await Profile.updateOne({userId: existingUser._id}, profileData, { upsert: true })
        res.status(200).send("done");
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.updateEducation = async (req, res) => {
    try {
        const educationData = req.body.data;
        await Education.updateOne({_id: educationData._id}, educationData, { upsert: true })
        res.status(200).send("done");
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.updateActivities = async (req, res) => {
    try {
        const activitiesData = req.body.data;
        await Activities.updateOne({_id: activitiesData._id}, activitiesData, { upsert: true })
        res.status(200).send("done");
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.changeStatusActivities = async (req, res) => {
    try {
        const id = req.params.id;
        const statusData = req.body;
        await Activities.updateOne({_id: id}, statusData, { upsert: true })
        res.status(200).send("done");
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.updateCertificate = async (req, res) => {
    try {
        const certificateData = req.body.data;
        await Certificate.updateOne({_id: certificateData._id}, certificateData, { upsert: true })
        res.status(200).send("done");
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}
exports.changeStatusCertificate = async (req, res) => {
    try {
        const id = req.params.id;
        const statusData = req.body;
        await Certificate.updateOne({_id: id}, statusData, { upsert: true })
        res.status(200).send("done");
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.removeEducation = async (req, res) => {
    try {
        const educationId = req.params.id;
        await Education.deleteOne({ _id: educationId });
        res.status(200).send("done");
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.addActivities = async (req, res) => {
    try {
        const activitiesData = req.body.data;
        await Activities.create(activitiesData)
        res.status(200).send("done");
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.addCertificate = async (req, res) => {
    try {
        const certificateData = req.body.data;
        await Certificate.create(certificateData)
        res.status(200).send("done");
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.getActivities = async (req, res) =>{
    try {
        const userId = req.params.userId;
        const existingActivities = await Activities.find({userId: userId});
        res.status(200).send(existingActivities);
        
    } catch(err){
        console.error(err);
        res.sendStatus(401);
    }    
}

exports.getCertificate = async (req, res) =>{
    try {
        const userId = req.params.userId;
        const existingCertificate = await Certificate.find({userId: userId});
        res.status(200).send(existingCertificate);
        
    } catch(err){
        console.error(err);
        res.sendStatus(401);
    }    
}

exports.getActivitiesById = async (req, res) =>{
    try {
        const id = req.params.id;
        const existingActivities = await Activities.findOne({_id: id});
        res.status(200).send(existingActivities);
        
    } catch(err){
        console.error(err);
        res.sendStatus(401);
    }    
}

exports.getCertificateById = async (req, res) =>{
    try {
        const id = req.params.id;
        const existingCertificate = await Certificate.findOne({_id: id});
        res.status(200).send(existingCertificate);
        
    } catch(err){
        console.error(err);
        res.sendStatus(401);
    }    
}

exports.removeActivities = async (req, res) => {
    try {
        const activitiesId = req.params.id;
        await Activities.deleteOne({ _id: activitiesId });
        res.status(200).send("done");
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}

exports.removeCertificate = async (req, res) => {
    try {
        const certificateId = req.params.id;
        await Certificate.deleteOne({ _id: certificateId });
        res.status(200).send("done");
    }
    catch(err){
        console.error(err);
        res.sendStatus(401);
    }
}