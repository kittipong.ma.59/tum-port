const db = require('../../db');

const getGfs = async () => {
    return new db.mongoose.mongo.GridFSBucket(db.connection.db, { bucketName: 'storages' });
}

exports.getFile = async (req, res) => {
    try {
        let query = {
            _id: new db.mongoose.Types.ObjectId(req.params.fileId)
        }
        const file = await db.connection.db.collection('storages.files').findOne(query);
        query = {
            files_id: new db.mongoose.Types.ObjectId(req.params.fileId)
        }
        const chunks = await db.connection.db.collection('storages.chunks').find(query).toArray();
        const url = chunks.map(chunk => chunk.data.toString('base64')).join('');
        // const gfs = await getGfs();
        // const downloadStream = gfs.openDownloadStream(new db.mongoose.Types.ObjectId(req.params.fileId));
        // downloadStream.on('data', async chunks => {
        //     const files = await gfs.find({ _id: db.mongoose.Types.ObjectId(req.params.fileId) }).toArray();
        res.status(200).json({ file: { ...file, url } });
        // });
    } catch (error) {
        console.error(error);
        return res.sendStatus(404);
    }
}

exports.uploadFile = (req, res) => {
    res.status(200).json({ file: req.file });
}

exports.deleteFile = async (req, res) => {
    try {
        const gfs = await getGfs();
        await gfs.delete(new db.mongoose.Types.ObjectId(req.params.id));
        res.status(200).send('success');
    } catch(error) {
        console.error(error);
        return res.sendStatus(404);
    }
}
