const mongoose = require('mongoose');

const User = mongoose.model('User');

exports.createUser = data => {
  return User.create(data);
}

exports.findUser = query => {
  return User.findOne(query);
}

exports.findAllUser = () => {
  return User.find({});
}
