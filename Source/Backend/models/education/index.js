const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  userId: String,
  school: String,
  educationLevel: String,
  curriculum: String,
  gpax: String,
  startYear: String,
  endYear: String
});

const model = mongoose.model('Education', schema);

module.exports = model;