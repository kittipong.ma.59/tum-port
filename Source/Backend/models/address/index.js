const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  district: String,
  amphoe: String,
  province: String,
  zipcode: Number,
  district_code: Number,
  amphoe_code: Number,
  province_code: Number
});

const model = mongoose.model('Address', schema);

module.exports = model;