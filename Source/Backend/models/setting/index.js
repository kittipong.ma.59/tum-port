const mongoose = require("mongoose");

var schema = new mongoose.Schema({
  userId: String,
  university: [String],
  category: [String]
}, {timestamps: true});

const model = mongoose.model('Setting', schema);

module.exports = model;