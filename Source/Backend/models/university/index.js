const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  university: String,
  thCode: String,
  enCode: String
});

const model = mongoose.model('University', schema);

module.exports = model;