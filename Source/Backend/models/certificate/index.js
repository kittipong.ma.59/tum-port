const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  userId: String,
  imageId: String,
  title: String,
  description: String,
  status: Boolean
});

const model = mongoose.model('Certificate', schema);

module.exports = model;