const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  userId: String,
  imageId: String,
  title: String,
  description: String,
  location: String,
  date: Number,
  calendarId: String,
  status: Boolean
});

const model = mongoose.model('Activities', schema);

module.exports = model;