const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  userId: String,
  nickname: String,
  identificationNo: String,
  dateOfBirth: Number,
  address: String,
  subdistrict: String,
  district: String,
  province: String,
  zipcode: String,
  nationality: String,
  race: String,
  religion: String,
  bloodType: String,
  weight: String,
  height: String,
  school: String,
  curriculum: String,
  phoneNo: String,
  facebook: String,
  line: String,
  instagram: String,
  fatherName: String,
  fatherOccupation: String,
  fatherPhoneNo: String,
  fatherAge: String,
  motherName: String,
  motherOccupation: String,
  motherPhoneNo: String,
  motherAge: String,
});

const model = mongoose.model('Profile', schema);

module.exports = model;