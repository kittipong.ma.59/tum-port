const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  SchoolID: String,
  SchoolName: String,
  SubDistrict: String,
  District: String,
  Province: String,
  PostCode: String,
  SchoolType: String,
  Department: String,
  Telephone: String,
  Fax: String,
  Website: String,
  Email: String,
  Latitude: Number,
  Longitude: Number
});

const model = mongoose.model('School', schema);

module.exports = model;