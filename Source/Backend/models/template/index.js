const mongoose = require("mongoose");

const Element = new mongoose.Schema({
  name: String,
  type: String,
  key: String,
  fontType: String,
  text: String,
  src: String,
  imageId: String,
  points: [Number],
  data: String,
  cornerRadius: Number,
  x: Number,
  y: Number,
  width: Number,
  height: Number,
  numPoints: Number,
  innerRadius: Number,
  outerRadius: Number,
  sides: Number,
  lineHeight: Number,
  align: String,
  verticalAlign: String,
  textDecoration: String,
  rotation: Number,
  radius: Number,
  fill: String,
  fillType: String,
  fontSize: String,
  fontFamily: String,
  fontStyle: String,
  stroke: String,
  strokeType: String,
  strokeWidth: Number,
  scale: Number,
  scaleX: Number,
  scaleY: Number,
  shadowColor: String,
  shadowBlur: Number,
  shadowOffsetX: Number,
  shadowOffsetY: Number,
  shadowOpacity: Number,
  opacity: Number,
  letterSpacing: Number,
  draggable: Boolean
}, {_id: false})

const Theme = mongoose.Schema({
  primary: String,
  secondary: String,
  info: String,
  dark: String,
  light: String,
  white: String,
  body: String
}, {_id: false})

const schema = new mongoose.Schema({
  image: [String],
  theme: Theme,
  frontCoverPage: { type: Number, default: -1},
  profilePage: { type: Number, default: -1},
  educationPage: { type: Number, default: -1},
  activityPage: { type: Number, default: -1},
  certificatePage: { type: Number, default: -1},
  activityCount: { type: Number, default: 2},
  certificateCount: { type: Number, default: 2},
  elements: [
    [ Element ]
  ]
}, { timestamps: true });

const model = mongoose.model('Template', schema);

module.exports = model;