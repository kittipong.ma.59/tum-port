const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  providerId: {
    type: String,
    enum: ['password', 'facebook', 'google'],
    required: true
  },
  firstname: String,
  lastname: String,
  email: {
    type: String,
    lowercase: true,
  },
  password: String,
  photoId: String,
  photoUrl: String,
  facebookId: String,
  googleId: String,
  admin: Boolean,
  resetPasswordToken: String,
  resetPasswordExpires: Date,
  readNewFeed: {
    type: Boolean,
    default: false
  },
});

const model = mongoose.model('User', schema);

module.exports = model;