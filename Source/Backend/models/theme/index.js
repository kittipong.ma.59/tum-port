const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  primary: String,
  secondary: String,
  info: String,
  dark: String,
  light: String,
  white: String,
  body: String
});

const model = mongoose.model('Theme', schema);

module.exports = model;