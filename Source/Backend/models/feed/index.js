const mongoose = require("mongoose");

var schema = new mongoose.Schema({
  _id: String,
  read_by: [{
    userId: {type:mongoose.Schema.Types.ObjectId},
    read_at: {type: Date, default: Date.now},
    _id: false
  }]
},{ strict: false });

const model = mongoose.model('Feed', schema);

module.exports = model;